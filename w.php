<?php

function set_getparam($get, $server_key){
	if(isset($get)){
		$_SERVER[$server_key] = $get;
	}elseif(isset($_SERVER[$server_key])){
		unset($_SERVER[$server_key]);
		if($server_key == "PHP_SELF" && isset($_SERVER["SCRIPT_NAME"])){
			unset($_SERVER["SCRIPT_NAME"]);
		}
	}
}

/*=================================================*/
/* メイン処理                                      */
/*=================================================*/
function get_mogura_log(){
	//外部サーバ用
	if(!defined("MOGURA_PATH") && isset($_GET["mogura_path"])){
		define('MOGURA_PATH',$_GET["mogura_path"]);
		set_getparam($_GET["ref"], "HTTP_REFERER");
		set_getparam($_GET["path"], "PHP_SELF");
		set_getparam($_GET["query_string"], "QUERY_STRING");
		
		//conf include
		include_once(constant("MOGURA_PATH").'inc/config.php');
		
		if(!constant("PHP_WRITE_MODE")){
			set_getparam($_GET["ip_forwarded"], "HTTP_X_FORWARDED_FOR");
			set_getparam($_GET["ip"], "REMOTE_ADDR");
			set_getparam($_GET["ua"], "HTTP_USER_AGENT");
			set_getparam($_GET["lang"], "HTTP_ACCEPT_LANGUAGE");
		}
	}

	if(!defined("MOGURA_PATH")){
		echo '<b>unknown [MOGURA_PATH]</b>';
	}elseif(!isset($_GET["GET_TITLE"])){
		//conf include
		include_once(constant("MOGURA_PATH").'inc/config.php');
		include_once(constant("MOGURA_PATH").'inc/function.php');
		
		$isDocomo = false;
		if(defined("GET_DOCOMO_ID_MODE") && constant("GET_DOCOMO_ID_MODE")){
			define ("ENC_FLG", preg_match("/^(UTF-8)$/i", mb_http_input("G")) ? true : false);
			$http_ua = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : '';
			if(constant("ENC_FLG")){
				$http_ua = @mb_convert_encoding($http_ua, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
			}
			$obj = new MobileCheck($http_ua);
			$env = $obj->CheckUA();
			if($env === 'docomo'){
				$obj->GetZone($env);
				$isDocomo = $obj->CheckIP($obj->zone);
			}
		}
		
		if(constant("PHP_WRITE_MODE") || $isDocomo){
			//Referer
			$ref = (isset($_SERVER["HTTP_REFERER"]) ? urlencode($_SERVER["HTTP_REFERER"]) : "");
			//Path
			$path = $_SERVER["PHP_SELF"] ? $_SERVER["PHP_SELF"] : $_SERVER["SCRIPT_NAME"];
			if($_SERVER["QUERY_STRING"]) $path = $path."?".$_SERVER["QUERY_STRING"];
			$path = urlencode($path);
			
			$img_size = (constant("COUNTER") && constant("COUNTER_TYPE") == 'img' ? "" : ' width="1" height="1" alt=""');
		}
		
		if(constant("PHP_WRITE_MODE")){
			//send_title
			if(!isset($send_title)) $send_title = "";
?>

<script type="text/javascript"><!--
(function(){
var w, h;
<?php
/* Browser Size */
/*
if (self.innerHeight) {
	w = self.innerWidth;
	h = self.innerHeight;
} else if (document.documentElement && document.documentElement.clientHeight) {
	w = document.documentElement.clientWidth;
	h = document.documentElement.clientHeight;
} else if (document.body) {
	w = document.body.clientWidth;
	h = document.body.clientHeight;
}
w = Math.round((w / 10)) * 10;
h = Math.round((h / 10)) * 10;
*/
?>
/* Monitor Size */
w = screen.width;
h = screen.height;

var args = "";
args += '?mode=img';
args += '&amp;guid=ON';
args += '&amp;ref=<?php echo $ref;?>';
args += '&amp;path=<?php echo $path;?>';
args += '&amp;monitor='+w+'x'+h;
args += '&amp;title='+encodeURIComponent(document.title);
args += '&amp;color='+(navigator.appName != "Netscape" ? screen.colorDepth : screen.pixelDepth);
args += '&amp;cookie='+(navigator.cookieEnabled ? 1 : 2);
args += '&amp;js=1';
args += '&amp;send_title=<?php echo $send_title?>';
document.write('<img src="<?php echo constant("W_PATH")?>' + args + '"<?php echo $img_size;?>>');
})();
//--></script>
<noscript>
<img src="<?php echo constant("W_PATH");?>?mode=img&amp;guid=ON&amp;ref=<?php echo $ref?>&amp;path=<?php echo $path?>&amp;js=2"<?php echo $img_size;?>>
</noscript>
<?php if(constant("COUNTER") && constant("COUNTER_TYPE") == 'txt') include_once(constant("MOGURA_PATH").'counter.php'); ?>

<?php
		}elseif($isDocomo){
?>
<img src="<?php echo constant("W_PATH");?>?mode=img&amp;guid=ON&amp;ref=<?php echo $ref?>&amp;path=<?php echo $path?>"<?php echo $img_size;?>>
<?php
		}else{
			include_once(constant("MOGURA_PATH").'writelog.php');
		}
	}
}

get_mogura_log();
?>