<?php
/*=================================================*/
/* テキストカウンター出力                          */
/*=================================================*/
if(defined("CONF_READ")){
	$db = DB::connect(constant("DSN"));
	if(DB::isError($db)){
		echo "CounterError.";
	}else{
		$res = $db->query("SELECT SUM(counter_val) as counter FROM ".constant("DB_COUNTER")." LIMIT 1;");
		if(DB::isError($res)){
			echo "CounterError.";
		}else{
			$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
			echo str_pad($row["counter"], constant("COUNTER_DIGIT"), "0", STR_PAD_LEFT);
		}
		$db->disconnect();
	}
}else{
	echo "conf read Error.";
}
?>
