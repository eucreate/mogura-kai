<?php
if(@constant("ADMIN_RUN") != "run") die('Open Error !!');

/*=========================================*/
/* メニューリストSQL                       */
/*=========================================*/
if(isset($_POST["val"])){
	$sql = "UPDATE ".constant("DB_TABLE_SETTING")." SET setting_val = '".$_POST["val"]."' WHERE setting_id = 'act'";
	$sql = @mb_convert_encoding($sql, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
	$res = $db->query($sql);
	check_err($res);
	reload();
}

$sql = "SELECT setting_val FROM "
		.constant("DB_TABLE_SETTING")
		." WHERE setting_id = 'act';";

$res = $db->query($sql);
check_err($res);

$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
$form_val = trim($row["setting_val"]);
$res->free();
//プラグインリスト
$p_list = "";
foreach(getfilelist("../".constant("DIR_PLUGIN")) as $v){
	$v = basename($v,".php");
   $p_list .= "<li>".$v."</li>";
}


//---MAIN
$form_title = set_img("image/icon02.gif").'アクション設定&nbsp;&#187;&nbsp;メニューリスト';
$form = <<<MESSAGE
<table cellpadding="0" cellspacing="0">
<tr><th colspan="2" class="end">メニューリストの変更</th></tr>
<tr valign="top">
<td>
<form action="?mode={$_GET["mode"]}" method="POST">
<textarea name="val" wrap="OFF">
{$form_val}
</textarea><br><input type="submit" value="　　　更新　　　">&nbsp;<input type="reset" value="リセット">
</form>
</td>
<td class="end">
<b>【 アクション一覧 】</b>
<ul>
{$p_list}
</ul>
</td>
</tr>
</table>
<br>
■&nbsp;メニューリストの変更方法
<table cellpadding="0" cellspacing="0">
<tr>
<th width="150">トップ項目の追加</th>
<th width="150" class="end">アクションの追加</th>
</tr>
<tr>
<td width="150">[トップ項目名]<br>「&nbsp;<b>[</b>&nbsp;」と「&nbsp;<b>]</b>&nbsp;」で囲む</td>
<td width="150" class="end">アクション||表示名<br>「&nbsp;<b>||</b>&nbsp;」で区切る</td>
</tr>
</table>

MESSAGE;
?>
