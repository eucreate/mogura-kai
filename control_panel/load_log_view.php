<?php
if(@constant("ADMIN_RUN") != "run") die('Open Error !!');

/*=========================================*/
/* ログ一覧SQL                             */
/*=========================================*/
$sql = array();
$main = array();
$sql["dl"] = true;
$op = isset($_GET["op"]) ? $_GET["op"] : "";
switch ($op) {
	case 'detail':
		if($_GET["val"] == "") break;
		$sql["select"] = "DATE_FORMAT(date, '%Y / %m') as ym,"
							."DATE_FORMAT(date, '%d') as d,"
							."DATE_FORMAT(date, '%Y-%m-') as del_m,"
							."DATE_FORMAT(date, '%Y-%m-%d') as del,"
							."COUNT(*) as c";
		$sql["group"] = "d";
		$sql["where"] = "date LIKE '".$_GET["val"]."%'";
		$sql["op"] = " order by date";
	   break;
	case 'del':
		if($_GET["val"] == "") break;
		$sql = "DELETE FROM ".constant("DB_TABLE_LOG")." WHERE date LIKE '".$_GET["val"]."%';";
		$sql = @mb_convert_encoding($sql, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
		$res = $db->query($sql);
		check_err($res);
		reload();
	   break;
	default:
		$sql["select"] = "DATE_FORMAT(date, '%Y / %m') as ym,DATE_FORMAT(date, '%Y-%m-') as de_ym,COUNT(*) as c";
		$sql["group"] = "ym";
		$sql["op"] = " order by date desc";
	   break;
}
$res = $db->query(mk_sql($sql));
check_err($res);
$main["total"] = 0;
while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	$main[] = $row;
	$main["total"] += $row["c"];
}
$res->free();

//---MAIN
$ico_dir_top = set_img("image/dir_top.gif","Back")."&nbsp;";
$ico_dir = set_img("image/dir.gif","Dir")."&nbsp;";
$ico_file = set_img("image/file.gif","File")."&nbsp;";
$ico_del = set_img("image/delete.gif","ログ削除")."&nbsp;";

$form_title = set_img("image/icon01.gif").'ログ管理&nbsp;&#187;&nbsp;ログ一覧';
switch ($op) {
	case 'detail':
$check_del = $main["0"]["ym"]." (".$main["total"]."件) [月合計]";
$check_del = str_pad($check_del, 35, " ");

$query = '?mode='.$_GET["mode"].'&op=del&val='.$main[0]["del_m"];

$form = <<<MESSAGE
<table cellpadding="0" cellspacing="0">
<tr>
<th width="300">日時</th>
<th width="150">ログ数</th>
<th class="end">削除</th>
</tr>

<tr class="sp">
<td width="300" class="sp">{$ico_dir_top}<a href="?mode=log_view">Back to Top</a></td>
<td width="150" class="sp">&nbsp;-&nbsp;</td>
<td class="sp_end">&nbsp;-&nbsp;</td>
</tr>

MESSAGE;
if($main){
$form .= <<<MESSAGE
<tr>
<td width="300">{$ico_dir}<b>{$main[0]["ym"]}</b></td>
<td width="150"><b>{$main["total"]}</b></td>
<td class="end"><a href="{$query}" onClick="return check_del('{$check_del}')">{$ico_del}ログ削除（月）</a></td>
</tr>
MESSAGE;
}else{
$form .= <<<MESSAGE
<tr>
<td width="300">{$ico_dir}UnknownLogs</td>
<td width="150">&nbsp;-&nbsp;</td>
<td class="end">&nbsp;-&nbsp;</td>
</tr>
MESSAGE;
}
	foreach($main as $v){
		if(!$v["d"] || !$v["c"]) continue;
		
		$check_del = $v["ym"]."/".$v["d"];
		$check_del = str_replace (" ", "", $check_del)." ( ".$v["c"]."件 )";
		$check_del = str_pad($check_del, 35, " ");
		
		$query = '?mode='.$_GET["mode"].'&op=del&val='.$v["del"];
		
		$form .= '<tr>'."\n";
		$form .= '<td width="300">'.$ico_file.$v["ym"]."&nbsp;/&nbsp;".$v["d"].'</td>'."\n";
		$form .= '<td width="150">'.$v["c"].'</td>'."\n";
		$form .= '<td class="end"><a href="'.$query.'" onClick="return check_del(\''.$check_del.'\')">'.$ico_del.'ログ削除</a></td>'."\n";
		$form .= '</tr>'."\n";
	}
$form .= <<<MESSAGE
</table>
MESSAGE;
	   break;
	default:
$form = <<<MESSAGE
<table cellpadding="0" cellspacing="0">
<tr>
<th width="300">日時</th>
<th width="150">ログ数</th>
<th class="end">削除</th>
</tr>
MESSAGE;
	foreach($main as $v){
		if(!$v["ym"] || !$v["c"]) continue;
		
		$query = '?mode='.$_GET["mode"].'&op=detail&val='.$v["de_ym"];
		
		$form .= '<tr>'."\n";
		$form .= '<td width="300">'.$ico_dir.'<a href="'.$query.'">'.$v["ym"].'</a></td>'."\n";
		$form .= '<td width="150">'.$v["c"].'</td>'."\n";
		$form .= '<td class="end">&nbsp;-&nbsp;</td>'."\n";
		$form .= '</tr>'."\n";
	}
$form .= <<<MESSAGE
</table>
MESSAGE;
	   break;
}


?>
