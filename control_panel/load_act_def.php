<?php
if(@constant("ADMIN_RUN") != "run") die('Open Error !!');

/*=========================================*/
/* 初期アクションSQL                       */
/*=========================================*/
if(isset($_POST["val"])){
	$sql = "UPDATE ".constant("DB_TABLE_SETTING")." SET setting_val = '".$_POST["val"]."' WHERE setting_id = 'def_act'";
	$sql = @mb_convert_encoding($sql, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
	$res = $db->query($sql);
	check_err($res);
	reload();
}

$sql = "SELECT setting_val FROM "
		.constant("DB_TABLE_SETTING")
		." WHERE setting_id = 'def_act';";

$res = $db->query($sql);
check_err($res);

$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
$form_val["def"] = $row["setting_val"];
$res->free();
//プラグインリスト
foreach(getfilelist("../".constant("DIR_PLUGIN")) as $v){
	$v = basename($v,".php");
   $form_val[] = $v;
}

//---MAIN
$form_title = set_img("image/icon02.gif").'アクション設定&nbsp;&#187;&nbsp;初期アクション';
$form = <<<MESSAGE
<table cellpadding="0" cellspacing="0" width="400">
<tr>
<th class="end">初期アクションの変更</th>
</tr>
<tr>
<td class="end">
<form action="?mode={$_GET["mode"]}" method="POST">
Plugin：
<select name="val">
MESSAGE;
	foreach($form_val as $k => $v){
		if($k == 'def') continue;
		if($v == $form_val["def"]){
			$form .= '<option value="'.$v.'" selected style="background-color:#666;color:#FFF;">&nbsp;&#187;&nbsp;'.$v.'</option>'."\n";
		}else{
			$form .= '<option value="'.$v.'">&nbsp;'.$v.'</option>';
		}
	}
$form .= <<<MESSAGE
</select>
<input type="submit" value="更新">
</form>
</td>
</tr>
</table>
MESSAGE;
?>
