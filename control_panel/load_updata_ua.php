<?php
if(@constant("ADMIN_RUN") != "run") die('Open Error !!');

/*=========================================*/
/* UA更新                                  */
/*=========================================*/
$updata_count = 0;

$sql = array();
$sql["from"] = constant("DB_TABLE_UA");

$res = $db->query(mk_sql($sql));
check_err($res);

while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	if(!$row["ua_name"]) continue;
	$ua = array();
	list($ua["browser"], $ua["browser_v"], $ua["os"], $ua["os_v"]) = getuseragent($row["ua_name"], "../");
	if($row["os"] === $ua["os"] && $row["os_v"] === $ua["os_v"] && $row["brow"] === $ua["browser"] && $row["brow_v"] === $ua["browser_v"]){
		continue;
	}
	$sql = "UPDATE ".constant("DB_TABLE_UA")." SET ".
			($row["os"] === $ua["os"] ? '' : "os = '".$ua["os"]."',").
			($row["os_v"] === $ua["os_v"] ? '' : "os_v = '".$ua["os_v"]."',").
			($row["brow"] === $ua["browser"] ? '' : "brow = '".$ua["browser"]."',").
			($row["brow_v"] === $ua["browser_v"] ? '' : "brow_v = '".$ua["browser_v"]."',");
	$sql = substr($sql, 0, -1)." WHERE ua_id = ".$row["ua_id"]." LIMIT 1;";
	$sql = @mb_convert_encoding($sql, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
	$res2 = $db->query($sql);
	check_err($res2);
	$row["os"] = $ua["os"];
	$row["os_v"] = $ua["os_v"];
	$row["brow"] = $ua["browser"];
	$row["brow_v"] = $ua["browser_v"];
	$form_val[] = $row;
	$updata_count++;
}
$res->free();

//テーブル最適化
$res = $db->query("OPTIMIZE TABLE ".constant("DB_TABLE_LOG").",".constant("DB_TABLE_UA"));
check_err($res);
$res->free();

//---MAIN
$form_title = set_img("image/icon05.gif").'その他の設定&nbsp;&#187;&nbsp;UserAgent更新';
if($updata_count){
$form = <<<MESSAGE
更新データ:&nbsp;{$updata_count}件
<table cellpadding="0" cellspacing="0" width="400">
<tr>
<th nowrap>ID</th>
<th colspan="2" class="end">UserAgent</th>
</tr>
MESSAGE;
foreach($form_val as $v){
	$form .= '<tr>'."\n";
	$form .= '<td rowspan="2" class="sp">'.$v["ua_id"].'</td>'."\n";
	$form .= '<td colspan="2" class="sp_end">'.$v["ua_name"].'</td>'."\n";
	$form .= '</tr>'."\n";
	$form .= '<tr>'."\n";
	$form .= '<td class="sp">'.$v["os"].'&nbsp;'.$v["os_v"].'</td>'."\n";
	$form .= '<td class="sp_end">'.$v["brow"].'&nbsp;'.$v["brow_v"].'</td>'."\n";
	$form .= '</tr>'."\n";
}
$form .= "</table>";
}else{
	$form = "<p>更新データは、ありません</p>";
}
?>
