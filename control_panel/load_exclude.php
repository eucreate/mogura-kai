<?php
if(@constant("ADMIN_RUN") != "run") die('Open Error !!');

/*=========================================*/
/* 拒否リストSQL                           */
/*=========================================*/
//追加・削除
$op = isset($_POST["op"]) ? $_POST["op"] : "";
switch ($op) {
	case 'new':
		if($_POST["val"] == "") break;
		$sql_k = "(exclude_type, exclude_val)";
		$sql_v = " VALUES ('".str_replace("exclude_", "", $_GET["mode"])."', '".$_POST["val"]."')";
		$sql = "INSERT INTO ".constant("DB_EXCLUDE")." ".$sql_k.$sql_v.";";
		$sql = @mb_convert_encoding($sql, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
		$res = $db->query($sql);
		check_err($res);
		reload();
	   break;
	case 'del':
		if($_POST["val"] == "") break;
		$sql = "DELETE FROM ".constant("DB_EXCLUDE")." WHERE exclude_type = '".str_replace("exclude_", "", $_GET["mode"])."' AND exclude_val = '".$_POST["val"]."';";
		$sql = @mb_convert_encoding($sql, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
		$res = $db->query($sql);
		check_err($res);
		reload();
	   break;
}

$label = "";
$sql = "SELECT DISTINCT * FROM ".constant("DB_EXCLUDE");
switch ($_GET["mode"]) {
	case 'exclude_id':
		$form_title = set_img("image/icon03.gif").'拒否設定&nbsp;&#187;&nbsp;ID';
	   $sql .= " WHERE exclude_type = 'id'";
	   $label = "ID";
	   break;
	case 'exclude_ip':
		$form_title = set_img("image/icon03.gif").'拒否設定&nbsp;&#187;&nbsp;IP Address';
	   $sql .= " WHERE exclude_type = 'ip'";
	   $label = "IP Address";
	   break;
	case 'exclude_host':
		$form_title = set_img("image/icon03.gif").'拒否設定&nbsp;&#187;&nbsp;Host';
	   $sql .= " WHERE exclude_type = 'host'";
	   $label = "Host";
	   break;
	case 'exclude_path':
		$form_title = set_img("image/icon03.gif").'拒否設定&nbsp;&#187;&nbsp;Path';
	   $sql .= " WHERE exclude_type = 'path'";
	   $label = "Path";
	   break;
	case 'exclude_url':
		$form_title = set_img("image/icon03.gif").'拒否設定&nbsp;&#187;&nbsp;URL';
	   $sql .= " WHERE exclude_type = 'url'";
	   $label = "URL";
	   break;
	case 'exclude_ua':
		$form_title = set_img("image/icon03.gif").'拒否設定&nbsp;&#187;&nbsp;UserAgent';
	   $sql .= " WHERE exclude_type = 'ua'";
	   $label = "UserAgent";
	   break;
}
$sql .= " order by exclude_val";
$sql .= ";";

$res = $db->query($sql);
check_err($res);

while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	$form_val[] = trim($row["exclude_val"]);
}
$res->free();

//---MAIN
$form = <<<MESSAGE
<p>
<a href="?mode=exclude_id">ID</a>
｜
<a href="?mode=exclude_ip">IP Address</a>
｜
<a href="?mode=exclude_host">Host</a>
｜
<a href="?mode=exclude_path">Path</a>
｜
<a href="?mode=exclude_url">URL</a>
｜
<a href="?mode=exclude_ua">UserAgent</a>
</p>
<table cellpadding="0" cellspacing="0">
<tr>
<th colspan="2" class="end">リストに追加</th>
</tr>
<tr>
<td align="right" width="80">{$label}:</td>
<td class="end">
<form action="?mode={$_GET["mode"]}" method="POST">
<input type="hidden" name="op" value="new">
<input type="text" name="val" value="" class="input_txt">
<input type="submit" value="追加&nbsp;&gt;">
</form>
</td>
</tr>
</table>
<br>
MESSAGE;
if(isset($form_val)){
	$form .= '<table cellpadding="0" cellspacing="0">'."\n";
   $form .= "<tr>\n";
   $form .= "<th>拒否リスト</th>\n";
   $form .= '<th width="50" class="end">削除</th>'."\n";
   $form .= "</tr>\n";
	foreach ($form_val as $v) {
	    $form .= "<tr>\n";
	    $form .= "<td>".$v."</td>\n";
	    $form .= '<td width="50" class="end">'
	    			.'<form action="?mode='.$_GET["mode"].'" method="POST">'
	    			.'<input type="hidden" name="op" value="del">'
	    			.'<input type="hidden" name="val" value="'.$v.'">'
	    			.'<input type="submit" value="削除">'
	    			.'</form>'."</td>\n";
	    $form .= "</tr>\n";
	}
	$form .= "</table>\n";
}

?>
