<?php
if(@constant("ADMIN_RUN") != "run") die('Open Error !!');

function reload(){
	header("Location: ".basename($_SERVER["SCRIPT_NAME"])."?mode=".$_GET["mode"]."&up");
	exit;
}

/*=========================================*/
/* 初期化・初期設定                        */
/*=========================================*/
//内部文字エンコード
@mb_internal_encoding("UTF-8");
//マルチバイト文字列正規表現関数で使用される文字エンコーディング
@mb_regex_encoding("UTF-8");
//HTTP出力文字エンコーディング
@mb_http_output("UTF-8");

$sql = "";
$form_title = "";
$form = "";


/*=========================================*/
/* DB接続                                  */
/*=========================================*/
$db = DB::connect(constant("DSN"));
check_err($db);
$db->query("SET NAMES utf8;");

/*=========================================*/
/* SQL初期処理                             */
/*=========================================*/
if(isset($_GET["val"])) $_GET["val"] = sql_escape($_GET["val"]);
if(isset($_POST["val"])) $_POST["val"] = sql_escape($_POST["val"]);
if(isset($_POST["val2"])) $_POST["val2"] = sql_escape($_POST["val2"]);

/*=========================================*/
/* MAIN                                    */
/*=========================================*/
$mode = isset($_GET["mode"]) ? $_GET["mode"] : "";
switch ($mode) {
	case 'log_view':
	   include_once("load_log_view.php");
	   break;
	case 'log_w':
	   include_once("load_log_w.php");
	   break;
	case 'act_def':
	   include_once("load_act_def.php");
	   break;
	case 'act_list':
	   include_once("load_act_list.php");
	   break;
	case 'exclude_id':
	case 'exclude_ip':
	case 'exclude_host':
	case 'exclude_path':
	case 'exclude_url':
	case 'exclude_ua':
	   include_once("load_exclude.php");
	   break;
	case 'ch_id':
	   include_once("load_ch_id.php");
	   break;
	case 'ch_url':
	   include_once("load_ch_url.php");
	   break;
	case 'skin':
	   include_once("load_skin.php");
	   break;
	case 'skin':
	   include_once("load_skin.php");
	   break;
	case 'conf':
	   include_once("load_conf.php");
	   break;
	case 'updata_ua':
	   include_once("load_updata_ua.php");
	   break;
	default:
	   include_once("load_default.php");
	   break;
}


/*=========================================*/
/* DB接続終了                              */
/*=========================================*/
$db->disconnect();
?>
