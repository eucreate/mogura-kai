<?php
if(@constant("ADMIN_RUN") != "run") die('Open Error !!');

/*=========================================*/
/* 設定確認                                */
/*=========================================*/
$icon["01"] = set_img("image/icon01.gif");
$icon["02"] = set_img("image/icon02.gif");
$icon["03"] = set_img("image/icon03.gif");
$icon["04"] = set_img("image/icon04.gif");
$icon["05"] = set_img("image/icon05.gif");

$val = isset($_GET["val"]) ? $_GET["val"] : "";

if(!$val || $val == "1"){
$form .= <<<MESSAGE
<div class="title">{$icon["01"]}ログ管理</div>
<p>ログ数の確認・ログの削除、解析コードの表示</p>
<ul>
<li><a href="?mode=log_view">ログ一覧</a></li>
<li><a href="?mode=log_w">ログ取得用</a></li>
</ul>
MESSAGE;
}

if(!$val || $val == "2"){
$form .= <<<MESSAGE
<div class="title">{$icon["02"]}アクション設定</div>
<p>アクセス解析を開いたときに使用するプラグインの設定、解析メニューの追加・削除・並べ替え</p>
<ul>
<li><a href="?mode=act_def">初期アクション</a></li>
<li><a href="?mode=act_list">メニューリスト</a></li>
</ul>
MESSAGE;
}

if(!$val || $val == "3"){
$form .= <<<MESSAGE
<div class="title">{$icon["03"]}拒否設定</div>
<p>ログの取得を拒否する項目の設定</p>
<ul>
<li><a href="?mode=exclude_id">ID</a></li>
<li><a href="?mode=exclude_ip">IP Address</a></li>
<li><a href="?mode=exclude_host">Host</a></li>
<li><a href="?mode=exclude_path">Path</a></li>
<li><a href="?mode=exclude_url">URL</a></li>
<li><a href="?mode=exclude_ua">UserAgent</a></li>
</ul>
MESSAGE;
}

if(!$val || $val == "4"){
$form .= <<<MESSAGE
<div class="title">{$icon["04"]}データ変換</div>
<p>ID・URLを分かりやすい表示に変換</p>
<ul>
<li><a href="?mode=ch_id">ID変換</a></li>
<li><a href="?mode=ch_url">URL変換</a></li>
</ul>
MESSAGE;
}

if(!$val || $val == "5"){
$form .= <<<MESSAGE
<div class="title">{$icon["05"]}その他の設定</div>
<p>スキンの変更、リスト更新時のDB内データの更新、config.phpの設定一覧</p>
<ul>
<li><a href="?mode=skin">スキン選択</a></li>
<li><a href="?mode=updata_ua">UserAgent更新</a></li>
<li><a href="?mode=conf">設定確認</a></li>
</ul>
MESSAGE;
}

$form = '<div class="def">'.$form.'</div>';

?>
