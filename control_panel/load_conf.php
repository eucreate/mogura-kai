<?php
if(@constant("ADMIN_RUN") != "run") die('Open Error !!');

/*=========================================*/
/* 設定確認                                */
/*=========================================*/
$form_title = set_img("image/icon05.gif").'その他の設定&nbsp;&#187;&nbsp;設定確認';
//-------------------------------------
//  セキュリティー
//-------------------------------------
$form .= '<b>セキュリティー</b>'."\n";
$form .= '<table cellspacing="0" cellpadding="0">'."\n";
$form .= '<tr><th width="200" nowrap>ユーザ認証</th><td width="300" class="sp_end">';
switch (constant("USERAUTH_FLG")) {
	case '0':
	   $form .= "無効";
	   break;
	case '1':
	   $form .= "BASIC認証";
	   break;
	case '2':
	   $form .= "フォーム認証";
	   break;
	default:
	   $form .= "不明";
	   break;
}
$form .= "</td></tr>\n";
$form .= '<tr><th width="200" nowrap class="bt">管理者モード</th><td width="300" class="end">';
switch (constant("ADMIN_MODE")) {
	case '1':
	   $form .= "ON";
	   break;
	case '2':
	   $form .= "ON&nbsp;（コントロールパネルのみユーザ認証）";
	   break;
	default:
	   $form .= "OFF";
	   break;
}
$form .= "</td></tr>\n";
$form .= "</table>\n";

//-------------------------------------
//  ディレクトリ設定
//-------------------------------------
$form .= '<br><b>ディレクトリ設定</b>'."\n";
$form .= '<table cellspacing="0" cellpadding="0">'."\n";
$form .= '<tr><th width="200" nowrap>プラグイン</th><td width="300" class="sp_end">'
		.constant("DIR_PLUGIN")."</td></tr>\n";
$form .= '<tr><th width="200" nowrap>リスト</th><td width="300" class="sp_end">'
		.constant("DIR_LIST")."</td></tr>\n";
$form .= '<tr><th width="200" nowrap>アイコン</th><td width="300" class="sp_end">'
		.constant("DIR_ICON")."</td></tr>\n";
$form .= '<tr><th width="200" nowrap class="bt">スキン</th><td width="300" class="end">'
		.constant("DIR_SKIN")."</td></tr>\n";
$form .= "</table>\n";

//-------------------------------------
//  ログ取得
//-------------------------------------
$form .= '<br><b>ログ取得</b>'."\n";
$form .= '<table cellspacing="0" cellpadding="0">'."\n";
$form .= '<tr><th width="200" nowrap>アクセス解析の使用</th><td width="300" class="sp_end">'
		.(constant("ACC_RUN") ? "ON" : "OFF")."</td></tr>\n";
$form .= '<tr><th width="200" nowrap>ログ取得スクリプトのパス</th><td width="300" class="sp_end">'
		.constant("W_PATH")."</td></tr>\n";
$form .= '<tr><th width="200" nowrap>PHPログ取得モード</th><td width="300" class="sp_end">'
		.(constant("PHP_WRITE_MODE") ? "JavaScript＋PHP" : "PHP")."</td></tr>\n";
$form .= '<tr><th width="200" nowrap>docomo端末ID取得モード</th><td width="300" class="sp_end">'
		.((defined("GET_DOCOMO_ID_MODE") && constant("GET_DOCOMO_ID_MODE")) ? "imgタグを使用する" : "imgタグを使用しない")."</td></tr>\n";
$form .= '<tr><th width="200" nowrap>エラー表示</th><td width="300" class="sp_end">'
		.(constant("ERROR_VIEW") ? "ON" : "OFF")."</td></tr>\n";
$form .= '<tr><th width="200" nowrap>変換前のエンコーディング</th><td width="300" class="sp_end">'
		.constant("TO_ENCODING")."</td></tr>\n";
$form .= '<tr><th width="200" nowrap>ID用クッキー有効期限</th><td width="300" class="sp_end">'
		.constant("COOKIE_EXPIRE")." 日</td></tr>\n";
$form .= '<tr><th width="200" nowrap>リロード対策</th><td width="300" class="sp_end">'
		.constant("UNWRITE")." 秒</td></tr>\n";
$form .= '<tr><th width="200" nowrap>タイトルの自動取得モード</th><td width="300" class="sp_end">';
switch (constant("GET_TITLE_MODE")) {
	case '0':
	   $form .= "タイトル取得なし";
	   break;
	case '1':
	   $form .= "ローカルファイルからタイトル取得";
	   break;
	case '2':
	   $form .= "URIからタイトル取得";
	   break;
	default:
	   $form .= "不明";
	   break;
}
$form .= "</td></tr>\n";
$form .= '<tr><th width="200" nowrap>グリニッジ標準時との時差</th><td width="300" class="sp_end">'
		.constant("TIME_DIFF")." 時間</td></tr>\n";
$form .= '<tr><th width="200" nowrap class="bt">ログの保存最大件数</th><td width="300" class="end">'
		.(constant("MAX_LOG") ? constant("MAX_LOG")." 件" : "OFF")."</td></tr>\n";
$form .= "</table>\n";

//-------------------------------------
//  カウンター
//-------------------------------------
$form .= '<br><b>カウンター</b>'."\n";
$form .= '<table cellspacing="0" cellpadding="0">'."\n";
$form .= '<tr><th width="200" nowrap>カウンターの表示</th><td width="300" class="sp_end">'
		.(constant("COUNTER") ? "ON" : "OFF")."</td></tr>\n";

$form .= '<tr><th width="200" nowrap>カウンタータイプ</th><td width="300" class="sp_end">';
switch (constant("COUNTER_TYPE")) {
	case 'img':
	   $form .= "画像カウンター";
	   break;
	case 'txt':
	   $form .= "テキストカウンター";
	   break;
	default:
	   $form .= "不明";
	   break;
}
$form .= "</td></tr>\n";

$form .= '<tr><th width="200" nowrap>画像ディレクトリ</th><td width="300" class="sp_end">';
for($i = 0; $i <= 9; $i++){
	$form .= set_img(constant("COUNTER_DIR").$i.".png");
}

$form .= "<br>".constant("COUNTER_DIR")."</td></tr>\n";
$form .= '<tr><th width="200" nowrap class="bt">桁数</th><td width="300" class="end">'
		.constant("COUNTER_DIGIT")." 桁</td></tr>\n";
$form .= "</table>\n";


//-------------------------------------
//  その他
//-------------------------------------
$form .= '<br><b>その他</b>'."\n";
$form .= '<table cellspacing="0" cellpadding="0">'."\n";
$form .= '<tr><th width="200" nowrap>丸める最大文字数</th><td width="300" class="sp_end">'
		.constant("STR_CUT")." 文字</td></tr>\n";

$form .= '<tr><th width="200" nowrap>1ページに表示する件数</th><td width="300" class="sp_end">'
		.constant("LIMIT")." 件</td></tr>\n";

$form .= '<tr><th width="200" nowrap>指定ログ表示モード</th><td width="300" class="sp_end">'
		.(constant("SELECT_LOG_MODE") ? "同ページ内に表示" : "新しいウィンドウ")."</td></tr>\n";

$form .= '<tr><th width="200" nowrap class="bt">リスト拡張(携帯機種)</th><td width="300" class="end">'
		.(constant("UA_PLUS_MOBILE") ? "ON" : "OFF")." </td></tr>\n";

$form .= "</table>\n";
?>
