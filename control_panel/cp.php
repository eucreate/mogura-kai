<?php
include_once("../inc/config.php");
include_once("../inc/function.php");

if(!preg_match("/^(1|2)$/", constant("ADMIN_MODE"))) die('Administrator Mode Off.');

define("ADMIN_RUN", "run");
//login check
$auth_flg = false;
switch (constant("USERAUTH_FLG")) {
    case 0:
		$auth_flg = true;
		break;
    case 1:
		if($_SERVER[PHP_AUTH_USER] == constant("USERAUTH_ID") && $_SERVER[PHP_AUTH_PW] == constant("USERAUTH_PWD")) $auth_flg = true;
		break;
    case 2:
		session_start();
		if($_SESSION['login']) $auth_flg = true;
		break;
}
if(!$auth_flg){
	//ユーザー認証
	$path_hack = "../";
	include_once("../inc/userauth.php");
}


//seting load
include_once("load.php");

//updata
$updata = isset($_GET["up"]) ? set_img("image/updata.gif","更新完了") : "";

//updata
$form_title = $form_title ? '<div class="title">'.$form_title.'</div>' : "";
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>mogura - コントロールパネル</title>
<link href="styles-site.css" rel="stylesheet" type="text/css">
<script type="text/javascript"><!--
function check_del(date_str){ 
	if(window.confirm('【 ログ削除 】'+"\n\n"+'削除対象 -> '+date_str+"\n")){
		return true;
	}else{
		return false;
	}
}
function view_navi(selected) {
  if (! document.getElementById) return;
  for (i = 1; i <= 5; i++) {
    if (! document.getElementById("navi" + i)) continue;
    if (i == selected) {
      document.getElementById("navi" + i).style.visibility = "visible";
    } else {
      document.getElementById("navi" + i).style.visibility = "hidden";
    }
  }
}
//--></script>

</head>

<body onClick="view_navi(0);">
<div id="Body">
<div id="Header">
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td><a href="cp.php"><?php echo set_img("image/logo_sub.gif","コントロールパネル"); ?></a></td>
<td align="right" valign="top"><a href="../w3a.php"><img src="image/bt_top_acc.gif" alt="解析画面へ戻る" width="116" height="23" border="0"></a></td>
</tr>
</table>
</div>

<div id="NavigationArea">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="30">&nbsp;</td>
<td><a href="cp.php" class="ico_00" onMouseover="view_navi(0);">トップ</a></td>
<td><a href="cp.php?val=1" class="ico_01" onMouseover="view_navi(1);">ログ管理</a></td>
<td><a href="cp.php?val=2" class="ico_02" onMouseover="view_navi(2);">アクション設定</a></td>
<td><a href="cp.php?val=3" class="ico_03" onMouseover="view_navi(3);">拒否設定</a></td>
<td><a href="cp.php?val=4" class="ico_04" onMouseover="view_navi(4);">データ変換</a></td>
<td><a href="cp.php?val=5" class="ico_05" onMouseover="view_navi(5);">その他の設定</a></td>
</tr>
</table>
</div>

<div id="DataArea">
<?php echo $form_title;?>
<?php echo $updata;?>
<?php echo $form;?>
</div>

<div id="Footer">
  <hr size="1">
  <div class="copy"><a href="http://fmono.sub.jp/" target="_blank">&copy;&nbsp;OSAKA&nbsp;PHP</a>&nbsp;/&nbsp;Edit&nbsp;:&nbsp;<a href="http://tekito.jp/" target="_blank">tekito</a></div>
</div>

</div>

<div id="navi1">
<ul>
<li><a href="?mode=log_view">ログ一覧</a></li>
<li><a href="?mode=log_w">ログ取得用</a></li>
</ul>
</div>
<div id="navi2">
<ul>
<li><a href="?mode=act_def">初期アクション</a></li>
<li><a href="?mode=act_list">メニューリスト</a></li>
</ul>
</div>
<div id="navi3">
<ul>
<li><a href="?mode=exclude_id">ID</a></li>
<li><a href="?mode=exclude_ip">IP Address</a></li>
<li><a href="?mode=exclude_host">Host</a></li>
<li><a href="?mode=exclude_path">Path</a></li>
<li><a href="?mode=exclude_url">URL</a></li>
<li><a href="?mode=exclude_ua">UserAgent</a></li>
</ul>
</div>
<div id="navi4">
<ul>
<li><a href="?mode=ch_id">ID変換</a></li>
<li><a href="?mode=ch_url">URL変換</a></li>
</ul>
</div>
<div id="navi5">
<ul>
<li><a href="?mode=skin">スキン選択</a></li>
<li><a href="?mode=updata_ua">UA更新</a></li>
<li><a href="?mode=conf">設定確認</a></li>
</ul>
</div>

</body>
</html>
