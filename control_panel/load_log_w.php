<?php
if(@constant("ADMIN_RUN") != "run") die('Open Error !!');

$path_include = realpath(dirname(__FILE__).'/../').'/';

$form_title = set_img("image/icon01.gif").'ログ管理&nbsp;&#187;&nbsp;ログ取得用';

$t = $_SERVER['HTTP_HOST'].dirname(empty($_SERVER["PHP_SELF"]) ? $_SERVER["SCRIPT_NAME"] : $_SERVER["PHP_SELF"]);
$t = "http://".preg_replace("/(control_panel)$/", "", $t);

$w_path = constant("W_PATH");

$img_size = (constant("COUNTER") && constant("COUNTER_TYPE") == 'img') ? '' : ' width="1" height="1"';

$form = <<<MESSAGE
<p>下記のコードを解析したいページに記述して下さい。</p><br>
<table cellpadding="0" cellspacing="0" width="100%">
<tr><th class="end">WriteType:<b>PHP</b>&nbsp;（拡張子がPHPのページ）</th></tr>
<tr><td class="end">
&lt;?php<br>
define('MOGURA_PATH','{$path_include}');<br>
include_once(constant("MOGURA_PATH").'w.php');<br>
?&gt;
</td></tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="100%">
<tr><th class="end">WriteType:<b>JavaScript and Image</b>&nbsp;（拡張子がPHP以外のページ）</th></tr>
<tr><td class="end">
&lt;script type="text/javascript" src="{$t}w_js.php"&gt;&lt;/script&gt;<br>
&lt;noscript&gt;<br>
&lt;img src="{$w_path}?mode=img&amp;amp;guid=ON&amp;amp;js=2"{$img_size} alt=""&gt;<br>
&lt;/noscript&gt;
</td></tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="100%">
<tr><th class="end">WriteType:<b>Download File</b>&nbsp;（ダウンロードカウンター）</th></tr>
<tr><td class="end">
&lt;a href="{$t}writelog.php?dl=***"&gt;
ダウンロード
&lt;/a&gt;
<hr size=1>
*** → ダウンロードファイルのパス
</td></tr>
</table>
	
<br>
<table cellpadding="0" cellspacing="0" width="100%">
<tr><th class="end">WriteType:<b>PHP outside</b>&nbsp;（拡張子がPHPのページ＆外部サーバー）</th></tr>
<tr><td class="end">
&lt;?php<br>
\$mogura = '{$t}w.php?mogura_path='.urlencode('{$path_include}');<br>
if(isset(\$_SERVER["HTTP_REFERER"])) \$mogura .= "&amp;ref=".urlencode(\$_SERVER["HTTP_REFERER"]);<br>
\$mogura_access_path = empty(\$_SERVER["PHP_SELF"]) ? \$_SERVER["SCRIPT_NAME"] : \$_SERVER["PHP_SELF"];<br>
if(isset(\$mogura_access_path)) \$mogura .= "&amp;path=".urlencode(\$mogura_access_path);<br>
if(isset(\$_SERVER["QUERY_STRING"])) \$mogura .= "&amp;query_string=".urlencode(\$_SERVER["QUERY_STRING"]);<br>
if(isset(\$_SERVER["HTTP_X_FORWARDED_FOR"])) \$mogura .= "&amp;ip_forwarded=".urlencode(\$_SERVER["HTTP_X_FORWARDED_FOR"]);<br>
if(isset(\$_SERVER["REMOTE_ADDR"])) \$mogura .= "&amp;ip=".urlencode(\$_SERVER["REMOTE_ADDR"]);<br>
if(isset(\$_SERVER["HTTP_USER_AGENT"])) \$mogura .= "&amp;ua=".urlencode(\$_SERVER["HTTP_USER_AGENT"]);<br>
if(isset(\$_SERVER["HTTP_ACCEPT_LANGUAGE"])) \$mogura .= "&amp;lang=".urlencode(\$_SERVER["HTTP_ACCEPT_LANGUAGE"]);<br>
readfile(\$mogura);<br>
?&gt;
</td></tr>
</table>
MESSAGE;

?>
