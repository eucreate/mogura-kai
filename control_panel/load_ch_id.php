<?php
if(@constant("ADMIN_RUN") != "run") die('Open Error !!');

/*=========================================*/
/* 拒否リストSQL                           */
/*=========================================*/
//追加・削除
$op = isset($_POST["op"]) ? $_POST["op"] : "";
switch ($op) {
	case 'new':
		if($_POST["val"] == "" || $_POST["val2"] == "") break;
		$sql_k = "(ch_id, ch_name)";
		$sql_v = " VALUES ('".$_POST["val"]."', '".$_POST["val2"]."')";
		$sql = "INSERT INTO ".constant("DB_CH_ID")." ".$sql_k.$sql_v.";";
		$sql = @mb_convert_encoding($sql, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
		$res = $db->query($sql);
		check_err($res);
		reload();
	   break;
	case 'del':
		if($_POST["val"] == "") break;
		$sql = "DELETE FROM ".constant("DB_CH_ID")." WHERE ch_id = '".$_POST["val"]."';";
		$sql = @mb_convert_encoding($sql, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
		$res = $db->query($sql);
		check_err($res);
		reload();
	   break;
}

$sql = "SELECT DISTINCT * FROM ".constant("DB_CH_ID");
$sql .= " order by ch_name";
$sql .= ";";

$res = $db->query($sql);
check_err($res);

while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	$form_val[trim($row["ch_id"])] = trim($row["ch_name"]);
}
$res->free();


//---MAIN
$form_title = set_img("image/icon04.gif").'データ変換&nbsp;&#187;&nbsp;ID変換';
$form = <<<MESSAGE
<form action="?mode={$_GET["mode"]}" method="POST">
<input type="hidden" name="op" value="new">
<table cellpadding="0" cellspacing="0">
<tr>
<th colspan="2" class="end">リストに追加</th>
</tr>
<tr>
<td align="right" width="50">ID:</td>
<td class="end"><input type="text" name="val" value="" class="input_txt"></td>
</tr>
<tr>
<td align="right" width="50">変換名:</td>
<td class="end"><input type="text" name="val2" value="" class="input_txt">
<input type="submit" value="追加&nbsp;&gt;">
</td>
</tr>
</table>
</form>
<br>
MESSAGE;
if(isset($form_val)){
	$form .= '<table cellpadding="0" cellspacing="0">'."\n";
   $form .= "<tr>\n";
   $form .= "<th>ID</th>\n";
   $form .= "<th>変換名</th>\n";
   $form .= '<th width="50" class="end">削除</th>'."\n";
   $form .= "</tr>\n";
	foreach ($form_val as $k => $v) {
	    $form .= "<tr>\n";
	    $form .= "<td>".$k."</td>\n";
	    $form .= "<td>".$v."</td>\n";
	    $form .= '<td width="50" class="end">'
	    			.'<form action="?mode='.$_GET["mode"].'" method="POST">'
	    			.'<input type="hidden" name="op" value="del">'
	    			.'<input type="hidden" name="val" value="'.$k.'">'
	    			.'<input type="submit" value="削除">'
	    			.'</form>'."</td>\n";
	    $form .= "</tr>\n";
	}
	$form .= "</table>\n";
}

?>
