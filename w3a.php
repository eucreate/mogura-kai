<?php
include_once("inc/load_setting.php");

$w3a["Loding"] = <<<EOM
<DIV id="load_page" style="position:absolute; top:50px; left: 20px; width:100%; background-color: #FFFFFF; visibility:hidden; text-align:center; z-index: 0;">
<img src="image/loading.gif" alt="データ読み込み中"  width="170" height="30" border="0">
</DIV>
<script type='text/javascript'>
<!--
loading_start();
//-->
</script>
EOM;


#-------------------------------------------------
#  SelectWindow
#-------------------------------------------------
$sel_point = set_img("image/selwin_point.gif");
$w3a["Select_Window"] = <<<EOM
<div id="SelLay" style="position:absolute;width:auto;visibility:hidden;">
<div id="SelMain" class="sel_bg">
<table width="100%" border="0" cellpadding="0" cellspacing="5" id="SelLay_topbar" class="topbar">
<tr valign="middle">
<td>{$sel_point}&nbsp;<b>指定ログ表示</b></td>
<td align="right"><a href="javascript:void(0)" onMouseDown="hideLayWin(lwObj); return false;"><img src="image/selwin_close.gif" width="21" height="21" alt="close" border="0"></a></td>
</tr>
</table>
<div class="cont">
<iframe name="select" width="100%" height="350">
この部分はインラインフレームを使用しています。
</iframe>
</div>
<div class="bottombar">&nbsp;</div>
</div>
</div>
EOM;


#-------------------------------------------------
#  ONLINE
#-------------------------------------------------
$online_timeout = 300;
$sql = array();
$sql["select"] = "COUNT(DISTINCT id) as uniq,now() as now,unix_timestamp() as ut";
$sql["where"] = "date >= '".gmdate('Y-m-d H:i:s', time()+constant('TIME_DIFF')*3600-$online_timeout)."' ";
$res = $db->query(mk_sql($sql));
check_err($res);
$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
$w3a["ONLINE"] = set_img("image/online.gif","オンラインユーザー").'&nbsp;現在のオンラインユーザー:&nbsp;<b>'.number_format($row["uniq"]).'</b><br><small>（'.$row["now"].'）</small>';
$login_ut = $row["ut"];
$res->free();


#-------------------------------------------------
#  SEARCH_BOX
#-------------------------------------------------
$p_sel_hidden =  isset($_GET["p_sel"]) ? '<input type="hidden" name="p_sel" value="'.htmlspecialchars($_GET["p_sel"]).'">'."\n" : "";
$fn = constant("FILENAME");
$w3a["SEARCH_BOX"] = <<<EOM
<form action="{$fn}" method="GET">
<input type="hidden" name="ym" value="{$ym}">
<input type="hidden" name="d" value="{$d}">
{$p_sel_hidden}
<input type="hidden" name="act" value="search">
<img src="image/search.gif" width="75" height="15" align="absmiddle">
<input type="text" name="sel" size=30 value="Search Keyword" class="search_txt" onblur="if(this.value==''){this.style.color=''; this.form.reset()}" onfocus="this.value = ''; this.style.color = '#000';">
&nbsp;<input type="submit" value="検索">
</form>
EOM;


#-------------------------------------------------
#  CALENDAR
#-------------------------------------------------
include_once($skin_dir."/calendar.php");


#-------------------------------------------------
#  NAVI_LIST
#-------------------------------------------------
if($menu_act){
	$navi_key = "";
	//アクション
	foreach ($menu_act as $v) {
		$v = trim($v);
		if($v == "") continue;
		if(preg_match("/^\[.+.]$/" ,$v)){ $navi_key = preg_replace("/^\[(.+)]$/" , "\\1",$v); continue; }
		list($tmp["act"], $tmp["name"]) = explode("||", $v);
		if($navi_key == "") $navi_key = $tmp["name"];
		$navi[$navi_key][$tmp["act"]] = '<a href="w3a.php'.query_edit("act",$tmp["act"]).'">'.$tmp["name"].'</a>';
	}
	//管理メニュー追加
	if(constant("ADMIN_MODE") == 1 || constant("ADMIN_MODE") == 2){
		$navi["管理ツール"][] = '<a href="./control_panel/cp.php">コントロールパネル</a>';
	}
	if(constant("USERAUTH_FLG") == 2 && constant("ADMIN_MODE") != 2){
		$navi["ログアウト"][] = '<a href="w3a.php?logout">ログアウト</a>';
	}
	unset($menu_act,$navi_key,$tmp);
	if(file_exists($skin_dir."/navi.php")) include_once($skin_dir."/navi.php");
}else{
	$w3a["NAVI_LIST"] = 'Navigation Error.';
}


#-------------------------------------------------
#  日付
#-------------------------------------------------
$now_d = $d <> 99 ? $d."\n日" : " ［月合計］";
$date_img = set_img("image/date.gif");
$w3a["DATA_DATE"] = <<<EOM
{$date_img}
解析日：
<b>
$year
年&nbsp;
$month
月&nbsp;
$now_d
</b>
EOM;
//指定期間
$seld_err = false;
if(isset($_GET["seld_t"]) && isset($_GET["seld_e"])){
	$w3a["DATA_DATE"] = "";
	$w3a["DATA_DATE"] .= $date_img.'解析日：&nbsp;';
	$w3a["DATA_DATE"] .= '<b>'.substr($_GET["seld_t"],0,4)."年&nbsp;".substr($_GET["seld_t"],4,2)."月&nbsp;".substr($_GET["seld_t"],6,2)."日".'</b>';
	$w3a["DATA_DATE"] .= '&nbsp;～&nbsp;';
	$w3a["DATA_DATE"] .= '<b>'.substr($_GET["seld_e"],0,4)."年&nbsp;".substr($_GET["seld_e"],4,2)."月&nbsp;".substr($_GET["seld_e"],6,2)."日".'</b>';
	$w3a["DATA_DATE"] .= '&nbsp;［期間指定］';
	
	if($_GET["seld_t"] > $_GET["seld_e"]) $seld_err = true;
}

#-------------------------------------------------
#  パス指定 
#-------------------------------------------------
if(isset($_GET["p_sel"])){
$p_sel_img = set_img("image/filter.gif","フィルター");
$p_sel_now = $_GET["p_sel"];
$p_sel_cansel = '&nbsp;&nbsp;<a href="'.constant("FILENAME").query_edit("p_sel","DELETE").'">'.set_img(constant("ICON_CLOSE")).'指定解除</a>';
$w3a["OPTION"] = <<<EOM
<br>$p_sel_img
ページ指定：
<b>
$p_sel_now
</b>
$p_sel_cansel
<hr size="1">
EOM;
}


#-------------------------------------------------
#  メイン上部表示
#-------------------------------------------------
//スキン読み込み
$html = read_skin($skin_file,$w3a,1);
echo $html["up"];
flush();


#-------------------------------------------------
#  アクション実行
#-------------------------------------------------
echo $html["main_up"];
//日付チェック
if(!preg_match("/^(00|99)$/i", $d) && !checkdate(substr($ym, 4, 2),$d,substr($ym, 0, 4))){
	echo '<div id="error">解析指定日が不正です。'
	."<br>".substr($ym, 0, 4).'/'.substr($ym, 4, 2).'/'.$d." は無効な日付です。"
	.'</div>';
}elseif($seld_err){
	echo '<div id="error">解析期間の指定が不正です。</div>';
}elseif($act){
	$plugin = constant("DIR_PLUGIN").$act.'.php';
	if(file_exists($plugin)){
		//指定ログ用 デコード
		#if($_GET["sel"] != "") $_GET["sel"] = urldecode($_GET["sel"]);
		//プラグイン読み込み
		include_once($plugin);
	}else{
		echo '<div id="error">アクション［<b>'.$act."</b>］は実行できませんでした。</div>";
	}
}else{
	echo '<div id="error">アクションが指定されていません。</div>';
}
echo $html["main_down"];
flush();


#-------------------------------------------------
#  メイン下部表示
#-------------------------------------------------
$html["down"] = str_replace('{-PROCESS-}','Processing time&nbsp;['.round(getmicrotime()-STIME, 2).'&nbsp;sec]', $html["down"]);
echo $html["down"];


#-------------------------------------------------
#  DB接続終了
#-------------------------------------------------
$db->disconnect();
?>
