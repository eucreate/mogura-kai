<?php
if(!defined("CONF_READ")) include_once('inc/config.php');
$index_list = array('index.html', 'index.htm', 'index.cgi', 'index.php', 'index.xhtm', 'index.shtml', 'index.shtm', 'index.xcg', 'index.hdml');

class WriteClass{
	//debug
	function debug($data){
		echo "<XMP>";
		print_r($data);
		echo "</XMP>";
		exit;
	}
	//SQL修正
	function quote_smart($value, $type=0){
		if(constant("ENC_FLG") && !$type) $value = @mb_convert_encoding($value, "UTF-8", constant("TO_ENCODING"));
		if(get_magic_quotes_gpc()) $value = stripslashes($value);
        $link = mysqli_connect(DB_HOST_MOGURA, DB_USER_MOGURA, DB_PWD_MOGURA, DB_NAME_MOGURA);
		if (!$type && (!strlen($value) || !ctype_digit(strval($value)))){
			return "'".mysqli_real_escape_string($link, $value)."'";
		}else{
			return mysqli_real_escape_string($link, $value);
		}
	}
	//エラーチェック
	function check_err($db, $res){
		if(DB::isError($res)){
			echo "<font color=\"#CC0000\"><b>DB Error:</b>&nbsp;";
            $link = mysqli_connect(DB_HOST_MOGURA, DB_USER_MOGURA, DB_PWD_MOGURA, DB_NAME_MOGURA);
			if((defined("DB_TYPE_MOGURA") ? constant("DB_TYPE_MOGURA") : constant("DB_TYPE")) == "mysql"){
				echo mysqli_error($link);
			}else{
				echo DB::errorMessage($res);
			}
			$db->disconnect();
			die("</font>");
		}
	}
	function check_err_duplicate($db, $res, $sql, $table){
		if(!DB::isError($res)){
			return;
		}
		
		if(mysqli_errno() === 1062){
			//ロック
			$db->query("LOCK TABLES ".$table." WRITE");
			$res = $db->query($sql);
			if(!DB::isError($res)){
				//ロック解除
				$db->query("UNLOCK TABLES");
				return;
			}
			$db->query("REPAIR TABLE ".$table);
			$res = $db->query($sql);
			//ロック解除
			$db->query("UNLOCK TABLES");
		}
		WriteClass::check_err($db, $res);
	}
	//カウンター生成
	function mk_counter($num = 0){
		if($num){
			$num = str_pad($num, constant("COUNTER_DIGIT"), "0", STR_PAD_LEFT);
			$counter = array("width" => 0,"height" => 0);
			while(strlen($num)){
				$n = substr($num, 0, 1);
				$num = substr($num, 1);
				list($width, $height) = getimagesize(constant("COUNTER_DIR").$n.".png");
				$counter["num"][] = $n;
				$counter["width"] += $width;
				if($counter["height"] < $height) $counter["height"] = $height;
			}
			
			if($im = @imagecreate($counter["width"], $counter["height"])){
				$set_w = 0;
				foreach ($counter["num"] as $v) {
					list($width, $height) = getimagesize(constant("COUNTER_DIR").$v.".png");
					$src_im = imagecreatefrompng(constant("COUNTER_DIR").$v.".png");
					imagecopy($im, $src_im,
					$set_w, 0,
					0, 0,
					$width, $height);
					imagedestroy($src_im);
					$set_w += $width;
				}
				header("Content-type: image/png"); 
				imagepng($im);
				imagedestroy($im);
				exit;
			}
		}else{
			header("Content-Type: image/gif");
			echo base64_decode("R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==");
			exit;
		}
		exit;
	}
	//タイトル取得
	function get_title($path){
		if(!$path) return;
		
		$title = "";
		$d = "";
		switch(constant("GET_TITLE_MODE")){
			case 0:
				return;
				break;
			case 1:
				//ローカルファイルからタイトル取得
				if(defined("MOGURA_PATH") && !constant("PHP_WRITE_MODE")){
					$temp_path = strtok($path, '?');
					$url = (strrpos($temp_path, '/') === strlen($temp_path) - 1) ? '' : basename($temp_path);
				}else{
					$url = str_replace(dirname($_SERVER["PHP_SELF"] ? strtok($_SERVER["PHP_SELF"], '?') : $_SERVER["SCRIPT_NAME"]), '', dirname(__FILE__)).'/';
					$temp_path = strtok($path, '?');
					$url .= $temp_path;
				}
				if(strrpos($temp_path, '/') === strlen($temp_path) - 1){
					global $index_list;
					foreach($index_list as $index){
						if(file_exists($url.$index)){
							$url .= $index;
							break;
						}
					}
				}
				if(function_exists('file_get_contents')){
					$d = @file_get_contents($url);
				}else{
					$d = @implode('',@file($url));
				}
				break;
			case 2:
				//URIからタイトル取得
				$url = "http://".$_SERVER["HTTP_HOST"].$path;
				
				if($_SERVER["QUERY_STRING"]){
					$url .= "?".$_SERVER["QUERY_STRING"]."&GET_TITLE";
				}else{
					$url .= "?GET_TITLE";
				}
				if(function_exists('file_get_contents')){
					$d = @file_get_contents($url);
				}else{
					$d = @implode('',@file($url));
				}
				break;
		}
		if($d){
			$d = @mb_convert_encoding($d, "UTF-8", constant("TO_ENCODING"));
			preg_match("/<title[^>]*>([^<]*)<\/title>/i",$d,$t);
			$title = trim($t[1]);
		}
		return $title;
	}
	//配列に指定値があるかチェック
	function inc_array($str,$arr){
		$str = trim($str);
		$retval = FALSE;
		if(is_array($arr) && $str != ""){
			foreach($arr as $v){
				$v = trim($v);
				if($v == "") continue;
				if(stristr($v, $str) != ""){
					$retval = TRUE;
					break;
				}
			}
		}
		return $retval;
	}
	
	//拒否リストの読み込み
	function get_exclude($db){
		$res = $db->query("SELECT * FROM ".constant("DB_EXCLUDE").";");
		WriteClass::check_err($db, $res);
		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
			$exclude[$row["exclude_type"]][] = $row["exclude_val"];
		}
		$res->free();
		
		return $exclude;
	}
	
	//CRC16
	function crc16($data){
		$crc = 0xFFFF;
		for ($i = 0; $i < strlen($data); $i++){
			$x = (($crc >> 8) ^ ord($data[$i])) & 0xFF;
			$x ^= $x >> 4;
			$crc = (($crc << 8) ^ ($x << 12) ^ ($x << 5) ^ $x) & 0xFFFF;
		}
		return $crc;
	}
	
	//ID取得
	function get_id($data){
		$crc48 = sprintf('%010u%05u', crc32($data), WriteClass::crc16($data));
		return sprintf('%05s', base_convert(substr($crc48, 0, 8), 10, 36)).sprintf('%05s', base_convert(substr($crc48, 8), 10, 36));
	}

}
/*=================================================*/
/* モード[PHP]用カウンター出力                     */
/*=================================================*/
$mode = (isset($_GET["mode"]) ? $_GET["mode"] : "php");
if($mode != "img") $mode = "php";
if($mode == "php" && isset($_GET["counter"])) WriteClass::mk_counter($_GET["counter"]);

/*=================================================*/
/* メイン処理                                      */
/*=================================================*/
// DB接続
$db = DB::connect(constant("DSN"));
// エラーチェック
WriteClass::check_err($db, $db);
$db->query("SET NAMES utf8;");
$write_flg = true;

//-------------------------------------
//  共通データ
//-------------------------------------
//初期化
$w3a_buf = array();
//IP
if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])
&& ereg('^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$',$_SERVER['HTTP_X_FORWARDED_FOR'])){
	$w3a_buf["ip"] = $_SERVER['HTTP_X_FORWARDED_FOR'];
}else{
	$w3a_buf["ip"] = $_SERVER["REMOTE_ADDR"];
}
//ID
$w3a_buf["id"] = "";
if(isset($_COOKIE["w3a"]["id"])){
	$w3a_buf["id"] = $_COOKIE["w3a"]["id"];
}

if(!isset($env)){
	define ("ENC_FLG", preg_match("/^(UTF-8)$/i", mb_http_input("G")) ? true : false);
	$http_ua = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : '';
	if(constant("ENC_FLG")){
		$http_ua = @mb_convert_encoding($http_ua, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
	}
	@include_once('inc/function.php');
	$obj = new MobileCheck($http_ua);
	$env = $obj->CheckUA();
}
if($env !== 'other'){
	$obj->GetZone($env);
	$result = $obj->CheckIP($obj->zone);
	if($result){
		$mobile_id = $obj->GetSub($env);
		
		if(isset($mobile_id) && strlen(trim($mobile_id))){
			$w3a_buf["id"] = WriteClass::get_id($mobile_id);
		}elseif($w3a_buf["id"] === "" || strlen($w3a_buf["id"]) !== 10 || strpos($w3a_buf["id"], '.') !== false){
			$w3a_buf["id"] = WriteClass::get_id(microtime());
		}
	}
}

if($w3a_buf["id"] === ""){
	foreach(explode(".", $w3a_buf["ip"]) as $v){ $w3a_buf["id"] .= dechex($v); }
}

if($w3a_buf["id"]) @setcookie("w3a[id]",$w3a_buf["id"],time()+60*60*24*constant("COOKIE_EXPIRE"),'/');
//date
$w3a_buf["date"] = gmdate("Y-m-d H:i:s", time()+constant("TIME_DIFF")*3600);
//HOST
$w3a_buf["host"] = @gethostbyaddr($w3a_buf["ip"]);

if(getcwd() == dirname(__FILE__) && $mode != "img"){
	$direct_access = isset($_GET["mogura_path"]) ? false : true;
}else{
	$direct_access = false;
}

if($direct_access){
	//-------------------------------------
	//  ダウンロードカウンター
	//-------------------------------------
	if(!isset($_GET["dl"])){
		//DB接続終了
		$db->disconnect();
		die("Direct Access Error.");
	}
	$write_flg = true;
	//拒否リストの読み込み
	$exclude = WriteClass::get_exclude($db);
	//DL
	$w3a_buf["dl"] = $_GET["dl"];
	
	//-------------------------------------
	//  拒否処理
	//-------------------------------------
	if($write_flg && WriteClass::inc_array($w3a_buf["id"], $exclude["id"])) $write_flg = false;
	if($write_flg && WriteClass::inc_array($w3a_buf["host"], $exclude["host"])) $write_flg = false;
	if($write_flg && WriteClass::inc_array($w3a_buf["ip"], $exclude["host"])) $write_flg = false;
	if($write_flg && WriteClass::inc_array($http_ua, $exclude["ua"])) $write_flg = false;
	//-------------------------------------
	//  ログ追加
	//-------------------------------------
	if($write_flg){
		while (list ($k, $v) = each ($w3a_buf)) {
			$v = trim($v);
			if($v == "") continue;
			
			$sql_k .= WriteClass::quote_smart($k, 1).",";
			$sql_v .= WriteClass::quote_smart($v).",";
		}
		$sql_k = "(".substr($sql_k,0,strlen($sql_k)-1).")";
		$sql_v = " VALUES (".substr($sql_v,0,strlen($sql_v)-1).")";
		$sql = "INSERT INTO ".constant("DB_TABLE_LOG")." ".$sql_k.$sql_v.";";
		$res = $db->query($sql);
		WriteClass::check_err_duplicate($db, $res, $sql, constant("DB_TABLE_LOG"));
		//指定数以上のログ削除
		if(constant("MAX_LOG")){
			$res = $db->query("SELECT no FROM ".constant("DB_TABLE_LOG")." order by no desc limit ".constant("MAX_LOG").",1;");
			if($res->numRows()){
				$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
				$db->query("DELETE FROM ".constant("DB_TABLE_LOG")." WHERE no <= ".$row["no"].";");
			}
		}
	}
	//DB接続終了
	$db->disconnect();
	//Location
	header("Location: ".$_GET["dl"]);
	exit;
}elseif(constant("ACC_RUN") || (!isset($_GET["GET_TITLE"]) && $mode == "php")){
	//-------------------------------------
	//  通常ログ
	//-------------------------------------
	//文字エンコーディング
	$def_enc = @mb_internal_encoding();
	@mb_internal_encoding("UTF-8");
	
	//拒否リストの読み込み
	$exclude = WriteClass::get_exclude($db);
	
	//Path
	if(isset($_GET["path"])){
		if(!isset($_GET["send_title"]) && isset($_GET["js"]) && $_GET["js"] == '1'){
			$w3a_buf["path"] = ereg_replace("(https?|ftp|news)(://[[:alnum:]\+\$\;\?\.%,!#~*:@&=_-]+)(/.*)","\\3",$_GET["path"]);
		}else{
			$w3a_buf["path"] = $_GET["path"];
		}
	}else{
		if(getcwd() == dirname(__FILE__)){
			if(isset($_SERVER["HTTP_REFERER"])){
				$w3a_buf["path"] = ereg_replace("(https?|ftp|news)(://[[:alnum:]\+\$\;\?\.%,!#~*:@&=_-]+)(/.*)","\\3",$_SERVER["HTTP_REFERER"]);
			}else{
				$w3a_buf["path"] = "";
			}
		}else{
			$w3a_buf["path"] = $_SERVER["PHP_SELF"] ? $_SERVER["PHP_SELF"] : $_SERVER["SCRIPT_NAME"];
			if($_SERVER["QUERY_STRING"]) $w3a_buf["path"] = $w3a_buf["path"]."?".$_SERVER["QUERY_STRING"];
		}
	}
	
	//Referer
	if(isset($_GET["ref"])){
		$w3a_buf["ref"] = $_GET["ref"];
	}else{
		if(getcwd() == dirname(__FILE__)){
			$w3a_buf["ref"] = "";
		}else{
			if(isset($_SERVER["HTTP_REFERER"])) $w3a_buf["ref"] = $_SERVER["HTTP_REFERER"];
		}
	}
	if(isset($w3a_buf["ref"])){
		$w3a_buf["ref"] = @mb_convert_encoding($w3a_buf["ref"], "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
		$w3a_buf["ref_q"] = strstr($w3a_buf["ref"], '?');
		if($w3a_buf["ref_q"]){
			$w3a_buf["ref"] = str_replace ($w3a_buf["ref_q"], "", $w3a_buf["ref"]);
			$w3a_buf["ref_q"] = substr($w3a_buf["ref_q"], 1);
		}
	}
	
	//Title
	$get_title = array();
	$get_title['send_title'] = isset($_GET["send_title"]) ? trim($_GET["send_title"]) : "";
	$get_title['title'] = isset($_GET["title"]) ? trim($_GET["title"]) : "";
	if($get_title['send_title']){
		$w3a_buf["title"] = @mb_convert_encoding($get_title["send_title"], "UTF-8", constant("TO_ENCODING"));
	}elseif($get_title['title']){
		$w3a_buf["title"] = @mb_convert_encoding($get_title['title'], "UTF-8", constant("TO_ENCODING"));
	}else{
		if(getcwd() == dirname(__FILE__) && isset($_GET["mogura_path"])){
			#$w3a_buf["title"] = WriteClass::get_title($w3a_buf["path"]);
		}else{
			$w3a_buf["title"] = WriteClass::get_title($w3a_buf["path"]);
		}
	}
	unset($get_title);

	//UserAgent
	if(isset($_SERVER["HTTP_USER_AGENT"])){
		$ua = WriteClass::quote_smart($_SERVER["HTTP_USER_AGENT"]);
		$sql = "SELECT ua_id FROM ".constant("DB_TABLE_UA")." WHERE ua_name = ".$ua.";";
		$res = $db->query($sql);
		WriteClass::check_err($db, $res);
		$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
		$w3a_buf["ua"] = $row["ua_id"];
		$res->free();
		if($w3a_buf["ua"] == ""){
			if(constant("ENC_FLG")) $ua = @mb_convert_encoding($ua, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
			$ua_list = array();
			list($ua_list["browser"], $ua_list["browser_v"], $ua_list["os"], $ua_list["os_v"]) = getuseragent($http_ua, dirname(__FILE__).'/');
			//UA追加
			$sql = "INSERT INTO ".constant("DB_TABLE_UA")." (ua_name,os,os_v,brow,brow_v) values (".$ua.",'".$ua_list["os"]."','".$ua_list["os_v"]."','".$ua_list["browser"]."','".$ua_list["browser_v"]."');";
			$res = $db->query($sql);
			WriteClass::check_err_duplicate($db, $res, $sql, constant("DB_TABLE_UA"));
			//UA取得
			$sql = "SELECT ua_id FROM ".constant("DB_TABLE_UA")." WHERE ua_name = ".$ua." ORDER BY ua_id;";
			$res = $db->query($sql);
			WriteClass::check_err($db, $res);
			$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
			$w3a_buf["ua"] = $row["ua_id"];
			if($res->numRows() > 1){
				$sql = "DELETE FROM ".constant("DB_TABLE_UA")." WHERE ua_id IN(";
				while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
					$sql .= $row["ua_id"].',';
				}
				$db->query(substr($sql, 0, -1).');');
			}
			$res->free();
		}
	}
	
	//Language
	if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])){
		if(strpos($_SERVER['HTTP_ACCEPT_LANGUAGE'],';')){
			list($w3a_buf['lang']) =  explode(';',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
		}else{
			$w3a_buf['lang'] = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
		}
		list($w3a_buf['lang']) = explode(',',$w3a_buf['lang']);
		$w3a_buf['lang'] = strtolower($w3a_buf['lang']);
	}
	
	//monitor
	$w3a_buf["monitor"] = isset($_GET["monitor"]) ? $_GET["monitor"] : "";
	
	//color
	$w3a_buf["color"] = isset($_GET["color"]) ? $_GET["color"] : "";
	
	//cookie
	$w3a_buf["cookie"] = isset($_GET["cookie"]) ? $_GET["cookie"] : "";
	
	//JavaScript
	$w3a_buf["js"] = isset($_GET["js"]) ? $_GET["js"] : "";
	
	//-------------------------------------
	//  拒否処理
	//-------------------------------------
	if(isset($exclude["id"]))   if($write_flg && WriteClass::inc_array($w3a_buf["id"], $exclude["id"])){ $write_flg = false; }
	if(isset($exclude["host"])) if($write_flg && WriteClass::inc_array($w3a_buf["host"], $exclude["host"])){ $write_flg = false; }
	if(isset($exclude["host"])) if($write_flg && WriteClass::inc_array($w3a_buf["ip"], $exclude["host"])){ $write_flg = false; }
	if(isset($exclude["path"])) if($write_flg && WriteClass::inc_array($w3a_buf["path"], $exclude["path"])){ $write_flg = false; }
	if(isset($exclude["url"]))  if($write_flg && WriteClass::inc_array($w3a_buf["ref"], $exclude["url"])){ $write_flg = false; }
	if(isset($exclude["ua"]))   if($write_flg && WriteClass::inc_array($http_ua, $exclude["ua"])){ $write_flg = false; }
	//リロード対策
	if($write_flg && constant("UNWRITE") > 0){
		$sql = "SELECT path FROM ".constant("DB_TABLE_LOG")
				." WHERE date >= DATE_ADD(now(),interval -".constant("UNWRITE")." SECOND)"
				." AND id = '".$w3a_buf["id"]."'"
				." order by date desc"
				." LIMIT 1;";
		$res = $db->query($sql);
		unset($row);
		$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
		WriteClass::check_err($db, $res);
		if($row["path"] && $row["path"] == $w3a_buf["path"]) $write_flg = false;
	}
	//-------------------------------------
	//  ログ追加
	//-------------------------------------
	if($write_flg){
		$sql_k = "";
		$sql_v = "";
		while (list ($k, $v) = each ($w3a_buf)) {
			$v = trim($v);
			if($v == "") continue;
			
			$sql_k .= WriteClass::quote_smart($k, 1).",";
			$sql_v .= WriteClass::quote_smart($v).",";
		}
		$sql_k = "(".substr($sql_k,0,strlen($sql_k)-1).")";
		$sql_v = " VALUES (".substr($sql_v,0,strlen($sql_v)-1).")";
		$sql = "INSERT INTO ".constant("DB_TABLE_LOG")." ".$sql_k.$sql_v.";";
		if(constant("ENC_FLG")) $sql = @mb_convert_encoding($sql, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
		$res = $db->query($sql);
		WriteClass::check_err_duplicate($db, $res, $sql, constant("DB_TABLE_LOG"));
		//指定数以上のログ削除
		if(constant("MAX_LOG")){
			$res = $db->query("SELECT no FROM ".constant("DB_TABLE_LOG")." order by no desc limit ".constant("MAX_LOG").",1;");
			if($res->numRows()){
				$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
				$db->query("DELETE FROM ".constant("DB_TABLE_LOG")." WHERE no <= ".$row["no"].";");
			}
		}
	}

	//-------------------------------------
	//  カウンター数値
	//-------------------------------------
	$counter = 0;
	if(constant("COUNTER")){
		//ユニーク取得
		$sql = "SELECT path FROM ".constant("DB_TABLE_LOG")." WHERE date like '".gmdate("Y-m-d", time()+constant("TIME_DIFF")*3600)."%'"." AND id = '".$w3a_buf["id"]."' LIMIT 1;";
		$res = $db->query($sql);
		WriteClass::check_err($db, $res);
		//カウンター増加
		if($write_flg && $res->fetchRow()){
			//ロック
			$db->query("LOCK TABLES ".constant("DB_COUNTER")." WRITE");
			$res = $db->query("SELECT * FROM ".constant("DB_COUNTER")." WHERE counter_date = '".gmdate("Y-m-d", time()+constant("TIME_DIFF")*3600)."' LIMIT 1;");
			if($res->fetchRow()){
				$db->query("UPDATE ".constant("DB_COUNTER")." SET counter_val=counter_val+1 WHERE counter_date = '".gmdate("Y-m-d", time()+constant("TIME_DIFF")*3600)."';");
			}else{
				$db->query("INSERT INTO ".constant("DB_COUNTER")." (counter_date,counter_val) VALUES ('".gmdate("Y-m-d", time()+constant("TIME_DIFF")*3600)."',1);");
			}
			//ロック解除
			$db->query("UNLOCK TABLES");
		}
		//カウンター値
		$res = $db->query("SELECT SUM(counter_val) as counter FROM ".constant("DB_COUNTER")." LIMIT 1;");
		WriteClass::check_err($db, $res);
		$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
		$counter = $row["counter"];
	}
	
	//-------------------------------------
	//  DB接続終了
	//-------------------------------------
	$db->disconnect();
	//文字エンコーディング
	@mb_internal_encoding($def_enc);
}
/*======================================*/
/* カウンター出力                       */
/*======================================*/
if(constant("ACC_RUN")){
	// カウンター表示
	if(constant("COUNTER")){
		if($mode == "img"){
			// PHP_WRITE_MODE [1]
			if(constant("COUNTER_TYPE") == 'txt'){
				WriteClass::mk_counter();
			}elseif(constant("COUNTER_TYPE") == 'img'){
				WriteClass::mk_counter($counter);
			}
		}else{
			// PHP_WRITE_MODE [0]
			if(constant("COUNTER_TYPE") == 'txt'){
				$counter = str_pad($counter, constant("COUNTER_DIGIT"), "0", STR_PAD_LEFT);
				echo $counter;
			}elseif(constant("COUNTER_TYPE") == 'img'){
				echo '<img src="'.constant("W_PATH").'?mode=php&counter='.$counter.'">';
			}
		}
		unset($w3a_buf, $row, $write_flg, $exclude, $include_path);

	// カウンター表示なし
	}elseif($mode == "img"){
		header("Content-Type: image/gif");
		echo base64_decode("R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==");
		exit;
	}
}
?>
