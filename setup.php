<?php
@include_once('inc/config.php');
@include_once('inc/function.php');

//内部文字エンコード
@mb_internal_encoding('UTF-8');
//HTTP出力文字エンコーディング
@mb_http_output('UTF-8');

if(defined("CONF_READ")){
$sql = array();
$create_table_opt = 'ENGINE=MyISAM DEFAULT CHARACTER SET utf8';
/*=========================================*/
/* ERR MSG                                 */
/*=========================================*/
$msg = array(
'ver02' => 'MySQLバージョン取得 - 失敗',
'log01' => 'ログ保存テーブル［<b>'.constant("DB_TABLE_LOG").'</b>］作成 - 成功',
'log02' => 'ログ保存テーブル作成［<b>'.constant("DB_TABLE_LOG").'</b>］ - 失敗',
'con01' => 'カウンター保存テーブル［<b>'.constant("DB_COUNTER").'</b>］作成 - 成功',
'con02' => 'カウンター保存テーブル［<b>'.constant("DB_COUNTER").'</b>］作成 - 失敗',
'uat01' => 'UserAgent保存テーブル［<b>'.constant("DB_TABLE_UA").'</b>］作成 - 成功',
'uat02' => 'UserAgent保存テーブル［<b>'.constant("DB_TABLE_UA").'</b>］作成 - 失敗',
'set01' => '設定保存テーブル［<b>'.constant("DB_TABLE_SETTING").'</b>］作成 - 成功',
'set02' => '設定保存テーブル［<b>'.constant("DB_TABLE_SETTING").'</b>］作成 - 失敗',
'cid01' => 'ID変換リスト保存テーブル［<b>'.constant("DB_CH_ID").'</b>］作成 - 成功',
'cid02' => 'ID変換リスト保存テーブル［<b>'.constant("DB_CH_ID").'</b>］作成 - 失敗',
'cul01' => 'URL変換リスト保存テーブル［<b>'.constant("DB_CH_URL").'</b>］作成 - 成功',
'cul02' => 'URL変換リスト保存テーブル［<b>'.constant("DB_CH_URL").'</b>］作成 - 失敗',
'exc01' => '拒否リスト保存テーブル［<b>'.constant("DB_EXCLUDE").'</b>］作成 - 成功',
'exc02' => '拒否リスト保存テーブル［<b>'.constant("DB_EXCLUDE").'</b>］作成 - 失敗',
'del01' => '全テーブルの削除 - 成功',
'del02' => '全テーブルの削除 - 失敗',
'def01' => '設定初期値の追加 - 成功',
'def02' => '設定初期値の追加 - 失敗',
);

/*=========================================*/
/* ログ保存テーブル                        */
/*=========================================*/
$t = constant("DB_TABLE_LOG");
$sql["log"] = <<<SQL_STR
DROP TABLE IF EXISTS {$t};
CREATE TABLE IF NOT EXISTS {$t} (
  no int(11) NOT NULL auto_increment,
  id varchar(10) NOT NULL default '',
  date datetime NOT NULL default '0000-00-00 00:00:00',
  path tinytext,
  ip varchar(15) default NULL,
  host tinytext,
  ref text,
  ref_q text,
  ua int(5) default NULL,
  title text,
  lang varchar(10) default NULL,
  monitor varchar(15) default NULL,
  color int(10) default NULL,
  cookie int(1) default NULL,
  js int(1) default NULL,
  dl tinytext,
  PRIMARY KEY  (no)
) {$create_table_opt} AUTO_INCREMENT=0;
ALTER TABLE {$t} ADD INDEX iddate (id, date);
ALTER TABLE {$t} ADD INDEX date (date);
ALTER TABLE {$t} ADD INDEX ua (ua);
SQL_STR;

/*=========================================*/
/* ログ保存テーブル                        */
/*=========================================*/
$t = constant("DB_COUNTER");
$sql["con"] = <<<SQL_STR
DROP TABLE IF EXISTS {$t};
CREATE TABLE IF NOT EXISTS {$t} (
  counter_date date NOT NULL default '0000-00-00',
  counter_val int(11) NOT NULL default '0'
) {$create_table_opt};
SQL_STR;

/*=========================================*/
/* UserAgent保存テーブル                   */
/*=========================================*/
$t = constant("DB_TABLE_UA");
$sql["uat"] = <<<SQL_STR
DROP TABLE IF EXISTS {$t};
CREATE TABLE IF NOT EXISTS {$t} (
  ua_id int(11) NOT NULL auto_increment,
  ua_name text,
  os text,
  os_v varchar(20) default NULL,
  brow text,
  brow_v varchar(20) default NULL,
  PRIMARY KEY  (ua_id)
) {$create_table_opt} AUTO_INCREMENT=0;
ALTER TABLE {$t} ADD INDEX ua_name (ua_name(50));
SQL_STR;

/*=========================================*/
/* 設定保存テーブル                        */
/*=========================================*/
$t = constant("DB_TABLE_SETTING");
$sql["set"] = <<<SQL_STR
DROP TABLE IF EXISTS {$t};
CREATE TABLE IF NOT EXISTS {$t} (
  setting_id varchar(255) NOT NULL default '',
  setting_val text NOT NULL,
  PRIMARY KEY  (setting_id)
) {$create_table_opt};
SQL_STR;

/*=========================================*/
/* ID変換リスト保存テーブル                */
/*=========================================*/
$t = constant("DB_CH_ID");
$sql["cid"] = <<<SQL_STR
DROP TABLE IF EXISTS {$t};
CREATE TABLE IF NOT EXISTS {$t} (
  ch_id varchar(10) NOT NULL default '',
  ch_name varchar(255) NOT NULL default '',
  PRIMARY KEY  (ch_id)
) {$create_table_opt};
SQL_STR;

/*=========================================*/
/* URL変換リスト保存テーブル               */
/*=========================================*/
$t = constant("DB_CH_URL");
$sql["cul"] = <<<SQL_STR
DROP TABLE IF EXISTS {$t};
CREATE TABLE IF NOT EXISTS {$t} (
  ch_url varchar(255) NOT NULL default '',
  ch_urlname text NOT NULL,
  PRIMARY KEY  (ch_url)
) {$create_table_opt};
SQL_STR;

/*=========================================*/
/* 拒否リスト保存テーブル                  */
/*=========================================*/
$t = constant("DB_EXCLUDE");
$sql["exc"] = <<<SQL_STR
DROP TABLE IF EXISTS {$t};
CREATE TABLE IF NOT EXISTS {$t} (
  exclude_type varchar(20) NOT NULL default '',
  exclude_val text NOT NULL
) {$create_table_opt};
SQL_STR;

/*=========================================*/
/* テーブル削除                            */
/*=========================================*/
$sql["del"] = 'DROP TABLE IF EXISTS '.constant("DB_TABLE_LOG").';';
$sql["del"] .= 'DROP TABLE IF EXISTS '.constant("DB_COUNTER").';';
$sql["del"] .= 'DROP TABLE IF EXISTS '.constant("DB_TABLE_UA").';';
$sql["del"] .= 'DROP TABLE IF EXISTS '.constant("DB_TABLE_SETTING").';';
$sql["del"] .= 'DROP TABLE IF EXISTS '.constant("DB_CH_ID").';';
$sql["del"] .= 'DROP TABLE IF EXISTS '.constant("DB_CH_URL").';';
$sql["del"] .= 'DROP TABLE IF EXISTS '.constant("DB_EXCLUDE").';';


/*=========================================*/
/* 設定初期値                              */
/*=========================================*/
$t = constant("DB_TABLE_SETTING");
$sql["def"] = <<<SQL_STR
INSERT INTO {$t} (setting_id, setting_val) VALUES ('def_act', 'top');
INSERT INTO {$t} (setting_id, setting_val) VALUES ('act', '[アクセス解析]\r\ntop||トップ\r\nsearch||ログ検索\r\nsearchEncoded||エンコード検索\r\n[時間解析]\r\ntime||時間別\r\nday||日別\r\nweek||曜日別\r\nmonth||月別\r\ntransit||月推移\r\n[リンク元解析]\r\npage||ページ\r\npage2||ページ2\r\npage3||タイトル別\r\nref||リンク元\r\neng||検索エンジン\r\nkeyword||検索キーワード\r\n[ユーザー環境解析]\r\nos||OS\r\nbrowser||ブラウザ\r\nlang||言語\r\nuser_env||ユーザー環境\r\narea||地域\r\n[接続元解析]\r\ndomain||ドメイン\r\nisp||ISP\r\nhost||ホスト\r\n[追跡]\r\ntrack||ユーザー追跡\r\ntrackaday||日別ユーザー追跡\r\nbot||クローラ追跡\r\n[その他]\r\nraw_log||生ログ\r\ndownload||ダウンロード\r\np_sel||ページ指定');
INSERT INTO {$t} (setting_id, setting_val) VALUES ('skin', 'mogura');
SQL_STR;
}

/*=========================================*/
/* 旧版MySQL判別（CREATE文切り替え用）     */
/*=========================================*/
function isOldMySQL(&$old_mysql){
	global $db,$msg;
	$old_mysql = true;
	
	$res = $db->query('SELECT VERSION();');
	if(DB::isError($res)){
		echo '<p class="err">&gt; '.$msg['ver02'].'</p>';
		echo '<p class="def">【エラー内容】</p>';
		echo '<p class="def">Error Message: <b>' . $res->getMessage() . "</b></p>\n";
		echo '<p class="def">Debug: <b>' . $res->getDebugInfo() . "</b></p>\n";
		
		return false;
	}
	
	$row = $res->fetchRow(DB_FETCHMODE_ORDERED);
	if($row && !preg_match('/^(3\.|4\.0).*/', trim($row[0]))){
		$old_mysql = false;
	}
	
	$res->free();
	
	return true;
}

/*=========================================*/
/* SQL送信                                 */
/*=========================================*/
function q($sql_key, $old_mysql){
	global $db,$sql,$create_table_opt,$msg;
	
	$flg = true;
	foreach (explode(";", $sql[$sql_key]) as $v) {
		if(!$flg) break;
		$v = trim($v);
		if(!$v){
			continue;
		}else{
			$v .= ";";
		}
		$v = @mb_convert_encoding($v, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
		if($old_mysql){
			$v = str_replace($create_table_opt, 'TYPE=MyISAM', $v);
		}
		$res = $db->query($v);
		$flg = (DB::isError($res) ? false : true);
	}
	if($flg){
		echo '<p>&gt; '.$msg[$sql_key."01"].'</p>';
	}else{
		echo '<p class="err">&gt; '.$msg[$sql_key."02"].'</p>';
		echo '<p class="def">【エラー内容】</p>';
		echo '<p class="def">Error Message: <b>' . $res->getMessage() . "</b></p>\n";
		echo '<p class="def">Debug: <b>' . $res->getDebugInfo() . "</b></p>\n";
	}
	return $flg;
}
/*=========================================*/
/* PEAR チェック                           */
/*=========================================*/
function file_exists_ex($path){
	$fp = @fopen($path, 'r', 1);
	if($fp){
		fclose($fp);
		return true;
	}
	return false;
}

$pear_inc_ok = true;
if($pear_inc_ok) $pear_inc_ok = file_exists_ex('PEAR.php') ? true : false;
if($pear_inc_ok) $pear_inc_ok = file_exists_ex('DB.php') ? true : false;
if($pear_inc_ok) $pear_inc_ok = file_exists_ex('DB/'.(defined("DB_TYPE_MOGURA") ? constant("DB_TYPE_MOGURA") : constant("DB_TYPE")).'.php') ? true : false;

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="robots" content="noindex,nofollow,noarchive">
<style type="text/css">
<!--
BODY {
	letter-spacing: 0.2em;
	font-family: Verdana, Arial, "MS UI Gothic", Osaka, Helvetica, sans-serif;
	background-color: #EEE;
	margin: 30px;
	padding: 0px;
	font-size: 12px;
	background-image: none;
}
A         { color: #36C; }
A:link    { text-decoration: none; }
A:visited { text-decoration: none; }
A:hover   { text-decoration: underline; color: #F30; }
p {
	margin-left: 30px;
	color: #00C;
}
.def{
	color: #333;
}
.err{
	color: #C00;
}
#copy{
	font-size: 10px;
	color: #333333;
	text-align: right;
}
//-->
</style>
<title>mogura - Setup</title>
</head>
<body>
<h2>mogura - Setup</h2>
<?php

flush();
if(defined("CONF_READ")){
	$_GET["step"] = isset($_GET["step"]) ? $_GET["step"] : "";
	switch ($_GET["step"]) {
	case 1:
	   echo '<p class="def">セットアップするとログ等<b>全てのデータが初期化</b>されるので注意してください。</p>';
	   echo '<p class="err">セットアップを実行しますか？</p>';
	   echo '<p class="def"><a href="setup.php">いいえ</a>｜<a href="setup.php?step=2">はい</a></p>';
	   break;
	case 2:
	   echo '<p class="def">セットアップを開始します。</p>';
		// DB接続
		$db = DB::connect(constant("DSN"));
		if(DB::isError($db)){
			echo '<p class="err">&gt; データベース［<b>'.(defined("DB_NAME_MOGURA") ? constant("DB_NAME_MOGURA") : constant("DB_NAME")).'</b>］に接続出来ませんでした</p>';
			echo '<p class="err">&gt; config.phpのデータベースの設定を見直してください</p>';
			echo '<p class="err">&gt; データベース［<b>'.(defined("DB_NAME_MOGURA") ? constant("DB_NAME_MOGURA") : constant("DB_NAME")).'</b>］が作成されているか確認して下さい</p>';
			echo '<p class="def">【エラー内容】</p>';
			echo '<p class="def">Error Message: <b>' . $db->getMessage() . "</b></p>\n";
			echo '<p class="def">Debug: <b>' . $db->getDebugInfo() . "</b></p>\n";
		   echo '<p class="def"><a href="setup.php">戻る</a></p>';
		}else{
			$db->query("SET NAMES utf8;");
			$db_flg = true;
			$old_mysql = true;
			echo '<p>&gt; DB接続</p>';
			if($db_flg) $db_flg = isOldMySQL($old_mysql);
			if($db_flg) $db_flg = q("log", $old_mysql);
			if($db_flg) $db_flg = q("con", $old_mysql);
			if($db_flg) $db_flg = q("uat", $old_mysql);
			if($db_flg) $db_flg = q("set", $old_mysql);
			if($db_flg) $db_flg = q("cid", $old_mysql);
			if($db_flg) $db_flg = q("cul", $old_mysql);
			if($db_flg) $db_flg = q("exc", $old_mysql);
			if($db_flg) $db_flg = q("def", false);
			
			$db->disconnect();
			echo '<p>&gt; DB切断</p>';
			
			if($db_flg){
				echo '<p class="def"><a href="setup.php?step=3">次へ</a></p>';
			}else{
				echo '<p class="def"><a href="setup.php?step=4">次へ</a></p>';
			}
		}
	   break;
	case 3:
		echo '<p>正常にセットアップ終了しました。</p>';
		echo '<p class="def"><b>「setup.php」</b>をサーバーから削除してください。</p>';
		echo '<p class="def">□ <a href="control_panel/cp.php?mode=log_w">ログ取得用コード確認ページを開く</a>（コントロールパネル）</p>';
		echo '<p class="def">□ <a href="w3a.php">Moguraメイン画面を開く</a></p>';
		echo '<p class="def">□ <a href="setup.php">setup.phpトップ</a></p>';
	   break;
	case 4:
		echo '<p class="err">正常にセットアップ終了出来ませんでした。</p>';
		echo '<p class="def">□ <a href="setup.php">setup.phpトップ</a></p>';
	   break;

	case 11:
	   echo '<p class="def">データベース内のmogura関連<b>全データ及びテーブル</b>を削除します。</p>';
	   echo '<p class="err">アンセットアップを実行しますか？</p>';
	   echo '<p class="def"><a href="setup.php">いいえ</a>｜<a href="setup.php?step=12">はい</a></p>';
	   break;
	case 12:
	   echo '<p class="def">アンセットアップを開始します。</p>';
		// DB接続
		$db = DB::connect(constant("DSN"));
		if(DB::isError($db)){
			echo '<p class="err">&gt; DB接続出来ませんでした</p>';
			echo '<p class="def">【エラー内容】</p>';
			echo '<p class="def">Error Message: <b>' . $db->getMessage() . "</b></p>\n";
			echo '<p class="def">Debug: <b>' . $db->getDebugInfo() . "</b></p>\n";
		}else{
			$db->query("SET NAMES utf8;");
			$db_flg = true;
			echo '<p>&gt; DB接続</p>';
			if($db_flg) $db_flg = q("del", false);
			$db->disconnect();
			echo '<p>&gt; DB切断</p>';
			
			if($db_flg){
				echo '<p class="def"><a href="setup.php?step=13">次へ</a></p>';
			}else{
				echo '<p class="def"><a href="setup.php?step=14">次へ</a></p>';
			}
		}
	   break;
	case 13:
		echo '<p>正常にアンセットアップ終了しました。</p>';
		echo '<p class="def"><b>mogura関連のファイル</b>をサーバーから削除してください。</p>';
		echo '<p class="def">□ <a href="setup.php">setup.phpトップ</a></p>';
	   break;
	case 14:
		echo '<p class="err">正常にアンセットアップ終了出来ませんでした。</p>';
		echo '<p class="def">□ <a href="setup.php">setup.phpトップ</a></p>';
	   break;


	default:
	   echo '<p class="def">このスクリプトは<b>PHPアクセス解析「mogura」</b>をセットアップする為のものです。</p>';
		if($pear_inc_ok){
	   	echo '<p>◆ PEARは正常に読み込まれています。</p>';
		   echo '<p class="def">□ <a href="setup.php?step=1">セットアップ</a></p>';
		   echo '<p class="def">□ <a href="setup.php?step=11">アンセットアップ</a>（mogura関連テーブルの削除）</p>';
	   }else{
	   	echo '<p class="err">◇ PEARの読み込みに失敗しました。</p>';
	   	echo '<p class="err"><a href="http://tekito.jp/webapp/mogura/" target="_blank">http://tekito.jp/webapp/mogura/</a>を開き、「ダウンロード」の項を参照してPEARをアップロードしてください。</p>';
	   }
	}
}else{
	echo '<p class="err">config.phpの読み込みに失敗しました。</p>';
}
?>
<div id="copy"><hr size="1"><a href="http://fmono.sub.jp/" target="_blank">&copy;&nbsp;OSAKA&nbsp;PHP</a>&nbsp;/&nbsp;Edit&nbsp;:&nbsp;<a href="http://tekito.jp/" target="_blank">tekito</a></div>
</body>
</html>
