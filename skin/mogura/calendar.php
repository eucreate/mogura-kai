<?php
if(constant("W3A") != "run") die('Open Error !!');

/*=========================================*/
/* 日別データ存在フラグ                    */
/*=========================================*/
$sql = array();
$sql["select"] = "dayofmonth(date) as day";
$sql["where"] = "date like '".mk_sql_date($ym)."%'";
$sql["group"] = "day";

$res = $db->query(mk_sql($sql,1));
check_err($res);

$dataflgs = array();
$max_day = 1;
while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	$dataflgs[$row["day"]] = true;
	if($max_day < $row["day"]) $max_day = $row["day"];
}
$res->free();

//月移動時
if($d == "00") $d = sprintf("%02d", $max_day);

/*=========================================*/
/* カレンダー                              */
/*=========================================*/
$year = substr($ym, 0, 4);
$month = substr($ym, 4, 2);

// 1日目の曜日
$wday = date("w", mktime(0, 0, 0, $month, 1, $year));

// 前月
#$prev_month = 'ym='.date("Ym", mktime(0, 0, 0, $month, 0, $year)).'&d=00';
$prev_month = query_edit("ym", date("Ym", mktime(0, 0, 0, $month, 0, $year)));
$prev_month = query_edit("seld_t","DELETE", $prev_month);
$prev_month = query_edit("seld_e","DELETE", $prev_month);
$prev_month = query_edit("d", "00", $prev_month);
// 次月
#$next_month = 'ym='.date("Ym", mktime(0, 0, 0, $month+1, 1, $year)).'&d=00';
$next_month = query_edit("ym", date("Ym", mktime(0, 0, 0, $month+1, 1, $year)));
$next_month = query_edit("seld_t","DELETE", $next_month);
$next_month = query_edit("seld_e","DELETE", $next_month);
$next_month = query_edit("d", "00", $next_month);

//月合計
$all_day = query_edit("d", "99");
$all_day = query_edit("seld_t","DELETE", $all_day);
$all_day = query_edit("seld_e","DELETE", $all_day);
//今日のログ
$today_log = query_edit("d", "DELETE");
$today_log = query_edit("ym", "DELETE", $today_log);
$today_log = query_edit("seld_t","DELETE", $today_log);
$today_log = query_edit("seld_e","DELETE", $today_log);


/*=========================================*/
/* カレンダー生成                          */
/*=========================================*/
$cal = "";
// Before Blank
for ($i=0; $i < $wday; $i++) { 
	$cal .= '<td nowrap class="cal_blank">&nbsp;</td>'."\n"; 
}

// Calendar Create
$day = 1;
while(checkdate($month,$day,$year)){
	$value = "";
	$q = "";
	
	if(isset($dataflgs[$day])){
		$q = query_edit("d",sprintf("%02d", $day));
		$q = query_edit("seld_t","DELETE", $q);
		$q = query_edit("seld_e","DELETE", $q);
		$value = '<a href="'.constant("FILENAME").$q.'" target="_top" class="cal_a">'.$day.'</a>';
	}else{
		$value = $day;
	}
	
	// Selected All
	if($d == 99){
		$cal .= '<td nowrap class="cal_sel">'.$value."</td>\n";
	
	// Selected Day
	}elseif($day == $d){ 
		$cal .= '<td nowrap class="cal_sel">'.$value."</td>\n";
	
	// Sunday
	}elseif($wday == 0){ 
		$cal .= '<td nowrap class="cal_sun">'.$value."</td>\n";
	
	// Saturday
	}elseif($wday == 6){ 
		$cal .= '<td nowrap class="cal_sat">'.$value."</td>\n";
	
	// Weekday
	}else{ 
		$cal .= '<td nowrap class="cal_def">'.$value."</td>\n";
	}
	
	// 改行
	if($wday == 6) $cal .= "</tr><tr align=center>";
	$day++;
	$wday++;
	$wday = $wday % 7;
}

// After Blank
if($wday > 0){
	while($wday < 7){
		$cal .= '<td nowrap class="cal_blank">&nbsp;</td>'."\n";
		$wday++;
	}
}else{
	$cal .= "<td nowrap colspan=7></td>\n";
}

$cal_img["next"] = set_img("image/cal_next.gif", "翌月");
$cal_img["prev"] = set_img("image/cal_prev.gif", "前月");
$cal_img["total"] = set_img("image/cal_total.gif", "月合計");
$cal_img["today"] = set_img("image/cal_today.gif", "今日のログ");

$fn = constant("FILENAME");

$w3a["CALENDAR"] = set_img($skin_dir."/image/calendar.gif","Calendar");
$w3a["CALENDAR"] .= <<<CAL
<div class="date">
<a href="{$fn}{$prev_month}" target="_top">{$cal_img["prev"]}</a>
&nbsp;&nbsp;{$year}年{$month}月&nbsp;&nbsp;
<a href="{$fn}{$next_month}" target="_top">{$cal_img["next"]}</a>
</div>
<table border="0" cellpadding="3" cellspacing="3">
<tr>
<!--
<th class="cal_sun"><img src="image/sun.gif" width="15" height="15" alt="日"></th>
<th class="cal_def"><img src="image/mon.gif" width="15" height="15" alt="月"></th>
<th class="cal_def"><img src="image/tue.gif" width="15" height="15" alt="火"></th>
<th class="cal_def"><img src="image/wed.gif" width="15" height="15" alt="水"></th>
<th class="cal_def"><img src="image/thu.gif" width="15" height="15" alt="木"></th>
<th class="cal_def"><img src="image/fri.gif" width="15" height="15" alt="金"></th>
<th class="cal_sat"><img src="image/sat.gif" width="15" height="15" alt="土"></th>
-->
<th class="cal_sun">日</th>
<th class="cal_def">月</th>
<th class="cal_def">火</th>
<th class="cal_def">水</th>
<th class="cal_def">木</th>
<th class="cal_def">金</th>
<th class="cal_sat">土</th>
</tr>
<tr align=center>
$cal
</tr>
</table>
<div class="option">
<a href="{$fn}{$all_day}" target="_top">{$cal_img["total"]}</a>
<a href="{$fn}{$today_log}" target="_top">{$cal_img["today"]}</a>
&nbsp;</div>
CAL;

//指定日表示
$w3a["CALENDAR"] .= '<form action="'.constant("FILENAME").'" method="GET" name="form">';
//Y
$w3a["CALENDAR"] .= '<select name="y">';
for ($i=date("Y"); $i > date("Y")-5; $i--) {
	$s = ($i == date("Y") ? " selected" : "");
	$w3a["CALENDAR"] .= '<option value="'.$i.'"'.$s.'>'.$i.'</option>'."\n";
}
$w3a["CALENDAR"] .= '</select>&nbsp;/&nbsp;';
//M
$w3a["CALENDAR"] .= '<select name="m">';
for ($i=1; $i <= 12; $i++) {
	$i = sprintf("%02d", $i);
	$s = ($i == date("m") ? " selected" : "");
	$w3a["CALENDAR"] .= '<option value="'.$i.'"'.$s.'>'.$i.'</option>'."\n";
}
$w3a["CALENDAR"] .= '</select>&nbsp;/&nbsp;';
//D
$w3a["CALENDAR"] .= '<select name="d">';
$w3a["CALENDAR"] .= '<option value="00" selected>--</option>'."\n";
for ($i=1; $i <= 31; $i++) {
	$i = sprintf("%02d", $i);
	$w3a["CALENDAR"] .= '<option value="'.$i.'">'.$i.'</option>'."\n";
}
$w3a["CALENDAR"] .= '</select>';
$w3a["CALENDAR"] .= '<br><br><input type="submit" value="指定日へ移動"></form>';

//解析期間の指定
$w3a["CALENDAR"] .= '<br>';
$w3a["CALENDAR"] .= set_img($skin_dir."/image/dateselect.gif","DateSelect");
$w3a["CALENDAR"] .= '<form action="'.constant("FILENAME").'" method="GET" name="form">';
//Y
$w3a["CALENDAR"] .= '<select name="seld_top_y">';
for ($i=date("Y"); $i > date("Y")-5; $i--) {
	$s = ($i == date("Y") ? " selected" : "");
	$w3a["CALENDAR"] .= '<option value="'.$i.'"'.$s.'>'.$i.'</option>'."\n";
}
$w3a["CALENDAR"] .= '</select>&nbsp;/&nbsp;';
//M
$w3a["CALENDAR"] .= '<select name="seld_top_m">';
for ($i=1; $i <= 12; $i++) {
	$i = sprintf("%02d", $i);
	$s = ($i == date("m") ? " selected" : "");
	$w3a["CALENDAR"] .= '<option value="'.$i.'"'.$s.'>'.$i.'</option>'."\n";
}
$w3a["CALENDAR"] .= '</select>&nbsp;/&nbsp;';
//D
$w3a["CALENDAR"] .= '<select name="seld_top_d">';
$w3a["CALENDAR"] .= '<option value="01" selected>01</option>'."\n";
for ($i=2; $i <= 31; $i++) {
	$i = sprintf("%02d", $i);
	$w3a["CALENDAR"] .= '<option value="'.$i.'">'.$i.'</option>'."\n";
}
$w3a["CALENDAR"] .= '</select>';
$w3a["CALENDAR"] .= '<br>から<br>';
//Y
$w3a["CALENDAR"] .= '<select name="seld_end_y">';
for ($i=date("Y"); $i > date("Y")-5; $i--) {
	$s = ($i == date("Y") ? " selected" : "");
	$w3a["CALENDAR"] .= '<option value="'.$i.'"'.$s.'>'.$i.'</option>'."\n";
}
$w3a["CALENDAR"] .= '</select>&nbsp;/&nbsp;';
//M
$w3a["CALENDAR"] .= '<select name="seld_end_m">';
for ($i=1; $i <= 12; $i++) {
	$i = sprintf("%02d", $i);
	$s = ($i == date("m") ? " selected" : "");
	$w3a["CALENDAR"] .= '<option value="'.$i.'"'.$s.'>'.$i.'</option>'."\n";
}
$w3a["CALENDAR"] .= '</select>&nbsp;/&nbsp;';
//D
$w3a["CALENDAR"] .= '<select name="seld_end_d">';
$w3a["CALENDAR"] .= '<option value="01" selected>01</option>'."\n";
for ($i=2; $i <= 31; $i++) {
	$i = sprintf("%02d", $i);
	$w3a["CALENDAR"] .= '<option value="'.$i.'">'.$i.'</option>'."\n";
}
$w3a["CALENDAR"] .= '</select>';
$w3a["CALENDAR"] .= '<br><br><input type="submit" value="解析期間の指定"></form>';

?>
