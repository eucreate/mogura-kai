<?php
header("Content-type: application/x-javascript");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
/*=================================================*/
/* メイン処理                                      */
/*=================================================*/
//conf include
include_once(dirname(__FILE__).'/inc/config.php');
if(!defined("CONF_READ")) exit;
//Path
$path = $_SERVER["HTTP_REFERER"];
$path = urlencode($path);
//W_PATH
$w_path = constant("W_PATH");
//img_size
$img_size = (constant("COUNTER") && constant("COUNTER_TYPE") == 'img') ? "" : ' width="1" height="1" alt=""';

echo <<<JS
(function(){
var src = '{$w_path}';
var args = '?mode=img';
var ref;
try{
	ref = encodeURIComponent(parent.document.referrer);
}catch(e){
	ref = encodeURIComponent(document.referrer);
}
var path = document.URL ? encodeURIComponent(document.URL.split('#')[0]) : '{$path}';
var w, h;

JS;
/* Browser Size */
/*
if (self.innerHeight) {
	w = self.innerWidth;
	h = self.innerHeight;
} else if (document.documentElement && document.documentElement.clientHeight) {
	w = document.documentElement.clientWidth;
	h = document.documentElement.clientHeight;
} else if (document.body) {
	w = document.body.clientWidth;
	h = document.body.clientHeight;
}
w = Math.round((w / 10)) * 10;
h = Math.round((h / 10)) * 10;
*/
echo <<<JS
/* Monitor Size */
w = screen.width;
h = screen.height;
var title;
try{
	title = encodeURIComponent(parent.document.title);
}catch(e){
	title = encodeURIComponent(document.title);
}

args += '&amp;guid=ON';
args += '&amp;ref='+ref;
args += '&amp;path='+path;
args += '&amp;monitor='+w+'x'+h;
args += '&amp;title='+title;
args += '&amp;color='+(navigator.appName != "Netscape" ? screen.colorDepth : screen.pixelDepth);
args += '&amp;cookie='+(navigator.cookieEnabled ? 1 : 2);
args += '&amp;js=1';
document.write('<img src="' + src + args + '"{$img_size}>');
JS;
if(constant("COUNTER") && constant("COUNTER_TYPE") == 'txt'){
	echo "\ndocument.write('";
	include_once(dirname(__FILE__).'/counter.php');
	echo "');";
}
echo "\n})();";
?>
