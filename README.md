# mogura改

PHP＋MySQLで動作するアクセス解析です。フリーソフトとして公開します。

オーサカPHPで配布されていたMoguraの改造版です。

改造者（スライム）(https://raihon.com/webapp/mogura/)のEU-Createによる再改造版です。

# 免責事項
本スクリプトの使用によって生じた、いかなる損害についても、オリジナル作者（hi）、改造者（スライム）、再改造者（EU-Create）および同梱プラグイン作者は一切の責任を負いません。自己責任でご利用ください。