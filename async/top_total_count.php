<?php
include_once("../inc/async_common.php");

$total_count = array(
"total_pv" => 0,
"total_uniq" => 0,
);

$sql = array();
$sql["select"] = "LEFT(date, 10) as ymd,COUNT(*) as pv,COUNT(DISTINCT id) as uniq";
$sql["group"] = "ymd";

$res = $db->query(mk_sql($sql));
if(DB::isError($res)){
	die;
}else{
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$total_count["total_pv"] += $row["pv"];
		$total_count["total_uniq"] += $row["uniq"];
	}
	$res->free();
	echo urlencode(number_format($total_count["total_pv"])."\t".number_format($total_count["total_uniq"]));
}

$db->disconnect();
exit;
?>