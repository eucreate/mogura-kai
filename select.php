<?php
// 初期化
$err = "";
$where = "";

#-------------------------------------------------
include_once("inc/load_setting.php");
if($send_sel == ""){
	$err = 'Open Error !!';
}else{
	//実行ファイル名
	define ("FILENAME_SEL", 'select.php');

	if($act){
		//日付修正
		if($d == "00"){
			$sql = array();
			$sql["select"] = "DATE_FORMAT(date,'%d') as day";
			$sql["where"] = "date like '".mk_sql_date($ym)."%'";
			$sql["group"] = "day";
			$sql["op"] = "order by day desc";
			if(isset($sql_dl)){
				$sql["dl"] = true;
			}
			$sql["limit"] = "1";

			$res = $db->query(mk_sql($sql));
			check_err($res);

			$d = 1;
			$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
			if($d < $row["day"]) $d = $row["day"];
			$res->free();
		}

		define ("SELECT_LOG_FLG",TRUE);
		$plugin = constant("DIR_PLUGIN").$act.'.php';
		if(file_exists($plugin)){
			//プラグイン読み込み
			ob_start();
			include_once($plugin);
			ob_end_clean();
			if(!$where) $err = 'SQL Error';
		}else{
			$err = 'アクション［<b>'.$act."</b>］は実行できませんでした。";
		}
	}else{
		$err = 'アクションが指定されていません。';
	}
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Language" content="ja">
<meta name="robots" content="noindex,nofollow,noarchive">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Select Logs Viewer</title>
<link href="css_js/select_log.css" rel="stylesheet" type="text/css">
<script language="javascript" src="css_js/js01.js"></script>
</head>
<body onLoad="loading_end();">
<a name="top"></a>
<!--Loading-->
<DIV id="load_page" style="position:absolute; top:100px; width:95%; visibility:hidden; text-align:center; left: 0px;">
<img src="image/loading.gif" alt="データ読み込み中"  width="170" height="30" border="0">
</DIV>
<script type='text/javascript'>
<!--
loading_start();
-->
</script>
<!--Loading end-->
<?php
if($err){
	echo '<div id="error"><b>Error:</b>&nbsp;'.$err.'</div>';
}else{
	$sql = array();
	$sql["select"] = "COUNT(*)";
	//from set
	if(preg_match('/ua_id|ua_name|(^|[^h])os([^t]|$)|brow/', $where)){
		$sql["from"] = "(".constant("DB_TABLE_LOG")." left join ".constant("DB_TABLE_UA")
		." on ".constant("DB_TABLE_LOG").".ua = ".constant("DB_TABLE_UA").".ua_id)";
	}else{
		$sql["from"] = constant("DB_TABLE_LOG");
	}
	if(preg_match('/ch_id|ch_name/', $where)){
		$sql["from"] = "(".$sql["from"]." left join ".constant("DB_CH_ID")
		." on ".constant("DB_TABLE_LOG").".id = ".constant("DB_CH_ID").".ch_id)";
	}
	//where set
	$sql["where"] = $where;
	//DL
	if(isset($sql_dl)){
		$sql["dl"] = true;
	}
	//max_val
	$db->query("SET NAMES utf8");
	$res = $db->query(mk_sql($sql));
	check_err($res);
	$row = $res->fetchRow(DB_FETCHMODE_ORDERED);
	$max_val = $row[0];
	$res->free();
	
	$sql["select"] = "no";
	//order set
	$sql["op"] = "order by date";
	//move
	if($send_p !== "all"){
		$move_p = $send_p." , ".constant("LIMIT");
		$move = move_bt($send_p, $max_val);
	}
	if($move_p) $sql["limit"] = $move_p;
	$db->query("SET NAMES utf8");
	$res = $db->query(mk_sql($sql));
	check_err($res);
	if($res->numRows() > 0){
		$sql["where"] = "no IN(";
		while ($row = $res->fetchRow(DB_FETCHMODE_ORDERED)){
			$sql["where"] .= $row[0].",";
		}
		$sql["where"] = substr($sql["where"], 0, -1);
		$sql["where"] .= ")";
	}
	$res->free();
	
	unset($sql["limit"]);
	//select set
	$sql["select"] = (isset($select) ? $select : "*");
	//from set
	$sql["from"] = "(".constant("DB_TABLE_LOG")." left join ".constant("DB_TABLE_UA")
	." on ".constant("DB_TABLE_LOG").".ua = ".constant("DB_TABLE_UA").".ua_id)";
	$sql["from"] = "(".$sql["from"]." left join ".constant("DB_CH_ID")
	." on ".constant("DB_TABLE_LOG").".id = ".constant("DB_CH_ID").".ch_id)";
	$db->query("SET NAMES utf8");
	$res = $db->query(mk_sql($sql));
	check_err($res);
	
	echo 'Selected Object&nbsp;&gt;&nbsp;<b>'.str_replace('---', ' ', (strpos($send_sel,'G--') === 0 ? substr($send_sel,3) : $send_sel)).'</b><hr size="1">';
	//表示データ生成
	if($max_val){
		echo '<b>'.number_format($max_val).'</b>件のログが見つかりました。<br><br>';
		
		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
			//ログナンバー
			echo "LogNo.<b>".$row["no"]."</b>";
			
			echo '<table width="100%" cellpadding="0" cellspacing="0">';
			//date
			echo '<tr><th width="80">Date:</th><td>'.$row["date"].'</td></tr>';
			//ID
			$track = query_edit("act", "track");
			$track = query_edit("sel", $row["id"], $track);
			$row["id"] = '<a href="'.constant("FILENAME").$track.'" target="_parent">'.set_img(constant("ICON_TRACK"),"追跡").'</a>'
							."&nbsp;"
							.'<a href="'.constant("FILENAME").$track.'" target="_parent">'.($row["ch_name"] ? $row["ch_name"]."&nbsp;&nbsp;[&nbsp;ID:".$row["id"]."&nbsp;]" : $row["id"]).'</a>';
			echo '<tr><th width="80">ID:</th><td nowrap>'.$row["id"].'</td></tr>';
			//path
			if(trim($row["path"])){
				echo '<tr><th width="80">Path:</th><td>';
				echo '<a href="'.constant("FILENAME").query_edit("p_sel",$row["path"]).'" target="_parent">'.set_img(constant("ICON_SELPATH"),"ページ指定").'</a>&nbsp;';
				if($row["title"] != "") echo "[&nbsp;".$row["title"]."&nbsp;]<br>";
				echo '&nbsp;'.$row["path"].'</td></tr>';
			}
			//IP.host
			if(trim($row["ip"])) echo '<tr><th width="80">IP:</th><td>'.$row["ip"].'</td></tr>';
			if(trim($row["host"])) echo '<tr><th width="80">Host:</th><td>'.$row["host"].'</td></tr>';
			//Referer
			if(trim($row["ref"])){
				
				if($row["ref_q"]){
					$row["ref"] .= "?".$row["ref_q"];
				}
				$jump = urldecode($row["ref"]);
				$jump = @mb_convert_encoding($jump, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
				$row["ref"] = '<a href="./inc/redirect.php?redirect='.urlencode($row["ref"]).'" target="_blank">'.set_img(constant("ICON_JUMP"),"ジャンプ").'&nbsp;'.$jump.'</a>';
				echo '<tr><th width="80">Referer:</th><td>'.$row["ref"].'</td></tr>';
			}
			//UserAgent
			if(trim($row["ua_name"])) echo '<tr><th width="80">UserAgent:</th><td>'.$row["ua_name"].'</td></tr>';
			echo "</table>";
			echo "<br>\n";
		}
	}else{
		echo "Unknown Logs.\n";
	}
	$res->free();
	if($move) echo $move."<br>";
}
//DB接続終了
$db->disconnect();
?>
</body>
</html>
