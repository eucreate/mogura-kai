<?php
$os_list = array();
global $os_list;
/*==============================================================================*/
/*    ■ OSリスト                                                               */
/*                                                                              */
/*    $os_list['Type']['UserAgent'] = 'Ver';                                    */
/*    Type -> [win] or [mac] or [other]                                         */
/*                                                                              */
/*    Type -> [other]                                                           */
/*    $os_list['other']['UserAgent'] = array('OS' => 'Ver');                    */
/*                                                                              */
/*    OS NULL -> Unknown OS                                                     */
/*    Ver NULL -> N/A                                                           */
/*                                                                              */
/*==============================================================================*/
# Windows
$os_list['win']['NT 10.'] = '10';
$os_list['win']['NT 6.3'] = '8.1';
$os_list['win']['NT 6.2'] = '8';
$os_list['win']['NT 6.1'] = '7';
$os_list['win']['NT 6.0'] = 'Vista';
$os_list['win']['NT 5.2'] = 'XP';
$os_list['win']['NT 5.1'] = 'XP';
$os_list['win']['NT 5.0'] = '2000';
$os_list['win']['Phone OS 10'] = 'Mobile 10.x';
$os_list['win']['Phone 10'] = 'Mobile 10.x';
$os_list['win']['Phone OS 8'] = 'Phone 8.x';
$os_list['win']['Phone 8'] = 'Phone 8.x';
$os_list['win']['Phone OS 7'] = 'Phone 7.x';
$os_list['win']['Phone 7'] = 'Phone 7.x';
$os_list['win']['Phone 6'] = 'Phone 6.x';
$os_list['win']['Phone'] = 'Phone';
$os_list['win']['Windows CE'] = 'CE';
$os_list['win']['WS 2003'] = 'XP';
$os_list['win']['XP'] = 'XP';
$os_list['win']['9x 4.90'] = 'Me';
$os_list['win']['ME'] = 'Me';
$os_list['win']['WS 2000'] = '2000';
$os_list['win']['Vista'] = 'Vista';
$os_list['win']['98'] = '98';
$os_list['win']['95'] = '95';
$os_list['win']['3.1'] = '3.1';
$os_list['win']['32'] = '32';
$os_list['win']['67'] = '67';
$os_list['win']['NT'] = 'NT';

# Mac
$os_list['mac']['Mac OS X 10_17'] = '10.17';
$os_list['mac']['Mac OS X 10.17'] = '10.17';
$os_list['mac']['Mac OS X 10_16'] = '10.16';
$os_list['mac']['Mac OS X 10.16'] = '10.16';
$os_list['mac']['Mac OS X 10_15'] = '10.15';
$os_list['mac']['Mac OS X 10.15'] = '10.15';
$os_list['mac']['Mac OS X 10_14'] = '10.14';
$os_list['mac']['Mac OS X 10.14'] = '10.14';
$os_list['mac']['Mac OS X 10_13'] = '10.13';
$os_list['mac']['Mac OS X 10.13'] = '10.13';
$os_list['mac']['Mac OS X 10_12'] = '10.12 Sierra';
$os_list['mac']['Mac OS X 10.12'] = '10.12 Sierra';
$os_list['mac']['Mac OS X 10_11'] = '10.11 El Capitan';
$os_list['mac']['Mac OS X 10.11'] = '10.11 El Capitan';
$os_list['mac']['Mac OS X 10_10'] = '10.10 Yosemite';
$os_list['mac']['Mac OS X 10.10'] = '10.10 Yosemite';
$os_list['mac']['Mac OS X 10_9'] = '10.9 Mavericks';
$os_list['mac']['Mac OS X 10.9'] = '10.9 Mavericks';
$os_list['mac']['Mac OS X 10_8'] = '10.8 Mountain Lion';
$os_list['mac']['Mac OS X 10.8'] = '10.8 Mountain Lion';
$os_list['mac']['Mac OS X 10_7'] = '10.7 Lion';
$os_list['mac']['Mac OS X 10.7'] = '10.7 Lion';
$os_list['mac']['Mac OS X 10_6'] = '10.6 SnowLeopard';
$os_list['mac']['Mac OS X 10.6'] = '10.6 SnowLeopard';
$os_list['mac']['Mac OS X 10_5'] = '10.5 Leopard';
$os_list['mac']['Mac OS X 10.5'] = '10.5 Leopard';
$os_list['mac']['Mac OS X 10_4'] = '10.4 Tiger';
$os_list['mac']['Mac OS X 10.4'] = '10.4 Tiger';
$os_list['mac']['Mac OS X 10_3'] = '10.3 Panther';
$os_list['mac']['Mac OS X 10.3'] = '10.3 Panther';
$os_list['mac']['Mac OS X'] = '';

#Bot Crawler Emu
$os_list['other']['ISIM'] = array('DoCoMo' => 'iモードエミュレータ');
$os_list['other']['OPWV'] = array('au' => 'OpenWave SDK');
$os_list['other']['emu.mobile.goo.ne.jp'] = array('goo' => 'モバイルエミュレータ');

#Android
$os_list['other']['Android 12.3'] = array('Android' => '12.3');
$os_list['other']['Android 12.2'] = array('Android' => '12.2');
$os_list['other']['Android 12.1'] = array('Android' => '12.1');
$os_list['other']['Android 12.0'] = array('Android' => '12.0');
$os_list['other']['Android 12.'] = array('Android' => '12.x');
$os_list['other']['Android 11.3'] = array('Android' => '11.3');
$os_list['other']['Android 11.2'] = array('Android' => '11.2');
$os_list['other']['Android 11.1'] = array('Android' => '11.1');
$os_list['other']['Android 11.0'] = array('Android' => '11.0');
$os_list['other']['Android 11.'] = array('Android' => '11.x');
$os_list['other']['Android 10.3'] = array('Android' => '10.3');
$os_list['other']['Android 10.2'] = array('Android' => '10.2');
$os_list['other']['Android 10.1'] = array('Android' => '10.1');
$os_list['other']['Android 10.0'] = array('Android' => '10.0');
$os_list['other']['Android 10.'] = array('Android' => '10.x');
$os_list['other']['Android 9.3'] = array('Android' => '9.3');
$os_list['other']['Android 9.2'] = array('Android' => '9.2');
$os_list['other']['Android 9.1'] = array('Android' => '9.1');
$os_list['other']['Android 9.0'] = array('Android' => '9.0');
$os_list['other']['Android 9.'] = array('Android' => '9.x');
$os_list['other']['Android 8.3'] = array('Android' => '8.3');
$os_list['other']['Android 8.2'] = array('Android' => '8.2');
$os_list['other']['Android 8.1'] = array('Android' => '8.1');
$os_list['other']['Android 8.0'] = array('Android' => '8.0');
$os_list['other']['Android 8.'] = array('Android' => '8.x');
$os_list['other']['Android 7.3'] = array('Android' => '7.3');
$os_list['other']['Android 7.2'] = array('Android' => '7.2');
$os_list['other']['Android 7.1'] = array('Android' => '7.1');
$os_list['other']['Android 7.0'] = array('Android' => '7.0');
$os_list['other']['Android 7.'] = array('Android' => '7.x');
$os_list['other']['Android 6.3'] = array('Android' => '6.3');
$os_list['other']['Android 6.2'] = array('Android' => '6.2');
$os_list['other']['Android 6.1'] = array('Android' => '6.1');
$os_list['other']['Android 6.0'] = array('Android' => '6.0');
$os_list['other']['Android 6.'] = array('Android' => '6.x');
$os_list['other']['Android 5.1'] = array('Android' => '5.1');
$os_list['other']['Android 5.0'] = array('Android' => '5.0');
$os_list['other']['Android 5.'] = array('Android' => '5.x');
$os_list['other']['Android 4.4'] = array('Android' => '4.4');
$os_list['other']['Android 4.3'] = array('Android' => '4.3');
$os_list['other']['Android 4.2'] = array('Android' => '4.2');
$os_list['other']['Android 4.1'] = array('Android' => '4.1');
$os_list['other']['Android 4.0'] = array('Android' => '4.0');
$os_list['other']['Android 4.'] = array('Android' => '4.x');
$os_list['other']['Android 3.2'] = array('Android' => '3.2');
$os_list['other']['Android 3.1'] = array('Android' => '3.1');
$os_list['other']['Android 3.0'] = array('Android' => '3.0');
$os_list['other']['Android 3.'] = array('Android' => '3.x');
$os_list['other']['Android 2.3'] = array('Android' => '2.3');
$os_list['other']['Android 2.2'] = array('Android' => '2.2');
$os_list['other']['Android 2.1'] = array('Android' => '2.1');
$os_list['other']['Android 2.0'] = array('Android' => '2.0');
$os_list['other']['Android 2.'] = array('Android' => '2.x');
$os_list['other']['Android 1.6'] = array('Android' => '1.6');
$os_list['other']['Android 1.5'] = array('Android' => '1.5');
$os_list['other']['Android 1.0'] = array('Android' => '1.0');
$os_list['other']['Android 1.'] = array('Android' => '1.x');
$os_list['other']['Android'] = array('Android' => '');

#Chromium OS
$os_list['other']['(X11; U; CrOS i686'] = array('Chromium OS' => '');

#iPad iPod iPhone
$os_list['other']['iPad; CPU OS 15'] = array('iPad' => '15.x');
$os_list['other']['iPad; U; CPU OS 15'] = array('iPad' => '15.x');
$os_list['other']['iPad; CPU OS 14'] = array('iPad' => '14.x');
$os_list['other']['iPad; U; CPU OS 14'] = array('iPad' => '14.x');
$os_list['other']['iPad; CPU OS 13'] = array('iPad' => '13.x');
$os_list['other']['iPad; U; CPU OS 13'] = array('iPad' => '13.x');
$os_list['other']['iPad; CPU OS 12'] = array('iPad' => '12.x');
$os_list['other']['iPad; U; CPU OS 12'] = array('iPad' => '12.x');
$os_list['other']['iPad; CPU OS 11'] = array('iPad' => '11.x');
$os_list['other']['iPad; U; CPU OS 11'] = array('iPad' => '11.x');
$os_list['other']['iPad; CPU OS 10'] = array('iPad' => '10.x');
$os_list['other']['iPad; U; CPU OS 10'] = array('iPad' => '10.x');
$os_list['other']['iPad; CPU OS 9'] = array('iPad' => '9.x');
$os_list['other']['iPad; U; CPU OS 9'] = array('iPad' => '9.x');
$os_list['other']['iPad; CPU OS 8'] = array('iPad' => '8.x');
$os_list['other']['iPad; U; CPU OS 8'] = array('iPad' => '8.x');
$os_list['other']['iPad; CPU OS 7'] = array('iPad' => '7.x');
$os_list['other']['iPad; U; CPU OS 7'] = array('iPad' => '7.x');
$os_list['other']['iPad; CPU OS 6'] = array('iPad' => '6.x');
$os_list['other']['iPad; U; CPU OS 6'] = array('iPad' => '6.x');
$os_list['other']['iPad; CPU OS 5'] = array('iPad' => '5.x');
$os_list['other']['iPad; U; CPU OS 5'] = array('iPad' => '5.x');
$os_list['other']['iPad; U; CPU OS 4'] = array('iPad' => '4.x');
$os_list['other']['iPad; U; CPU OS 3'] = array('iPad' => '3.x');
$os_list['other']['iPad;'] = array('iPad' => '');

$os_list['other']['iPod touch; CPU iPhone OS 15'] = array('iPod touch' => '15.x');
$os_list['other']['iPod touch; U; CPU iPhone OS 15'] = array('iPod touch' => '15.x');
$os_list['other']['iPod; CPU iPhone OS 15'] = array('iPod touch' => '15.x');
$os_list['other']['iPod; U; CPU iPhone OS 15'] = array('iPod touch' => '15.x');
$os_list['other']['iPod touch; CPU iPhone OS 14'] = array('iPod touch' => '14.x');
$os_list['other']['iPod touch; U; CPU iPhone OS 14'] = array('iPod touch' => '14.x');
$os_list['other']['iPod; CPU iPhone OS 14'] = array('iPod touch' => '14.x');
$os_list['other']['iPod; U; CPU iPhone OS 14'] = array('iPod touch' => '14.x');
$os_list['other']['iPod touch; CPU iPhone OS 13'] = array('iPod touch' => '13.x');
$os_list['other']['iPod touch; U; CPU iPhone OS 13'] = array('iPod touch' => '13.x');
$os_list['other']['iPod; CPU iPhone OS 13'] = array('iPod touch' => '13.x');
$os_list['other']['iPod; U; CPU iPhone OS 13'] = array('iPod touch' => '13.x');
$os_list['other']['iPod touch; CPU iPhone OS 12'] = array('iPod touch' => '12.x');
$os_list['other']['iPod touch; U; CPU iPhone OS 12'] = array('iPod touch' => '12.x');
$os_list['other']['iPod; CPU iPhone OS 12'] = array('iPod touch' => '12.x');
$os_list['other']['iPod; U; CPU iPhone OS 12'] = array('iPod touch' => '12.x');
$os_list['other']['iPod touch; CPU iPhone OS 11'] = array('iPod touch' => '11.x');
$os_list['other']['iPod touch; U; CPU iPhone OS 11'] = array('iPod touch' => '11.x');
$os_list['other']['iPod; CPU iPhone OS 11'] = array('iPod touch' => '11.x');
$os_list['other']['iPod; U; CPU iPhone OS 11'] = array('iPod touch' => '11.x');
$os_list['other']['iPod touch; CPU iPhone OS 10'] = array('iPod touch' => '10.x');
$os_list['other']['iPod touch; U; CPU iPhone OS 10'] = array('iPod touch' => '10.x');
$os_list['other']['iPod; CPU iPhone OS 10'] = array('iPod touch' => '10.x');
$os_list['other']['iPod; U; CPU iPhone OS 10'] = array('iPod touch' => '10.x');
$os_list['other']['iPod touch; CPU iPhone OS 9'] = array('iPod touch' => '9.x');
$os_list['other']['iPod touch; U; CPU iPhone OS 9'] = array('iPod touch' => '9.x');
$os_list['other']['iPod; CPU iPhone OS 9'] = array('iPod touch' => '9.x');
$os_list['other']['iPod; U; CPU iPhone OS 9'] = array('iPod touch' => '9.x');
$os_list['other']['iPod touch; CPU iPhone OS 8'] = array('iPod touch' => '8.x');
$os_list['other']['iPod touch; U; CPU iPhone OS 8'] = array('iPod touch' => '8.x');
$os_list['other']['iPod; CPU iPhone OS 8'] = array('iPod touch' => '8.x');
$os_list['other']['iPod; U; CPU iPhone OS 8'] = array('iPod touch' => '8.x');
$os_list['other']['iPod touch; CPU iPhone OS 7'] = array('iPod touch' => '7.x');
$os_list['other']['iPod touch; U; CPU iPhone OS 7'] = array('iPod touch' => '7.x');
$os_list['other']['iPod; CPU iPhone OS 7'] = array('iPod touch' => '7.x');
$os_list['other']['iPod; U; CPU iPhone OS 7'] = array('iPod touch' => '7.x');
$os_list['other']['iPod; CPU iPhone OS 6'] = array('iPod touch' => '6.x');
$os_list['other']['iPod; U; CPU iPhone OS 6'] = array('iPod touch' => '6.x');
$os_list['other']['iPod; CPU iPhone OS 5'] = array('iPod touch' => '5.x');
$os_list['other']['iPod; U; CPU iPhone OS 5'] = array('iPod touch' => '5.x');
$os_list['other']['iPod; U; CPU iPhone OS 4'] = array('iPod touch' => '4.x');
$os_list['other']['iPod; U; CPU iPhone OS 3'] = array('iPod touch' => '3.x');
$os_list['other']['iPod; U; CPU iPhone OS 2'] = array('iPod touch' => '2.x');
$os_list['other']['iPod; U; CPU like Mac OS X;'] = array('iPod touch' => '1.x');
$os_list['other']['iPod;'] = array('iPod touch' => '');

$os_list['other']['iPhone; CPU iPhone OS 15'] = array('iPhone' => '15.x');
$os_list['other']['iPhone; U; CPU iPhone OS 15'] = array('iPhone' => '15.x');
$os_list['other']['iPhone; CPU iPhone OS 14'] = array('iPhone' => '14.x');
$os_list['other']['iPhone; U; CPU iPhone OS 14'] = array('iPhone' => '14.x');
$os_list['other']['iPhone; CPU iPhone OS 13'] = array('iPhone' => '13.x');
$os_list['other']['iPhone; U; CPU iPhone OS 13'] = array('iPhone' => '13.x');
$os_list['other']['iPhone; CPU iPhone OS 12'] = array('iPhone' => '12.x');
$os_list['other']['iPhone; U; CPU iPhone OS 12'] = array('iPhone' => '12.x');
$os_list['other']['iPhone; CPU iPhone OS 11'] = array('iPhone' => '11.x');
$os_list['other']['iPhone; U; CPU iPhone OS 11'] = array('iPhone' => '11.x');
$os_list['other']['iPhone; CPU iPhone OS 10'] = array('iPhone' => '10.x');
$os_list['other']['iPhone; U; CPU iPhone OS 10'] = array('iPhone' => '10.x');
$os_list['other']['iPhone; CPU iPhone OS 9'] = array('iPhone' => '9.x');
$os_list['other']['iPhone; U; CPU iPhone OS 9'] = array('iPhone' => '9.x');
$os_list['other']['iPhone; CPU iPhone OS 8'] = array('iPhone' => '8.x');
$os_list['other']['iPhone; U; CPU iPhone OS 8'] = array('iPhone' => '8.x');
$os_list['other']['iPhone; CPU iPhone OS 7'] = array('iPhone' => '7.x');
$os_list['other']['iPhone; U; CPU iPhone OS 7'] = array('iPhone' => '7.x');
$os_list['other']['iPhone; CPU iPhone OS 6'] = array('iPhone' => '6.x');
$os_list['other']['iPhone; U; CPU iPhone OS 6'] = array('iPhone' => '6.x');
$os_list['other']['iPhone; CPU iPhone OS 5'] = array('iPhone' => '5.x');
$os_list['other']['iPhone; U; CPU iPhone OS 5'] = array('iPhone' => '5.x');
$os_list['other']['iPhone; U; CPU iPhone OS 4'] = array('iPhone' => '4.x');
$os_list['other']['iPhone; U; CPU iPhone OS 3'] = array('iPhone' => '3.x');
$os_list['other']['iPhone; U; CPU iPhone OS 2'] = array('iPhone' => '2.x');
$os_list['other']['iPhone; U; CPU like Mac OS X'] = array('iPhone' => '1.x');
$os_list['other']['iPhone'] = array('iPhone' => '');

#Chrome OS
$os_list['other']['CrOS'] = array('Chrome OS' => '');

#BlackBerry
$os_list['other']['BlackBerry'] = array('BlackBerry' => '');

# OtherOS
$os_list['other']['Mac_PowerPC'] = array('Macintosh' => 'PowerPC');
$os_list['other']['Darwin/'] = array('Darwin' => '');
$os_list['other']['68K'] = array('Macintosh' => '68K');
$os_list['other']['Macintosh'] = array('Macintosh' => '');
$os_list['other']['FreeBSD']  = array('FreeBSD' => '');
$os_list['other']['OpenBSD'] = array('OpenBSD' => '');
$os_list['other']['SUSE/'] = array('Linux' => 'SUSE');
$os_list['other']['Ubuntu'] = array('Linux' => 'Ubuntu');
$os_list['other']['CentOS/'] = array('Linux' => 'CentOS');
$os_list['other']['Debian-'] = array('Linux' => 'Debian');
$os_list['other']['Vine/7'] = array('Vine Linux' => '7.x');
$os_list['other']['Vine/6'] = array('Vine Linux' => '6.x');
$os_list['other']['Vine/5'] = array('Vine Linux' => '5.x');
$os_list['other']['Vine/4'] = array('Vine Linux' => '4.x');
$os_list['other']['Vine/3'] = array('Vine Linux' => '3.x');
$os_list['other']['Vine/2'] = array('Vine Linux' => '2.x');
$os_list['other']['Vine/'] = array('Vine Linux' => '');
$os_list['other']['Fedora/'] = array('Linux' => 'Fedora');
$os_list['other']['Linux'] = array('Linux' => '');
$os_list['other']['IRIX'] = array('IRIX' => '');
$os_list['other']['SunOS'] = array('SunOS' => '');
$os_list['other']['OS/2'] = array('OS/2' => '');
$os_list['other']['WebTV'] = array('WebTV' => '');
$os_list['other']['DreamPassport'] = array('DreamCast' => '');
$os_list['other']['Nintendo 3DS'] = array('Nintendo' => '3DS');
$os_list['other']['Nintendo WiiU'] = array('Nintendo' => 'WiiU');
$os_list['other']['Nintendo Wii'] = array('Nintendo' => 'Wii');
$os_list['other']['Nintendo DSi'] = array('Nintendo' => 'DSi');
$os_list['other']['Nitro)'] = array('Nintendo' => 'DS');
$os_list['other']['PlayStation Vita'] = array('PS Vita' => '');
$os_list['other']['PLAYSTATION 4'] = array('PLAYSTATION' => '4');
$os_list['other']['PLAYSTATION 3'] = array('PLAYSTATION' => '3');
$os_list['other']['PSP (PlayStation Portable); 2'] = array('PSP' => '2');
$os_list['other']['SymbianOS'] = array('SymbianOS' => '');

// Proxy専用機または一般的なProxy
$os_list['other']['Froute Mobile Gateway'] = array('Froute Mobile Gateway' => '');
?>