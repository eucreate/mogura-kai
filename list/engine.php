<?php
$engine_list = array();
global $engine_list;
/*==============================================================================*/
/*    ■ 検索エンジンリスト                                                     */
/*                                                                              */
/*    $engine_list['URI'] = array('Name', 'QueryKey', 'Encode');                */
/*    Encode -> 検索エンジンの文字コードを指定                                  */
/*              無い場合は[auto]                                                */
/*                                                                              */
/*==============================================================================*/
//広く
$engine_list['.google.'] = array('Google', 'q', '');
$engine_list['wrs.search.yahoo.co.jp'] = array('Yahoo!', 'K', '');
$engine_list['search.yahoo-labs.'] = array('Yahoo!labs', 'p', '');
$engine_list['.yahoo.'] = array('Yahoo!', 'p', '');
$engine_list['.bing.com'] = array('bing', 'q', '');
$engine_list['search.msn.'] = array('MSN', 'q', '');
$engine_list['.goo.ne.jp'] = array('goo', 'MT', '');
$engine_list['.nifty.com'] = array('@nifty@search', 'q', '');
$engine_list['.nifty.com/'] = array('@nifty@search', 'Text', '');
$engine_list['.infoseek.co.jp'] = array('Infoseek', 'qt', '');
$engine_list['.excite.co.jp'] = array('Excite', 'search', '');
$engine_list['biglobe.ne.jp'] = array('Biglobe', 'q', '');
$engine_list['.baidu.'] = array('Baidu', 'wd', '');
$engine_list['livedoor.com'] = array('Livedoor', 'q', '');
$engine_list['.lycos.'] = array('Lycos', 'query', '');
$engine_list['so-net.ne.jp'] = array('So-net', 'query', '');
$engine_list['.jword.jp'] = array('JWord', 'q', '');

//Mobile
$engine_list['docomo.ne.jp'] = array('DoCoMo', 'key', '');
$engine_list['ezsch.ezweb.ne.jp/'] = array('EZweb', 'query', '');
$engine_list['mbkn.jp/'] = array('モバゲータウン', 'q', '');
$engine_list['kbg.jp/'] = array('ケータイBIGLOBE', 'extrakey', '');
$engine_list['mobile.excite.co.jp'] = array('Exciteモバイル', 'extrakey', '');
$engine_list['froute.jp'] = array('froute', 'k', '');
$engine_list['yicha.jp'] = array('yicha', 'keyword', '');
$engine_list['crooz.jp'] = array('crooz', 'query', '');
$engine_list['yappo.jp'] = array('yappo', 'k', '');
$engine_list['ohnew.co.jp'] = array('OH!NEW?', 'k', '');
$engine_list['puupuu.net'] = array('ぷうぷ検索', 'k', '');

//PC
$engine_list['auone.jp'] = array('AU Oneサーチ', 'q', '');
$engine_list['.rakuten.co.jp'] = array('楽天', 'qt', '');
$engine_list['search.fresheye.com'] = array('Fresheye', 'kw', '');
$engine_list['bach.istc.kobe-u.ac.jp'] = array('Metcha Search', 'q', '');
$engine_list['allabout.co.jp'] = array('AllAbout', 'qs', '');
$engine_list['joyjoy.com'] = array('J.O.Y.', 'key', '');
$engine_list['search.hatena.ne.jp'] = array('Hatena::Search', 'word', '');
$engine_list['b.hatena.ne.jp'] = array('HatenaBookmark', 'q', '');
$engine_list['zapanet.info/tundere'] = array('ツンデレサーチ(zapanet)', 'q', '');
$engine_list['sagool.jp'] = array('SAGOOL', 'q', '');
$engine_list['search.lifemile.jp'] = array('ライフマイル', 'Keywords', '');
$engine_list['eal-8.com'] = array('えある', 'Keywords', '');
$engine_list['www.ceek.jp'] = array('Ceek', 'q', '');
$engine_list['www.mooter.co.jp'] = array('Mooter', 'keywords', '');
$engine_list['.luna.tv'] = array('Lunascape', 'q', '');
$engine_list['.lunascape.jp/'] = array('Lunascape', 'p', '');
$engine_list['kynny.com'] = array('kynny', 'q', '');
$engine_list['icecoffee.hobby-site.com/index.php'] = array('リフレッシュ Finder', 'q', '');
$engine_list['search.osaifu.com/'] = array('お財布サーチ', 'w', '');
$engine_list['search.point-museum.com/'] = array('ポイントミュージアム', 'Keywords', '');
$engine_list['search.point-ex.jp/'] = array('PointExchange', 'Keywords', '');
$engine_list['218.224.236.119/sh/'] = array('impluse', 'KEYVALUE', '');
$engine_list['search.naver.jp'] = array('NAVER', 'q', '');
$engine_list['clusty.jp'] = array('clusty', 'query', ''); //但し最初のページのみ
$engine_list['.aol.jp'] = array('AOL', 'query', '');
$engine_list['.myaol.jp'] = array('AOL', 'query', '');
$engine_list['jp.aol.com'] = array('AOL', 'query', '');
$engine_list['kotochu.fresheye.com'] = array('コトバウチュウ', 'kw', '');
$engine_list['.marsflag.com'] = array('MARSFLAG', 'phrase', '');
$engine_list['.blogpeople.net'] = array('BlogPeople', 'keyword', '');
$engine_list['.namaan.net'] = array('NAMAAN', 'query', '');
$engine_list['pex.jp'] = array('PeX', 'Keywords', '');
$engine_list['ss.adingo.jp'] = array('adingo', 'Keywords', '');
$engine_list['www.cybozu.net'] = array('cybozu.net', 'Keywords', '');
$engine_list['research-panel.jp'] = array('ResearchPanel', 'Keywords', '');
$engine_list['web-search.ameba.jp'] = array('Ameba', 'q', '');
$engine_list['search.tameneko.com'] = array('貯めネコに小判', 'Keywords', '');
$engine_list['ser.cocacola.co.jp'] = array('コカ・コーラパーク', 'Keywords', '');
$engine_list['.unisearch.jp'] = array('UNITCOM', 'keyword', '');
$engine_list['.babylon.com'] = array('babylon', 'q', '');
$engine_list['.went.jp'] = array('直子の代筆', 'Keywords', '');
$engine_list['.starthome.jp'] = array('StartHome', 'keywords', '');
$engine_list['.gpoint.jp'] = array('Ｇポイント', 'keywords', '');
$engine_list['.ecnavi.jp'] = array('ＥＣナビ', 'Keywords', '');
$engine_list['.realworld.jp'] = array('リアルワールド', 'keywords', '');
$engine_list['aqfr.net'] = array('永久不滅リサーチ', 'Keywords', '');
$engine_list['yosoo.net'] = array('予想ネット', 'Keywords', '');
$engine_list['gendama.jp'] = array('Gendama', 'keywords', '');
$engine_list['warau.jp'] = array('Warau.JP', 'search_word', '');
$engine_list['.chance.com'] = array('チャンスイット', 'q', '');
$engine_list['.jlisting.jp'] = array('JLISTING', 'q', '');

//海外
$engine_list['alexa.com'] = array('Alexa', 'q', '');
$engine_list['search.aol.com'] = array('AOL', 'q', '');
$engine_list['.naver.com'] = array('NAVER', 'query', '');
$engine_list['hotbot.com'] = array('HOTBOT', 'query', '');
$engine_list['altavista.com'] = array('AltaVista', 'q', '');
$engine_list['alltheweb.com'] = array('AlltheWeb', 'q', '');
$engine_list['ask.com'] = array('ask.com', 'q', '');
$engine_list['netscape.com'] = array('Netscape Search', 'query', '');
$engine_list['euroseek.com'] = array('euroseek', 'string', '');
$engine_list['baypup.com'] = array('Baypup', 'query', '');
$engine_list['.dmoz.org/'] = array('dmoz', 'search', '');
$engine_list['.ebig.com.au/'] = array('eBig', 'q', '');

$engine_list['furiousfamily.com'] = array('BUSINESS SUPPORT', 'q', '');
$engine_list['eyeballme.com'] = array('DATA RECOVERY', 'q', '');
$engine_list['.halfmoon.jp/'] = array('halfmoon', 'search', '');
$engine_list['erocg.info/'] = array('halfmoon', 'word', '');
$engine_list['netatama.net/'] = array('netatama', 'q', '');
$engine_list['.burn4free-toolbar.com/'] = array('burn4free', 'w', '');
$engine_list['search.sweetim.com/'] = array('sweetim', 'q', '');

//存在しない
$engine_list['search.excite.com'] = array('Excite.com', 'search', '');
$engine_list['asearch.cab.infoweb.ne.jp'] = array('@Search', 'q', '');
$engine_list['infoseek.com'] = array('InfoSeek', 'qt', '');
$engine_list['search.live.com'] = array('Live', 'q', '');
$engine_list['kensaku.org'] = array('kensaku.org', 'key', '');
$engine_list['www.fastsearch.com'] = array('Fast Search', 'q', '');
$engine_list['www.go.com'] = array('GO.com', 'qt', '');
$engine_list['overture.com'] = array('overture', 'Keywords', '');
$engine_list['looksmart.co.jp'] = array('looksmart', 'QueryString', '');
$engine_list['looksmart.com'] = array('looksmart', 'qt', '');
$engine_list['ask.jp'] = array('ask.jp', 'q', '');
$engine_list['csj.co.jp'] = array('CSJ', 'query', '');
$engine_list['bulkfeeds.net'] = array('Bulkfeeds', 'q', '');
$engine_list['cocolog-nifty.com'] = array('ココログ', 'textfield', '');
?>
