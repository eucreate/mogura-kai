<?php
$isp_list = array();
global $isp_list;
/*==============================================================================*/
/*    ■ ISPリスト                                                              */
/*                                                                              */
/*    $isp_list['Domain'] = 'ISP';                                              */
/*                                                                              */
/*==============================================================================*/

//OCN--------------------------------------------------------------------------
$isp_list['%.ocn.ne.jp'] = 'NTTコミュニケーションズ';

//YahooBB-------------------------------------------------------------
$isp_list['%.bbtec.net'] = 'ヤフー';

//biglobe----------------------------------------------------------------------
$isp_list['%.mesh.ad.jp'] = 'ＮＥＣビッグローブ';

//InfoWeb----------------------------------------------------------------------
$isp_list['%.infoweb.ne.jp'] = 'ニフティ';

//so-net.ne.jp--------------------------------------------------------------------
$isp_list['%.so-net.co.jp'] = 'ソネットエンタテインメント';
$isp_list['%.so-net.ne.jp'] = 'ソネットエンタテインメント';

//dti.ne.jp--------------------------------------------------------------------
$isp_list['%.dti.ne.jp'] = 'ドリーム・トレイン・インターネット';

//hi-ho------------------------------------------------------------------------
$isp_list['%.hi-ho.ne.jp'] = 'ハイホー';

//plala------------------------------------------------------------------------
$isp_list['%.plala.or.jp'] = 'ＮＴＴぷらら';

//nttpc------------------------------------------------------------------------
$isp_list['%.nttpc.co.jp'] = 'ＮＴＴＰＣコミュニケーションズ';

//携帯各社---------------------------------------------------------------------
$isp_list['%.docomo.ne.jp'] = 'エヌ・ティ・ティ・ドコモ';
$isp_list['%.mopera.net'] = 'エヌ・ティ・ティ・ドコモ';

$isp_list['%.ezweb.ne.jp'] = 'KDDI';
$isp_list['%.au-net.ne.jp'] = 'KDDI';

$isp_list['%.jp-c.ne.jp'] = 'ソフトバンク';
$isp_list['%.jp-d.ne.jp'] = 'ソフトバンク';
$isp_list['%.jp-h.ne.jp'] = 'ソフトバンク';
$isp_list['%.jp-k.ne.jp'] = 'ソフトバンク';
$isp_list['%.jp-n.ne.jp'] = 'ソフトバンク';
$isp_list['%.jp-q.ne.jp'] = 'ソフトバンク';
$isp_list['%.jp-r.ne.jp'] = 'ソフトバンク';
$isp_list['%.jp-s.ne.jp'] = 'ソフトバンク';
$isp_list['%.jp-t.ne.jp'] = 'ソフトバンク';

$isp_list['%.access-internet.ne.jp'] = 'SoftBankアクセスポイント';
$isp_list['%.openmobile.ne.jp'] = 'SoftBankアクセスポイント';
$isp_list['%.panda-world.ne.jp'] = 'SoftBankアクセスポイント';
$isp_list['%.pcsitebrowser.ne.jp'] = 'SoftBankアクセスポイント';

$isp_list['%.livedoor.net'] = 'livedorr wireless';
$isp_list['%.prin.ne.jp'] = 'ウィルコム';
$isp_list['%.uqwimax.jp'] = 'UQコミュニケーションズ';
//
$isp_list['%.e-mobile.ne.jp'] = 'イー・モバイル';

//IP
$isp_list['210.236.67.%'] = '刈谷市役所';
$isp_list['220.213.187.%'] = '福井県'; //Fukui Prefecture

$isp_list['143.89.%'] = 'SOFTBANK TELECOM';
$isp_list['124.215.249.%'] = 'KDDI';
$isp_list['133.163.%'] = '富士通ＩＤＣ';
$isp_list['61.8.88.%'] = 'パナソニック テレコム';
$isp_list['210.190.117.%'] = 'NTT東日本-北海道';
$isp_list['202.214.149.%'] = '京セラ';
$isp_list['138.212.%'] = '旭化成';
$isp_list['202.222.77.%'] = 'インテック';
$isp_list['202.32.113.%'] = '大和ハウス工業';
$isp_list['210.128.112.%'] = 'バイエル';

//engine
$isp_list['%.biglobe.ne.jp'] = 'ＮＥＣビッグローブ';
$isp_list['%.infoseek.co.jp'] = 'Infoseek楽天';
$isp_list['%.excite.co.jp'] = 'Excite';
$isp_list['%.naver.jp'] = 'NHN Japan';
$isp_list['%.yahoo.co.jp'] = 'ヤフー';
$isp_list['%.yahoofs.jp'] = 'ヤフー';
$isp_list['%research-panel.jp'] = 'リサーチパネル';
$isp_list['%.google.co.jp'] = 'グーグル';
$isp_list['%.googlebot.com'] = 'Google Inc.';
$isp_list['%.google.com'] = 'Google Inc.';
$isp_list['%.google.cn'] = 'Google';
$isp_list['%.yahoo.net'] = 'Yahoo! Inc.';
$isp_list['%.yahoo.com'] = 'Yahoo! Inc.';
$isp_list['%.msn.com'] = 'Microsoft Corp';
$isp_list['%.aol.com'] = 'America Online，Inc.';
$isp_list['%.baidu.com'] = '百度';
$isp_list['%.msn.co.jp'] = 'MSNジャパン';
$isp_list['%.live.com'] = 'Microsoft Corp';
$isp_list['%.bing.com'] = 'Microsoft Corp';
$isp_list['%.baidu.jp'] = '百度';
$isp_list['%.goo.ne.jp'] = 'Goo';
$isp_list['%.auone.jp'] = 'Au one';
$isp_list['%.naviiz.com'] = 'ナビーズ';
$isp_list['%.pex.jp'] = 'ポイント交換サービスPeX';
$isp_list['%.s.luna.tv'] = 'Lunascape';
$isp_list['%.ekiten.jp'] = 'エキテン！';
$isp_list['%.impulse-navi.ne.jp'] = 'インパルス';
$isp_list['%218.224.236.119'] = 'インパルス';
$isp_list['%.netisland.jp'] = 'ネットアイランド';
$isp_list['%.seonum.com'] = '一括検索';

//海外
$isp_list['150.70.%'] = 'トレンドマイクロ';
$isp_list['216.104.%'] = 'トレンドマイクロ';
$isp_list['207.46.%'] = 'Microsoft';
$isp_list['128.24.%'] = 'Rockwell International';
$isp_list['118.100.%'] = 'TELEKOM MALAYSIA';
$isp_list['118.101.%'] = 'TELEKOM MALAYSIA';
$isp_list['119.27.62.%'] = 'Blue coat';
$isp_list['98.192.%'] = 'Comcast Cable';
$isp_list['98.194.%'] = 'Comcast Cable';
$isp_list['%.mediaWays.net'] = 'mediaWays';
$isp_list['%.altushost.com'] = 'AltusHost';
$isp_list['124.126.%'] = 'Research Institution of Telecom';
$isp_list['124.127.%'] = 'Research Institution of Telecom';

$isp_list['148.177.%'] = 'Johnson&Johnson';
$isp_list['74.125.%'] = 'Google';

//.ne.jp
$isp_list['%.117.ne.jp'] = '大和生研';
$isp_list['%.1st.ne.jp'] = 'ファーストネットジャパン';
$isp_list['%.aba.ne.jp'] = 'アイスクエア';
$isp_list['%.aia.ne.jp'] = 'エー・アイ・エー';
$isp_list['%.aics.ne.jp'] = '大塚商会';
$isp_list['%.airnet.ne.jp'] = 'アドテックス';
$isp_list['%.akina.ne.jp'] = 'アクシス';
$isp_list['%.akira.ne.jp'] = 'アキラ';
$isp_list['%.allnet.ne.jp'] = 'TITUS COMMUNICATIONS CORPORATION';
$isp_list['%.alpha-net.ne.jp'] = 'アルファ総合研究所';
$isp_list['%.alpha-web.ne.jp'] = '大塚商会';
$isp_list['%.alumnet.ne.jp'] = 'アルムネット';
$isp_list['%.anchor.ne.jp'] = 'オージス総研';
$isp_list['%.angel.ne.jp'] = 'データシステム';
$isp_list['%.asama.ne.jp'] = 'アサマインターネット';
$isp_list['%.asj.ne.jp'] = 'アドミラルシステム';
$isp_list['%.att.ne.jp'] = 'JENS';
$isp_list['%.auric.ne.jp'] = 'オーリック・ウェブ・ジャパン';
$isp_list['%.bb4u.ne.jp'] = '伊藤忠ケーブルシステム';
$isp_list['%.bellcity.ne.jp'] = 'システム';
$isp_list['%.bias.ne.jp'] = 'バイアスネット';
$isp_list['%.bit-drive.ne.jp'] = 'SONY';
$isp_list['%.biwa.ne.jp'] = 'ドリーム・トレイン・インターネット';
$isp_list['%.bmobile.ne.jp'] = '日本通信';
$isp_list['%.canal.ne.jp'] = 'ベンチャーネット';
$isp_list['%.cats.ne.jp'] = '天都';
$isp_list['%.catv.ne.jp'] = 'イッツ・コミュニケーションズ';
$isp_list['%.canvas.ne.jp'] = 'DOCANVAS';
$isp_list['%.ccn-net.ne.jp'] = 'ひまわりネットワーク';
$isp_list['%.cds.ne.jp'] = 'CDSネット';
$isp_list['%.clio.ne.jp'] = 'ホンダクリオ信州';
$isp_list['%.cnet.ne.jp'] = 'システム開発';
$isp_list['%.comlink.ne.jp'] = 'ウインテックコミュニケーションズ';
$isp_list['%.cosmos.ne.jp'] = 'コスモスネット';
$isp_list['%.cyberhome.ne.jp'] = 'ファミリーネット・ジャパン';
$isp_list['%.cyborg.ne.jp'] = 'マイティウイングス';
$isp_list['%.cypress.ne.jp'] = 'サイプレス';
$isp_list['%.cyberbb.ne.jp'] = 'サイバーネット';
$isp_list['%.cyberstation.ne.jp'] = '鉄道システム株式会社';

$isp_list['%.dance.ne.jp'] = 'ダンス';
$isp_list['%.d-b.ne.jp'] = 'デジタルバンク';
$isp_list['%.dcn.ne.jp'] = 'ディーシーエヌ';
$isp_list['%.dear.ne.jp'] = 'ソルコム';
$isp_list['%.dion.ne.jp'] = 'ＫＤＤＩ';
$isp_list['%.dokidoki.ne.jp'] = 'マジカルサイト';
$isp_list['%.dreams.ne.jp'] = '電脳';
$isp_list['%.drive.ne.jp'] = 'シーポイント';
$isp_list['%ii-okinawa.ne.jp'] = '沖縄通信ネットワーク';
$isp_list['%.invoice.ne.jp'] = 'インボイス';

$isp_list['%.ixent.ne.jp'] = 'プロックスシステムデザイン';
$isp_list['%.dsnw.ne.jp'] = 'ディーエスネットワークス';
$isp_list['%.eaccess.ne.jp'] = 'イー･アクセス';
$isp_list['%.eagle-net.ne.jp'] = 'eagle-net';
$isp_list['%.echigo.ne.jp'] = '越後インターネット';
$isp_list['%.echna.ne.jp'] = 'エクナ';
$isp_list['%.edit.ne.jp'] = 'EditNet';
$isp_list['%.em-net.ne.jp'] = 'つなぐネットコミュニケーションズ';
$isp_list['%.enjoy.ne.jp'] = 'デオデオ';

$isp_list['%.e-osaka.ne.jp'] = ' 大阪エクセレント・アイ・ディ・シー';
$isp_list['%.erste.ne.jp'] = 'フォルクスワーゲングループジャパン';
$isp_list['%.express.ne.jp'] = 'エクスプレスコミュニケーションズ';
$isp_list['%.FaceToFace.ne.jp'] = 'しんきん情報システムセンター';
$isp_list['%.fastnet.ne.jp'] = 'ファストネット';
$isp_list['%.firstserver.ne.jp'] = 'ファーストサーバ';
$isp_list['%.freebit.ne.jp'] = 'フリービット・ドットコム';
$isp_list['%.freecom.ne.jp'] = 'トライネットワークインターナショナル';
$isp_list['%.freeserve.ne.jp'] = 'フリーサーブ';
$isp_list['%.fureai-ch.ne.jp'] = 'ふれあいチャンネル';
$isp_list['%.ginzado.ne.jp'] = '銀座堂';
$isp_list['%.gol.ne.jp'] = 'フュージョン・ネットワークサービス';
$isp_list['%.gss.ne.jp'] = 'ギンガシステムソリューション';
$isp_list['%.gyao.ne.jp'] = 'USEN Group';
$isp_list['%.hanamaru.ne.jp'] = '恒徳産業';
$isp_list['%.harenet.ne.jp'] = '三洋コンピュータ';
$isp_list['%.hatena.ne.jp'] = 'はてな';
$isp_list['%.hidecnet.ne.jp'] = 'ハイデックシステムズ'; //サービス終了
$isp_list['%.highway.ne.jp'] = 'CSKネットワークシステムズ';
$isp_list['%.h-ix.ne.jp'] = 'ほくでん情報テクノロジー';
$isp_list['%.home.ne.jp'] = 'アットホームジャパン';
$isp_list['%.hotcn.ne.jp'] = '北海道総合通信網';
$isp_list['%.hypersys.ne.jp'] = 'ハイパー・システムズ';
$isp_list['%.iam.ne.jp'] = 'Iamネット';
$isp_list['%.icn.ne.jp'] = ' ケイテックスジャパン';
$isp_list['%.i-chubu.ne.jp'] = 'トーエネック インターネット中部';
$isp_list['%.icn.ne.jp'] = ' ケイテックスジャパン';
$isp_list['%.incl.ne.jp'] = ' 石川コンピュータ・センター';
$isp_list['%.infoaomori.ne.jp'] = '富士通青森システムエンジニアリング';
$isp_list['%.infonia.ne.jp'] = '日本インターネットアクセス';
$isp_list['%.infosakyu.ne.jp'] = '富士通中国システムズ';
$isp_list['%.infosnow.ne.jp'] = '富士通北海道システムエンジニアリング';
$isp_list['%.infotokyo.ne.jp'] = '富士通システムソリューションズ';
$isp_list['%.izumo.ne.jp'] = 'いずもトータルネット';
$isp_list['%.jah.ne.jp'] = 'コスモメディア';
$isp_list['%.jan.ne.jp'] = '日本・アルカディア・ネットワーク';
$isp_list['%.japan-net.ne.jp'] = 'インフォウェア';
$isp_list['%.jet.ne.jp'] = '仙南情報技術センター';
$isp_list['%.jnc.ne.jp'] = 'ジェイエヌシー';
$isp_list['%.jomon.ne.jp'] = 'JOMON Internet Service';
$isp_list['%.justnet.ne.jp'] = 'ウェ ブオンラインネットワークス';
$isp_list['%.kanden.ne.jp'] = '関電情報システム';
$isp_list['%.kannet.ne.jp'] = '関越ネットワークシステム';
$isp_list['%.kbn.ne.jp'] = '香川テレビ放送網';
$isp_list['%.kcom.ne.jp'] = 'KCOM';
$isp_list['%.keihanna.ne.jp'] = 'けいはんなネット';
$isp_list['%.kddi-ok.ne.jp'] = 'KDDI沖縄';
$isp_list['%.kisweb.ne.jp'] = '関電情報システム';
$isp_list['%.kiwi.ne.jp'] = 'キウイ';
$isp_list['%.knet.ne.jp'] = 'ケーネット';
$isp_list['%.ksky.ne.jp'] = 'Knet';
$isp_list['%.kvh.ne.jp'] = 'KVH';
$isp_list['%.kvision.ne.jp'] = 'Kビジョン';
$isp_list['%.mahoroba.ne.jp'] = 'アミック';
$isp_list['%.matsumoto.ne.jp'] = '松本商工会議所';
$isp_list['%.maxs.ne.jp'] = 'アクシズ';
$isp_list['%.bbcat.jp'] = 'アクシズ';
$isp_list['%.mco.ne.jp'] = 'メディアちゃんぷる沖縄';
$isp_list['%.mediawars.ne.jp'] = 'メディアウォーズ';
$isp_list['%.megaegg.ne.jp'] = 'エネルギアコミニュケーションズ';
$isp_list['%.me-h.ne.jp'] = 'NTT-ME北海道';
$isp_list['%.meon.ne.jp'] = 'NTT西日本－中国';

$isp_list['%.metallic.ne.jp'] = '東京めたりっく通信';
$isp_list['%.minc.ne.jp'] = ' 南日本情報処理センター';
$isp_list['%.miracle.ne.jp'] = 'San-in Net';
$isp_list['%.mis.ne.jp'] = '情報空間';
$isp_list['%.mnet.ne.jp'] = 'アイコムティ';
$isp_list['%.net-ibaraki.ne.jp'] = '茨城県高度情報化推進協議会';
$isp_list['%.netlaputa.ne.jp'] = 'ネットラピュタ';
$isp_list['%.neweb.ne.jp'] = 'ＫＤＤＩ';
$isp_list['%.nima-cho.ne.jp'] = '仁摩町有線放送電話協会';
$isp_list['%.ni-po.ne.jp'] = 'ニイカワポータル';
$isp_list['%.nkansai.ne.jp'] = '近畿コンピュータサービス';
$isp_list['%.nmt.ne.jp'] = 'ニューメディア徳島';
$isp_list['%.nnet.ne.jp'] = 'エヌネット';
$isp_list['%.nolnet.ne.jp'] = 'エヌオーエルネット';
$isp_list['%.nsk.ne.jp'] = 'ネスク';
$isp_list['%.nttpc.ne.jp'] = 'NTTPCコミュニケーションズ';
$isp_list['%.obicnet.ne.jp'] = 'オービック';

$isp_list['%.odn.ne.jp'] = '日本テレコム';

$isp_list['%.oitaweb.ne.jp'] = '富士通大分ソフトウェアラボラトリ';
$isp_list['%.omp.ne.jp'] = '大阪メディアポート';
$isp_list['%.owari.ne.jp'] = 'インターネット尾張';

$isp_list['%.parknet.ne.jp'] = 'パークネット';
$isp_list['%.pep.ne.jp'] = '東芝情報システム';
$isp_list['%.pias.ne.jp'] = 'NTT-Docomo';
$isp_list['%.planet.ne.jp'] = 'プラネット';
$isp_list['%.point.ne.jp'] = 'ドリーム・トレイン・インターネット'; //東京
$isp_list['%.portnet.ne.jp'] = '神戸ポートネット';
$isp_list['%.psinet.ne.jp'] = 'ピーエスアイネット';
$isp_list['%.psn.ne.jp'] = 'パーソナル・ソフトウェア';
$isp_list['%.pwd.ne.jp'] = 'KDDI';
$isp_list['%.px0256.ne.jp'] = 'ＰＸ０２５６インターネット';
$isp_list['%.rmc.ne.jp'] = '竜王メディアセンター';
$isp_list['%.safins.ne.jp'] = '福島情報サービス';
$isp_list['%.sakura-utopia.ne.jp'] = 'さくらケーシーエス';
$isp_list['%.sannet.ne.jp'] = 'NTTデータ三洋システム';
$isp_list['%.satsuma.ne.jp'] = '富士通鹿児島インフォネット';
$isp_list['%.sdx.ne.jp'] = '彩ネット';
$isp_list['%.seikyou.ne.jp'] = '生協インターネット';
$isp_list['%.shinenet.ne.jp'] = 'シャイン・オン';
$isp_list['%.sni.ne.jp'] = '佐賀新聞・長崎新聞インターネット';
$isp_list['%.southernx.ne.jp'] = 'オーシーシー';
$isp_list['%.speednet.ne.jp'] = 'スピードネット';
$isp_list['%.spnet.ne.jp'] = '県央インターネット';
$isp_list['%.sunfield.ne.jp'] = ' サンフィールド・インターネット';
$isp_list['%.sunshine.ne.jp'] = 'ケーシーエス';
$isp_list['%.synapse.ne.jp'] = 'グッドコミュニケーションズ';
$isp_list['%.sub.ne.jp'] = 'ドリーム・トレイン・インターネット法人';
$isp_list['%.tako.ne.jp'] = 'ネクストアイ';
$isp_list['%.tcpweb.ne.jp'] = 'WebWave';
$isp_list['%.tam.ne.jp'] = 'ティエイエムインターネットサービス';
$isp_list['%.technowave.ne.jp'] = 'リコーテクノシステムズ';
$isp_list['%.tiki.ne.jp'] = 'エヌディエス';
$isp_list['%.tns.ne.jp'] = 'トヨタデジタルクルーズ';
$isp_list['%.tochigi-ip.ne.jp'] = '栃木共同ネットワークサービス';
$isp_list['%.tribe.ne.jp'] = 'パナソニックテレコム';
$isp_list['%.ttcn.ne.jp'] = '東京通信ネットワーク';
$isp_list['%.tv-naruto.ne.jp'] = 'テレビ鳴門';
$isp_list['%.twin.ne.jp'] = 'ゼクシス';
$isp_list['%.t-com.ne.jp'] = 'ビック東海';

$isp_list['%.ucom.ne.jp'] = 'UCOM';

$isp_list['%.u-netsurf.ne.jp'] = '日本ユニシス情報システム';
$isp_list['%.urban.ne.jp'] = 'Urban Internet Co.，Ltd.';
$isp_list['%.valley.ne.jp'] = '富士通長野システムエンジニアリング';
$isp_list['%.vc-net.ne.jp'] = 'VC-net';
$isp_list['%.vectant.ne.jp'] = 'ヴェクタント';
$isp_list['%.wakwak.ne.jp'] = 'ＮＴＴ－ＭＥ';
$isp_list['%.wbs.ne.jp'] = '富士通ソフトウェアテクノロジーズ';
$isp_list['%.webport.ne.jp'] = '(株)マルチメディア・アンド・ネットワークサービス';
$isp_list['%.wide.ne.jp'] = 'ネディア';
$isp_list['%.wind.ne.jp'] = '群馬インターネット';
$isp_list['%.xaxon.ne.jp'] = 'ザクソン';
$isp_list['%.xephion.ne.jp'] = 'ＮＴＴ－ＭＥ';
$isp_list['%.yanbaru.ne.jp'] = 'エフケイ';
$isp_list['%.yournet.ne.jp'] = 'フリービット';

//.or.jp
$isp_list['%.accs.or.jp'] = '財団法人研究学園都市コミュニティケーブルサービス';
$isp_list['%.across.or.jp'] = 'ドリームウェーブ静岡';
$isp_list['%.aikis.or.jp'] = 'テレコムわかやま';
$isp_list['%.alles.or.jp'] = 'ビーイング';
$isp_list['%.amie.or.jp'] = 'アウズ';
$isp_list['%.asahi-net.or.jp'] = '朝日ネット';
$isp_list['%.aya.or.jp'] = 'アヤインターネットサービス';
$isp_list['%.big.or.jp'] = 'ビッグカンパニー';
$isp_list['%.big-c.or.jp'] = 'ビッグ';
$isp_list['%.bscnet.or.jp'] = 'ネットマークス';
$isp_list['%.cjn.or.jp'] = '名古屋情報センター';
$isp_list['%.ckp.or.jp'] = 'サイバー関西プロジェクト';
$isp_list['%.coara.or.jp'] = 'コアラ';
$isp_list['%.comel.or.jp'] = 'COMEL Internet Service';
$isp_list['%.coralnet.or.jp'] = 'コーラルネット';
$isp_list['%.din.or.jp'] = 'ドルフィンインターナショナル';
$isp_list['%.enshu-net.or.jp'] = 'ヤマハモーターソリューション';
$isp_list['%.e-housing.or.jp'] = '財団法人住宅管理協会';
$isp_list['%.fais.or.jp'] = 'つくばWANネットワーク';
$isp_list['%.fitweb.or.jp'] = '北電情報システムサービス';
$isp_list['%.fureai.or.jp'] = 'ふれあい情報通信網';
$isp_list['%.hakodate.or.jp'] = '函館インフォメーションネットワーク';
$isp_list['%.hitwave.or.jp'] = '北陸コンピュータ・サービス';
$isp_list['%.ic-net.or.jp'] = 'ＩＣ-ＮＥＴ';
$isp_list['%.iic.or.jp'] = 'インテック';
$isp_list['%.iij4u.or.jp'] = 'インターネットイニシアティブ';
$isp_list['%.iijnet.or.jp'] = 'インターネットイニシアティブ';
$isp_list['%.i-kochi.or.jp'] = '高知システムズ';
$isp_list['%.inaker.or.jp'] = 'サルード';
$isp_list['%.index.or.jp'] = 'NRIセキュアテクノロジーズ';
$isp_list['%.inet-shibata.or.jp'] = '新潟通信サービス';
$isp_list['%.ink.or.jp'] = '特定非営利活動法人秋田県アイティ基盤協会';
$isp_list['%.interlink.or.jp'] = 'インターリンク';
$isp_list['%.interq.or.jp'] = 'ＧＭＯインターネット';
$isp_list['%.inetshonai.or.jp'] = '日情システムソリューションズ';
$isp_list['%.inetmie.or.jp'] = 'インターネット三重';
$isp_list['%.intio.or.jp'] = '関彰商事';
$isp_list['%.ipc-tokai.or.jp'] = 'インターネット・プロ東海';
$isp_list['%.iwami.or.jp'] = 'マイメディア';
$isp_list['%.janis.or.jp'] = '長野県協同電算';
$isp_list['%.jiii.or.jp'] = '社団法人発明協会';
$isp_list['%.kamome.or.jp'] = 'ネットフォレスト';
$isp_list['%.kinet.or.jp'] = '川崎インターネット';
$isp_list['%.kix.or.jp'] = '新日鉄ソリューションズ';
$isp_list['%.kyoto-inet.or.jp'] = 'イージェーワークス';
$isp_list['%.lasdec.or.jp'] = '財団法人地方自治情報センター';
$isp_list['%.linkclub.or.jp'] = 'リンククラブ';
$isp_list['%.mbn.or.jp'] = 'ドリームネット';
$isp_list['%.memenet.or.jp'] = 'インフォミーム';
$isp_list['%.mfi.or.jp'] = 'コム';
$isp_list['%.miyazaki-nw.or.jp'] = '宮崎県ソフトウェアセンター';
$isp_list['%.mmtr.or.jp'] = 'ももたろうインターネット';
$isp_list['%.myk.or.jp'] = '日建レンタコム';
$isp_list['%.nasuinfo.or.jp'] = '那須インフォネット';
$isp_list['%.nasu-net.or.jp'] = 'フェニックスデザイン';
$isp_list['%.naxnet.or.jp'] = 'NAX Company Corporation.Ltd.';
$isp_list['%.netaid.or.jp'] = 'ネットアイアールディー';
$isp_list['%.netspace.or.jp'] = '日立製作所';
$isp_list['%.niji.or.jp'] = '虹ネット';
$isp_list['%.nnc.or.jp'] = 'NNCネットワーク事務局';
$isp_list['%.okazaki-med.or.jp'] = '岡崎市医師会';
$isp_list['%.owl.or.jp'] = 'セゾン情報システムズ';
$isp_list['%.phoenix-c.or.jp'] = 'ジンオフィスサービス';
$isp_list['%.picky.or.jp'] = '財団法人京都高度技術研究所';
$isp_list['%.platz.or.jp'] = 'ラット';
$isp_list['%.rim.or.jp'] = 'インタードットネット';
$isp_list['%.rinku.or.jp'] = 'あさかわシステム';
$isp_list['%.sainet.or.jp'] = '彩ネット';
$isp_list['%.sakuranet.or.jp'] = 'スマートバリュー';
$isp_list['%.sala.or.jp'] = 'SALA-INTERNET SERVICE';
$isp_list['%.sky-net.or.jp'] = 'スカイネット';
$isp_list['%.softopia.or.jp'] = 'ソフトピアジャパン';
$isp_list['%.sopia.or.jp'] = 'ソピアフォンス';
$isp_list['%.spice.or.jp'] = 'スパイス';
$isp_list['%.sun-inet.or.jp'] = 'サン・インターネット';
$isp_list['%.takauji.or.jp'] = 'サンライズシステムズ';
$isp_list['%.tama.or.jp'] = '多摩インターネット';
$isp_list['%.tenrikyo.or.jp'] = '天理教';
$isp_list['%.t-cnet.or.jp'] = '三協システム';
$isp_list['%.try-net.or.jp'] = 'トライネットワークインターナショナル';
$isp_list['%.tgn.or.jp'] = 'テプコシステムズ';
$isp_list['%.usiwakamaru.or.jp'] = 'ヴァンズ';
$isp_list['%.yacc.or.jp'] = '山梨地域インターネット協会';
$isp_list['%.yin.or.jp'] = '山梨インターネット';
$isp_list['%.yomogi.or.jp'] = 'エム・ビー・エス';
$isp_list['%.yutopia.or.jp'] = '特定非営利活動法人ゆーとぴあネット';
$isp_list['%.yyy.or.jp'] = 'ワイワイワイネット';

//.co.jp
$isp_list['%.accutechs.co.jp'] = 'アキュテクス';
$isp_list['%.ace-net.co.jp'] = 'エー・シー・イー';
$isp_list['%.advance.jp'] = 'アドバンス';
$isp_list['%.annies.co.jp'] = 'アニーズ・クラフト';
$isp_list['%.asp-kk.co.jp'] = '全日空システム企画';
$isp_list['%.atlas.jp'] = 'アドラス';
$isp_list['%.aplus.co.jp'] = 'アプラス';
$isp_list['%.atworks.co.jp'] = 'エーティーワークス';
$isp_list['%.alchemia.co.jp'] = 'アルケミア';
$isp_list['%.bbsec.co.jp'] = 'ブロードバンドセキュリティ';
$isp_list['%.bit-isle.co.jp'] = 'ビットアイル';
$isp_list['%.beacon-it.co.jp'] = 'ビーコンインフォメーションテクノロジー';
$isp_list['%.blg.co.jp'] = 'ブックローン';
$isp_list['%.chabashira.co.jp'] = 'メディア・ミックス静岡';
$isp_list['%.cec-ltd.co.jp'] = 'シーイーシー';
$isp_list['%.chukai.co.jp'] = '中海テレビ放送';
$isp_list['%.cnci.co.jp'] = 'コミュニティネットワークセンター';
$isp_list['%.conami.co.jp'] = 'コナミ';
$isp_list['%.compaq.co.jp'] = 'コンパックコンピュータ';
$isp_list['%.crayfish.co.jp'] = 'クレイフィッシュ';
$isp_list['%.ctc.co.jp'] = '中部テレコミュニケーション';
$isp_list['%.ctc-g.co.jp'] = '伊藤忠テクノソリューションズ';
$isp_list['%.cyberagent.co.jp'] = 'サイバーエージェント';
$isp_list['%.cybozu.co.jp'] = 'サイボウズ';
$isp_list['%.cybird.co.jp'] = 'サイバード';
$isp_list['%.cyberhome.co.jp'] = 'ファミリーネット・ジャパン';
$isp_list['%.daikin.co.jp'] = 'ダイキン工業';
$isp_list['%.d-cruise.co.jp'] = 'トヨタデジタルクルーズ';
$isp_list['%.ddi.co.jp'] = 'ＫＤＤＩ';
$isp_list['%.densan-s.co.jp'] = '電算システム';
$isp_list['%.daiken.co.jp'] = '大建工業';
$isp_list['%.dnp.co.jp'] = '大日本印刷';
$isp_list['%.dtc.co.jp'] = 'デジタルテクノロジー';
$isp_list['%.dwango.co.jp'] = 'ドワンゴ';
$isp_list['%.east.ntt.co.jp'] = '東日本電信電話';
$isp_list['%.editnet.co.jp'] = 'EditNet';
$isp_list['%.ekzm.co.jp'] = 'エクザム';
$isp_list['%.emobile.co.jp'] = 'イー・モバイル';
$isp_list['%.enecom.co.jp'] = 'エネルギア・コミュニケーションズ';
$isp_list['%.firstriding.co.jp'] = 'ファーストライディングテクノロジー';
$isp_list['%.fsi.co.jp'] = '富士ソフトエービーシ';
$isp_list['%.fujitsu.co.jp'] = '富士通';
$isp_list['%.fujixerox.co.jp'] = '富士ゼロックス';
$isp_list['%.fusioncom.co.jp'] = 'フュージョン・コミュニケーションズ';
$isp_list['%.globalcoms.co.jp'] = 'グローバルコミニュケーションズ';
$isp_list['%.hands-inc.co.jp'] = 'ハンズ';
$isp_list['%.hankyu-hanshin.co.jp'] = 'アイテック阪急阪神';
$isp_list['%.hudson.co.jp'] = 'ハドソン';
$isp_list['%.hba.co.jp'] = 'HBA';
$isp_list['%.hi-net.co.jp'] = 'ハイネット';
$isp_list['%.hikari.co.jp'] = '光通信';
$isp_list['%.hitachi.co.jp'] = '日立製作所';
$isp_list['%.hitachi-skss.co.jp'] = 'ＳＫサポートサービス';
$isp_list['%.icntv.co.jp'] = 'アイ・キャン';
$isp_list['%.inax.co.jp'] = 'INAX';
$isp_list['%.increws.co.jp'] = 'インクルーズ';
$isp_list['%.internap.co.jp'] = 'インターナップ・ジャパン';
$isp_list['%.ibrains.co.jp'] = 'インタラクティブブレインズ';
$isp_list['%.inv.co.jp'] = 'ビークル';
$isp_list['%.infotechnica.co.jp'] = 'インフォテクニカ';
$isp_list['%.itsol.co.jp'] = 'アイティーソリューションズ';
$isp_list['%.iri.co.jp'] = 'インターネット総合研究所';
$isp_list['%.isao.co.jp'] = 'イサオ';
$isp_list['%.j-cnet.co.jp'] = 'ジャパンケーブルネット';
$isp_list['%.j-cnet.jp'] = 'ジャパンケーブルネット';
$isp_list['%.jdserve.co.jp'] = '日本デジタル配信';
$isp_list['%.jpta.scs.co.jp'] = '住商情報システム';
$isp_list['%.jtis.co.jp'] = 'ジェイアール東海情報システム';
$isp_list['%.justsystem.co.jp'] = 'ジャストシステム';
$isp_list['%k-denkai.co.jp'] = '鹿島電解';
$isp_list['%.kai.co.jp'] = 'カイクリエイツ';
$isp_list['%.katch.co.jp'] = 'キャッチネットワーク';
$isp_list['%.kccs.co.jp'] = '京セラコミュニケーションシステム';
$isp_list['%.keyence.co.jp'] = 'キーエンス';
$isp_list['%.kmn.co.jp'] = 'ケイエムエヌ';
$isp_list['%.kmsc.co.jp'] = '関西マルチメディアサービス';
$isp_list['%.kuronekoyamato.co.jp'] = 'ヤマトシステム開発';
$isp_list['%.krp.co.jp'] = '京都リサーチパーク';
$isp_list['%.kodansha.co.jp'] = '講談社';
$isp_list['%.lexues.co.jp'] = 'レキサス';
$isp_list['%.maruichi-sansho.co.jp'] = 'マルイチ産商';
$isp_list['%.medias.co.jp'] = '知多メディアスネットワーク';
$isp_list['%.mei.co.jp'] = '松下電器産業';
$isp_list['%.meidensha.co.jp'] = '明電舎';
$isp_list['%.metallic.co.jp'] = '東京めたりっく通信';
$isp_list['%.mew.co.jp'] = 'パナソニック電工インフォメーションシステムズ';
$isp_list['%.mixi.co.jp'] = 'ミクシィ';
$isp_list['%.mki-net.co.jp'] = 'MKIネットワーク・ソリューションズ';
$isp_list['%.nat.co.jp'] = '日本アドバンストテクノロジー';
$isp_list['%.naris.co.jp'] = 'ナリス化粧品';
$isp_list['%.nagoya-metallic.co.jp'] = '名古屋めたりっく通信';
$isp_list['%.nasnet.co.jp'] = '日経統合システム';
$isp_list['%.nec.co.jp'] = '日本電気';
$isp_list['%.nekonet.co.jp'] = 'ヤマトシステム開発';
$isp_list['%.neive.co.jp'] = 'ネイブ';
$isp_list['%.nippon-access.co.jp'] = '日本アクセス';
$isp_list['%.nissanchem.co.jp'] = '日産化学工業';
$isp_list['%.NIandC.co.jp'] = '日本情報通信';
$isp_list['%.nifty.co.jp'] = 'ニフティ';
$isp_list['%.nns-catv.co.jp'] = '日本ネットワークサービス';
$isp_list['%.nsplanning.co.jp'] = 'エヌエスプランニング';
$isp_list['%.nsc-kk.co.jp'] = '日本システムコンサルト';
$isp_list['%.ntt.co.jp'] = '日本電信電話';
$isp_list['%.ntt-it.co.jp'] = 'エヌ・ティ・ティ・アイティ';
$isp_list['%.nttcom.co.jp'] = 'NTTコミュニケーションズ';
$isp_list['%.nttdata.co.jp'] = 'NTTデータ';
$isp_list['%.nttdata-tokai.co.jp'] = 'エヌ・ティ・ティ・データ東海';
$isp_list['%.nttdocomo.co.jp'] = 'エヌ・ティ・ティ・ドコモ';
$isp_list['%.ntts.co.jp'] = 'エヌ・ティ・ティ・ソフトウェア';
$isp_list['%.nttx.co.jp'] = 'ＮＴＴ－Ｘ';
$isp_list['%.oki.co.jp'] = '沖電気工業';
$isp_list['%.oracle.co.jp'] = '日本オラクル';
$isp_list['%.oitechno.co.jp'] = 'オオイテクノ';
$isp_list['%.panasonic.co.jp'] = '松下電器産業';
$isp_list['%.panasonic.com'] = 'パナソニック';
$isp_list['%.pia.co.jp'] = 'ぴあ';
$isp_list['%.pca.co.jp'] = 'ピー・シー・エー';
$isp_list['%.omron.co.jp'] = 'オムロン';
$isp_list['%.parknet.co.jp'] = 'パークネット';
$isp_list['%.phaseone.co.jp'] = 'フェイズワン';
$isp_list['%.qic.co.jp'] = 'キューデンインフォコム';
$isp_list['%.rakuten.co.jp'] = '楽天';
$isp_list['%.ricoh.co.jp'] = 'リコー';
$isp_list['%.seibu-denki.co.jp'] = '西部電気工業';
$isp_list['%.senkyo.co.jp'] = '仙台共同印刷';
$isp_list['%.siliconstudio.co.jp'] = 'シリコンスタジオ';
$isp_list['%.sharp.co.jp'] = 'シャープ';
$isp_list['%.sofu.co.jp'] = ' 創風システム';
$isp_list['%.sony.co.jp'] = 'ソニー';
$isp_list['%.Sony.CO.JP'] = 'ソニー';
$isp_list['%.sonique.co.jp'] = 'ソニック';
$isp_list['%.softbanktelecom.co.jp'] = 'ソフトバンクテレコム';
$isp_list['%.sunnyhealth.co.jp'] = 'サニーヘルス';
$isp_list['%.stream.co.jp'] = 'Ｊストリーム';
$isp_list['%.takara.co.jp'] = '宝酒造';
$isp_list['%.tbs.co.jp'] = 'ＴＢＳテレビ';
$isp_list['%.t-ctb.co.jp'] = 'シーティービーメディア';
$isp_list['%.tis.co.jp'] = 'ティアイエス';
$isp_list['%.togis.co.jp'] = '東邦ガス情報システム';
$isp_list['%.tose.co.jp'] = 'トーセ';
$isp_list['%.tonami.co.jp'] = 'トナミ運輸';
$isp_list['%.toyodenki.co.jp'] = '東洋電機製造';
$isp_list['%.toyotasmile.co.jp'] = 'トヨタすまいるライフ';
$isp_list['%.toshiba.co.jp'] = '東芝';
$isp_list['%.toyama-tic.co.jp'] = '富山県総合情報センター';
$isp_list['%.toppan.co.jp'] = '凸版印刷';

$isp_list['%.u-4.co.jp'] = 'ユーメディア';
$isp_list['%.unisys.co.jp'] = '日本ユニシス';
$isp_list['%.west.ntt.co.jp'] = '西日本電信電話';
$isp_list['%.yano.co.jp'] = '矢野経済研究所';
$isp_list['%.yuden.co.jp'] = '太陽誘電';
$isp_list['%.kobe-fugetsudo.co.jp'] = '神戸風月堂';
$isp_list['%.ymir.co.jp'] = 'ユミルリンク';
$isp_list['%.koanet.co.jp'] = 'コーア株式会社';
$isp_list['%.fork.co.jp'] = '株式会社フォーク';
$isp_list['%mutenet.co.jp'] = 'ミュートネット';
$isp_list['%.systemd.co.jp'] = 'システムディ';
$isp_list['%.webjapan.co.jp'] = 'オフィス２４';
$isp_list['%.weathermap.co.jp'] = 'ウェザーマップ';

//.ad.jp
$isp_list['%.admix-net.ad.jp'] = '社団法人 行政情報システム研究所';
$isp_list['%.air.ad.jp'] = 'エアネット';
$isp_list['%.aoi.ad.jp'] = 'アオイソフトウェア';
$isp_list['%.are-works.ad.jp'] = 'アールワークス';
$isp_list['%.attokyo.ad.jp'] = 'アット東京';
$isp_list['%.bartok.ad.jp'] = 'ジェンマエンジニアリング';
$isp_list['%.bbtower.ad.jp'] = 'ブロードバンドタワー';
$isp_list['%.bears.ad.jp'] = '富士通九州システムズ';
$isp_list['%.bekknet.ad.jp'] = ' ベッコアメ・インターネット';
$isp_list['%.bento.ad.jp'] = 'CIJ';
$isp_list['%.cpi.ad.jp'] = 'KDDIウェブコミュニケーションズ';
$isp_list['%.crnet.ad.jp'] = 'シー・アール';
$isp_list['%.cyberlinks.ad.jp'] = 'サイバーリンクス';
$isp_list['%.cypress.ad.jp'] = ' サイプレス';
$isp_list['%.dcn.ad.jp'] = 'ディーシーエヌ';
$isp_list['%.din.ad.jp'] = ' ドルフィンインターナショナル';
$isp_list['%.dnp.ad.jp'] = '大日本印刷';
$isp_list['%.dti.ad.jp'] = ' ドリーム・トレイン・インターネット';
$isp_list['%.emobile.ad.jp'] = 'イー・モバイル';
$isp_list['%.fine.ad.jp'] = 'キヤノンネットワークコミュニケーションズ';
$isp_list['%.fitweb.ad.jp'] = '北電情報システムサービス';
$isp_list['%.fortune.ad.jp'] = 'フォーチュン';
$isp_list['%.forward.ad.jp'] = '日立情報システムズ';
$isp_list['%.good-comm.ad.jp'] = 'グッドコミュニケーションズ';
$isp_list['%.harenet.ad.jp'] = '三洋コンピュータ';
$isp_list['%.hits.ad.jp'] = '高速情報通信システム';
$isp_list['%.home.ad.jp'] = 'テクノロジーネットワークス';
$isp_list['%.hotnet.ad.jp'] = '北海道総合通信網';
$isp_list['%.htnet.ad.jp'] = '北陸通信ネットワーク';
$isp_list['%.ia-net.ad.jp'] = '富士通東北システムズ';
$isp_list['%.ibis.ad.jp'] = ' 富士通新潟システムズ';
$isp_list['%.ics.ad.jp'] = '共同印刷ビジネスソリューションズ';
$isp_list['%.iij.ad.jp'] = 'インターネットイニシアティブ';
$isp_list['%.ii-okinawa.ad.jp'] = '沖縄通信ネットワーク';
$isp_list['%.infovalley.ad.jp'] = '富士通長野システムエンジニアリング';
$isp_list['%.interlink.ad.jp'] = ' インターリンク';
$isp_list['%.intervia.ad.jp'] = 'エヌ・ティ・ティ・データ';
$isp_list['%.isfnet.ad.jp'] = 'エヌ・アンド・アイ・システムズ';
$isp_list['%.itscom.ad.jp'] = 'イッツ・コミュニケーションズ';
$isp_list['%.jpix.ad.jp'] = '日本インターネットエクスチェンジ';
$isp_list['%.jtnet.ad.jp'] = '日本たばこ産業';
$isp_list['%.keywave.ad.jp'] = ' 平和情報センター';
$isp_list['%.kids-way.ad.jp'] = 'キッズウェイ';
$isp_list['%.knet.ad.jp'] = 'Knet';
$isp_list['%.k-opti.ad.jp'] = 'ケイ・オプティコム';
$isp_list['%.maffin.ad.jp'] = '農林水産省研究ネットワーク';
$isp_list['%.mcnet.ad.jp'] = 'エヌ・ティ・ティ・スマートコネクト';
$isp_list['%.mei.ad.jp'] = 'メイテツコム';

$isp_list['%.mex.ad.jp'] = 'メディアエクスチェンジ';
$isp_list['%.mfeed.ad.jp'] = 'インターネットマルチフィード';
$isp_list['%.mind.ad.jp'] = '三菱電機情報ネットワーク';
$isp_list['%.mirai.ad.jp'] = 'ミライコミュニケーションネットワーク';
$isp_list['%.ncom.ad.jp'] = '新潟通信サービス';
$isp_list['%.next-web.ad.jp'] = 'ネクストウェブ';
$isp_list['%.nia.ad.jp'] = '日本インターネットアクセス';
$isp_list['%.nnet.ad.jp'] = 'エヌネット';
$isp_list['%.ns-absonne.ad.jp'] = '新日鉄ソリューションズ';
$isp_list['%.nsk.ad.jp'] = ' ネスク';
$isp_list['%.ocn.ad.jp'] = 'エヌ・ティ・ティ・コミュニケーションズ';
$isp_list['%.odn.ad.jp'] = 'ソフトバンクテレコム';
$isp_list['%.ptop.ad.jp'] = '沖電気工業';
$isp_list['%.qtnet.ad.jp'] = '九州通信ネットワーク';
$isp_list['%.ryukyu.ad.jp'] = '沖縄富士通システムエンジニアリング';
$isp_list['%.sakura.ad.jp'] = 'さくらインターネット';
$isp_list['%.shinshu.ad.jp'] = '塩尻市';
$isp_list['%.sinet.ad.jp'] = '国立情報学研究所';
$isp_list['%.smartvalue.ad.jp'] = 'スマートバリュー';
$isp_list['%.snow.ad.jp'] = '富士通北海道システムズ';
$isp_list['%.sonytelecom.ad.jp'] = 'ソニー';
$isp_list['%.sphere.ad.jp'] = 'エヌ・ティ・ティ ピー・シー コミュニケーションズ';
$isp_list['%.sprite.ad.jp'] = 'エヌアイエスプラス';
$isp_list['%.ssd.ad.jp'] = 'ソニーグローバルソリューションズ';
$isp_list['%.starnet.ad.jp'] = 'スターネット';
$isp_list['%.stnet.ad.jp'] = 'STNet';
$isp_list['%.swanet.ad.jp'] = 'PFU';
$isp_list['%.tam.ad.jp'] = 'ティ・エイ・エム';
$isp_list['%.tcp-net.ad.jp'] = 'TCP';
$isp_list['%.technowave.ad.jp'] = 'リコーテクノシステムズ';
$isp_list['%.tins.ad.jp'] = ' つくばインターネットサービス';
$isp_list['%.tocn.ad.jp'] = '東北インテリジェント通信';
$isp_list['%.topic.ad.jp'] = '東北学術研究インターネットコミュニティ';
$isp_list['%.ttnet.ad.jp'] = '東京通信ネットワーク';
$isp_list['%.usen.ad.jp'] = '有線ブロードネットワークス';
$isp_list['%.viplt.ad.jp'] = 'NTT西日本－北陸';
$isp_list['%.wag.ad.jp'] = 'インテックシステム研究所';
$isp_list['%.web.ad.jp'] = '富士通';
$isp_list['%.wide.ad.jp'] = 'WIDE インターネット';
$isp_list['%.wintech.ad.jp'] = 'ウインテックコミュニケーションズ';
$isp_list['%.with.ad.jp'] = 'WITH Networks';
$isp_list['%.xephion.ad.jp'] = 'エヌ・ティ・ティエムイー';
$isp_list['%.zero.ad.jp'] = 'ゼロ';

//.ed.jp--------------------------------------------------------------------------------------
$isp_list['%.kurashiki-oky.ed.jp'] = '倉敷情報学習センター';
$isp_list['%.toyota.ed.jp'] = '豊田市教育委員会';
$isp_list['%.owariasahi.ed.jp'] = '尾張旭市教育委員会';
$isp_list['%.asn.ed.jp'] = '青森県教育ネットワーク';

//.jp
$isp_list['%.adingo.jp'] = 'adingo';
$isp_list['%.aqstage.jp'] = 'エヌ・ティ・ティ ネオメイト';
$isp_list['%.advanscope.jp'] = 'アドバンスコープ';
$isp_list['%.bbexcite.jp'] = 'エキサイト';
$isp_list['%.broadcenter.jp'] = 'ビック東海';
$isp_list['%.bbix.jp'] = 'ＩＣ-ＮＥＴ';
$isp_list['%.shizuoka.jp'] = '静岡市';
$isp_list['%.city.gujo.gifu.jp'] = '郡上市';
$isp_list['%.city.iwaki.fukushima.jp'] = 'いわき市';
$isp_list['%.city.iwamizawa.hokkaido.jp'] = '北海道岩見沢市';
$isp_list['%.city.tottori.tottori.jp'] = '鳥取市役所';
$isp_list['%.citv.jp'] = 'ペンディアム';
$isp_list['%.coreserver.jp'] = 'デジロック';
$isp_list['%.chukyo-hosp.jp'] = '中京病院';
$isp_list['%.cwj.jp'] = 'サイバーウェイブジャパン';
$isp_list['%.datacoa.jp'] = 'データコア';
$isp_list['%.digital-all.jp'] = 'デジタルアライアンス';
$isp_list['%.domiru.jp'] = 'DOMIRU';
$isp_list['%.doubleroute.jp'] = 'ぷららネットワークス';
$isp_list['%.fenics.jp'] = '富士通';
$isp_list['%.global-netcore.jp'] = 'グローバルネットコア';
$isp_list['%.gmo-access.jp'] = 'ＧＭＯインターネット';
$isp_list['%.hbb.jp'] = '北海道ギガビット 協同組合';
$isp_list['%.hgc.jp'] = '東京大学医科学研究所ヒトゲノム解析センター';
$isp_list['%.hiroshima.jp'] = '広島県';
$isp_list['%.idc.jp'] = 'ヤフー';

$isp_list['%.ims.jp'] = 'アイエムエス・ドット・ジェーピー';
$isp_list['%.i-revonet.jp'] = 'インターネットレボリューション';
$isp_list['%.pref.ishikawa.jp'] = '石川県';

$isp_list['%.j-cnet.jp'] = 'ジャパンケーブルネット';
$isp_list['%.jig.jp'] = 'jig.jp';
$isp_list['%.jsd-nsp.jp'] = '北海道ジェイ・アール・システム開発';

$isp_list['%.3sannet.jp'] = 'ハック';
$isp_list['%.ks-sol.jp'] = '関電システムソリューションズ';
$isp_list['%.kyoto-inetbb.jp'] = 'イージェーワークス';
$isp_list['%.nbgi.jp'] = 'バンダイナムコ未来研究所';
$isp_list['%.is-ja.jp'] = '石川県農協電算センター';
$isp_list['%.mc-idc.jp'] = '宮銀コンピューターサービス';
$isp_list['%.mctv.jp'] = '松阪ケーブルテレビ・ステーション';
$isp_list['%.maine.jp'] = '乗馬用品ＭＥｉＮＥ';
$isp_list['%.mexne.jp'] = 'メディアエクスチェンジ';
$isp_list['%.mis.jp'] = '情報空間';

$isp_list['%.nabic.jp'] = 'NS・コンピュータサービス';
$isp_list['%.nuks.jp'] = 'ナックス';
$isp_list['%.nhncorp.jp'] = 'NHN Japan';
$isp_list['%.okayama.jp'] = '岡山県';
$isp_list['%.ouk.jp'] = 'オーク';
$isp_list['%.rppp.jp'] = 'フェニックスクラブ インターネットサービス';
$isp_list['%.evonet.jp'] = '双日システムズ';
$isp_list['%.surfline.jp'] = ' SurfLine';
$isp_list['%.soka.jp'] = '創価学会';
$isp_list['%.town.kamikawa.hyogo.jp'] = '神河町';
$isp_list['%.tose-tokyo.jp'] = 'トーセ';
$isp_list['%.tims.jp'] = '東芝情報システム';
$isp_list['%.ubsecure.jp'] = 'ユービーセキュア';
$isp_list['%.valuecore.jp'] = ' バリューコア';
$isp_list['%.xcon.jp'] = 'クロスコネクト新潟';
$isp_list['%.ybnet.jp'] = '矢島町役場　企画商工観光課';
$isp_list['%.yoshizou.jp'] = ' 吉蔵エックスワイゼットソリューションズ';
$isp_list['%.zoot.jp'] = 'インターリンク';

//.lg.jp
$isp_list['%.toyama.lg.jp'] = '富山県';
$isp_list['%.nasushiobara.lg.jp'] = '那須塩原市役所';

//.go.jp
$isp_list['%.env.go.jp'] = '環境省';
$isp_list['%.kishou.go.jp'] = '気象庁';
$isp_list['%.mod.go.jp'] = '防衛省';
$isp_list['%.riken.go.jp'] = '理化学研究所';

$isp_list['%.ktr.mlit.go.jp'] = '国土交通省';
$isp_list['%.mlit.go.jp'] = '国土交通省';
$isp_list['%.jaea.go.jp'] = '日本原子力研究開発機構';
//.info
$isp_list['%.sbb-sys.info'] = 'ソフトバンクBB';

//.net
$isp_list['%.0038.net'] = 'フュージョン・コミュニケーションズ';
$isp_list['%.2iij.net'] = 'インターネットイニシアティブ';

$isp_list['%.asua.net'] = 'アスア';
$isp_list['%.bitcat.net'] = 'livedoor';
$isp_list['%.cilas.net'] = 'ドリーム・トレイン・インターネット';
$isp_list['%.cyerry.net'] = '岡山県久米郡美咲町';
$isp_list['%.datacoa.net'] = 'データコア';
$isp_list['%.data-hotel.net'] = 'ライブドア';
$isp_list['%.eaccess.net'] = 'イー・アクセス';
$isp_list['%.fiberbit.net'] = 'エフビットコミニュケーションズ';
$isp_list['%.freebit.net'] = 'フリービット';
$isp_list['%.fuji-f.net'] = 'フジミック';
$isp_list['%.giganet.net'] = 'オンキヨーエンターテインメントテクノロジー';
$isp_list['%.hokkai.net'] = 'ホッカイ・ネット';
$isp_list['%.humeia.net'] = 'ヒューメイア';
$isp_list['%.hi-hoinc.net'] = 'ハイホー';
$isp_list['%.il24.net'] = 'インターリンク';
$isp_list['%.inter.net'] = 'GMOマネージドホスティング';
$isp_list['%.interq.net'] = 'グローバルメディアオンライン';
$isp_list['%.isao.net'] = 'ISAO';
$isp_list['%.netaro.net'] = 'ねたろードットねっと';
$isp_list['%.net3-tv.net'] = 'ティエイエムインターネットサービス';
$isp_list['%.kagoya.net'] = 'カゴヤ・ジャパン';
$isp_list['%.psi.net'] = 'PSINet，Inc';
$isp_list['%.secomtrust.net'] = 'セコムトラストシステムズ';
$isp_list['%.uu.net'] = 'UUNET Technologies Inc.';
$isp_list['%.vbnet.net'] = 'VBNET Inc.';
$isp_list['%.vrtc.net'] = ' ブイ・アール・テクノセンター';
$isp_list['%.zero-isp.NET'] = 'ＧＭＯインターネット';
$isp_list['%.hinet.net'] = 'HINET';

//.com
$isp_list['%.104.com'] = 'イチレイヨン';
$isp_list['%.7-dj.com'] = '富士通青森システムエンジニアリング';
$isp_list['%.cisco.com'] = 'Cisco Systems， Inc.';
$isp_list['%.digi-rock.com'] = 'デジロック';
$isp_list['%.dososhin.com'] = '道祖神';
$isp_list['%.e-realize.com'] = 'リアライズ';
$isp_list['%.ejworks.com'] = 'イージェーワークス';
$isp_list['%.fj-inetspc.com'] = '富士通四国システムズ';
$isp_list['%.freejpn.com'] = 'シークネット';
$isp_list['%.f-regi.com'] = 'フューチャーコマース';
$isp_list['%.fujitsu.com'] = '富士通関西中部ネットテック';
$isp_list['%.future-s.com'] = 'フューチャースピリッツ';
$isp_list['%.gamania.com'] = 'ガマニアデジタルエンターテインメント';
$isp_list['%.gaiax.com'] = 'ガイアックス';
$isp_list['%.george24.com'] = 'インボイス';
$isp_list['%.hamanasu.com'] = 'はまなすインフォメーション';
$isp_list['%.hitachi-cnet.com'] = '日立電線ネットワークス';
$isp_list['%.hp.com'] = 'hewlett-packard company';
$isp_list['%.ibm.com'] = 'International Business Machines Corporation';
$isp_list['%.inetcore.com'] = 'インテック・ネットコア';
$isp_list['%.iplanet-inc.com'] = 'アイプラネット';
$isp_list['%.ip-core.com'] = 'アイピーコア';
$isp_list['%.j-bee.com'] = '富士通鹿児島インフォネット';
$isp_list['%.jp.att.com'] = 'AT&Tジャパン';
$isp_list['%.kansai-bb.com'] = '関西ブロードバンド';
$isp_list['%.kddi.com'] = 'KDDI';
$isp_list['%.kocoe.com'] = '神戸コミュニティ・エクスチェンジ';
$isp_list['%.k-opti.com'] = 'ケイ・オプティコム';
$isp_list['%.livedoor.com'] = 'ライブドア';
$isp_list['%.mediatti.com'] = 'メディアッティ・コミュニケーションズ';
$isp_list['%.motorola.com'] = 'Motorola， Inc.';
$isp_list['%.mizuho-sc.com'] = 'みずほ証券';
$isp_list['%.nifty.com'] = 'ニフティ';
$isp_list['%.nri-net.com'] = 'エヌ・アール・アイ・ネットワークコミュニケーションズ';
$isp_list['%.nttbiz.com'] = 'エヌ・ティ・ティ・ビズリンク';
$isp_list['%.orange-ftgroup.com'] = 'イクアント・ジャパン';
$isp_list['%.pccwglobal.com'] = 'ピーシーシーダブリュー・グローバル・ジャパン';
$isp_list['%.rel-ltd.com'] = 'RELATION';
$isp_list['%.square-enix.com'] = 'スクウェア・エニックス';
$isp_list['%.xrea.com'] = 'デジロック';
$isp_list['%.xrea.jp'] = 'デジロック';

//biz
$isp_list['%.genoa.biz'] = 'ジェノア';

//不明
$isp_list['%.tokyo-bay.ne.jp'] = '安房運輸';
$isp_list['%.coms.ne.jp'] = '日本コムスペック';
$isp_list['%.lancenet.or.jp'] = 'ランス';
$isp_list['%.intership.ne.jp'] = '松戸市コンピュータサービス'; //凍結

//.ac.jp
//大学
//--北海道ここから-->
$isp_list['%.asahikawa-med.ac.jp'] = '旭川医科大学';
$isp_list['%.otaru-uc.ac.jp'] = '小樽商科大学';
$isp_list['%.obihiro.ac.jp'] = '帯広畜産大学';
$isp_list['%.kitami-it.ac.jp'] = '北見工業大学';
$isp_list['%.hokudai.ac.jp'] = '北海道大学';
$isp_list['%.hokkyodai.ac.jp'] = '北海道教育大学';
$isp_list['%.muroran-it.ac.jp'] = '室蘭工業大学';
$isp_list['%.kushiro-pu.ac.jp'] = '釧路公立大学';
$isp_list['%.fun.ac.jp'] = '公立はこだて未来大学';
$isp_list['%.sapmed.ac.jp'] = '札幌医科大学';
$isp_list['%.scu.ac.jp'] = '札幌市立大学';
$isp_list['%.nayoro.ac.jp'] = '名寄市立大学';
$isp_list['%.asahikawa-u.ac.jp'] = '旭川大学';
$isp_list['%.sapporo-u.ac.jp'] = '札幌大学';
$isp_list['%.sapporo-otani.ac.jp'] = '札幌大谷大学';
$isp_list['%.sgu.ac.jp'] = '札幌学院大学';
$isp_list['%.siu.ac.jp'] = '札幌国際大学';
$isp_list['%.seisa.ac.jp'] = '星槎大学';
$isp_list['%.chitose.ac.jp'] = '千歳科学技術大学';
$isp_list['%.tenshi.ac.jp'] = '天使大学';
$isp_list['%.bioindustry.nodai.ac.jp'] = '東京農業大学（オホーツクキャンパス）'; //n
$isp_list['%.tus.ac.jp'] = '東京理科大学（長万部キャンパス）'; //n
$isp_list['%.dohto.ac.jp'] = '道都大学';
$isp_list['%.t-komazawa.ac.jp'] = '苫小牧駒澤大学';
$isp_list['%.rchokkaido-cn.ac.jp'] = '日本赤十字北海道看護大学';
$isp_list['%.hakodate-u.ac.jp'] = '函館大学';
$isp_list['%.fujijoshi.ac.jp'] = '藤女子大学';
$isp_list['%.hokusho-u.ac.jp'] = '北翔大学';
$isp_list['%.hokusei.ac.jp'] = '北星学園大学';
$isp_list['%.hokkai-s-u.ac.jp'] = '北海学園大学';
$isp_list['%.hokkai.ac.jp'] = '北海商科大学';
$isp_list['%.hoku-iryo-u.ac.jp'] = '北海道医療大学';
$isp_list['%.hit.ac.jp'] = '北海道工業大学';
$isp_list['%.do-johodai.ac.jp'] = '北海道情報大学';
$isp_list['%.do-bunkyodai.ac.jp'] = '北海道文教大学';
$isp_list['%.hokuyakudai.ac.jp'] = '北海道薬科大学';
$isp_list['%.rakuno.ac.jp'] = '酪農学園大学';
$isp_list['%.wakhok.ac.jp'] = '稚内北星学園大学';
$isp_list['%.sapporo-otani.ac.jp'] = '札幌大谷大学';
$isp_list['%.otaru-jc.ac.jp'] = '小樽短期大学';
$isp_list['%.oojc.ac.jp'] = '帯広大谷短期大学';
$isp_list['%.midorigaoka.ac.jp'] = '釧路短期大学';
$isp_list['%.koen.ac.jp'] = '光塩学園女子短期大学';
$isp_list['%.kokugakuin-jc.ac.jp'] = '國學院短期大学';
$isp_list['%.senshu-hc.ac.jp'] = '専修大学北海道短期大学';
$isp_list['%.takushoku-hc.ac.jp'] = '拓殖大学北海道短期大学';
$isp_list['%.hakodate-otani.ac.jp'] = '函館大谷短期大学';
$isp_list['%.hakodate-jc.ac.jp'] = '函館短期大学';
$isp_list['%.bwmjc.ac.jp'] = '文化女子大学室蘭短期大学';
$isp_list['%.haec.ac.jp'] = '北海道自動車短期大学';
$isp_list['%.musashi-jc.ac.jp'] = '北海道武蔵女子短期大学';
//--北海道ここまで-->
//--青森ここから-->
$isp_list['%.hirosaki-u.ac.jp'] = '弘前大学';
$isp_list['%.auhw.ac.jp'] = '青森県立保健大学';
$isp_list['%.nebuta.ac.jp'] = '青森公立大学';
$isp_list['%.aomori-u.ac.jp'] = '青森大学';
$isp_list['%.aomoricgu.ac.jp'] = '青森中央学院大学';
$isp_list['%vmas.kitasato-u.ac.jp'] = '北里大学（十和田キャンパス）';
$isp_list['%.tojo.ac.jp'] = '東北女子大学';
$isp_list['%.hachinohe-u.ac.jp'] = '八戸大学';
$isp_list['%.hi-tech.ac.jp'] = '八戸工業大学';
$isp_list['%.hirogaku-u.ac.jp'] = '弘前学院大学';
$isp_list['%.aomori-akenohoshi.ac.jp'] = '青森明の星短期大学';
$isp_list['%.chutan.ac.jp'] = '青森中央短期大学';
$isp_list['%.toutan.ac.jp'] = '東北女子短期大学';
$isp_list['%.hwjc.jp'] = '弘前福祉短期大学';
$isp_list['%.hachinohe-ct.ac.jp'] = '八戸工業高等専門学校';

//--青森ここまで-->
//--岩手ここから-->
$isp_list['%.iwate-u.ac.jp'] = '岩手大学';
$isp_list['%.iwate-pu.ac.jp'] = '岩手県立大学';
$isp_list['%.iwate-med.ac.jp'] = '岩手医科大学';
$isp_list['%.fujidai.net'] = '富士大学';
$isp_list['%.morioka-u.ac.jp'] = '盛岡大学';
$isp_list['%.iwate-nurse.ac.jp'] = '岩手看護短期大学';

$isp_list['%.shuko.ac.jp'] = '修紅短期大学';
//--岩手ここまで-->
//--宮城ここから-->
$isp_list['%.tohoku.ac.jp'] = '東北大学';
$isp_list['130.34.%'] = '東北大学';
$isp_list['%.prc.miyakyo-u.ac.jp'] = '宮城教育大学';
$isp_list['%.myu.ac.jp'] = '宮城大学';
$isp_list['%.isenshu-u.ac.jp'] = '石巻専修大学';
$isp_list['%.sendaidaigaku.jp'] = '仙台大学';
$isp_list['%.sendai-shirayuri.ac.jp'] = '仙台白百合女子大学';
$isp_list['%.tohoku-gakuin.ac.jp'] = '東北学院大学';
$isp_list['%.tfu.ac.jp'] = '東北福祉大学';
$isp_list['%.tbgu.ac.jp'] = '東北文化学園大学';
$isp_list['%.shokei.ac.jp'] = '尚絅学院大学';
$isp_list['%.tohtech.ac.jp'] = '東北工業大学';
$isp_list['%.mishima.ac.jp'] = '東北生活文化大学';
$isp_list['%.tohoku-pharm.ac.jp'] = '東北薬科大学';
$isp_list['%.mgu.ac.jp'] = '宮城学院女子大学';
$isp_list['%.seiwagakuen.jp'] = '聖和学園短期大学';
$isp_list['%.miyagi-seishin.ac.jp'] = '宮城誠真短期大学';
$isp_list['%.jc-21.ac.jp'] = '東北電子専門学校';
//--宮城ここまで-->
//--秋田ここから-->
$isp_list['%.akita-u.ac.jp'] = '秋田大学';

$isp_list['%.akita-pu.ac.jp'] = '秋田県立大学';
$isp_list['%.aiu.ac.jp'] = '国際教養大学';
$isp_list['%.well.ac.jp'] = '秋田看護福祉大学';
$isp_list['%.nau.ac.jp'] = 'ノースアジア大学';
$isp_list['%.amcac.ac.jp'] = '秋田公立美術工芸短期大学';
$isp_list['%.seirei-wjc.ac.jp'] = '聖霊女子短期大学';
$isp_list['%.rcakita-jc.ac.jp'] = '日本赤十字秋田短期大学';
$isp_list['%.misono-jc.ac.jp'] = '聖園学園短期大学';
//--秋田ここまで-->
//--山形ここから-->
$isp_list['%.yamagata-u.ac.jp'] = '山形大学';
$isp_list['%.yachts.ac.jp'] = '山形県立保健医療大学';
$isp_list['%.tuad.ac.jp'] = '東北芸術工科大学';
$isp_list['%.koeki-u.ac.jp'] = '東北公益文科大学';
$isp_list['%.yone.ac.jp'] = '山形県立米沢女子短期大学';
$isp_list['%.uyo.ac.jp'] = '羽陽学園短期大学';
$isp_list['%.yamagata-jc.ac.jp'] = '山形短期大学';
//--山形ここまで-->
//--福島ここから-->
$isp_list['%.fukushima-u.ac.jp'] = '福島大学';
$isp_list['%.u-aizu.ac.jp'] = '会津大学';
$isp_list['%.fmu.ac.jp'] = '福島県立医科大学';
$isp_list['%.iwakimu.ac.jp'] = 'いわき明星大学';
$isp_list['%.ohu-u.ac.jp'] = '奥羽大学';
$isp_list['%.koriyama-kgc.ac.jp'] = '郡山女子大学';
$isp_list['%.ce.nihon-u.ac.jp'] = '日本大学（工学部）';
$isp_list['%.tonichi-kokusai-u.ac.jp'] = '東日本国際大学';
$isp_list['%.fukushima-college.com'] = '福島学院大学';
$isp_list['%.tonichi-kokusai-u.ac.jp'] = 'いわき短期大学';
$isp_list['%.ssg.ac.jp'] = '桜の聖母短期大学';
//--福島ここまで-->
//--茨城ここから-->
$isp_list['%.ibaraki.ac.jp'] = '茨城大学';
$isp_list['%.tsukuba.ac.jp'] = '筑波大学';
$isp_list['%.tsukuba-tech.ac.jp'] = '筑波技術大学';
$isp_list['%.ipu.ac.jp'] = '茨城県立医療大学';
$isp_list['%.icc.ac.jp'] = '茨城キリスト教大学';
$isp_list['%.ktt.ac.jp'] = 'つくば国際大学';
$isp_list['%.tsukuba-g.ac.jp'] = '筑波学院大学';
$isp_list['%.tokiwa.ac.jp'] = '常磐大学';
$isp_list['%.rku.ac.jp'] = '流通経済大学';
$isp_list['%.taisei.ac.jp'] = '茨城女子短期大学';
$isp_list['%.mitotandai.ac.jp'] = '水戸短期大学';
//--茨城ここまで-->
//--栃木ここから-->
$isp_list['%.utsunomiya-u.ac.jp'] = '宇都宮大学';
$isp_list['%.ashitech.ac.jp'] = '足利工業大学';
$isp_list['%.kyowa-u.ac.jp'] = '宇都宮共和大学';
$isp_list['%.iuhw.ac.jp'] = '国際医療福祉大学';
$isp_list['%.sakushin.ac.jp'] = '作新学院大学';
$isp_list['%.jichi.ac.jp'] = '自治医科大学';
$isp_list['%.dokkyomed.ac.jp'] = '獨協医科大学';
$isp_list['%.hakuoh.ac.jp'] = '白鴎大学';
$isp_list['%.bunsei.ac.jp'] = '文星芸術大学';
$isp_list['%.ashikaga.ac.jp'] = '足利短期大学';
$isp_list['%.ujc.ac.jp'] = '宇都宮短期大学';
$isp_list['%.kokugakuintochigi.ac.jp'] = '國學院大學栃木短期大学';
$isp_list['%.sakushin-u.ac.jp'] = '作新学院大学女子短期大学部';
$isp_list['%.sano-c.ac.jp'] = '佐野短期大学';
//--栃木ここまで-->
//--群馬ここから-->
$isp_list['%.gunma-u.ac.jp'] = '群馬大学';
$isp_list['%.tcue.ac.jp'] = '高崎経済大学';
$isp_list['%.maebashi-it.ac.jp'] = '前橋工科大学';
$isp_list['%.gchs.ac.jp'] = '群馬県立県民健康科学大学';
$isp_list['%.gpwu.ac.jp'] = '群馬県立女子大学';
$isp_list['%.kanto-gakuen.ac.jp'] = '関東学園大学';
$isp_list['%.kyoai.ac.jp'] = '共愛学園前橋国際大学';
$isp_list['%.kiryu-jc.ac.jp'] = '桐生大学';
$isp_list['%.shoken-gakuen.ac.jp'] = '群馬社会福祉大学';
$isp_list['%.paz.ac.jp'] = '群馬パース大学';
$isp_list['%.jobu.ac.jp'] = '上武大学';
$isp_list['%.souzou.ac.jp'] = '創造学園大学';
$isp_list['%.takasaki-u.ac.jp'] = '高崎健康福祉大学';
$isp_list['%.tuc.ac.jp'] = '高崎商科大学';
$isp_list['%.tokyo-fukushi.ac.jp'] = '東京福祉大学';
$isp_list['%.ikuei-g.ac.jp'] = '育英短期大学';
$isp_list['%.shorei.ac.jp'] = '群馬松嶺福祉短期大学';
$isp_list['%.tacc.ac.jp'] = '高崎芸術短期大学';
$isp_list['%.niitan.jp'] = '新島学園短期大学';
$isp_list['%.hirakatagakuen.ac.jp'] = '明和学園短期大学';
$isp_list['%.yamasaki.ac.jp'] = '山崎学園グループ';
//--群馬ここまで-->
//--埼玉ここから-->
$isp_list['%.saitama-u.ac.jp'] = '埼玉大学';
$isp_list['%.spu.ac.jp'] = '埼玉県立大学';
$isp_list['%.urawa.ac.jp'] = '浦和大学';
$isp_list['%.omiyalaw.ac.jp'] = '大宮法科大学院大学';
$isp_list['%.kyoei.ac.jp'] = '共栄大学';
$isp_list['%.saitama-med.ac.jp'] = '埼玉医科大学';
$isp_list['%.saigaku.ac.jp'] = '埼玉学園大学';
$isp_list['%.sit.ac.jp'] = '埼玉工業大学';
$isp_list['%.josai.ac.jp'] = '城西大学';
$isp_list['%.surugadai.ac.jp'] = '駿河台大学';
$isp_list['%.seigakuin.jp'] = '聖学院大学';
$isp_list['%.jumonji-u.ac.jp'] = '十文字学園女子大学';
$isp_list['%.bunri-c.ac.jp'] = '西武文理大学';
$isp_list['%.tiu.ac.jp'] = '東京国際大学';
$isp_list['%.toho-music.ac.jp'] = '東邦音楽大学';
$isp_list['%.dokkyo.ac.jp'] = '獨協大学';
$isp_list['%.nims.ac.jp'] = '日本医療科学大学';
$isp_list['%.nit.ac.jp'] = '日本工業大学';
$isp_list['%.nihonyakka.jp'] = '日本薬科大学';
$isp_list['%.human.ac.jp'] = '人間総合科学大学';
$isp_list['%.bunkyo.ac.jp'] = '文教大学';
$isp_list['%.hiu.ac.jp'] = '平成国際大学';
$isp_list['%.musashino.ac.jp'] = '武蔵野学院大学';
$isp_list['%.meikai.ac.jp'] = '明海大学';
$isp_list['%.mejiro.ac.jp'] = '目白大学';
$isp_list['%.iot.ac.jp'] = 'ものつくり大学';
$isp_list['%.shobi-u.ac.jp'] = '尚美学園大学';
$isp_list['%.akikusa.ac.jp'] = '秋草学園短期大学';
$isp_list['%.kawaguchi.ac.jp'] = '川口短期大学';
$isp_list['%.kyoei.ac.jp'] = '共栄学園短期大学';
$isp_list['%.kgef.ac.jp'] = '国際学院埼玉短期大学';
$isp_list['%.sai-junshin.ac.jp'] = '埼玉純真女子短期大学';
$isp_list['%.saijo.ac.jp'] = '埼玉女子短期大学';
$isp_list['%.sjc.ac.jp'] = '埼玉短期大学';
$isp_list['%.musashigaoka.ac.jp'] = '武蔵丘短期大学';
$isp_list['%.yamamura-tandai.ac.jp'] = '山村学園短期大学';
$isp_list['%.arsnet.ac.jp'] = 'アルスコンピュータ専門学校';
//--埼玉ここまで-->
//--千葉ここから-->
$isp_list['%.chiba-u.ac.jp'] = '千葉大学';
$isp_list['%.u-air.ac.jp'] = '放送大学';
$isp_list['%.aikoku-u.ac.jp'] = '愛国学園大学';
$isp_list['%.uekusa.ac.jp'] = '植草学園大学';
$isp_list['%.edogawa-u.ac.jp'] = '江戸川大学';
$isp_list['%.kgwu.ac.jp'] = '川村学園女子大学';
$isp_list['%.kandagaigo.ac.jp'] = '神田外語大学';
$isp_list['%.u-keiai.ac.jp'] = '敬愛大学';
$isp_list['%.budo-u.ac.jp'] = '国際武道大学';
$isp_list['%.saniku.ac.jp'] = '三育学院大学';
$isp_list['%.shumei-u.ac.jp'] = '秀明大学';
$isp_list['%.shukutoku.ac.jp'] = '淑徳大学';
$isp_list['%.jiu.ac.jp'] = '城西国際大学';
$isp_list['%.seitoku.ac.jp'] = '聖徳大学';
$isp_list['%.seiwa-univ.ac.jp'] = '清和大学';
$isp_list['%.cis.ac.jp'] = '千葉科学大学';
$isp_list['%.cku.ac.jp'] = '千葉経済大学';
$isp_list['%.it-chiba.ac.jp'] = '千葉工業大学';
$isp_list['%.cuc.ac.jp'] = '千葉商科大学';
$isp_list['%.cgu.ac.jp'] = '中央学院大学';
$isp_list['%.thu.ac.jp'] = '帝京平成大学';
$isp_list['%.tci.ac.jp'] = '東京基督教大学';
$isp_list['%.tdc.ac.jp'] = '東京歯科大学';
$isp_list['%.tuis.ac.jp'] = '東京情報大学';
$isp_list['%.tokyoseitoku.ac.jp'] = '東京成徳大学';
$isp_list['%.toyogakuen-u.ac.jp'] = '東洋学園大学';
$isp_list['%.nihonbashi.ac.jp'] = '日本橋学館大学';
$isp_list['%.ryotokuji-u.ac.jp'] = '了徳寺大学';
$isp_list['%.reitaku-u.ac.jp'] = '麗澤大学';
$isp_list['%.wayo.ac.jp'] = '和洋女子大学';
$isp_list['%.edotan.jp'] = '江戸川短期大学';
$isp_list['%.showagakuin.ac.jp'] = '昭和学院短期大学';
$isp_list['%.seiwa-jc.ac.jp'] = '清和大学短期大学部';
$isp_list['%.chiba-kc.ac.jp'] = '千葉経済大学短期大学部';
$isp_list['%.chibameitoku.ac.jp'] = '千葉明徳短期大学';
$isp_list['%.thjc.jp'] = '帝京平成看護短期大学';
$isp_list['%.tmc-ipd.ac.jp'] = '東京経営短期大学';
$isp_list['%.tyg.jp'] = '東洋女子短期大学';
$isp_list['%.jc.ac.jp'] = '日本基督教短期大学';
//--千葉ここまで-->
//--神奈川ここから--> '神奈川県'
$isp_list['%.ynu.ac.jp'] = '横浜国立大学';
$isp_list['%.soken.ac.jp'] = '総合研究大学院大学';
$isp_list['%.kuhs.ac.jp'] = '神奈川県立保健福祉大学';
$isp_list['%.yokohama-cu.ac.jp'] = '横浜市立大学';
$isp_list['%.azabu-u.ac.jp'] = '麻布大学';
$isp_list['%.kanagawa-u.ac.jp'] = '神奈川大学';
$isp_list['%.kanagawa-it.ac.jp'] = '神奈川工科大学';
$isp_list['%.kdcnet.ac.jp'] = '神奈川歯科大学';
$isp_list['%.kamakura-u.ac.jp'] = '鎌倉女子大学';
$isp_list['%.kanto-gakuin.ac.jp'] = '関東学院大学';
$isp_list['%.sagami-wu.ac.jp'] = '相模女子大学';
$isp_list['%.sanno.ac.jp'] = '産業能率大学';
$isp_list['%.shoin-u.ac.jp'] = '松蔭大学';
$isp_list['%.shonan-it.ac.jp'] = '湘南工科大学';
$isp_list['%.marianna-u.ac.jp'] = '聖マリアンナ医科大学';
$isp_list['%.senzoku.ac.jp'] = '洗足学園音楽大学';
$isp_list['%.sbi-u.ac.jp'] = 'SBI大学院大学';
$isp_list['%.tsurumi-u.ac.jp'] = '鶴見大学';
$isp_list['%.dcu.ac.jp'] = '田園調布学園大学';
$isp_list['%.toin.ac.jp'] = '桐蔭学園';
$isp_list['%.toyoeiwa.ac.jp'] = '東洋英和女学院大学';
$isp_list['%.ferris.ac.jp'] = 'フェリス女学院大学';
$isp_list['%.study.jp'] = '八洲学園大学';
$isp_list['%.shodai.ac.jp'] = '横浜商科大学';
$isp_list['%.hamayaku.jp'] = '横浜薬科大学';
$isp_list['%.iisec.jp'] = '情報セキュリティ大学院大学';
$isp_list['%.tosei-showa-music.ac.jp'] = '昭和音楽大学';
$isp_list['%.joshibi.ac.jp'] = '女子美術大学';
$isp_list['%.shonan.bunkyo.ac.jp'] = '文教大学（湘南キャンパス）';
$isp_list['%.kawasaki-nursing-c.ac.jp'] = '川崎市立看護短期大学';
$isp_list['%.izumi-c.ac.jp'] = '和泉短期大学';
$isp_list['%.odawara.ac.jp'] = '小田原女子短期大学';
$isp_list['%.kamakura-u.ac.jp'] = '鎌倉女子大学短期大学部';
$isp_list['%.caritas.ac.jp'] = 'カリタス女子短期大学';
$isp_list['%.shokoku.ac.jp'] = '湘南国際女子短期大学';
$isp_list['%.shonan.ac.jp'] = '湘南短期大学';
$isp_list['%.shohoku.ac.jp'] = '湘北短期大学';
$isp_list['%.cecilia-wjc.ac.jp'] = '聖セシリア女子短期大学';
$isp_list['%.yokotan.ac.jp'] = '横浜女子短期大学';
$isp_list['%.soei.ac.jp'] = '横浜創英短期大学';
$isp_list['%.yokohama-art.ac.jp'] = '横浜美術短期大学';

//--神奈川ここまで-->
//--東京ここから-->
$isp_list['%.ocha.ac.jp'] = 'お茶の水女子大学';
$isp_list['%.uec.ac.jp'] = '電気通信大学';
$isp_list['%.u-tokyo.ac.jp'] = '東京大学';
$isp_list['%.tmd.ac.jp'] = '東京医科歯科大学';
$isp_list['%.tufs.ac.jp'] = '東京外国語大学';
$isp_list['%.kaiyodai.ac.jp'] = '東京海洋大学';
$isp_list['%.u-gakugei.ac.jp'] = '東京学芸大学';
$isp_list['%.geidai.ac.jp'] = '東京芸術大学';
$isp_list['%.titech.ac.jp'] = '東京工業大学';
$isp_list['%.tuat.ac.jp'] = '東京農工大学';
$isp_list['%.hit-u.ac.jp'] = '一橋大学';
$isp_list['%.grips.ac.jp'] = '政策研究大学院大学';
$isp_list['%.tmu.ac.jp'] = '首都大学東京';
$isp_list['%.aiit.ac.jp'] = '産業技術大学院大学';
$isp_list['%.aoyama.ac.jp'] = '青山学院大学';
$isp_list['%.asia-u.ac.jp'] = '亜細亜大学';
$isp_list['%.atomi.ac.jp'] = '跡見学園女子大学';
$isp_list['%.uenogakuen.ac.jp'] = '上野学園大学';
$isp_list['%.toho-univ.ac.jp'] = '映画専門大学院大学';
$isp_list['%.obirin.ac.jp'] = '桜美林大学';
$isp_list['%.otsuma.ac.jp'] = '大妻女子大学';
$isp_list['%.o-hara.ac.jp'] = '大原大学院大学';
$isp_list['%.kaetsu.ac.jp'] = '嘉悦大学';
$isp_list['%.gakushuin.ac.jp'] = '学習院大学';
$isp_list['%.kitasato-u.ac.jp'] = '北里大学';
$isp_list['%.kyoritsu-wu.ac.jp'] = '共立女子大学';
$isp_list['%.kyorin-u.ac.jp'] = '杏林大学';
$isp_list['%.globis.ac.jp'] = 'グロービス経営大学院大学';
$isp_list['%.keisen.ac.jp'] = '恵泉女学園大学';
$isp_list['%.kunitachi.ac.jp'] = '国立音楽大学';
$isp_list['%.keio.ac.jp'] = '慶應義塾大学';
$isp_list['%.kogakuin.ac.jp'] = '工学院大学';
$isp_list['%.kokugakuin.ac.jp'] = '國學院大学';
$isp_list['%.icu.ac.jp'] = '国際基督教大学';
$isp_list['%.icabs.ac.jp'] = '国際仏教学大学院大学';
$isp_list['%.kokushikan.ac.jp'] = '国士館大学';
$isp_list['%.komazawa-u.ac.jp'] = '駒澤大学';
$isp_list['%.komajo.ac.jp'] = '駒沢女子大学';
$isp_list['%.shibaura-it.ac.jp'] = '芝浦工業大学';
$isp_list['%.juntendo.ac.jp'] = '順天堂大学';
$isp_list['%.sophia.ac.jp'] = '上智大学';
$isp_list['%.showa-u.ac.jp'] = '昭和大学';
$isp_list['%.swu.ac.jp'] = '昭和女子大学';
$isp_list['%.shoyaku.ac.jp'] = '昭和薬科大学';
$isp_list['%.eiyo.ac.jp'] = '女子栄養大学';
$isp_list['%.shiraume.ac.jp'] = '白梅学園大学';
$isp_list['%.shirayuri.ac.jp'] = '白百合女子大学';
$isp_list['%.sugino-fc.ac.jp'] = '杉野服飾大学';
$isp_list['%.seikei.ac.jp'] = '成蹊大学';
$isp_list['%.seijo.ac.jp'] = '成城大学';
$isp_list['%.u-sacred-heart.ac.jp'] = '聖心女子大学';
$isp_list['%.seisen-u.ac.jp'] = '清泉女子大学';
$isp_list['%.seibo-jcn.ac.jp'] = '聖母大学';
$isp_list['%.slcn.ac.jp'] = '聖路加看護大学';
$isp_list['%.senshu-u.ac.jp'] = '専修大学';
$isp_list['%.soka.ac.jp'] = '創価大学';
$isp_list['%.tais.ac.jp'] = '大正大学';
$isp_list['%.daito.ac.jp'] = '大東文化大学';
$isp_list['%.takachiho.ac.jp'] = '高千穂大学';
$isp_list['%.takushoku-u.ac.jp'] = '拓殖大学';
$isp_list['%.tama.ac.jp'] = '多摩大学';
$isp_list['%.tamagawa.ac.jp'] = '玉川大学';
$isp_list['%.tamabi.ac.jp'] = '多摩美術大学';
$isp_list['%.chuo-u.ac.jp'] = '中央大学';
$isp_list['%.tsuda.ac.jp'] = '津田塾大学';
$isp_list['%.teikyo-u.ac.jp'] = '帝京大学';
$isp_list['%.dhw.co.jp'] = 'デジタルハリウッド大学';
$isp_list['%.tuj.ac.jp'] = 'テンプル大学';
$isp_list['%.u-tokai.ac.jp'] = '東海大学';
$isp_list['%.tokyo-med.ac.jp'] = '東京医科大学';
$isp_list['%.thcu.ac.jp'] = '東京医療保健大学';
$isp_list['%.tokyo-ondai.ac.jp'] = '東京音楽大学';
$isp_list['%.tokyo-kasei.ac.jp'] = '東京家政大学';
$isp_list['%.kasei-gakuin.ac.jp'] = '東京家政学院大学';
$isp_list['%.tku.ac.jp'] = '東京経済大学';
$isp_list['%.teu.ac.jp'] = '東京工科大学';
$isp_list['%.t-kougei.ac.jp'] = '東京工芸大学';
$isp_list['%.jikei.ac.jp'] = '東京慈恵会医科大学';
$isp_list['%.t-junshin.ac.jp'] = '東京純心女子大学';
$isp_list['%.tjk.ac.jp'] = '東京女学館大学';
$isp_list['%.twcu.ac.jp'] = '東京女子大学';
$isp_list['%.twmu.ac.jp'] = '東京女子医科大学';
$isp_list['%.twcpe.ac.jp'] = '東京女子体育大学';
$isp_list['%.tuts.ac.jp'] = '東京神学大学';
$isp_list['%.tsc-05.ac.jp'] = '東京聖栄大学';
$isp_list['%.tsc.ac.jp'] = '東京成徳大学';
$isp_list['%.zokei.ac.jp'] = '東京造形大学';
$isp_list['%.dendai.ac.jp'] = '東京電機大学';
$isp_list['%.tcu.ac.jp'] = '東京都市大学';
$isp_list['%.nodai.ac.jp'] = '東京農業大学';
$isp_list['%.fuji.ac.jp'] = '東京富士大学';
$isp_list['%.tokyomirai.ac.jp'] = '東京未来大学';
$isp_list['%.toyaku.ac.jp'] = '東京薬科大学';
$isp_list['%.sut.ac.jp'] = '東京理科大学';
$isp_list['%.toho-u.ac.jp'] = '東邦大学';
$isp_list['%.tohomusic.ac.jp'] = '桐朋学園大学';
$isp_list['%.toyo.ac.jp'] = '東洋大学';
$isp_list['%.nishogakusha-u.ac.jp'] = '二松学舎大学';
$isp_list['%.nihon-u.ac.jp'] = '日本大学';
$isp_list['%.nms.ac.jp'] = '日本医科大学';
$isp_list['%.kyoiku-u.jp'] = '日本教育大学院大学';
$isp_list['%.ndu.ac.jp'] = '日本歯科大学';
$isp_list['%.jcsw.ac.jp'] = '日本社会事業大学';
$isp_list['%.jwu.ac.jp'] = '日本女子大学';
$isp_list['%.jwcpe.ac.jp'] = '日本女子体育大学';
$isp_list['%.nvlu.ac.jp'] = '日本獣医生命科学大学';
$isp_list['%.redcross.ac.jp'] = '日本赤十字看護大学';
$isp_list['%.nittai.ac.jp'] = '日本体育大学';
$isp_list['%.dento-iryo.ac.jp'] = '日本伝統医療科学大学院大学';
$isp_list['%.nihonbunka-u.ac.jp'] = '日本文化大学';
$isp_list['%.hollywood.ac.jp'] = '日本ハリウッド大学院大学';
$isp_list['%.bunka.ac.jp'] = '文化女子大学';
$isp_list['%.bfgu-bunka.ac.jp'] = '文化ファッション大学院大学';
$isp_list['%.u-bunkyo.ac.jp'] = '文京学院大学';
$isp_list['%.hosei.ac.jp'] = '法政大学';
$isp_list['%.hoshi.ac.jp'] = '星薬科大学';
$isp_list['%.musashi.ac.jp'] = '武蔵大学';
$isp_list['%.musashino-u.ac.jp'] = '武蔵野大学';
$isp_list['%.musashino-music.ac.jp'] = '武蔵野音楽大学';
$isp_list['%.musabi.ac.jp'] = '武蔵野美術大学';
$isp_list['%.meiji.ac.jp'] = '明治大学';
$isp_list['%.meijigakuin.ac.jp'] = '明治学院大学';
$isp_list['%.my-pharm.ac.jp'] = '明治薬科大学';
$isp_list['%.mejiro.ac.jp'] = '目白大学';
$isp_list['%.meisei-u.ac.jp'] = '明星大学';
$isp_list['%.rikkyo.ne.jp'] = '立教大学';
$isp_list['%.ris.ac.jp'] = '立正大学';
$isp_list['%.luther.ac.jp'] = 'ルーテル学院大学';
$isp_list['%.lec.ac.jp'] = 'ＬＥＣ東京リーガルマインド大学';
$isp_list['%.wako.ac.jp'] = '和光大学';
$isp_list['%.waseda.ac.jp'] = '早稲田大学';
$isp_list['%.metro-u.ac.jp'] = '東京都立大学';//閉鎖予定
$isp_list['%.tmit.ac.jp'] = '東京都立科学技術大学';
$isp_list['%.metro-hs.ac.jp'] = '東京都立保健科学大学';
$isp_list['%.jissen.ac.jp'] = '実践女子大学';
$isp_list['%.tmca.ac.jp'] = '東京都立短期大学';
$isp_list['%.kokutan.net'] = '国際短期大学';
$isp_list['%.sugino.ac.jp'] = '杉野服飾大学短期大学部';
$isp_list['%.seibi.ac.jp'] = '星美学園短期大学';
$isp_list['%.meisen.ac.jp'] = '鶴川女子短期大学';
$isp_list['%.teikyo-jc.ac.jp'] = '帝京短期大学';
$isp_list['%.toita.ac.jp'] = '戸板女子短期大学';
$isp_list['%.ttc.u-tokai.ac.jp'] = '東海大学短期大学部［高輪］';
$isp_list['%.hosho.ac.jp'] = '東京交通短期大学';
$isp_list['%.tanakachiyo.ac.jp'] = '東京田中短期大学';
$isp_list['%.tokyobunka.ac.jp'] = '東京文化短期大学';
$isp_list['%.tokyorissho.ac.jp'] = '東京立正短期大学';
$isp_list['%.toho.ac.jp'] = '桐朋学園芸術短期大学';
$isp_list['%.toyoko.ac'] = '東横学園女子短期大学';
$isp_list['%.musashino-jrc.ac.jp'] = '日本赤十字武蔵野短期大学';
$isp_list['%.hosen.ac.jp'] = '宝仙学園短期大学';
$isp_list['%.yamazaki.ac.jp'] = 'ヤマザキ動物看護短期大学';
$isp_list['%.yamano.ac.jp'] = '山野美容芸術短期大学';
$isp_list['%.yamawaki-gakuen.ac.jp'] = '山脇学園短期大学';
$isp_list['%.jec.ac.jp'] = '日本電子専門学校';
$isp_list['%.neec.ac.jp'] = '日本工学院';
$isp_list['%.tokyo-ct.ac.jp'] = '国立東京工業専門学校';
//--東京ここまで-->
//--新潟ここから-->
$isp_list['%.niigata-u.ac.jp'] = '新潟大学';
$isp_list['%.nagaokaut.ac.jp'] = '長岡技術科学大学';
$isp_list['%.juen.ac.jp'] = '上越教育大学';
$isp_list['%.unii.ac.jp'] = '新潟県立大学';
$isp_list['%.niigata-cn.ac.jp'] = '新潟県立看護大学';
$isp_list['%.keiwa-c.ac.jp'] = '敬和学園大学';
$isp_list['%.iuj.ac.jp'] = '国際大学';
$isp_list['%.jigyo.ac.jp'] = '事業創造大学院大学';
$isp_list['%.nagaokauniv.ac.jp'] = '長岡大学';
$isp_list['%.nagaoka-id.ac.jp'] = '長岡造形大学';
$isp_list['%.nuhw.ac.jp'] = '新潟医療福祉大学';
$isp_list['%.niigataum.ac.jp'] = '新潟経営大学';
$isp_list['%.niit.ac.jp'] = '新潟工科大学';
$isp_list['%.nuis.ac.jp'] = '新潟国際情報大学';
$isp_list['%.nsu.ac.jp'] = '新潟産業大学';
$isp_list['%.n-seiryo.ac.jp'] = '新潟青陵大学';
$isp_list['%.niigatayakudai.jp'] = '新潟薬科大学';
$isp_list['%.nrgs.ac.jp'] = '新潟リハビリテーション大学院大学';
$isp_list['%.nicol.ac.jp'] = '県立新潟女子短期大学';
$isp_list['%.niigata-ct.ac.jp'] = '新潟工業短期大学';
$isp_list['%.niigatachuoh-jc.ac.jp'] = '新潟中央短期大学';
$isp_list['%.meirin-c.ac.jp'] = '明倫短期大学';
//--新潟ここまで-->
//--富山ここから-->
$isp_list['%.u-toyama.ac.jpl'] = '富山大学';
$isp_list['%.pu-toyama.ac.jp'] = '富山県立大学';
$isp_list['%.tuins.ac.jp'] = '富山国際大学';
$isp_list['%.takaoka.ac.jp'] = '高岡法科大学';
$isp_list['%.pu-toyama.ac.jp'] = '富山県立大学短期大学部';
$isp_list['%.toyama-c.ac.jp'] = '富山短期大学';
$isp_list['%.t-fukushi.urayama.ac.jp'] = '富山福祉短期大学';
$isp_list['%.urayama.ac.jp'] = '浦山学園';
//--富山ここまで-->
//--石川ここから-->
$isp_list['%.kanazawa-u.ac.jp'] = '金沢大学';
$isp_list['%.jaist.ac.jp'] = '北陸先端科学技術大学院大学';
$isp_list['%.ishikawa-nu.ac.jp'] = '石川県立看護大学';
$isp_list['%.kanazawa-bidai.ac.jp'] = '金沢美術工芸大学';
$isp_list['%.kanazawa-med.ac.jp'] = '金沢医科大学';
$isp_list['%.kanazawa-gu.ac.jp'] = '金沢学院大学';
$isp_list['%.kanazawa-it.ac.jp'] = '金沢工業大学';
$isp_list['%.seiryo-u.ac.jp'] = '金沢星稜大学';
$isp_list['%.kinjo.ac.jp'] = '金城大学';
$isp_list['%.hokuriku-u.ac.jp'] = '北陸大学';
$isp_list['%.hokurikugakuin.ac.jp'] = '北陸学院大学';
$isp_list['%.komatsu-c.ac.jp'] = '小松短期大学';
$isp_list['%.seiryo.ac.jp'] = '星陵女子短期大学';
//--石川ここまで-->
//--福井ここから-->
$isp_list['%.fukui-u.ac.jp'] = '福井大学';
$isp_list['%.fpu.ac.jp'] = '福井県立大学';
$isp_list['%.jin-ai.ac.jp'] = '仁愛女子大学';
$isp_list['%.fukui-ut.ac.jp'] = '福井工業大学';
$isp_list['%.tsuruga.ac.jp'] = '敦賀短期大学';
//--福井ここまで-->
//--山梨ここから-->
$isp_list['%.yamanashi.ac.jp'] = '山梨大学';
$isp_list['%.tsuru.ac.jp'] = '都留文科大学';
$isp_list['%.yamanashi-ken.ac.jp'] = '山梨県立大学';
$isp_list['%.kenkoudai.com'] = '健康科学大学';
$isp_list['%.ntu.ac.jp'] = '帝京科学大学';
$isp_list['%.min.ac.jp'] = '身延山大学';
$isp_list['%.y-eiwa.ac.jp'] = '山梨英和大学';
$isp_list['%.ygu.ac.jp'] = '山梨学院大学';
$isp_list['%.ohtsuki.ac.jp'] = '大月短期大学';
$isp_list['%.teikyo-gjc.ac.jp'] = '帝京学園短期大学';
//--山梨ここまで-->
//--長野ここから-->
$isp_list['%.shinshu-u.ac.jp'] = '信州大学';
$isp_list['%.nagano-nurs.ac.jp'] = '長野県看護大学';
$isp_list['%.saku.ac.jp'] = '佐久大学';
$isp_list['%.suwa.sut.ac.jp'] = '諏訪東京理科大学';
$isp_list['%.seisen-jc.ac.jp'] = '清泉女学院大学';
$isp_list['%.nagano.ac.jp'] = '長野大学';
$isp_list['%.matsu.ac.jp'] = '松本大学';
$isp_list['%.mdu.ac.jp'] = '松本歯科大学';
$isp_list['%.nagano-kentan.ac.jp'] = '長野県短期大学';
$isp_list['%.iidawjc.ac.jp'] = '飯田女子短期大学';
$isp_list['%.uedawjc.ac.jp'] = '上田女子短期大学';
$isp_list['%.shintan.ac.jp'] = '信州短期大学';
$isp_list['%.honan.ac.jp'] = '信州豊南短期大学';
$isp_list['%.keitan.ac.jp'] = '長野経済短期大学';
$isp_list['%.nagajo-junior-college.ac.jp'] = '長野女子短期大学';
$isp_list['%.matsutan.ac.jp'] = '松本短期大学';
$isp_list['%.isahaya-cc.ac.jp'] = 'いさはやコンピュータ・カレッジ';
//--長野ここまで-->
//--岐阜ここから-->
$isp_list['%.gifu-u.ac.jp'] = '岐阜大学';
$isp_list['133.66.%'] = '岐阜大学';
$isp_list['%.gifu-cn.ac.jp'] = '岐阜県立看護大学';
$isp_list['%.gifu-pu.ac.jp'] = '岐阜薬科大学';
$isp_list['%.iamas.ac.jp'] = '情報科学芸術大学院大学';
$isp_list['%.asahi-u.ac.jp'] = '朝日大学';
$isp_list['%.jinno.ac.jp'] = '岐阜医療科学大学';
$isp_list['%.gifu-keizai.ac.jp'] = '岐阜経済大学';
$isp_list['%.shotoku.ac.jp'] = '岐阜聖徳学園大学';
$isp_list['%.gijodai.ac.jp'] = '岐阜女子大学';
$isp_list['%.chukyogakuin-u.ac.jp'] = '中京学院大学';
$isp_list['%.chubu-gu.ac.jp'] = '中部学院大学';
$isp_list['%.tokaigakuin-u.ac.jp'] = '東海学院大学';
$isp_list['%.gifu-cwc.ac.jp'] = '岐阜市立女子短期大学';
$isp_list['%.ogaki-tandai.ac.jp'] = '大垣女子短期大学';
$isp_list['%.u-gifu-ms.ac.jp'] = '岐阜医療技術短期大学';
$isp_list['%.shotoku.jp'] = '岐阜聖徳学園大学短期大学部';
$isp_list['%.shogen.ac.jp'] = '正眼短期大学';
$isp_list['%.takayamacollege.ac.jp'] = '高山自動車短期大学';
$isp_list['%.j-chukyo.ac.jp'] = '中京短期大学';
$isp_list['%.nakanihon.ac.jp'] = '中日本自動車短期大学';
//--岐阜ここまで-->
//--静岡ここから-->
$isp_list['%.shizuoka.ac.jp'] = '静岡大学';
$isp_list['%.hama-med.ac.jp'] = '浜松医科大学';
$isp_list['%.u-shizuoka-ken.ac.jp'] = '静岡県立大学';
$isp_list['%.shizuoka-eiwa.ac.jp'] = '静岡英和学院大学';
$isp_list['%.ssu.ac.jp'] = '静岡産業大学';
$isp_list['%.suac.ac.jp'] = '静岡文化芸術大学';
$isp_list['%.sist.ac.jp'] = '静岡理工科大学';
$isp_list['%.tokoha-u.ac.jp'] = '常葉学園大学';
$isp_list['%.suw.ac.jp'] = '静岡福祉大学';
$isp_list['%.seirei.ac.jp'] = '聖隷クリストファー大学';
$isp_list['%.hamamatsu-u.ac.jp'] = '浜松大学';
$isp_list['%.hgu.ac.jp'] = '浜松学院大学';
$isp_list['%.gpi.ac.jp'] = '光産業創成大学院大学';
$isp_list['%.fuji-tokoha-u.ac.jp'] = '富士常葉大学';
$isp_list['%.tokoha-jc.ac.jp'] = '常葉学園短期大学';
$isp_list['%.tokoha.ac.jp'] = '常葉学園';
$isp_list['%.numazu-ct.ac.jp'] = '沼津工業高等専門学校';
$isp_list['%.rad.ac.jp'] = 'ルネサンス・デザイン・アカデミー';
//--静岡ここまで-->
//--愛知ここから-->
$isp_list['%.nagoya-u.ac.jp'] = '名古屋大学';
$isp_list['%.aichi-edu.ac.jp'] = '愛知教育大学';
$isp_list['%.nitech.ac.jp'] = '名古屋工業大学';
$isp_list['%.tut.ac.jp'] = '豊橋技術科学大学';
$isp_list['%.aichi-pu.ac.jp'] = '愛知県立大学';
$isp_list['%.aichi-nurs.ac.jp'] = '愛知県立看護大学';
$isp_list['%.aichi-fam-u.ac.jp'] = '愛知県立芸術大学';
$isp_list['%.nagoya-cu.ac.jp'] = '名古屋市立大学';
$isp_list['%.aichi-u.ac.jp'] = '愛知大学';
$isp_list['%.aichi-med-u.ac.jp'] = '愛知医科大学';
$isp_list['%.aichi-gakuin.ac.jp'] = '愛知学院大学';
$isp_list['163.214.%'] = '愛知学院大学';
$isp_list['%.gakusen.ac.jp'] = '愛知学泉大学';
$isp_list['%.aut.ac.jp'] = '愛知工科大学';
$isp_list['%.aitech.ac.jp'] = '愛知工業大学';
$isp_list['%.asu-group.net'] = '愛知産業大学';
$isp_list['%.aasa.ac.jp'] = '愛知淑徳大学';
$isp_list['%.owari.ac.jp'] = '愛知新城大谷大学';
$isp_list['%.aichi-toho.ac.jp'] = '愛知東邦大学';
$isp_list['%.abu.ac.jp'] = '愛知文教大学';
$isp_list['%.aichi-mizuho.ac.jp'] = '愛知みずほ大学';
$isp_list['%.ohkagakuen-u.ac.jp'] = '桜花学園大学';
$isp_list['%.kinjo-u.ac.jp'] = '金城学院大学';
$isp_list['%.sugiyama-u.ac.jp'] = '椙山女学園大学';
$isp_list['%.seijoh-u.ac.jp'] = '星城大学';
$isp_list['%.daido-it.ac.jp'] = '大同大学';
$isp_list['%.chukyo-u.ac.jp'] = '中京大学';
$isp_list['%.chujo-u.ac.jp'] = '中京女子大学';
$isp_list['%.chubu.ac.jp'] = '中部大学';
$isp_list['%.tokaigakuen-c.ac.jp'] = '東海学園大学';
$isp_list['%.doho.ac.jp'] = '同朋大学';
$isp_list['%.toyota-ti.ac.jp'] = '豊田工業大学';
$isp_list['%.sozo.ac.jp'] = '豊橋創造大学';
$isp_list['%.meion.ac.jp'] = '名古屋音楽大学';
$isp_list['%.nufs.ac.jp'] = '名古屋外国語大学';
$isp_list['%.ngu.jp'] = '名古屋学院大学';
$isp_list['%.nuas.ac.jp'] = '名古屋学芸大学';
$isp_list['%.nagoya-ku.ac.jp'] = '名古屋経済大学';
$isp_list['%.nua.ac.jp'] = '名古屋芸術大学';
$isp_list['%.nagoya-su.ac.jp'] = '名古屋産業大学';
$isp_list['%.nucba.ac.jp'] = '名古屋商科大学';
$isp_list['%.nagoya-wu.ac.jp'] = '名古屋女子大学';
$isp_list['%.nzu.ac.jp'] = '名古屋造形芸術大学';
$isp_list['%.nagoya-bunri.ac.jp'] = '名古屋文理大学';
$isp_list['%.nanzan-u.ac.jp'] = '南山大学';
$isp_list['%.rctoyota.ac.jp'] = '日本赤十字豊田看護大学';
$isp_list['%.n-fukushi.ac.jp'] = '日本福祉大学';
$isp_list['%.uhe.ac.jp'] = '人間環境大学';
$isp_list['%.fujita-hu.ac.jp'] = '藤田保健衛生大学';
$isp_list['%.meijo-u.ac.jp'] = '名城大学';
$isp_list['%.aichi-kiwami.ac.jp'] = '愛知きわみ看護短期大学';
$isp_list['%.konan.ac.jp'] = '愛知江南短期大学';
$isp_list['%.ai-bunkyo.ac.jp'] = '愛知文教女子短期大学';
$isp_list['%.ichinomiya.ac.jp'] = '一宮女子短期大学';
$isp_list['%.okazaki-c.ac.jp'] = '岡崎女子短期大学';
$isp_list['%.nagoyacollege.ac.jp'] = '名古屋短期大学';
$isp_list['%.nfcc-nagoya.com'] = '名古屋文化短期大学';
$isp_list['%.ryujo.ac.jp'] = '名古屋柳城短期大学';
$isp_list['%.nanzan-tandai.ac.jp'] = '南山短期大学';
$isp_list['%.ims.ac.jp'] = '分子科学研究所';
$isp_list['%.nakanishi.ac.jp'] = '中西学園';
$isp_list['%.hal.ac.jp'] = 'HAL';

//--愛知ここまで-->
//--三重ここから-->
$isp_list['%.mie-u.ac.jp'] = '三重大学';
$isp_list['%.mcn.ac.jp'] = '三重県立看護大学';
$isp_list['%.kogakkan-u.ac.jp'] = '皇學館大学';
$isp_list['%.suzuka-u.ac.jp'] = '鈴鹿医療科学大学';
$isp_list['%.suzuka-iu.ac.jp'] = '鈴鹿国際大学';
$isp_list['%.mie-chukyo-u.ac.jp'] = '三重中京大学';
$isp_list['%.yokkaichi-u.ac.jp'] = '四日市大学';
$isp_list['%.y-nm.ac.jp'] = '四日市看護医療大学';
$isp_list['%.tsu-cc.ac.jp'] = '三重短期大学';
$isp_list['%.i-chubu.ne.jp'] = '鈴鹿国際大学短期大学部';
$isp_list['%.takada-jc.ac.jp'] = '高田短期大学';
//--三重ここまで-->
//--滋賀ここから-->
$isp_list['%.shiga-u.ac.jp'] = '滋賀大学';
$isp_list['%.shiga-med.ac.jp'] = '滋賀医科大学';
$isp_list['%.usp.ac.jp'] = '滋賀県立大学';
$isp_list['%.seian.ac.jp'] = '成安造形大学';
$isp_list['%.seisen.ac.jp'] = '聖泉大学';
$isp_list['%.nagahama-i-bio.ac.jp'] = '長浜バイオ大学';
$isp_list['%.osaka-seikei.ac.jp'] = 'びわこ成蹊スポーツ大学';
$isp_list['%.sumire.ac.jp'] = '滋賀女子短期大学';
$isp_list['%.newton.ac.jp'] = '滋賀文化短期大学';
$isp_list['%.s-bunkyo.ac.jp'] = '滋賀文教短期大学';
$isp_list['%.seisen.ac.jp'] = '聖泉大学短期大学部';
//--滋賀ここまで-->
//--京都ここから-->
$isp_list['%.kyoto-u.ac.jp'] = '京都大学';
$isp_list['%.kyokyo-u.ac.jp'] = '京都教育大学';
$isp_list['%.kit.ac.jp'] = '京都工芸繊維大学';
$isp_list['%.kcua.ac.jp'] = '京都市立芸術大学';
$isp_list['%.kpu.ac.jp'] = '京都府立大学';
$isp_list['%.kpu-m.ac.jp'] = '京都府立医科大学';
$isp_list['%.otani.ac.jp'] = '大谷大学';
$isp_list['%.kufs.ac.jp'] = '京都外国語大学';
$isp_list['%.kyotogakuen.ac.jp'] = '京都学園大学';
$isp_list['%.koka.ac.jp'] = '京都光華女子大学';
$isp_list['%.kyoto-saga.ac.jp'] = '京都嵯峨芸術大学';
$isp_list['%.kyoto-su.ac.jp'] = '京都産業大学';
$isp_list['%.kcg.edu'] = '京都情報大学院大学';
$isp_list['%.kyoto-wu.ac.jp'] = '京都女子大学';
$isp_list['%.kyoto-seika.ac.jp'] = '京都精華大学';
$isp_list['%.kyoto-art.ac.jp'] = '京都造形芸術大学';
$isp_list['%.kyoto-sosei.ac.jp'] = '京都創成大学';
$isp_list['%.tachibana-u.ac.jp'] = '京都橘大学';
$isp_list['%.notredame.ac.jp'] = '京都ノートルダム女子大学';
$isp_list['%.kbu.ac.jp'] = '京都文教大学';
$isp_list['%.kyoto-phu.ac.jp'] = '京都薬科大学';
$isp_list['%.shuchiin.ac.jp'] = '種智院大学';
$isp_list['%.doshisha.ac.jp'] = '同志社大学';
$isp_list['%.dwc.doshisha.ac.jp'] = '同志社女子大学';
$isp_list['%.hanazono.ac.jp'] = '花園大学';
$isp_list['%.bukkyo-u.ac.jp'] = '佛教大学';
$isp_list['%.meiji-u.ac.jp'] = '明治国際医療大学';
$isp_list['%.ritsumei.ac.jp'] = '立命館大学';
$isp_list['%.ryukoku.ac.jp'] = '龍谷大学';
$isp_list['%.kyoto-msc.jp'] = '京都医療科学大学';
$isp_list['%.ikenobo-c.ac.jp'] = '池坊短期大学';
$isp_list['%.kacho-college.ac.jp'] = '華頂短期大学';
$isp_list['%.kyoto.medtech.ac.jp'] = '京都医療技術短期大学';
$isp_list['%.kyoto-econ.ac.jp'] = '京都経済短期大学';
$isp_list['%.kyoto-jc.ac.jp'] = '京都短期大学';
$isp_list['%.seizan.ac.jp'] = '京都西山短期大学';
$isp_list['%.seibo.ac.jp'] = '聖母女学院短期大学';
$isp_list['%.kyoto-kct.ac.jp'] = '京都コンピュータ学院';
//--京都ここまで-->
//--大阪ここから-->
$isp_list['%.osaka-u.ac.jp'] = '大阪大学';
$isp_list['%.osaka-kyoiku.ac.jp'] = '大阪教育大学';
$isp_list['%.osaka-cu.ac.jp'] = '大阪市立大学';
$isp_list['%.osakafu-u.ac.jp'] = '大阪府立大学';
$isp_list['%.aino.ac.jp'] = '藍野大学';
$isp_list['%.campuscity.jp'] = 'LCA大学院大学';
$isp_list['%.otemon.ac.jp'] = '追手門学院大学';
$isp_list['%.osaka-aoyama.ac.jp'] = '大阪青山大学';
$isp_list['%.osaka-med.ac.jp'] = '大阪医科大学';
$isp_list['%.osaka-ohtani.ac.jp'] = '大阪大谷大学';
$isp_list['%.daion.ac.jp'] = '大阪音楽大学';
$isp_list['%.meijo.ac.jp'] = '大阪観光大学';
$isp_list['%.osaka-gu.ac.jp'] = '大阪学院大学';
$isp_list['%.kawasakigakuen.ac.jp'] = '大阪河崎リハビリテーション大学';
$isp_list['%.osaka-ue.ac.jp'] = '大阪経済大学';
$isp_list['%.keiho-u.ac.jp'] = '大阪経済法科大学';
$isp_list['%.osaka-geidai.ac.jp'] = '大阪芸術大学';
$isp_list['%.oit.ac.jp'] = '大阪工業大学';
$isp_list['%.oiu.ac.jp'] = '大阪国際大学';
$isp_list['%.osaka-sandai.ac.jp'] = '大阪産業大学';
$isp_list['133.64.%'] = '大阪産業大学';
$isp_list['%.osaka-dent.ac.jp'] = '大阪歯科大学';
$isp_list['%.osaka-shoin.ac.jp'] = '大阪樟蔭女子大学';
$isp_list['%.ouc.daishodai.ac.jp'] = '大阪商業大学';
$isp_list['%.wilmina.ac.jp'] = '大阪女学院大学';
$isp_list['%.osaka-seikei.ac.jp'] = '大阪成蹊大学';
$isp_list['%.jonan.ac.jp'] = '大阪総合保育大学';
$isp_list['%.ouhs.ac.jp'] = '大阪体育大学';
$isp_list['%.osakac.ac.jp'] = '大阪電気通信大学';
$isp_list['%.ohs.ac.jp'] = '大阪人間科学大学';
$isp_list['%.oups.ac.jp'] = '大阪薬科大学';
$isp_list['%.kansai-u.ac.jp'] = '関西大学';
$isp_list['%.kmu.ac.jp'] = '関西医科大学';
$isp_list['%.kansai.ac.jp'] = '関西医療大学';
$isp_list['%.kansaigaidai.ac.jp'] = '関西外国語大学';
$isp_list['%.fuksi-kagk-u.ac.jp'] = '関西福祉科学大学';
$isp_list['%.kindai.ac.jp'] = '近畿大学';
$isp_list['%.shijonawate-gakuen.ac.jp'] = '四條畷学園大学';
$isp_list['%.shitennoji.ac.jp'] = '四天王寺国際仏教大学';
$isp_list['%.setsunan.ac.jp'] = '摂南大学';
$isp_list['%.kinran.ac.jp'] = '千里金蘭大学';
$isp_list['%.soai.ac.jp'] = '相愛大学';
$isp_list['%.tgu.ac.jp'] = '太成学院大学';
$isp_list['%.tezukayama.ac.jp'] = '帝塚山学院大学';
$isp_list['%.sftokiwakai.ac.jp'] = '常磐会学園大学';
$isp_list['%.baika.ac.jp'] = '梅花女子大学';
$isp_list['%.hagoromo.ac.jp'] = '羽衣国際大学';
$isp_list['%.hannan-u.ac.jp'] = '阪南大学';
$isp_list['%.higashiosaka-c.com'] = '東大阪大学';
$isp_list['%.poole.ac.jp'] = 'プール学院大学';
$isp_list['%.heian.ac.jp'] = '平安女学院大学';
$isp_list['%.andrew.ac.jp'] = '桃山学院大学';
$isp_list['%.morinomiya-u.ac.jp'] = '森ノ宮医療大学';
$isp_list['%.aino.ac.jp'] = '藍野学院短期大学';
$isp_list['%.ohtani-w.ac.jp'] = '大阪大谷大学短期大学部';
$isp_list['%.occ.ac.jp'] = '大阪キリスト教短期大学';
$isp_list['%.kun-ei.ac.jp'] = '大阪薫英女子短期大学';
$isp_list['%.kenko-fukushi.ac.jp'] = '大阪健康福祉短期大学';
$isp_list['%.owjc.jp'] = '大阪女子短期大学';
$isp_list['%.osaka-shinai.ac.jp'] = '大阪信愛女学院短期大学';
$isp_list['%.chiyoda.ac.jp'] = '大阪千代田短期大学';
$isp_list['%.oyg.ac.jp'] = '大阪夕陽丘学園短期大学';
$isp_list['%.kwc.ac.jp'] = '関西女子短期大学';
$isp_list['%.sakaijoshi.ac.jp'] = '堺女子短期大学';
$isp_list['%.shoinhigashi.ac.jp'] = '樟蔭東女子短期大学';
$isp_list['%.tokiwakai.ac.jp'] = '常磐会短期大学';
$isp_list['%.osaka-pct.ac.jp'] = '大阪府立工業高等専門学校';
//--大阪ここまで-->
//--兵庫ここから-->
$isp_list['%.kobe-u.ac.jp'] = '神戸大学';
$isp_list['%.hyogo-u.ac.jp'] = '兵庫教育大学';
$isp_list['%.kobe-cufs.ac.jp'] = '神戸市外国語大学';
$isp_list['%.kobe-ccn.ac.jp'] = '神戸市看護大学';
$isp_list['%.u-hyogo.ac.jp'] = '兵庫県立大学';
$isp_list['%.ashiya-u.ac.jp'] = '芦屋大学';
$isp_list['%.thomas.ac.jp'] = '聖トマス大学';
$isp_list['%.otemae.ac.jp'] = '大手前大学';
$isp_list['%.kwansei.ac.jp'] = '関西学院大学';
$isp_list['%.kuins.ac.jp'] = '関西国際大学';
$isp_list['%.kusw.ac.jp'] = '関西福祉大学';
$isp_list['%.kinwu.ac.jp'] = '近畿医療福祉大学';
$isp_list['%.kindaihimeji-u.ac.jp'] = '近大姫路大学';
$isp_list['%.koshien.ac.jp'] = '甲子園大学';
$isp_list['%.ipc.konan-u.ac.jp'] = '甲南大学';
$isp_list['%.konan-wu.ac.jp'] = '甲南女子大学';
$isp_list['%.kaisei.ac.jp'] = '神戸海星女子学院大学';
$isp_list['%.kobegakuin.ac.jp'] = '神戸学院大学';
$isp_list['%.kobe-du.ac.jp'] = '神戸芸術工科大学';
$isp_list['%.kobe-kiu.ac.jp'] = '神戸国際大学';
$isp_list['%.shoin.ac.jp'] = '神戸松蔭女子学院大学';
$isp_list['%.kobeshukugawa.ac.jp'] = '神戸夙川学院大学';
$isp_list['%.kic.ac.jp'] = '神戸情報大学院大学';
$isp_list['%.kobe-c.ac.jp'] = '神戸女学院大学';
$isp_list['%.kobe-wu.ac.jp'] = '神戸女子大学';
$isp_list['%.kobe-shinwa.ac.jp'] = '神戸親和女子大学';
$isp_list['%.kobe-tokiwa.ac.jp'] = '神戸常盤大学';
$isp_list['%.kobe-fashion.ac.jp'] = '神戸ファッション造形大学';
$isp_list['%.kobepharma-u.ac.jp'] = '神戸薬科大学';
$isp_list['%.kobe-yamate.ac.jp'] = '神戸山手大学';
$isp_list['%.seiwa-u.ac.jp'] = '聖和大学';
$isp_list['%.sonoda-u.ac.jp'] = '園田学園女子大学';
$isp_list['%.takara-univ.ac.jp'] = '宝塚造形芸術大学';
$isp_list['%.himeji-du.ac.jp'] = '姫路獨協大学';
$isp_list['%.hyogo-dai.ac.jp'] = '兵庫大学';
$isp_list['%.hyo-med.ac.jp'] = '兵庫医科大学';
$isp_list['%.huhs.ac.jp'] = '兵庫医療大学';
$isp_list['%.mukogawa-u.ac.jp'] = '武庫川女子大学';
$isp_list['%.umds.ac.jp'] = '流通科学大学';
$isp_list['%.ashiya-c.ac.jp'] = '芦屋女子短期大学';
$isp_list['%.kenmei.ac.jp'] = '賢明女子学院短期大学';
$isp_list['%.koshien-c.ac.jp'] = '甲子園短期大学';
$isp_list['%.kobe-bunka.ac.jp'] = '神戸文化短期大学';
$isp_list['%.sangitan.ac.jp'] = '産業技術短期大学';
$isp_list['%.shukugawa-c.ac.jp'] = '夙川学院短期大学';
$isp_list['%.glory-shoei.ac.jp'] = '頌栄短期大学';
$isp_list['%.toshoku.ac.jp'] = '東洋食品工業短期大学';
$isp_list['%.himeji-hc.ac.jp'] = '姫路日ノ本短期大学';
$isp_list['%.minatogawa.ac.jp'] = '湊川短期大学';
$isp_list['%.kobe-kosen.ac.jp'] = '神戸高専';
//--兵庫ここまで-->
//--奈良ここから-->
$isp_list['%.nara-edu.ac.jp'] = '奈良教育大学';
$isp_list['%.nara-wu.ac.jp'] = '奈良女子大学';
$isp_list['%.naist.jp'] = '奈良先端科学技術大学院大学';
$isp_list['%.aist-nara.ac.jp'] = '奈良先端科学技術大学院大学';
$isp_list['%.narapu.ac.jp'] = '奈良県立大学';
$isp_list['%.naramed-u.ac.jp'] = '奈良県立医科大学';
$isp_list['%.kio.ac.jp'] = '畿央大学';
$isp_list['%.tezukayama-u.ac.jp'] = '帝塚山大学';
$isp_list['%.tenri-u.ac.jp'] = '天理大学';
$isp_list['%.nara-u.ac.jp'] = '奈良大学';
$isp_list['%.nara-su.ac.jp'] = '奈良産業大学';
$isp_list['%.naragei.ac.jp'] = '奈良芸術短期大学';
$isp_list['%.narasaho-c.ac.jp'] = '奈良佐保短期大学';
$isp_list['%.narabunka.ac.jp'] = '奈良文化女子短期大学';
$isp_list['%.hakuho.ac.jp'] = '白鳳女子短期大学';
//--奈良ここまで-->
//--和歌山ここから-->
$isp_list['%.wakayama-u.ac.jp'] = '和歌山大学';
$isp_list['%.wakayama-med.ac.jp'] = '和歌山県立医科大学';
$isp_list['%.koyasan-u.ac.jp'] = '高野山大学';
$isp_list['%.shinai-u.ac.jp'] = '和歌山信愛女子短期大学';
//--和歌山ここまで-->
//--鳥取ここから-->
$isp_list['%.tottori-u.ac.jp'] = '鳥取大学';
$isp_list['%.kankyo-u.ac.jp'] = '鳥取環境大学';
$isp_list['%.cygnus.ac.jp'] = '鳥取短期大学';
//--鳥取ここまで-->
//--島根ここから-->
$isp_list['%.shimane-u.ac.jp'] = '島根大学';
$isp_list['%.u-shimane.ac.jp'] = '島根県立大学';
$isp_list['%.swc.ac.jp'] = '島根県立島根女子短期大学';
//--島根ここまで-->
//--岡山ここから-->
$isp_list['%.okayama-u.ac.jp'] = '岡山大学';
$isp_list['%.oka-pu.ac.jp'] = '岡山県立大学';
$isp_list['%.owc.ac.jp'] = '岡山学院大学';
$isp_list['%.osu.ac.jp'] = '岡山商科大学';
$isp_list['%.ous.ac.jp'] = '岡山理科大学';
$isp_list['%.kawasaki-m.ac.jp'] = '川崎医科大学';
$isp_list['%.ipu-japan.ac.jp'] = '環太平洋大学';
$isp_list['%.kiui.jp'] = '吉備国際大学';
$isp_list['%.kusa.ac.jp'] = '倉敷芸術科学大学';
$isp_list['%.hisc.co.jp'] = 'くらしき作陽大学';
$isp_list['%.sguc.ac.jp'] = '山陽学園大学';
$isp_list['%.shujitsu.ac.jp'] = '就実大学';
$isp_list['%.cjc.ac.jp'] = '中国学園大学';
$isp_list['%.ndsu.ac.jp'] = 'ノートルダム清心女子大学';
$isp_list['%.mimasaka.ac.jp'] = '美作大学';
$isp_list['%.kurashiki-cu.ac.jp'] = '倉敷市立短期大学';
$isp_list['%.niimi-c.ac.jp'] = '新見公立短期大学';
$isp_list['%.junsei.ac.jp'] = '順正短期大学';
//--岡山ここまで-->
//--広島ここから-->
$isp_list['%.hiroshima-u.ac.jp'] = '広島大学';
$isp_list['133.41.%'] = '広島大学';
$isp_list['%.onomichi-u.ac.jp'] = '尾道大学';
$isp_list['%.pu-hiroshima.ac.jp'] = '県立広島大学';
$isp_list['%.hiroshima-pu.ac.jp'] = '県立広島大学';
$isp_list['%.hiroshima-cu.ac.jp'] = '広島市立大学';
$isp_list['%.eum.ac.jp'] = 'エリザベト音楽大学';
$isp_list['%.jrchcn.ac.jp'] = '日本赤十字広島看護大学';
$isp_list['%.hijiyama-u.ac.jp'] = '比治山大学';
$isp_list['%.hue.ac.jp'] = '広島経済大学';
$isp_list['%.cc.it-hiroshima.ac.jp'] = '広島工業大学';
$isp_list['%.hirokoku-u.ac.jp'] = '広島国際大学';
$isp_list['%.hkg.ac.jp'] = '広島国際学院大学';
$isp_list['%.shudo-u.ac.jp'] = '広島修道大学';
$isp_list['%.hju.ac.jp'] = '広島女学院大学';
$isp_list['%.hbg.ac.jp'] = '広島文化学園大学';
$isp_list['%.h-bunkyo.ac.jp'] = '広島文教女子大学';
$isp_list['%.fukuyama-u.ac.jp'] = '福山大学';
$isp_list['%.heisei-u.ac.jp'] = '福山平成大学';
$isp_list['%.yasuda-u.ac.jp'] = '安田女子大学';
$isp_list['%.fukuyama-jc.ac.jp'] = '福山市立女子短期大学';
$isp_list['%.sanyo.ac.jp'] = '山陽女子短期大学';
$isp_list['%.suzugamine.ac.jp'] = '鈴峯女子短期大学';
//--広島ここまで-->
//--山口ここから-->
$isp_list['%.yamaguchi-u.ac.jp'] = '山口大学';
$isp_list['%.shimonoseki-cu.ac.jp'] = '下関市立大学';
$isp_list['%.yamaguchi-pu.ac.jp'] = '山口県立大学';
$isp_list['%.frontier-u.jp'] = '宇部フロンティア大学';
$isp_list['%.toua-u.ac.jp'] = '東亜大学';
$isp_list['%.tokuyama-u.ac.jp'] = '徳山大学';
$isp_list['%.baiko.ac.jp'] = '梅光学院大学';
$isp_list['%.y-gakugei.ac.jp'] = '山口学芸大学';
$isp_list['%.hagi.ac.jp'] = '山口福祉文化大学';
$isp_list['%.yama.sut.ac.jp'] = '山口東京理科大学';
$isp_list['%.iwakuni.ac.jp'] = '岩国短期大学';
$isp_list['%.ube-c.ac.jp'] = '宇部フロンティア大学短期大学部';
$isp_list['%.shimonoseki-jc.ac.jp'] = '下関短期大学';
$isp_list['%.yamaguchi-jca.ac.jp'] = '山口芸術短期大学';
$isp_list['%.yamaguchi-jc.ac.jp'] = '山口短期大学';
$isp_list['%.yic.ac.jp'] = '専門学校ＹＩＣグループ';
//--山口ここまで-->
//--徳島ここから-->
$isp_list['%.tokushima-u.ac.jp'] = '徳島大学';
$isp_list['%.naruto-u.ac.jp'] = '鳴門教育大学';
$isp_list['%.shikoku-u.ac.jp'] = '四国大学';
$isp_list['%.bunri-u.ac.jp'] = '徳島文理大学';
$isp_list['%.tokuco.ac.jp'] = '徳島工業短期大学';
//--徳島ここまで-->
//--香川ここから-->
$isp_list['%.kagawa-u.ac.jp'] = '香川大学';
$isp_list['%.sg-u.ac.jp'] = '四国学院大学';
$isp_list['%.takamatsu-u.ac.jp'] = '高松大学';
$isp_list['%.kjc.ac.jp'] = '香川短期大学';
$isp_list['%.setouchi.ac.jp'] = '瀬戸内短期大学';
//--香川ここまで-->
//--愛媛ここから-->
$isp_list['%.ehime-u.ac.jp'] = '愛媛大学';
$isp_list['%.epu.ac.jp'] = '愛媛県立医療技術大学';
$isp_list['%.catherine.ac.jp'] = '聖カタリナ大学';
$isp_list['%.matsuyama-u.ac.jp'] = '松山大学';
$isp_list['192.218.200.%'] = '松山大学';
$isp_list['%.shinonome.ac.jp'] = '松山東雲女子大学';
$isp_list['%.ehime-chs.ac.jp'] = '愛媛県立医療技術短期大学';
$isp_list['%.meitan.ac.jp'] = '今治明徳短期大学';
$isp_list['%.aitan.ac.jp'] = '愛媛女子短期大学';
//--愛媛ここまで-->
//--高知ここから-->
$isp_list['%.kochi-u.ac.jp'] = '高知大学';
$isp_list['%.kochi-ms.ac.jp'] = '高知大学医学部';
$isp_list['%.kochi-wu.ac.jp'] = '高知女子大学';
$isp_list['%.kochi-tech.ac.jp'] = '高知工科大学';
$isp_list['%.kochi-gc.ac.jp'] = '高知学園短期大学';
//--高知ここまで-->
//--福岡ここから-->
$isp_list['%.kyushu-u.ac.jp'] = '九州大学';
$isp_list['%.kyutech.ac.jp'] = '九州工業大学';
$isp_list['%.fukuoka-edu.ac.jp'] = '福岡教育大学';
$isp_list['%.kitakyu-u.ac.jp'] = '北九州市立大学';
$isp_list['%.kyu-dent.ac.jp'] = '九州歯科大学';
$isp_list['%.fukuoka-pu.ac.jp'] = '福岡県立大学';
$isp_list['%.fwu.ac.jp'] = '福岡女子大学';
$isp_list['%.knwu.ac.jp'] = '九州栄養福祉大学';
$isp_list['%.kyukyo-u.ac.jp'] = '九州共立大学';
$isp_list['%.kiu.ac.jp'] = '九州国際大学';
$isp_list['%.ip.kyusan-u.ac.jp'] = '九州産業大学';
$isp_list['%.kiis.ac.jp'] = '九州情報大学';
$isp_list['%.kwuc.ac.jp'] = '九州女子大学';
$isp_list['%.mii.kurume-u.ac.jp'] = '久留米大学';
$isp_list['%.kurume-it.ac.jp'] = '久留米工業大学';
$isp_list['%.yber-u.ac.jp'] = 'サイバー大学';
$isp_list['%.uoeh-u.ac.jp'] = '産業医科大学';
$isp_list['%.seinan-gu.ac.jp'] = '西南学院大学';
$isp_list['%.seinan-jo.ac.jp'] = '西南女学院大学';
$isp_list['%.st-mary.ac.jp'] = '聖マリア学院大学';
$isp_list['%.daiichi-cps.ac.jp'] = '第一薬科大学';
$isp_list['%.chikushi.ac.jp'] = '筑紫女学園大学';
$isp_list['%.kyu-teikyo.ac.jp'] = '帝京大学福岡キャンパス';
$isp_list['%.junshin.org'] = '東和大学';
$isp_list['%.nakamura-u.ac.jp'] = '中村学園大学';
$isp_list['%.nishitech.ac.jp'] = '西日本工業大学';
$isp_list['%.jrckicn.ac.jp'] = '日本赤十字九州国際看護大学';
$isp_list['%.fukuoka-u.ac.jp'] = '福岡大学';
$isp_list['%.dfu.ac.jp'] = '福岡医療福祉大学';
$isp_list['%.fukuoka-ue.ac.jp'] = '福岡経済大学';
$isp_list['%.fit.ac.jp'] = '福岡工業大学';
$isp_list['%.fukuoka-int-u.ac.jp'] = '福岡国際大学';
$isp_list['%.fdcnet.ac.jp'] = '福岡歯科大学';
$isp_list['%.fukujo.ac.jp'] = '福岡女学院大学';
$isp_list['%.orioaishin.ac.jp'] = '折尾愛真短期大学';
$isp_list['%.kyushuotani.ac.jp'] = '九州大谷短期大学';
$isp_list['%.zac.kyusan-u.ac.jp'] = '九州造形短期大学';
$isp_list['%.denkigakuen.ac.jp'] = '九州電機短期大学';
$isp_list['%.kjc.kindai.ac.jp'] = '近畿大学九州短期大学';
$isp_list['%.kurume-shinai.ac.jp'] = '久留米信愛女学院短期大学';
$isp_list['%.koran.ac.jp'] = '香蘭女子短期大学';
$isp_list['%.seika.ac.jp'] = '精華女子短期大学';
$isp_list['%.ftokai-u.ac.jp'] = '東海大学福岡短期大学';
$isp_list['%.nishitan.ac.jp'] = '西日本短期大学';
$isp_list['%.hcc.ac.jp'] = '東筑紫短期大学';
$isp_list['%.fdcnet.ac.jp'] = '福岡医療短期大学';
$isp_list['%.fjct.fit.ac.jp'] = '福岡工業大学短期大学部';
$isp_list['%.fukuoka-wjc.ac.jp'] = '福岡女子短期大学';
$isp_list['%.ariake-nct.ac.jp'] = '有明工業専門学校';
//--福岡ここまで-->
//--佐賀ここから-->
$isp_list['%.saga-u.ac.jp'] = '佐賀大学';
$isp_list['%.nisikyu-u.ac.jp'] = '西九州大学';
$isp_list['%.saganet.ne.jp'] = '九州龍谷短期大学';
$isp_list['%.asahigakuen.ac.jp'] = '佐賀女子短期大学';
$isp_list['%.saga-jc.ac.jp'] = '佐賀短期大学';
//--佐賀ここまで-->
//--長崎ここから-->
$isp_list['%.nagasaki-u.ac.jp'] = '長崎大学';
$isp_list['%.sun.ac.jp'] = '長崎県立大学';
$isp_list['%.kwassui.ac.jp'] = '活水女子大学';
$isp_list['%.nwjc.ac.jp'] = '長崎ウエスレヤン大学';
$isp_list['%.nagasaki-gaigo.ac.jp'] = '長崎外国語大学';
$isp_list['%.niu.ac.jp'] = '長崎国際大学';
$isp_list['%.n-junshin.ac.jp'] = '長崎純心大学';
$isp_list['%.nias.ac.jp'] = '長崎総合科学大学';
$isp_list['%.tamaki.ac.jp'] = '玉木女子短期大学';
$isp_list['%.nagasaki-gaigo.ac.jp'] = '長崎外国語短期大学';
$isp_list['%.nagasaki-joshi.ac.jp'] = '長崎女子短期大学';
$isp_list['%.njc.ac.jp'] = '長崎短期大学';
//--長崎ここまで-->
//--熊本ここから-->
$isp_list['%.kumamoto-u.ac.jp'] = '熊本大学';
$isp_list['%.pu-kumamoto.ac.jp'] = '熊本県立大学';
$isp_list['%.kyushu-ns.ac.jp'] = '九州看護福祉大学';
$isp_list['%.klc.ac.jp'] = '九州ルーテル学院大学';
$isp_list['%.kumagaku.ac.jp'] = '熊本学園大学';
$isp_list['%.kumamoto-hsu.ac.jp'] = '熊本保健科学大学';
$isp_list['%.shokei-gakuen.ac.jp'] = '尚絅大学';
$isp_list['%.sojo-u.ac.jp'] = '崇城大学';
$isp_list['%.heisei-music.ac.jp'] = '平成音楽大学';
$isp_list['%.shokei-gakuen.ac.jp'] = '尚絅短期大学';
$isp_list['%.nkjc.ac.jp'] = '中九州短期大学';
$isp_list['%.knct.ac.jp'] = '熊本高等専門学校';
//--熊本ここまで-->
//--大分ここから-->
$isp_list['%.oita-u.ac.jp'] = '大分大学';
$isp_list['%.oita-nhs.ac.jp'] = '大分県立看護科学大学';
$isp_list['%.nbu.ac.jp'] = '日本文理大学';
$isp_list['%.beppu-u.ac.jp'] = '別府大学';
$isp_list['%.apu.ac.jp'] = '立命館アジア太平洋大学';
$isp_list['%.oita-pjc.ac.jp'] = '大分県立芸術文化短期大学';
$isp_list['%.coara.or.jp'] = '大分短期大学';
$isp_list['%.higashikyusyu.ac.jp'] = '東九州短期大学';
$isp_list['%.mizobe.ac.jp'] = '別府溝部学園短期大学';
$isp_list['%.beppu-u.ac.jp'] = '別府大学短期大学部';
//--大分ここまで-->
//--宮崎ここから-->
$isp_list['%.miyazaki-u.ac.jp'] = '宮崎大学';
$isp_list['%.mpu.ac.jp'] = '宮崎県立看護大学';
$isp_list['%.miyazaki-mu.ac.jp'] = '宮崎公立大学';
$isp_list['%.phoenix.ac.jp'] = '九州保健福祉大学';
$isp_list['%.nankyudai.ac.jp'] = '南九州大学';
$isp_list['%.miyazaki-mic.ac.jp'] = '宮崎国際大学';
$isp_list['%.miyasankei-u.ac.jp'] = '宮崎産業経営大学';
$isp_list['%.ursula.jp'] = '聖心ウルスラ学園短期大学';
$isp_list['%.mkjc.ac.jp'] = '南九州短期大学';
$isp_list['%.mwjc.ac.jp'] = '宮崎女子短期大学';
//--宮崎ここまで-->
//--鹿児島ここから-->
$isp_list['%.kagoshima-u.ac.jp'] = '鹿児島大学';
$isp_list['%.nifs-k.ac.jp'] = '鹿屋体育大学';
$isp_list['%.iuk.ac.jp'] = '鹿児島国際大学';
$isp_list['%.k-junshin.ac.jp'] = '鹿児島純心女子大学';
$isp_list['%.shigakukan.ac.jp'] = '志學館大学';
$isp_list['%.daiichi-koudai.ac.jp'] = '第一工業大学';
$isp_list['%.k-kentan.ac.jp'] = '鹿児島県立短期大学';
$isp_list['%.jkajyo.ac.jp'] = '鹿児島女子短期大学';
$isp_list['%.tsuzuki-edu.ac.jp'] = '第一幼児教育短期大学';
//--鹿児島ここまで-->
//--沖縄ここから-->
$isp_list['%.u-ryukyu.ac.jp'] = '琉球大学';
$isp_list['%.ryudai.jp'] = '琉球大学総合情報処理センター';
$isp_list['%.okinawa-nurs.ac.jp'] = '沖縄県立看護大学';
$isp_list['%.okigei.ac.jp'] = '沖縄県立芸術大学';
$isp_list['%.okinawa-u.ac.jp'] = '沖縄大学';
$isp_list['%.ocjc.ac.jp'] = '沖縄キリスト教学院大学';
$isp_list['%.okiu.ac.jp'] = '沖縄国際大学';
$isp_list['%.meio-u.ac.jp'] = '名桜大学';
$isp_list['%.owjc.ac.jp'] = '沖縄女子短期大学';
//--沖縄ここまで-->
//--.ac.jpここから-->
$isp_list['%.nao.ac.jp'] = '国立天文台';
$isp_list['%.asojuku.ac.jp'] = '麻生専門学校グループ';
//--.ac.jpここまで-->

// CATV
//北海道
//JCOM 札幌 不明
//テレビ寿都放送 不明
$isp_list['%.potato.ne.jp'] = '旭川ケーブルテレビ';
$isp_list['%.lan-do.ne.jp'] = '旭川ケーブルテレビ';
$isp_list['%.ncv.ne.jp'] = 'ニューメディア';
//苫小牧ケーブルテレビ TikiTikiインターネット
//大滝ケーブルテレビ インターネットなし
$isp_list['%.octv.ne.jp'] = '帯広シティーケーブル';
$isp_list['%.vill.nishiokoppe.hokkaido.jp'] = '西興部村コミュニケーションネットワーク';
$isp_list['%.kctvnet.ne.jp'] = '釧路ケーブルテレビ';
$isp_list['210.229.186.%'] = '釧路ケーブルテレビ';

//青森
$isp_list['%.actv.ne.jp'] = '青森ケーブルテレビ';
$isp_list['%.htv-net.ne.jp'] = '八戸テレビ放送';
//風間浦村営共聴システム インターネットなし
//田子町ケーブルテレビジョン
$isp_list['%.mctvnet.ne.jp'] = '三沢市ケーブルテレビジョン';
//弘前ケーブルテレビ

//岩手県
$isp_list['%.icn-net.ne.jp'] = '一関ケーブルネットワーク';
$isp_list['%.ictnet.ne.jp'] = '岩手ケーブルテレビジョン';
$isp_list['%.waiwai-net.ne.jp'] = 'えさしわいわいネット';
$isp_list['%.ginga-net.ne.jp'] = '北上ケーブルテレビ';
$isp_list['%.tonotv.com'] = '遠野テレビ';
//花巻ケーブルテレビ TikiTikiインターネット
$isp_list['%.catv-mic.ne.jp'] = '水沢テレビ';
$isp_list['%.wtv-net.jp'] = '和賀有線テレビ';

//秋田
$isp_list['%.cna.ne.jp'] = '秋田ケーブルテレビ';
$isp_list['%.ont.ne.jp'] = '由利本荘市CATVセンター';
//大館ケーブルテレビ TikiTikiインターネット

//宮城
$isp_list['%.k-macs.ne.jp'] = '気仙沼ケーブルネットワーク (K-NET)';
$isp_list['%.c-marinet.ne.jp'] = '塩釜ケーブルテレビ（マリネット）';
$isp_list['%.cat-v.ne.jp'] = '仙台CATV';
$isp_list['%.mni.ne.jp'] = '宮城ネットワーク';
//利府・青山CATV管理組合';
//大崎ケーブルテレビ TikiTikiインターネット

//山形
$isp_list['%.catvy.ne.jp'] = 'ケーブルテレビ山形';
$isp_list['%.omn.ne.jp'] = 'ニュ－メディア';

//福島
$isp_list['%.nct.ne.jp'] = '西会津町ケーブルテレビ (NCT)';
//たじまケーブルテレビジョン

//茨城
$isp_list['%.accsnet.ne.jp'] = '（財）研究学園都市コミュニティケーブルサービス';
$isp_list['%.rcctv.jp'] = 'リバーシティ・ケーブルテレビ';
$isp_list['%.speedway.ne.jp'] = '日本通信放送';
$isp_list['%.jway.ne.jp'] = 'JWAY';

//栃木
$isp_list['%.ucatv.ne.jp'] = '宇都宮ケーブルテレビ';
$isp_list['%.tvoyama.ne.jp'] = 'テレビ小山放送';
$isp_list['%.bc9.ne.jp'] = '鹿沼ケーブルテレビ';
$isp_list['%.sctv.jp'] = '佐野ケーブルテレビ';
$isp_list['%.watv.ne.jp'] = '足利ケーブルテレビ';
$isp_list['%.cc9.ne.jp'] = '栃木ケーブルテレビ';
$isp_list['%.i-berry.ne.jp'] = '真岡ケーブルテレビ';
//茂木町ケーブルテレビ
//粟野ケーブルテレビ
//塩原テレビ共同聴視事業協同組合
//大日光ケーブルテレビ

//群馬
$isp_list['%.otv.ne.jp'] = '光ケーブルネット';
$isp_list['%.ktv.ne.jp'] = '光ケーブルネット（桐生局）';
$isp_list['%.nanmoku.ne.jp'] = 'なんもくふれあいテレビ';
$isp_list['%.uenomura.ne.jp'] = 'うえのテレビ';
//草津テレビ
//宝町テレビ共同受信施設組合
//嬬恋ケーブルビジョン
//東吾妻町あづまケーブルテレビ
$isp_list['%.kannamachi.jp'] = '神流町ケーブルテレビ';
//六合村健康管理等情報連絡施設

//埼玉
$isp_list['%.cablenet.ne.jp'] = 'ケーブルネット埼玉';
$isp_list['%.tcat.ne.jp'] = 'JCN関東';
$isp_list['%.kcv-net.ne.jp'] = '川越ケーブルテレビ';
$isp_list['%.hctv.ne.jp'] = '東松山ケーブルテレビ';
//飯能ケーブルテレビ
$isp_list['%.warabi.ne.jp'] = '蕨ケーブルビジョン';
$isp_list['%.ccn.ne.jp'] = 'J:COM所沢';
//メディアッティ東上 （メディアッティ東上）<J:COM系>
//本庄ケーブルテレビ
$isp_list['%.s-cat.ne.jp'] = '狭山ケーブルテレビ';
//行田ケーブルテレビ
$isp_list['%.tvkumagaya.ne.jp'] = '熊谷ケーブルテレビ';
$isp_list['%.ictv.ne.jp'] = '入間ケーブルテレビ';
//リバーシティ・ケーブルテレビ
$isp_list['%.chichibu.ne.jp'] = ' 泉企画';

//千葉
$isp_list['%.icnet.ne.jp'] = 'いちかわケーブルネットワーク';
$isp_list['%.catv296.ne.jp'] = '広域高速ネット二九六';
$isp_list['%.icntv.ne.jp'] = 'いちはらコミュニティー・ネットワーク・テレビ';
$isp_list['%.fnnr.j-cnet.jp'] = 'ＪＣＮ船橋習志野';
$isp_list['%.fnnr-static.j-cnet.jp'] = 'ＪＣＮ船橋習志野';
$isp_list['%.seaple.ne.jp'] = 'ＪＣＮ船橋習志野';
$isp_list['%.cnc.jp'] = 'ケーブルネットワーク千葉';
$isp_list['%.nctv.co.jp'] = '成田ケーブルテレビ';
$isp_list['%.rurbannet.ne.jp'] = '千葉ニュータウンセンター';
$isp_list['%.catv9.ne.jp'] = '銚子テレビ放送';
$isp_list['%.koalanet.ne.jp'] = 'ＪＣＮコアラ葛飾';
$isp_list['%.snu.ne.jp'] = 'スーパーネットワークユー';

//東京
$isp_list['%.tcv.jp'] = '東京ケーブルビジョン';
$isp_list['%mediatti.net'] = 'J:COM';
$isp_list['%.ohta.j-cnet.jp'] = '大田ケーブルネットワーク';
$isp_list['%.ohta-static.j-cnet.jp'] = '大田ケーブルネットワーク';
$isp_list['%.kakt.j-cnet.jp'] = 'ＪＣＮコアラ葛飾';

$isp_list['%.baynet.ne.jp'] = '東京ベイネットワーク';
$isp_list['%.cts.ne.jp'] = '南東京ケーブルテレビ';
$isp_list['%.rosenet.ne.jp'] = 'ケーブルテレビジョン東京';

$isp_list['%.tctv.ne.jp'] = '城北ニューメディア';
$isp_list['%.ocv.ne.jp'] = '小田急情報サービス';
$isp_list['%.toshima.ne.jp'] = '豊島ケーブルネットワーク';
$isp_list['%.ctn.co.jp'] = 'シティテレビ中野（JCN中野）';
$isp_list['%.nkno.j-cnet.jp'] = 'シティテレビ中野（JCN中野）';

$isp_list['%.kitanet.ne.jp'] = '北ケーブルネットワーク';
$isp_list['%.adachi.ne.jp'] = 'ケーブルテレビ足立';
$isp_list['%.tcn-catv.ne.jp'] = '東京ケーブルネットワーク';
$isp_list['%.mmcatv.co.jp'] = '武蔵野三鷹ケーブルテレビ';
$isp_list['%.parkcity.ne.jp'] = '武蔵野三鷹ケーブルテレビ';

$isp_list['%.mytv.co.jp'] = 'マイ・テレビ';
$isp_list['%.ttv.ne.jp'] = '多摩テレビ';
$isp_list['%.t-net.ne.jp'] = '多摩ケーブルネットワーク';
$isp_list['%.hinocatv.ne.jp'] = '日野ケーブルテレビ';
$isp_list['%.htoj.j-cnet.jp'] = '八王子テレメディア';
$isp_list['%.htmnet.ne.jp'] = '八王子テレメディア';
$isp_list['%.m-net.ne.jp'] = 'マイ・テレビ';

//神奈川
$isp_list['%.kamakuranet.ne.jp'] = '鎌倉ケーブルコミュニケーションズ';

$isp_list['%.ctktv.ne.jp'] = 'シティテレコムかながわ';
$isp_list['%.catv-yokohama.ne.jp'] = '横浜ケーブルビジョン';
$isp_list['%.htoj.j-cnet.jp'] = '小田原ケーブルテレビ';
$isp_list['%.kamakuranet.ne.jp'] = '鎌倉ケーブルコミュニケーションズ';

$isp_list['%.c3-net.ne.jp'] = 'ＪＣＮ横浜';
$isp_list['%.icc.ne.jp'] = 'ＪＣＮ横浜';
$isp_list['%.ttmy.ne.jp'] = 'ＪＣＮ横浜';

$isp_list['%.scn-net.ne.jp'] = '湘南ケーブルネットワーク';
$isp_list['%.netyou.jp'] = 'YOUテレビ';
//ケーブルシティ横浜 (CCY)
$isp_list['%.ayu.ne.jp'] = '厚木伊勢原ケーブルネットワーク';
$isp_list['%.tmtv.ne.jp'] = '横浜都市みらい';

//山梨
$isp_list['%.nns.ne.jp'] = '日本ネットワークサービス';
//ケーブルネットワーク大月（大月テレビ利用者組合・大月ケーブルビジョン）
$isp_list['%.cvk.ne.jp'] = '(有)峡西シーエーテーブイ';
$isp_list['%.kawaguchiko.ne.jp'] = 'ケーブルテレビ河口湖';
$isp_list['%.lcnet.jp'] = '河口湖有線テレビ放送';
$isp_list['%.fgo.jp'] = '(株)CATV富士五湖';
$isp_list['%.nus.ne.jp'] = '白根ケーブルネットワーク';
$isp_list['%.fruits.ne.jp'] = '山梨CATV';
$isp_list['%.kcnet.ne.jp'] = '峡東ケーブルネット';
$isp_list['61.117.150.%'] = '峡東ケーブルネット';

//笛吹きらめきテレビ（IFT）
$isp_list['%.katsunuma.ne.jp'] = '勝沼町CATV組合';
//都留市テレビ利用者組合
//いちのみやふれあいテレビ（笛吹市一宮有線テレビ）(IFT)
//小淵沢町農村多元情報システム (にこにこすていしょん)
//北富士有線テレビ放送
//ケーブルテレビ富士
//富士川シーエーティーヴィ
//峡南CATV
//富沢町テレビ共聴組合
//上野原ブロードバンドコミュニケーションズ
$isp_list['%.ubcnet.jp'] = '上野原ブロードバンドコミュニケーションズ';

//新潟県
//エヌ・シィ・ティ (NCT) <CCJ系>
$isp_list['%.nct9.ne.jp'] = 'エヌ・シィ・ティ';
$isp_list['%.ecatv.ne.jp'] = 'ニューメディア新潟センター';
$isp_list['%.tlp.ne.jp'] = 'ニューメディア新潟センター';

$isp_list['%.e-sadonet.tv'] = '佐渡テレビジョン';
$isp_list['%.joetsu.ne.jp'] = '上越ケーブルビジョン';
//コミュニケーションネットワーク佐渡 (CNS)
//刈羽村ケーブルテレビ
$isp_list['%.nou.ne.jp'] = '能生インターネット';
$isp_list['%.uct.ne.jp'] = '魚沼ケーブルテレビ';
//上越市三和ケーブルテレビ
//上越市吉川ケーブルテレビ
//上越市安塚ケーブルテレビ
//朝日村総合情報ネットワークシステム（村上市）
//山北町情報通信施設（村上市）

//長野
$isp_list['%.avis.ne.jp'] = '電算';
$isp_list['%.iiyama-catv.ne.jp'] = '飯山市';
//小谷村ケーブルテレビ
//ふう太ネット木島平　(木島平村)
$isp_list['%.kijimadaira.jp'] = '木島平村';
//テレビ菜の花　(野沢温泉村)
//中野市豊田情報センター (中野市豊田地区　TCV)
//テレビ北信ケーブルビジョン (中野市（豊田地区を除く）・山ノ内町　THV)
//須高ケーブルテレビ (須坂市・小布施町　STV)
//インフォメーション・ネットワーク・コミュニティ（長野市（戸隠地区・鬼無里地区を除く）　INC長野ケーブルテレビ）
//戸隠ケーブルテレビ (長野市戸隠地区)
//鬼無里ケーブルテレビ　(長野市鬼無里地区)
//コミュニティーネットワーク信州新町 (CNS)
//信州ケーブルテレビジョン（千曲市　ケーブルネット千曲）
//上田ケーブルビジョン (上田市（丸子地区・武石地区を除く）・東御市（北御牧地区を除く）・青木村・坂城町　UCV)
$isp_list['%.ueda.ne.jp'] = '上田ケーブルビジョン';
$isp_list['%.marukotv.jp'] = '丸子テレビ放送';
//とうみケーブルテレビ (東御市北御牧地区　TCT)
$isp_list['%.ctk23.ne.jp'] = 'コミュニティテレビこもろ';
$isp_list['%.kokuyou.ne.jp'] = '長和町ケーブルテレビ施設';
//佐久ケーブルテレビ (佐久市（望月地区の一部を除く）SCT)
$isp_list['%.sakunet.ne.jp'] = '佐久ケーブルテレビ';
//佐久高原ケーブルビジョン (佐久穂町　SCV)
//協和ビジョン (軽井沢町　KVC)
//西軽井沢ケーブルテレビ　(御代田町)
$isp_list['%.lcv.ne.jp'] = 'エルシーブイ';
$isp_list['%.tvm.ne.jp'] = 'テレビ松本ケーブルビジョン';
$isp_list['%.anc-tv.ne.jp'] = 'あづみ野テレビ';
//飯田ケーブルテレビ (ICTV)
$isp_list['%.inacatv.ne.jp'] = '伊那ケーブルテレビジョン';
//アルプスケーブルビジョン (ACV)
$isp_list['%.cek.ne.jp'] = 'エコーシティー･駒ケ岳';
$isp_list['%.takamori.ne.jp'] = '長野県高森町役場';
$isp_list['%.ch-you.ne.jp'] = 'チャンネル・ユー';
//蓼科ケーブルビジョン (立科町（白樺湖畔を除く）・佐久市望月地区の一部　TCV)
//朝日村有線テレビ (AYT)
//とよおか放送ネットワーク (THN)
//ふれあいネットワーク長谷 (CNH)
//ケーブルテレビミアサ (CTM)
//コミュニケーションネットワーク阿南
//木曽広域ケーブルテレビ
//阿智村情報化事業サービス
//生坂村コミュニケーションネットワーク (ICN)
//泰阜村コミュニケーションネットワーク (YCN)
$isp_list['%.miasa.ne.jp'] = '美麻村';

//岐阜
$isp_list['%.ogaki-tv.ne.jp'] = '大垣ケーブルテレビ';
//おりべネットワーク
//シーシーエヌ
$isp_list['%.ctk.ne.jp'] = 'ケーブルテレビ可児';
$isp_list['%.ccnet.ne.jp'] = '中部ケーブルネットワーク';
$isp_list['%.gujo-tv.ne.jp'] = '郡上ケーブルテレビ放送センター';
$isp_list['61.87.124.%'] = '郡上市';
$isp_list['61.87.125.%'] = '郡上市';
$isp_list['%.hidatakayama.ne.jp'] = '飛騨高山ケーブルネットワーク';
$isp_list['%.seiryu.ne.jp'] = '下呂ネットサービス';
$isp_list['%.ccy.ne.jp'] = '山県市有線テレビ';
$isp_list['%.kawaue.jp'] = 'かわうえケーブルテレビ';
$isp_list['%.50913.ne.jp'] = '東白川CATV';
$isp_list['%.hida-catv.jp'] = '飛騨市CATV';
$isp_list['%.gujocity.net'] = 'インフォメーションネットワーク郡上八幡';
//アミックスコム
//恵那市山岡ケーブルテレビネットワーク
//恵那市串原ケーブルテレビネットワーク
$isp_list['%.amixcom.jp'] = 'アミックスコム';
//さかうち田園ネット
//シーテック（CCNet養老局）
$isp_list['%.mirai.ne.jp'] = '未来精工';

//静岡
$isp_list['%.i-younet.ne.jp'] = '(株)伊豆急ケーブルネットワーク';
$isp_list['%.hctnet.ne.jp'] = '浜松ケーブルテレビ';
//ビック東海（@TCOM）<ビック東海系>
$isp_list['%.tokai.or.jp'] = 'ザ・トーカイ';
$isp_list['%.tnc.ne.jp'] = 'ザ・トーカイ';
$isp_list['%.thn.ne.jp'] = 'ビック東海';
$isp_list['%.maotv.ne.jp'] = '御前崎ケーブルテレビ';
$isp_list['%.s-cnet.ne.jp'] = 'ドリームウェーブ静岡';
$isp_list['%.gotemba.ne.jp'] = '御殿場ケーブルメディア';
//東伊豆有線テレビ放送 (HI-CAT)
//下田有線テレビ放送 (SHK)
//東豆有線
//伊東テレビクラブ
//小林テレビ設備
//シオヤ
//伊豆太陽農業共同組合
//小山町テレビ共聴組合

//愛知県
$isp_list['%.orihime.ne.jp'] = 'アイ・シー・シー';
$isp_list['%.c-d-k.ne.jp'] = 'ICC 一宮ケーブルテレビ';
$isp_list['%.toptower.ne.jp'] = '稲沢シーエーティーヴィ';
$isp_list['%.katch.ne.jp'] = 'キャッチネットワーク';
$isp_list['%.gctv.ne.jp'] = 'グリーンシティケーブルテレビ';
$isp_list['%.cac-net.ne.jp'] = 'ＣＡＣ';
$isp_list['%.starcat.ne.jp'] = 'スターキャット';
$isp_list['%.tac-net.ne.jp'] = '知多半島ケーブルネットワーク';
$isp_list['%.medias.ne.jp'] = '知多メディアスネットワーク';
$isp_list['%.ccnw.ne.jp'] = '中部ケーブルネットワーク';
$isp_list['%.tctvnet.ne.jp'] = '中部ケーブルネットワーク';
$isp_list['%.kctv.ne.jp'] = '中部ケーブルネットワーク';
$isp_list['%.tees.ne.jp'] = '豊橋ケーブルネットワーク';
$isp_list['%.clovernet.ne.jp'] = '西尾張シーエーティーヴィ';
$isp_list['%.catvmics.ne.jp'] = 'ミクスネットワーク';
//ひまわりネットワーク
//三河湾ネットワーク
//名古屋ケーブルビジョン
//シーテック（CCNet豊川局）
$isp_list['%.ccnet-ai.ne.jp'] = ' コミュニティネットワークセンター';

//三重
$isp_list['%.amigo.ne.jp'] = 'アイティービー';
$isp_list['%.amigo2.ne.jp'] = 'アイティービー';
$isp_list['%.nava21.ne.jp'] = 'アドバンスコープ';
$isp_list['%.asint.jp'] = 'アドバンスコープ';
$isp_list['%.assp.jp'] = 'アドバンスコープ';
$isp_list['%.ict.ne.jp'] = '伊賀上野ケーブルテレビ';
$isp_list['%.mecha.ne.jp'] = 'ケーブルネット鈴鹿';
$isp_list['%.cty-net.ne.jp'] = 'シー・ティー・ワイ';
$isp_list['%.intsurf.ne.jp'] = 'ラッキータウンテレビ';
$isp_list['%.mctv.ne.jp'] = '松阪ケーブルテレビ・ステーション';
$isp_list['%.ccnetmie.ne.jp'] = '中部ケーブルネットワーク';
$isp_list['%.ciaotv.ne.jp'] = '飯南放送通信センター';

//富山
$isp_list['%.ctt.ne.jp'] = 'ケーブルテレビ富山';
$isp_list['%.cty8.com'] = 'ケーブルテレビ富山';
$isp_list['%.canet.ne.jp'] = '射水ケーブルネットワーク';
$isp_list['%.tcnet.ne.jp'] = '高岡ケーブルネットワーク';
$isp_list['%.tst.ne.jp'] = 'となみ衛星通信テレビ';
$isp_list['%.nice-tv.jp'] = '新川インフォメーションセンター';
$isp_list['%.cnh.ne.jp'] = '能越ケーブルネット';
$isp_list['%.milare-tv.ne.jp'] = 'ニイカワポータル';
$isp_list['%.knei.jp'] = '上婦負ケーブルテレビ';

//石川県
$isp_list['%.kagacable.ne.jp'] = '加賀ケーブルテレビ';
$isp_list['%.spacelan.ne.jp'] = '金沢ケーブルテレビネット';
$isp_list['%.scnet.tv'] = '金沢ケーブルテレビネット';
$isp_list['%.notojima.jp'] = '金沢ケーブルテレビネット';
$isp_list['%.tvk.ne.jp'] = 'テレビ小松';
$isp_list['%.asagaotv.ne.jp'] = 'あさがおテレビ';
//柳田ふれあいNet
$isp_list['%.kaga-tv.com'] = '加賀テレビ';
//北陸アイティエス
//輪島市ケーブルテレビ

//福井
//和泉ケーブルネットワーク
//大野ケーブルテレビ
//ケーブルテレビ若狭小浜（チャンネルO）
//おおいテレビ (OH-CATV)
//ケーブルネットワークかみなか (CNK)
//高浜町ケーブルテレビ
$isp_list['%.wakasa-takahama.tv'] = '近畿コンピュータサービス'; //nkansai?
//こしの国広域事務組合
$isp_list['%.vipa.ne.jp'] = 'NTT西日本-北陸';
$isp_list['%.ttn.ne.jp'] = '丹南ケーブルテレビ';
$isp_list['%.fctv.ne.jp'] = '福井ケーブルテレビ';
$isp_list['%.sctv.ne.jp'] = 'さかいケーブルテレビ';
$isp_list['%.mmnet-ai.ne.jp'] = '美方ケーブルネットワーク';
//南越前町ケーブルテレビ
$isp_list['%.rcn.ne.jp'] = '嶺南ケーブルネットワーク';
$isp_list['%.mitene.or.jp'] = '三谷商事';

//大阪
//ジェイコムウエスト（J:COM大阪、J:COMかわち、J:COMりんくう、J:COM堺、J:COM南大阪、J:COM和泉・泉大津、　J:COM 北摂、J:COM 大阪セントラル、J:COM 東大阪、J:COM 北河内、J:COM 吹田、J:COM 高槻、J:COM 豊中・池田）<J:COM系>
//近鉄ケーブルネットワーク (KCN)<KCN系>
//テレビ岸和田<KCN系
$isp_list['%.sensyu.ne.jp'] = '泉州ケーブルサービス';
//京阪神ケーブルビジョン
//ケイ・キャット (K-CAT)
//ケイ・キャット eo光テレビ※1
//KCN eo光テレビ※1

//滋賀
$isp_list['%.cable-net.ne.jp'] = 'ZTV滋賀放送局';
//ZTV
//甲賀ケーブルネットワーク (KCN)ZAQかな
$isp_list['%.e-omi.ne.jp'] = '東近江ケーブルネットワーク'; //違うかも
$isp_list['%.hottv.ne.jp'] = 'ZTV近江八幡支局';
//木之本町ケーブルテレビ
//K-CAT eo光テレビ※1

//京都
//KCN京都<KCN系>
$isp_list['%.kinet-tv.ne.jp'] = 'ＫＣＮ京都';
$isp_list['%.dsn.jp'] = 'ケイ・オプティコム';
//京都ケーブルコミュニケーションズ（みやびじょん）<J:COM系>
//ケイ・キャット
//洛西ケーブルビジョン
$isp_list['%.city.nantan.kyoto.jp'] = '南丹市情報センター';
$isp_list['%.town.kyotamba.kyoto.jp'] = '瑞穂町役場';
$isp_list['%.kyt-net.ne.jp'] = '与謝野町有線テレビ';
//全関西ケーブルテレビジョン京丹後局（2009年12月サービス開始予定）
//日本ケーブルビジョン
//京阪神ケーブルビジョン
//K-CAT eo光テレビ※1
$isp_list['%.shinet.ne.jp'] = '丹波町地域ネットワーク';

//ここまで
//兵庫
//ジェイコムウエスト（J:COM 宝塚・川西）<J:COM系>
//ケーブルネット神戸芦屋（J:COM 神戸・芦屋、J:COM 神戸・三木（旧ケーブルテレビ神戸））<J:COM系>
$isp_list['%.kobe-catv.ne.jp'] = 'ケーブルネット神戸芦屋';
//明石ケーブルテレビ (ACTV135) zaq?
$isp_list['%.sumoto.gr.jp'] = '淡路島テレビジョン';
$isp_list['%.sansan-net.jp'] = 'ケーブルネットワーク淡路';
$isp_list['%.banban.jp'] = 'BAN-BANテレビ';
//京阪神ケーブルビジョン zaq?
$isp_list['%.winknet.ne.jp'] = '姫路ケーブルテレビ';
$isp_list['%.cin.ne.jp'] = 'ｅ―ちくさネットワーク';
$isp_list['%.yabu-catv.or.jp'] = '養父市ケーブルテレビジョン';
$isp_list['%.asago-net.jp'] = '朝来市ケーブルテレビ';
$isp_list['%.yumenet.tv'] = '新温泉町ケーブルテレビ事業室';
//加東ケーブルビジョン（KCV）
//（「加東市滝野ケーブルコミュニケーション（TCC）」と「テレネットやしろ（TNY）」は、加東ケーブルビジョンに統合された。）
$isp_list['%.tccnet.tv'] = '加東市滝野ケーブルコミュニケーション';

//たかテレビ（多可町）
$isp_list['%.kamitv.ne.jp'] = 'たかテレビ';
$isp_list['%.kcni.ne.jp'] = '神河町ケーブルテレビネットワーク';
$isp_list['%.yumetv.jp'] = '夢前情報センター';

//K-CAT eo光テレビ※1

//奈良
//近鉄ケーブルネットワーク (KCN)<KCN系>
//こまどりケーブル<KCN系>
$isp_list['%.komadori.ne.jp'] = 'こまどりケーブル';

//和歌山
//ジェイコムウエスト（J:COM 和歌山）<J:COM系>
//全関西ケーブルテレビジョン紀の川放送局（ACTV 紀の川局）
//ZTV
//K-CAT eo光テレビ※1

//鳥取
//中海テレビ放送
$isp_list['%.chukai.ne.jp'] = '中海テレビ放送';
//鳥取中央有線放送 (TCC)
$isp_list['%.torichu.ne.jp'] = '鳥取中央有線放送';
$isp_list['%.hcvnet.jp'] = '鳥取中央有線放送';
$isp_list['%.tcbnet.ne.jp'] = '鳥取中央有線放送';
//.yurihama.jp
//.e-hokuei.net
$isp_list['%.inabapyonpyon.net'] = '鳥取テレトピア';
//日本海ケーブルネットワーク (NCN)
$isp_list['%.oninosato.jp'] = '伯耆町有線テレビ放送';

//島根
$isp_list['%.icv.ne.jp'] = '出雲ケーブルビジョン';
$isp_list['%.ginzan-tv.ne.jp'] = '石見銀山テレビ放送';
$isp_list['%.iwamicatv.jp'] = '石見ケーブルビジョン';
$isp_list['%.i-yume.ne.jp'] = '雲南市・飯南町事務組合';
$isp_list['%.kkm.ne.jp'] = '雲南市・飯南町事務組合';
$isp_list['%.okuizumo.ne.jp'] = '奥出雲町情報通信協会';
//鹿島ケーブルビジョン（鹿島すまいるネット）

$isp_list['%.mable.ne.jp'] = '山陰ケーブルビジョン';
$isp_list['%.sun-net.jp'] = 'サンネットにちはら';
$isp_list['%.hit-5.net'] = 'ひらたＣＡＴＶ';
$isp_list['%.herecall.jp'] = '浜田市三隅ケーブルテレビ';
$isp_list['%.ohtv.ne.jp'] = 'おおなんケーブルテレビ';

//岡山
$isp_list['%.ibara.ne.jp'] = '井原放送';
$isp_list['%.oninet.ne.jp'] = '岡山ネットワーク';
$isp_list['%.kcv.ne.jp'] = '笠岡放送';
$isp_list['%.kibi.ne.jp'] = '吉備ケーブルテレビ';
$isp_list['%.kct.ne.jp'] = '倉敷ケーブルテレビ';
$isp_list['%.kct.ad.jp'] = '倉敷ケーブルテレビ';
$isp_list['%.cnknet.jp'] = '玉島テレビ放送';
$isp_list['%.tamatele.ne.jp'] = '玉島テレビ放送';
$isp_list['%.tvt.ne.jp'] = 'テレビ津山';
$isp_list['%.nariwa.ne.jp'] = '高梁市成羽有線テレビジョン';
$isp_list['%.hinase.ne.jp'] = '日生有線テレビ';
$isp_list['%.yct.ne.jp'] = '矢掛放送';
//ACT新見放送
//鏡野町有線テレビ
//真庭市CATV
//みさきネット
//美作市ケーブルテレビ
//あわくら光ネット

//広島
//井原放送(岡山）
//オプティキャスト
$isp_list['%.bbbn.jp'] = '尾道ケーブルテレビ';
//きたひろネット（北広島町）
$isp_list['%.ccjnet.ne.jp'] = 'ケーブル・ジョイ';
$isp_list['%.kamon.ne.jp'] = '東広島ケーブルメディア';
$isp_list['%.hicat.ne.jp'] = 'ひろしまケーブルテレビ';
$isp_list['%.fch.ne.jp'] = ' ふれあいチャンネル';
$isp_list['%.cc22.ne.jp'] = 'ふれあいチャンネル西部支局';
$isp_list['%.ccv.ne.jp'] = 'ふれあいチャンネル';
$isp_list['%.mcat.ne.jp'] = '三原テレビ放送';
//せらケーブルねっと
//三次ケーブルビジョン (Pionet)
$isp_list['%.pionet.ne.jp'] = '三次ケーブルビジョン';
//安芸矢野ニュータウンCATV組合
//仁保南CATV施設管理組合
//A.CITYヒルズ&タワーズ管理組合
//ケーブルとよはま
$isp_list['%.hbs.ne.jp'] = '広島ケーブルテレビジョン';

//山口
//ケーブルネット下関（J:COM下関）<J:COM系>
//アイ・キャン
$isp_list['%.icn-tv.ne.jp'] = 'アイ・キャン';
//Kビジョン
$isp_list['%.kvision.ne.jp'] = 'Ｋビジョン';
//シティーケーブル周南
$isp_list['%.ccsnet.ne.jp'] = 'シティーケーブル周南';
//萩ケーブルネットワーク
$isp_list['%.haginet.ne.jp'] = '萩ケーブルネットワーク';
//ほっちゃテレビ（長門市ケーブルテレビ）
$isp_list['%.hot-cha.tv'] = '長門市ケーブルテレビ';
//山口ケーブルビジョン (C-able)
$isp_list['%.c-able.ne.jp'] = '山口ケーブルビジョン';

//美祢市有線テレビ
$isp_list['%.m-netbb.com'] = 'メディアリンク'; // 違うかも
//岐北地区テレビ共同受信施設組合
$isp_list['%.suo-cable.net'] = '周防ケーブルネット'; // 違うかも

//徳島
$isp_list['%.awaikeda.net'] = '池田ケーブルネットワーク';
//石井町有線放送農業協同組合（石井CATV）
//エーアイテレビ
$isp_list['%.tcn.ne.jp'] = 'ケーブルテレビ徳島';
$isp_list['%.kbctv.ne.jp'] = '国府町ＣＡＴＶ';
$isp_list['%.tv-naruto.ne.jp'] = 'テレビ鳴門';
//徳島県南メディアネットワーク (MTCTV)
$isp_list['%.jctv.ne.jp'] = '徳島中央テレビ';
$isp_list['%.whk.ne.jp'] = '那賀町ケーブルテレビ';
$isp_list['%.e-awa.net'] = '東阿波ケーブルテレビ';
//ひのき (キューテレビ)
//テレビ阿波
$isp_list['%.njctv.ne.jp'] = '那賀町上流ケーブルテレビ';
//ケーブルテレビあなん
//上板町有線テレビ
//ケーブルネットおえ
//ピカラ光てれび※1
//スカパー!光※1

//香川
$isp_list['%.kbn.ne.jp'] = '香川テレビ放送網';
//ケーブルメディア四国（高松ケーブルテレビ）(CMS)
$isp_list['%.sanuki.ne.jp'] = 'さぬき市ケーブルネットワーク';
//中讃ケーブルビジョン（中讃テレビ）(CVC)
$isp_list['%.mcbnet.ne.jp'] = '三豊ケーブルテレビ放送';
//高松市塩江ケーブルネットワーク
//苗羽坂手テレビ共聴施設組合

//愛媛
$isp_list['%.icknet.ne.jp'] = '今治シーエーティーブィ';
//宇和島ケーブルテレビ (UCAT)
//愛媛CATV※3
$isp_list['%.e-catv.ne.jp'] = '愛媛シーエーティヴィ';
$isp_list['%.cnw.ne.jp'] = 'ケーブルネットワーク西瀬戸';
$isp_list['%.hearts.ne.jp'] = 'ハートネットワーク';
//八西地域総合情報センター（八西CATV）
//野村ケーブルテレビ（かぼちゃ）
//西予CATV
$isp_list['%.cosmostv.jp'] = '四国中央テレビ';
$isp_list['%.namikata.ne.jp'] = '今治市波方ＣＡＴＶ';

//高知
$isp_list['%.kcb-net.ne.jp'] = '高知ケーブルテレビ';
$isp_list['%.gallery.ne.jp'] = '西南地域ネットワーク';
$isp_list['%.scatv.ne.jp'] = 'よさこいケーブルネット';
//香南施設農業協同組合（香南ケーブルテレビ）
$isp_list['%.shimanto.tv'] = '四万十町ケーブルネットワーク';
//甲浦テレビ共同聴視施設組合
//土佐有線テレビ施設組合
//梼原あんしん光ネット

//四国
$isp_list['%.catvnet.ne.jp'] = 'STNet';

//福岡
//ジェイコム福岡（J:COM福岡）<J:COM系>（旧・福岡ケーブルネットワーク）
//ケーブルビジョン21（福岡ケーブルネットワークに吸収合併された）<J:COM系>
$isp_list['%.fcv.ne.jp'] = '福岡ケーブルビジョン';
$isp_list['%.csf.ne.jp'] = '九州テレ・コミュニケーションズ';
//ジェイコム北九州（J:COM北九州）<J:COM系>
//北九州ケーブルビジョン
$isp_list['%.kumin.ne.jp'] = 'ＣＲＣＣメディア';
$isp_list['%.cv-net.jp'] = 'メック';
//ケーブルネットワーク桂川
$isp_list['%.ymdtv.jp'] = '嘉麻市ケーブルテレビジョン';

//佐賀
//伊万里ケーブルテレビジョン
$isp_list['%.hachigamenet.ne.jp'] = '伊万里ケーブルテレビジョン';
//佐賀シティビジョン（ぶんぶんテレビ）
$isp_list['%.bunbun.ne.jp'] = '佐賀シティビジョン';
$isp_list['%.aritanet.ne.jp'] = '有田ケーブルネットワーク';
$isp_list['%.people-i.ne.jp'] = '唐津ケーブルテレビジョン';
$isp_list['%.cableone.ne.jp'] = 'ケーブルワン';
$isp_list['%.chun2.ne.jp'] = '西海テレビ';
$isp_list['%.taku.ne.jp'] = '多久ケーブルテレビ';
$isp_list['%.ktknet.ne.jp'] = 'テレビ九州';
$isp_list['%.hagakure.ne.jp'] = '藤津ケーブルビジョン';
$isp_list['%.asunet.ne.jp'] = 'ネット鹿島';
$isp_list['%.netfour.ne.jp'] = 'ネットフォー';
//唐津市有線テレビジョン
$isp_list['%.hhc.ne.jp'] = '唐津市浜玉ケーブルネットワーク';
//多久市テレビ共同聴視組合
//ふじ有線テレビ

//長崎
$isp_list['%.tvs12.jp'] = 'テレビ佐世保';
$isp_list['%.shimabara.jp'] = 'ケーブルテレビジョン島原';
$isp_list['%.cncm.ne.jp'] = '長崎ケーブルメディア';
$isp_list['%.icv-net.ne.jp'] = '諫早ケーブルテレビジョン放送';
//諫早市小長井地域有線テレビジョン施設
//オクト・パルス
$isp_list['%.octp-net.ne.jp'] = 'オクト・パルス';
$isp_list['%.himawarinet.ne.jp'] = '西九州電設';
//波佐見ケーブルテレビ
$isp_list['%.fctv-net.jp'] = '福江ケーブルテレビ';
//美津島町有線テレビ (MYT) （対馬市市営）
$isp_list['203.215.56.%'] = 'コミュニティメディア';
//上対馬町比田勝テレビ協同聴視組合（対馬市）
//厳原有線テレビ利用共同組合（対馬市）
//西彼ケーブルネットワーク
//東彼ケーブルテレビ

//熊本
$isp_list['%.kcn-tv.ne.jp'] = '熊本ケーブルネットワーク';
$isp_list['%.acn-tv.ne.jp'] = '天草ケーブルネットワーク';
$isp_list['%.toyo-tv.jp'] = '八代ケーブルテレビ東陽センター'; //違うかも
$isp_list['%.sakamoto-catv.jp'] = '八代ケーブルテレビ坂本センター'; //違うかも
//みなみチャンネル

//大分
//大分ケーブルテレコム (OCT)
$isp_list['%.oct-net.jp'] = '大分ケーブルテレコム';
$isp_list['%.ocn-catv.ne.jp'] = '大分ケーブルネットワーク'; //違うかも
//佐賀関テレビ
//北大ケーブル情報センター
//姫島村ケーブルテレビ
//国東市ケーブルテレビセンター
//豊後高田市ケーブルネットワーク施設
$isp_list['%.kdt.ne.jp'] = '杵築市ケーブルネットワーク';
//CTBメディア (CTB)
$isp_list['%.ctb.ne.jp'] = 'ＣＴＢメディア';
//KCVコミュニケーションズ (KCV)
$isp_list['%.kcv.jp'] = 'ＫＣＶコミュニケーションズ';
//日田市大山情報センター
//玖珠テレビ
//豊後大野市おおのケーブルテレビ
//臼杵市ケーブルネットワーク (U-net)
$isp_list['%.oct-net.ne.jp'] = '臼杵ケーブルネット';
//ケーブルテレビ佐伯 (CTS)
$isp_list['%.cts-net.ne.jp'] = 'ケーブルテレビ佐伯';
//上浦ケーブルテレビ※CTS経由(テレビ・インターネット)
//宇目ケーブルテレビ※CTS経由(テレビ・インターネット)
//ケーブルテレビかまえ※CTS経由(テレビ・インターネット)
//直川ケーブルテレビ※CTS経由(テレビ・インターネット)
//本匠ケーブルテレビ（ホタル）※インターネットのみCTS経由
//弥生ケーブルテレビ※インターネットのみCTS経由
//ケーブルテレビつるみ※インターネットのみCTS経由
//米水津ケーブルテレビ※インターネットのみCTS経由

//宮崎
//ケーブルメディアワイワイ（わぃWaiTV）
$isp_list['%.wainet.ne.jp'] = 'ケーブルメディアワイワイ';
//宮崎ケーブルテレビ (MCN)
$isp_list['%.miyazaki-catv.ne.jp'] = '宮崎ケーブルテレビ';
//きららびじょん

//鹿児島
$isp_list['%.nils.ne.jp'] = '皇徳寺ケーブルテレビ';
//ビィーティーヴィーケーブルテレビ（BTVケーブルテレビ 都城局・鹿児島局）
$isp_list['%.mct.ne.jp'] = '南九州ケーブルテレビネット';
//西之表テレビ共同視聴施設組合
//瀬戸内ケーブルテレビ
//屋久町テレビ共同受信施設組合
//美浜テレビ共同アンテナ受信施設組合
//天城町ユイの里テレビ <AYT>
//和泊町有線テレビ
//姶良共同受信組合
//南部有線テレビ放送
//鹿児島光テレビ （BBIQ光テレビ）※1

//沖縄
//沖縄ケーブルネットワーク (OCN)
$isp_list['%.nirai.ne.jp'] = '沖縄ケーブルネットワーク';
$isp_list['%.ictweb.ne.jp'] = '石垣ケーブルテレビ';
$isp_list['%.miyako-net.ne.jp'] = '宮古テレビ';
$isp_list['%.miyako-ma.jp'] = '宮古テレビ';

//複数地域
$isp_list['%.kcn.ne.jp'] = '近鉄ケーブルネットワーク';

$isp_list['%.bai.ne.jp'] = 'ベイ・コミュニケーションズ';

$isp_list['%.ztv.ne.jp'] = 'ZTV';

$isp_list['%.btvm.ne.jp'] = 'ビィーティーヴィーケーブルテレビ';

$isp_list['%.zaq.ne.jp'] = '関西マルチメディアサービス';

$isp_list['%.aitai.ne.jp'] = 'ひまわりネットワーク';

$isp_list['%.bbiq.jp'] = '九州通信ネットワーク';

$isp_list['%.itscom.jp'] = '東急沿線のケーブルテレビ';
$isp_list['%.246.ne.jp'] = '東急沿線のケーブルテレビ';

$isp_list['%.eonet.ne.jp'] = 'ケイ・オプティコム';
$isp_list['%.netwave.or.jp'] = 'STNet';

//エネルギアコミニュケーションズ';
//ぴから
$isp_list['%.pikara.ne.jp'] = 'ST-Net';

//コミュファ光
$isp_list['%.commufa.jp'] = '中部テレコミニュケーション';
?>