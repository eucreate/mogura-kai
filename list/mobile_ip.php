<?php
$mobile_ip_list = array();
global $mobile_ip_list;
/*==============================================================================*/
/*    ■ モバイルIPリスト                                                       */
/*                                                                              */
/*    $mobile_ip_list['キャリア'][] = 'IP帯域';                                 */
/*                                                                              */
/*==============================================================================*/
# docomo
# 更新日時:2011-06-26
# http://www.nttdocomo.co.jp/service/developer/make/content/ip/
$mobile_ip_list['docomo'][] = '210.153.84.0/24';
$mobile_ip_list['docomo'][] = '210.136.161.0/24';
$mobile_ip_list['docomo'][] = '210.153.86.0/24';
$mobile_ip_list['docomo'][] = '124.146.174.0/24';
$mobile_ip_list['docomo'][] = '124.146.175.0/24';
$mobile_ip_list['docomo'][] = '202.229.176.0/24';
$mobile_ip_list['docomo'][] = '202.229.177.0/24';
$mobile_ip_list['docomo'][] = '202.229.178.0/24';
$mobile_ip_list['docomo'][] = '210.153.87.0/24';

# au
# 更新日時:2013-09-29
# http://www.au.kddi.com/ezfactory/tec/spec/ezsava_ip.html
$mobile_ip_list['au'][] = '111.107.116.64/26';
$mobile_ip_list['au'][] = '106.162.214.160/29';
$mobile_ip_list['au'][] = '111.107.116.192/28';
$mobile_ip_list['au'][] = '210.230.128.224/28';
$mobile_ip_list['au'][] = '219.108.158.0/27';
$mobile_ip_list['au'][] = '219.125.146.0/28';
$mobile_ip_list['au'][] = '61.117.2.32/29';
$mobile_ip_list['au'][] = '61.117.2.40/29';
$mobile_ip_list['au'][] = '219.108.158.40/29';
$mobile_ip_list['au'][] = '111.86.142.0/26';
$mobile_ip_list['au'][] = '111.86.141.64/26';
$mobile_ip_list['au'][] = '111.86.141.128/26';
$mobile_ip_list['au'][] = '111.86.141.192/26';
$mobile_ip_list['au'][] = '111.86.143.192/27';
$mobile_ip_list['au'][] = '111.86.143.224/27';
$mobile_ip_list['au'][] = '111.86.147.0/27';
$mobile_ip_list['au'][] = '111.86.142.128/27';
$mobile_ip_list['au'][] = '111.86.142.160/27';
$mobile_ip_list['au'][] = '111.86.142.192/27';
$mobile_ip_list['au'][] = '111.86.142.224/27';
$mobile_ip_list['au'][] = '111.86.143.0/27';
$mobile_ip_list['au'][] = '111.86.143.32/27';
$mobile_ip_list['au'][] = '111.86.147.32/27';
$mobile_ip_list['au'][] = '111.86.147.64/27';
$mobile_ip_list['au'][] = '111.86.147.96/27';
$mobile_ip_list['au'][] = '111.86.147.128/27';
$mobile_ip_list['au'][] = '111.86.147.160/27';
$mobile_ip_list['au'][] = '111.86.147.192/27';
$mobile_ip_list['au'][] = '111.86.147.224/27';
$mobile_ip_list['au'][] = '222.15.68.192/26';
$mobile_ip_list['au'][] = '59.135.39.128/27';
$mobile_ip_list['au'][] = '118.152.214.160/27';
$mobile_ip_list['au'][] = '118.152.214.128/27';
$mobile_ip_list['au'][] = '222.1.136.96/27';
$mobile_ip_list['au'][] = '222.1.136.64/27';
$mobile_ip_list['au'][] = '59.128.128.0/20';
$mobile_ip_list['au'][] = '111.86.140.40/30';
$mobile_ip_list['au'][] = '111.86.140.44/30';
$mobile_ip_list['au'][] = '111.86.140.48/30';
$mobile_ip_list['au'][] = '111.86.140.52/30';
$mobile_ip_list['au'][] = '111.86.140.56/30';
$mobile_ip_list['au'][] = '111.86.140.60/30';
$mobile_ip_list['au'][] = '111.87.241.144/28';

# softbank
# 更新日時:2013-09-29
# http://creation.mb.softbank.jp/mc/tech/tech_web/web_ipaddress.html
$mobile_ip_list['softbank'][] = '123.108.237.112/28';
$mobile_ip_list['softbank'][] = '123.108.239.224/28';
$mobile_ip_list['softbank'][] = '202.253.96.144/28';
$mobile_ip_list['softbank'][] = '202.253.99.144/28';
$mobile_ip_list['softbank'][] = '210.228.189.188/30';
$mobile_ip_list['softbank'][] = '123.108.237.128/28';
$mobile_ip_list['softbank'][] = '123.108.239.240/28';
$mobile_ip_list['softbank'][] = '202.253.96.160/28';
$mobile_ip_list['softbank'][] = '202.253.99.160/28';
$mobile_ip_list['softbank'][] = '210.228.189.196/30';

# willcom
# 更新日時:2014-01-16
# http://www.willcom-inc.com/ja/service/contents_service/create/center_info/index.html
$mobile_ip_list['willcom'][] = '114.20.49.0/24';
$mobile_ip_list['willcom'][] = '114.20.50.0/24';
$mobile_ip_list['willcom'][] = '114.20.51.0/24';
$mobile_ip_list['willcom'][] = '114.20.52.0/24';
$mobile_ip_list['willcom'][] = '114.20.53.0/24';
$mobile_ip_list['willcom'][] = '114.20.54.0/24';
$mobile_ip_list['willcom'][] = '114.20.55.0/24';
$mobile_ip_list['willcom'][] = '114.20.56.0/24';
$mobile_ip_list['willcom'][] = '114.20.57.0/24';
$mobile_ip_list['willcom'][] = '114.20.58.0/24';
$mobile_ip_list['willcom'][] = '114.20.59.0/24';
$mobile_ip_list['willcom'][] = '114.20.60.0/24';
$mobile_ip_list['willcom'][] = '114.20.61.0/24';
$mobile_ip_list['willcom'][] = '114.20.62.0/24';
$mobile_ip_list['willcom'][] = '114.20.63.0/24';
$mobile_ip_list['willcom'][] = '114.20.64.0/24';
$mobile_ip_list['willcom'][] = '114.20.65.0/24';
$mobile_ip_list['willcom'][] = '114.20.66.0/24';
$mobile_ip_list['willcom'][] = '114.20.67.0/24';
$mobile_ip_list['willcom'][] = '114.20.71.0/24';
$mobile_ip_list['willcom'][] = '114.20.128.0/17';
$mobile_ip_list['willcom'][] = '114.21.128.0/24';
$mobile_ip_list['willcom'][] = '114.21.129.0/24';
$mobile_ip_list['willcom'][] = '114.21.130.0/24';
$mobile_ip_list['willcom'][] = '114.21.131.0/24';
$mobile_ip_list['willcom'][] = '114.21.132.0/24';
$mobile_ip_list['willcom'][] = '114.21.133.0/24';
$mobile_ip_list['willcom'][] = '114.21.134.0/24';
$mobile_ip_list['willcom'][] = '114.21.135.0/24';
$mobile_ip_list['willcom'][] = '114.21.136.0/24';
$mobile_ip_list['willcom'][] = '114.21.137.0/24';
$mobile_ip_list['willcom'][] = '114.21.138.0/24';
$mobile_ip_list['willcom'][] = '114.21.139.0/24';
$mobile_ip_list['willcom'][] = '114.21.140.0/24';
$mobile_ip_list['willcom'][] = '114.21.141.0/24';
$mobile_ip_list['willcom'][] = '114.21.142.0/24';
$mobile_ip_list['willcom'][] = '114.21.143.0/24';
$mobile_ip_list['willcom'][] = '114.21.144.0/24';
$mobile_ip_list['willcom'][] = '114.21.145.0/24';
$mobile_ip_list['willcom'][] = '114.21.146.0/24';
$mobile_ip_list['willcom'][] = '114.21.147.0/24';
$mobile_ip_list['willcom'][] = '114.21.148.0/24';
$mobile_ip_list['willcom'][] = '114.21.149.0/24';
$mobile_ip_list['willcom'][] = '114.21.151.0/24';
$mobile_ip_list['willcom'][] = '221.109.128.0/18';
?>