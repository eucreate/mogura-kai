<?php
$crawler_list = array();
global $crawler_list;
/*==============================================================================*/
/*    ■ クローラリスト                                                         */
/*                                                                              */
/*    $crawler_list['UserAgent'] = 'Name';                                      */
/*                                                                              */
/*==============================================================================*/
//Google
$crawler_list['Googlebot-Mobile'] = 'Google-Mobile';
$crawler_list['Googlebot-Image'] = 'Google-Image';
$crawler_list['Googlebot'] = 'Google';
$crawler_list['Google Wireless Transcoder'] = 'Google-Wireless';
$crawler_list['Mediapartners-Google'] = 'Google';
$crawler_list['Feedfetcher-Google'] = 'Google-Feedfetcher';

//Yahoo!
$crawler_list['Yahoo! Slurp'] = 'Yahoo!';
$crawler_list['Y!J-MBS/'] = 'Yahoo!-Mobile';
$crawler_list['Y!J-SRD/'] = 'Yahoo!-Mobile';
$crawler_list['Y!J-BSC/'] = 'Yahoo!Blog';
$crawler_list['Y!J'] = 'Yahoo! Japan';
$crawler_list['YahooSeeker'] = 'Yahoo!Seeker';
$crawler_list['YahooFeedSeeker'] = 'Yahoo!FeedSeeker';
$crawler_list['Yahoo! DE Slurp'] = 'Yahoo!';
$crawler_list['YahooYSMcm/'] = 'Yahoo!YSMcm';
$crawler_list['Yahoo! SearchMonkey'] = 'Yahoo!SearchMonkey';
$crawler_list['Yahoo-MMCrawler'] = 'Yahoo!MM';

//Baidu
$crawler_list['Baiduspider'] = 'Baidu';
$crawler_list['BaiduImagespider'] = 'Baidu';
$crawler_list['BaiduMobaider/'] = 'Baidu-Mobile';

//MSN
$crawler_list['msnbot-media'] = 'MSN';
$crawler_list['msnbot-webmaster/'] = 'MSN-WebMaster';
$crawler_list['MSNBOT-MOBILE/'] = 'MSN-Mobile';
$crawler_list['msnbot'] = 'MSN';
$crawler_list['bingbot/'] = 'Bing';

//goo
$crawler_list['gooblogsearch/'] = 'goo';
$crawler_list['Slurp.so/Goo; slurp'] = 'goo';
$crawler_list['mogimogi'] = 'goo';
$crawler_list['moget'] = 'goo';
$crawler_list['ichiro/'] = 'goo';
$crawler_list['/emu.mobile.goo.ne.jp/'] = 'goo';
$crawler_list['ICC-Crawler/'] = 'ICC-Crawler';
$crawler_list['MetamojiCrawler/'] = 'Metamoji';
$crawler_list['JUST-CRAWLER'] = 'JUSTSYSTEMS';

//はてなアンテナ
$crawler_list['Hatena'] = 'Hatena';
$crawler_list['WWW::Document/'] = 'Hatena';

//Livedoor
$crawler_list['livedoor'] = 'Livedoor';
$crawler_list['LD_mobile_bot'] = 'Livedoor-Mobile';

//OhNew
$crawler_list['ohnewbot-mobile'] = 'ohnewbot';

//froute
$crawler_list['.froute.jp'] = 'froute';

//モバゲータウン
$crawler_list['moba-crawler'] = 'mbga.jp';

//フレッシュアイ
$crawler_list['indexpert/'] = 'Flesheye';
$crawler_list['FreshGet/'] = 'Flesheye';
//ぐるっぽ
$crawler_list['lupo.jp'] = 'lupo';

$crawler_list['emBot-GalaBuzz/'] = 'GalaBuzz';

$crawler_list['Kingsoft Internet Security'] = 'Kingsoft';

$crawler_list['Twitterbot/'] = 'Twitter';

$crawler_list['Download Ninja'] = 'Ninja';
$crawler_list['GetHTML'] = 'GetHTML';
$crawler_list['Getweb!'] = 'Getweb!';
$crawler_list['HTTrack'] = 'HTTrack';
$crawler_list['Arachmo'] = 'Arachmo';
$crawler_list['Website Explorer/'] = 'Website Explorer';
$crawler_list['Iria/'] = 'Iria';
$crawler_list['Irvine/'] = 'Irvine';
$crawler_list['naoFavicon4IE'] = 'naoFavicon4IE';
$crawler_list['Kraken/'] = 'Kraken';
$crawler_list['lwp-trivial/'] = 'lwp-trivial';
$crawler_list['fc2.com'] = 'FC2';
$crawler_list['hoge (co2h2onacl@gmail.com) '] = 'hoge';
$crawler_list['JS-Kit'] = 'JS-Kit';
$crawler_list['Embedly/'] = 'embed.ly';
$crawler_list['kmbot-'] = 'knowmore';

$crawler_list['Yeti/'] = 'NAVER';
$crawler_list['/twiceler/robot.html'] = 'cuill';
$crawler_list['Kingsoft'] = 'Kingsoft';
$crawler_list['HeartRails_Capture/'] = 'HeartRailsCapture';
$crawler_list['WWWC/'] = 'WWWC';
$crawler_list['Wget/'] = 'Wget';
$crawler_list['WGet/'] = 'Wget';
$crawler_list['Web-cache'] = 'Web-cache';
$crawler_list['HTMLParser'] = 'HTMLParser';
$crawler_list['HyperEstraier/'] = 'HyperEstraier';
$crawler_list['ScreenShot'] = 'ScreenShot';
$crawler_list['SiteSucker/'] = 'SiteSucker';
$crawler_list['cococ/'] = 'cococ';
$crawler_list['mobaxy'] = 'mobaxy';
$crawler_list['BlogPulseLive'] = 'BlogPulse';
$crawler_list['BlogFan.ORG'] = 'BlogFan';
$crawler_list['Apple-PubSub/'] = 'Apple-PubSub';
$crawler_list['MaSagool'] = 'SAGOOL';
$crawler_list['seo.cug.net link checker/'] = 'seo.cug.net';
$crawler_list['MegaTonTracker'] = 'MegaTon';
$crawler_list['wish-project'] = 'wish-project';
$crawler_list['JS-UZUSHIO'] = 'JS-UZUSHIO';
$crawler_list['PostRank/'] = 'PostRank';
$crawler_list['Daumoa/'] = 'Daum';
$crawler_list['PrivacyFinder/'] = 'PrivacyFinder';
$crawler_list['NetResearchServer'] = 'SearchHippo.com';
$crawler_list['T-H-U-N-D-E-R-S-T-O-N-E'] = 'Thunderstone';
$crawler_list['BlogPeople'] = 'BlogPeople';
$crawler_list['BLOG360'] = 'BLOG360';
$crawler_list['Ask Jeeves/Teoma'] = 'ask';
$crawler_list['aruyo/'] = 'AAA!Cafe';
$crawler_list['Scooter'] = 'AltaVista';
$crawler_list['AppleSyndication/54'] = 'Apple';
$crawler_list['ScoutJet'] = 'scoutjet';
$crawler_list['NetcraftSurveyAgent'] = 'Netcraft';
$crawler_list['yellowJacket/'] = 'HiveSpy';
$crawler_list['Charlotte/'] = 'Charlotte';
$crawler_list['SBIder/'] = 'SBIder/';
$crawler_list['Yandex'] = 'Yandex';
$crawler_list['Sphere Scout'] = 'Sphere';
$crawler_list['Voracious/'] = 'Voracious';
$crawler_list['Mail.Ru/'] = 'Mail.Ru';
$crawler_list['StackRambler/'] = 'Rambler';
$crawler_list['SayCheese/'] = 'SayCheese';
$crawler_list['search.thunderstone.com'] = 'thunderstone';
$crawler_list['Twingly Recon'] = 'Twingly Recon';
$crawler_list['DeckkrClient/'] = 'DeckkrClient';
$crawler_list['bookey/'] = 'bookey';
$crawler_list['www.evri.com/evrinid'] = 'evri';
$crawler_list['tspyyp@tom.com'] = 'tom';
$crawler_list['Shelob'] = 'Shelob';
$crawler_list['Grub/'] = 'InternetOpenIndex';
$crawler_list['MJ12bot/'] = 'majestic12';
$crawler_list['MSMOBOT/'] = 'librabot';
$crawler_list['baypup/'] = 'Baypup';
$crawler_list['pathtraq.com'] = 'Pathtraq';
$crawler_list['Gungho/'] = 'Pathtraq';
$crawler_list['BMChecker'] = 'Crawler';
$crawler_list['mixi-mobile-converter/'] = 'Mixi';
$crawler_list['tangomail'] = 'ConnectMail';
$crawler_list['wadaino.jp'] = 'wadaino.jp';
$crawler_list['Huasai/'] = 'Huasai';
$crawler_list['DoubleVerify'] = 'DoubleVerify';
$crawler_list['page_test'] = 'page_test';
$crawler_list['al_viewer'] = 'al_viewer';
$crawler_list['jp_viewer'] = 'jp_viewer';
$crawler_list['li_viewer'] = 'li_viewer';
$crawler_list['es_com_viewer'] = 'es_com_viewer';
$crawler_list['larbin'] = 'li_viewer';
$crawler_list['Toread-Crawler/'] = 'Toread';
$crawler_list['WebAlta Crawler/'] = 'WebAlta';
$crawler_list['www.alexa.com/'] = 'alexa';
$crawler_list['Me.dium/'] = 'OneRiot';
$crawler_list['panscient.com'] = 'Panscient';
$crawler_list['parchmenthill.com/'] = 'ParchmentHill';
$crawler_list['Exabot/'] = 'Exabot';
$crawler_list['Purebot/'] = 'PuritySearch';
$crawler_list['Linguee Bot'] = 'Linguee';
$crawler_list['GingerCrawler/'] = 'GINGER';
$crawler_list['Modiphibot/'] = 'Modiphi';
$crawler_list['aiHitBot/'] = 'aiHit';
$crawler_list['TinEye/'] = 'TinEye';
$crawler_list['CamontSpider/'] = 'CamontSpider';
$crawler_list['Speedy Spider'] = 'SpeedySpider';
$crawler_list['mxbot/'] = 'mxbot';
$crawler_list['abby/'] = 'abby';
$crawler_list['TweetmemeBot'] = 'TweetmemeBot';
$crawler_list['Daumoa'] = 'Daumoa';
$crawler_list['woriobot'] = 'Worio';
$crawler_list['NjuiceBot'] = 'Njuice';
$crawler_list['butterfly'] = 'TOPSY';
$crawler_list['BPImageWalker/'] = 'BDBrandProtect';
$crawler_list['bludarter'] = 'BluDarter';
$crawler_list['TurnitinBot/'] = 'Turnitin';
$crawler_list['Technoratibot/'] = 'Technorati';
$crawler_list['Java/'] = 'Java';
$crawler_list['archive.org_bot/'] = 'archive.org';
$crawler_list['GeoHasher/'] = 'GeoHasher';

//Validator
$crawler_list['Another_HTML-lint/'] = 'Another_HTML-lint';
$crawler_list['W3C_Validator/'] = 'W3C';
$crawler_list['W3C_CSS_Validator'] = 'W3C';
$crawler_list['W3C_'] = 'W3C';
//Blog
$crawler_list['BlogramCrawler/'] = 'blogram.jp';

//SiteMap
$crawler_list['Sitemap'] = 'SitemapGenerator';

//Checker
$crawler_list['Dead Link Checker;'] = 'LinkChecker';

//RSS Reader
$crawler_list['WebryReader/'] = 'BiglobeWebryReader';
$crawler_list['Spinn3r'] = 'Spinn3r';
$crawler_list['gooRSSreader'] = 'gooRSSreader';
$crawler_list['DailyFeed/'] = 'DailyFeed';
$crawler_list['Mixfeed/'] = 'mixfeed';
$crawler_list['HanRSS/'] = 'HanRSS';
$crawler_list['xFruits'] = 'xfruits';
$crawler_list['MicroAd/'] = 'MicroAd';
$crawler_list['Bloglines/'] = 'Bloglines';
$crawler_list['Liferea/'] = 'LIFEREA';
$crawler_list['Netvibes'] = 'Netvibes';
$crawler_list['spbot'] = 'SEOprofiler';
$crawler_list['Page2RSS'] = 'Page2RSS';

//クローラ
$crawler_list['Steeler/'] = 'Steeler';
$crawler_list['/Nutch'] = 'Nutch';
$crawler_list['Fetcher'] = 'RSSReader';
$crawler_list['Feed'] = 'RSSReader';
$crawler_list['Reader'] = 'RSSReader';
$crawler_list['RSS'] = 'RSSReader';
$crawler_list['Rss'] = 'RSSReader';
$crawler_list['Iron33'] = 'Crawler';
$crawler_list['Feed::Find/'] = 'Crawler';
$crawler_list['ia_archiver'] = 'Crawler';
$crawler_list['PycURL/'] = 'PycURL';
$crawler_list['Spider'] = 'Crawler';
$crawler_list['spider'] = 'Crawler';
$crawler_list['Slurp'] = 'Crawler';
$crawler_list['Crawler'] = 'Crawler';
$crawler_list['crawler'] = 'Crawler';
$crawler_list['bot'] = 'Crawler';
$crawler_list['Bot'] = 'Crawler';
$crawler_list['WordPress/'] = 'WordPress';

//SPAM
$crawler_list['Indy Library'] = 'SPAM';
$crawler_list['Snoopy'] = 'SPAM';
$crawler_list['libwww'] = 'SPAM';
$crawler_list['ISC Systems'] = 'SPAM';
$crawler_list['Mozilla/3.01 (compatible;)'] = 'SPAM';
$crawler_list['Mozilla/4.0 (compatible;)'] = 'SPAM';
$crawler_list['Mozilla/4.0+(compatible;+MSIE+6.0;+Windows+NT+5.0;)'] = 'SPAM';
$crawler_list['Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1) Netscape/8.0.4'] = 'SPAM';
$crawler_list['Mozilla/0.91 Beta (Windows)'] = 'SPAM';
$crawler_list['topicblogs/'] = 'SPAM';
$crawler_list['JACK-O`-LANTERN/'] = 'SPAM';
$crawler_list['urllib/'] = 'SPAM';
$crawler_list['Schmozilla/'] = 'SPAM';
$crawler_list['TrackBack/'] = 'SPAM';
$crawler_list['Jakarta Commons-HttpClient/'] = 'SPAM';
?>