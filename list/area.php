<?php
$area_list = array();
global $area_list;
/*==============================================================================*/
/*    ■ 地域リスト                                                             */
/*                                                                              */
/*    $area_list['%host%'] = '地域';                                            */
/*                                                                              */
/*==============================================================================*/

//OCN--------------------------------------------------------------------------
$area_list['%hokkaido.ocn.ne.jp'] = '北海道';
$area_list['%aomori.ocn.ne.jp'] = '青森県';
$area_list['%iwate.ocn.ne.jp'] = '岩手県';
$area_list['%miyagi.ocn.ne.jp'] = '宮城県';
$area_list['%akita.ocn.ne.jp'] = '秋田県';
$area_list['%yamagata.ocn.ne.jp'] = '山形県';
$area_list['%fukushima.ocn.ne.jp'] = '福島県';
$area_list['%ibaraki.ocn.ne.jp'] = '茨城県';
$area_list['%tochigi.ocn.ne.jp'] = '栃木県';
$area_list['%gunma.ocn.ne.jp'] = '群馬県';
$area_list['%saitama.ocn.ne.jp'] = '埼玉県';
$area_list['%chiba.ocn.ne.jp'] = '千葉県';
$area_list['%tokyo.ocn.ne.jp'] = '東京都';
$area_list['%kanagawa.ocn.ne.jp'] = '神奈川県';
$area_list['%niigata.ocn.ne.jp'] = '新潟県';
$area_list['%toyama.ocn.ne.jp'] = '富山県';
$area_list['%ishikawa.ocn.ne.jp'] = '石川県';
$area_list['%fukui.ocn.ne.jp'] = '福井県';
$area_list['%yamanashi.ocn.ne.jp'] = '山梨県';
$area_list['%nagano.ocn.ne.jp'] = '長野県';
$area_list['%gifu.ocn.ne.jp'] = '岐阜県';
$area_list['%shizuoka.ocn.ne.jp'] = '静岡県';
$area_list['%aichi.ocn.ne.jp'] = '愛知県';
$area_list['%mie.ocn.ne.jp'] = '三重県';
$area_list['%shiga.ocn.ne.jp'] = '滋賀県';
$area_list['%kyoto.ocn.ne.jp'] = '京都府';
$area_list['%osaka.ocn.ne.jp'] = '大阪府';
$area_list['%hyogo.ocn.ne.jp'] = '兵庫県';
$area_list['%nara.ocn.ne.jp'] = '奈良県';
$area_list['%wakayama.ocn.ne.jp'] = '和歌山県';
$area_list['%tottori.ocn.ne.jp'] = '鳥取県';
$area_list['%shimane.ocn.ne.jp'] = '島根県';
$area_list['%okayama.ocn.ne.jp'] = '岡山県';
$area_list['%hiroshima.ocn.ne.jp'] = '広島県';
$area_list['%yamaguchi.ocn.ne.jp'] = '山口県';
$area_list['%tokushima.ocn.ne.jp'] = '徳島県';
$area_list['%kagawa.ocn.ne.jp'] = '香川県';
$area_list['%ehime.ocn.ne.jp'] = '愛媛県';
$area_list['%kochi.ocn.ne.jp'] = '高知県';
$area_list['%fukuoka.ocn.ne.jp'] = '福岡県';
$area_list['%saga.ocn.ne.jp'] = '佐賀県';
$area_list['%nagasaki.ocn.ne.jp'] = '長崎県';
$area_list['%obiyama.kumamoto.ocn.ne.jp'] = '熊本県';
$area_list['%oomichi.oita.ocn.ne.jp'] = '大分県';
$area_list['%miyazaki.ocn.ne.jp'] = '宮崎県';
$area_list['%kagoshima.ocn.ne.jp'] = '鹿児島県';
$area_list['%okinawa.ocn.ne.jp'] = '沖縄県';

//biglobe----------------------------------------------------------------------
$area_list['%ymn.mesh.ad.jp'] = '山梨県';
$area_list['%ygt.mesh.ad.jp'] = '山形県';
$area_list['%ygc.mesh.ad.jp'] = '山口県';
$area_list['%wky.mesh.ad.jp'] = '和歌山県';
$area_list['%tym.mesh.ad.jp'] = '富山県';
$area_list['%ttr.mesh.ad.jp'] = '鳥取県';
$area_list['%tky.mesh.ad.jp'] = '東京都';
$area_list['%tks.mesh.ad.jp'] = '徳島県';
$area_list['%tk3.mesh.ad.jp'] = '東京都';
$area_list['%tk2.mesh.ad.jp'] = '東京都';
$area_list['%tk1.mesh.ad.jp'] = '東京都';
$area_list['%tcg.mesh.ad.jp'] = '栃木県';
$area_list['%szo.mesh.ad.jp'] = '静岡県';
$area_list['%stm.mesh.ad.jp'] = '埼玉県';
$area_list['%smn.mesh.ad.jp'] = '島根県';
$area_list['%sig.mesh.ad.jp'] = '滋賀県';
$area_list['%sag.mesh.ad.jp'] = '佐賀県';
$area_list['%osk.mesh.ad.jp'] = '大阪府';
$area_list['%oky.mesh.ad.jp'] = '岡山県';
$area_list['%okn.mesh.ad.jp'] = '沖縄県';
$area_list['%oit.mesh.ad.jp'] = '大分県';
$area_list['%nra.mesh.ad.jp'] = '奈良県';
$area_list['%nig.mesh.ad.jp'] = '新潟県';
$area_list['%ngs.mesh.ad.jp'] = '長崎県';
$area_list['%ngn.mesh.ad.jp'] = '長野県';
$area_list['%myz.mesh.ad.jp'] = '宮崎県';
$area_list['%myg.mesh.ad.jp'] = '宮城県';
$area_list['%mie.mesh.ad.jp'] = '三重県';
$area_list['%kyt.mesh.ad.jp'] = '京都府';
$area_list['%koc.mesh.ad.jp'] = '高知県';
$area_list['%kng.mesh.ad.jp'] = '神奈川県';
$area_list['%kmm.mesh.ad.jp'] = '熊本県';
$area_list['%kgw.mesh.ad.jp'] = '香川県';
$area_list['%kgs.mesh.ad.jp'] = '鹿児島';
$area_list['%iwa.mesh.ad.jp'] = '岩手県';
$area_list['%isk.mesh.ad.jp'] = '石川県';
$area_list['%iba.mesh.ad.jp'] = '茨城県';
$area_list['%hyg.mesh.ad.jp'] = '兵庫県';
$area_list['%hrs.mesh.ad.jp'] = '広島県';
$area_list['%hkd.mesh.ad.jp'] = '北海道';
$area_list['%gnm.mesh.ad.jp'] = '群馬県';
$area_list['%gif.mesh.ad.jp'] = '岐阜県';
$area_list['%fks.mesh.ad.jp'] = '福島県';
$area_list['%fko.mesh.ad.jp'] = '福岡県';
$area_list['%fki.mesh.ad.jp'] = '福井県';
$area_list['%ehm.mesh.ad.jp'] = '愛媛県';
$area_list['%chb.mesh.ad.jp'] = '千葉県';
$area_list['%aom.mesh.ad.jp'] = '青森県';
$area_list['%aki.mesh.ad.jp'] = '秋田県';
$area_list['%aic.mesh.ad.jp'] = '愛知県';

//InfoWeb----------------------------------------------------------------------
$area_list['%ymns%.infoweb.ne.jp'] = '山梨県';
$area_list['%ymgc%.infoweb.ne.jp'] = '山口県';
$area_list['%ymgt%.infoweb.ne.jp'] = '山形県';
$area_list['%ykhm%.infoweb.ne.jp'] = '神奈川県';
$area_list['%wkym%.infoweb.ne.jp'] = '和歌山県';
$area_list['%tyma%.infoweb.ne.jp'] = '富山県';
$area_list['%ttri%.infoweb.ne.jp'] = '鳥取県';
$area_list['%tkyo%.infoweb.ne.jp'] = '東京都';
$area_list['%tksm%.infoweb.ne.jp'] = '徳島県';
$area_list['%tcgi%.infoweb.ne.jp'] = '栃木県';
$area_list['%szok%.infoweb.ne.jp'] = '静岡県';
$area_list['%smne%.infoweb.ne.jp'] = '島根県';
$area_list['%sitm%.infoweb.ne.jp'] = '埼玉県';
$area_list['%shga%.infoweb.ne.jp'] = '滋賀県';
$area_list['%saga%.infoweb.ne.jp'] = '佐賀県';
$area_list['%oska%.infoweb.ne.jp'] = '大阪府';
$area_list['%ooit%.infoweb.ne.jp'] = '大分県';
$area_list['%okym%.infoweb.ne.jp'] = '岡山県';
$area_list['%oknw%.infoweb.ne.jp'] = '沖縄県';
$area_list['%nigt%.infoweb.ne.jp'] = '新潟県';
$area_list['%ngya%.infoweb.ne.jp'] = '愛知県';
$area_list['%ngsk%.infoweb.ne.jp'] = '長崎県';
$area_list['%ngno%.infoweb.ne.jp'] = '長野県';
$area_list['%nara%.infoweb.ne.jp'] = '奈良県';
$area_list['%myzk%.infoweb.ne.jp'] = '宮崎県';
$area_list['%mygi%.infoweb.ne.jp'] = '宮城県';
$area_list['%miex%.infoweb.ne.jp'] = '三重県';
$area_list['%kyto%.infoweb.ne.jp'] = '京都府';
$area_list['%kuch%.infoweb.ne.jp'] = '高知県';
$area_list['%kngw%.infoweb.ne.jp'] = '神奈川県';
$area_list['%kmmt%.infoweb.ne.jp'] = '熊本県';
$area_list['%kgwa%.infoweb.ne.jp'] = '香川県';
$area_list['%kgsm%.infoweb.ne.jp'] = '鹿児島';
$area_list['%iwte%.infoweb.ne.jp'] = '岩手県';
$area_list['%iskw%.infoweb.ne.jp'] = '石川県';
$area_list['%ibrk%.infoweb.ne.jp'] = '茨城県';
$area_list['%hygo%.infoweb.ne.jp'] = '兵庫県';
$area_list['%hrsm%.infoweb.ne.jp'] = '広島県';
$area_list['%hkid%.infoweb.ne.jp'] = '北海道';
$area_list['%gnma%.infoweb.ne.jp'] = '群馬県';
$area_list['%gifu%.infoweb.ne.jp'] = '岐阜県';
$area_list['%fkui%.infoweb.ne.jp'] = '福井県';
$area_list['%fksm%.infoweb.ne.jp'] = '福島県';
$area_list['%fkok%.infoweb.ne.jp'] = '福岡県';
$area_list['%ehme%.infoweb.ne.jp'] = '愛媛県';
$area_list['%chba%.infoweb.ne.jp'] = '千葉県';
$area_list['%aomr%.infoweb.ne.jp'] = '青森県';
$area_list['%akta%.infoweb.ne.jp'] = '秋田県';
$area_list['%aich%.infoweb.ne.jp'] = '愛知県';
$area_list['%atgi%.infoweb.ne.jp'] = '神奈川県';

$area_list['hmmt%.infoweb.ne.jp'] = '静岡県';
$area_list['oyma%.infoweb.ne.jp'] = '栃木県';
$area_list['tkbn%.infoweb.ne.jp'] = '東京都';
$area_list['hcou%.infoweb.ne.jp'] = '東京都';
$area_list['ktsk%.infoweb.ne.jp'] = '東京都';
$area_list['ohta%.infoweb.ne.jp'] = '東京都';
$area_list['nkno%.infoweb.ne.jp'] = '東京都';
$area_list['fnbs%.infoweb.ne.jp'] = '千葉県';
$area_list['ymgt%.infoweb.ne.jp'] = '山形県';
$area_list['aksi%.infoweb.ne.jp'] = '兵庫県';
$area_list['odwr%.infoweb.ne.jp'] = '神奈川県';
$area_list['kkgw%.infoweb.ne.jp'] = '兵庫県';
$area_list['kihn%.infoweb.ne.jp'] = '兵庫県';
$area_list['kyma%.infoweb.ne.jp'] = '栃木県';
$area_list['yaox%.infoweb.ne.jp'] = '大阪府';
//so-net.ne.jp--------------------------------------------------------------------
$area_list['%hkidnt%.ap.so-net.ne.jp'] = '北海道';
$area_list['%sppr%.ap.so-net.ne.jp'] = '北海道';
$area_list['%ngsk%.ap.so-net.ne.jp'] = '長崎県';
$area_list['%ymnsnt%.ap.so-net.ne.jp'] = '山梨県';
$area_list['%ymgt%.ap.so-net.ne.jp'] = '山形県';
$area_list['%ymgc%.ap.so-net.ne.jp'] = '山口県';
$area_list['%ykhm%.ap.so-net.ne.jp'] = '神奈川県';
$area_list['%wkym%.ap.so-net.ne.jp'] = '和歌山県';
$area_list['%uraw%.ap.so-net.ne.jp'] = '埼玉県';
$area_list['%toym%.ap.so-net.ne.jp'] = '富山県';
$area_list['%totr%.ap.so-net.ne.jp'] = '鳥取県';
$area_list['%tocg%.ap.so-net.ne.jp'] = '栃木県';
$area_list['%.toky%.ap.so-net.ne.jp'] = '東京都';
$area_list['%tkyo%.ap.so-net.ne.jp'] = '東京都';
$area_list['%ntky%.ap.so-net.ne.jp'] = '東京都';
$area_list['%tksm%.ap.so-net.ne.jp'] = '徳島県';
$area_list['%tubehm%.ap.so-net.ne.jp'] = '愛知県';
$area_list['%aic%.ap.so-net.ne.jp'] = '愛知県';
$area_list['%szok%.ap.so-net.ne.jp'] = '静岡県';
$area_list['%sndi%.ap.so-net.ne.jp'] = '宮城県';
$area_list['%sitm%.ap.so-net.ne.jp'] = '埼玉県';
$area_list['%simn%.ap.so-net.ne.jp'] = '島根県';
$area_list['%siga%.ap.so-net.ne.jp'] = '滋賀県';
$area_list['%saga%.ap.so-net.ne.jp'] = '佐賀県';
$area_list['%osak%.ap.so-net.ne.jp'] = '大阪府';

$area_list['110.66.25.%'] = '東京都';
$area_list['120.74.46.%'] = '東京都';
$area_list['121.103.82.%'] = '岐阜県';
$area_list['121.103.83.%'] = '岐阜県';
$area_list['121.103.84.%'] = '愛知県';
$area_list['121.103.85.%'] = '愛知県';
$area_list['121.103.86.%'] = '愛知県';
$area_list['121.103.87.%'] = '愛知県';
$area_list['121.103.88.%'] = '愛知県';
$area_list['121.103.89.%'] = '愛知県';
$area_list['121.103.90.%'] = '兵庫県';
$area_list['121.103.91.%'] = '兵庫県';
$area_list['219.98.30.%'] = '東京都';

$area_list['%okym%.ap.so-net.ne.jp'] = '岡山県';
$area_list['%oknw%.ap.so-net.ne.jp'] = '沖縄県';
$area_list['%oita%.ap.so-net.ne.jp'] = '大分県';
$area_list['%nigt%.ap.so-net.ne.jp'] = '新潟県';
$area_list['%ngya%.ap.so-net.ne.jp'] = '愛知県';
$area_list['%ngno%.ap.so-net.ne.jp'] = '長野県';
$area_list['%nara%.ap.so-net.ne.jp'] = '奈良県';
$area_list['%myzk%.ap.so-net.ne.jp'] = '宮崎県';
$area_list['%miyg%.ap.so-net.ne.jp'] = '宮城県';
$area_list['%mie-%.ap.so-net.ne.jp'] = '三重県';
$area_list['%kyot%.ap.so-net.ne.jp'] = '京都府';
$area_list['%kwsk%.ap.so-net.ne.jp'] = '神奈川県';
$area_list['%koci%.ap.so-net.ne.jp'] = '高知県';
$area_list['%kobe%.ap.so-net.ne.jp'] = '兵庫県';
$area_list['%kngw%.ap.so-net.ne.jp'] = '神奈川県';
$area_list['%kmmt%.ap.so-net.ne.jp'] = '熊本県';
$area_list['%kgsm%.ap.so-net.ne.jp'] = '鹿児島';
$area_list['%kagw%.ap.so-net.ne.jp'] = '香川県';
$area_list['%iwat%.ap.so-net.ne.jp'] = '岩手県';
$area_list['%iskw%.ap.so-net.ne.jp'] = '石川県';
$area_list['%ibrk%.ap.so-net.ne.jp'] = '茨城県';
$area_list['%hyog%.ap.so-net.ne.jp'] = '兵庫県';
$area_list['%hrsm%.ap.so-net.ne.jp'] = '広島県';
$area_list['%gunm%.ap.so-net.ne.jp'] = '群馬県';
$area_list['%gifu%.ap.so-net.ne.jp'] = '岐阜県';
$area_list['%fuki%.ap.so-net.ne.jp'] = '福井県';
$area_list['%fksm%.ap.so-net.ne.jp'] = '福島県';
$area_list['%fkok%.ap.so-net.ne.jp'] = '福岡県';
$area_list['%ehim%.ap.so-net.ne.jp'] = '愛媛県';
$area_list['%chib%.ap.so-net.ne.jp'] = '千葉県';
$area_list['%aomr%.ap.so-net.ne.jp'] = '青森県';
$area_list['%akit%.ap.so-net.ne.jp'] = '秋田県';
$area_list['%aici%.ap.so-net.ne.jp'] = '愛知県';
$area_list['%.toky%.ap.so-net.ne.jp'] = '東京都';
//dti.ne.jp--------------------------------------------------------------------
$area_list['%.hokkaido-ip.dti.ne.jp'] = '北海道';
$area_list['%.yamanashi-ip.dti.ne.jp'] = '山梨県';
$area_list['%.yamaguchi-ip.dti.ne.jp'] = '山口県';
$area_list['%.yamagata-ip.dti.ne.jp'] = '山形県';
$area_list['%.wakayama-ip.dti.ne.jp'] = '和歌山県';
$area_list['%.toyama-ip.dti.ne.jp'] = '富山県';
$area_list['%.tottori-ip.dti.ne.jp'] = '鳥取県';
$area_list['%.tokyo-ip.dti.ne.jp'] = '東京都';
$area_list['%.tokushima-ip.dti.ne.jp'] = '徳島県';
$area_list['%.tochigi-ip.dti.ne.jp'] = '栃木県';
$area_list['%.shizuoka-ip.dti.ne.jp'] = '静岡県';
$area_list['%.shimane-ip.dti.ne.jp'] = '島根県';
$area_list['%.shiga-ip.dti.ne.jp'] = '滋賀県';
$area_list['%.saitama-ip.dti.ne.jp'] = '埼玉県';
$area_list['%.saga-ip.dti.ne.jp'] = '佐賀県';
$area_list['%.osaka-ip.dti.ne.jp'] = '大阪府';
$area_list['%.oomichi.oita-ip.dti.ne.jp'] = '大分県';
$area_list['%.okinawa-ip.dti.ne.jp'] = '沖縄県';
$area_list['%.okayama-ip.dti.ne.jp'] = '岡山県';
$area_list['%.kumamoto-ip.dti.ne.jp'] = '熊本県';
$area_list['%.niigata-ip.dti.ne.jp'] = '新潟県';
$area_list['%.nara-ip.dti.ne.jp'] = '奈良県';
$area_list['%.nagasaki-ip.dti.ne.jp'] = '長崎県';
$area_list['%.nagano-ip.dti.ne.jp'] = '長野県';
$area_list['%.miyazaki-ip.dti.ne.jp'] = '宮崎県';
$area_list['%.miyagi-ip.dti.ne.jp'] = '宮城県';
$area_list['%.mie-ip.dti.ne.jp'] = '三重県';
$area_list['%.kyoto-ip.dti.ne.jp'] = '京都府';
$area_list['%.kochi-ip.dti.ne.jp'] = '高知県';
$area_list['%.kanagawa-ip.dti.ne.jp'] = '神奈川県';
$area_list['%.kagoshima-ip.dti.ne.jp'] = '鹿児島県';
$area_list['%.kagawa-ip.dti.ne.jp'] = '香川県';
$area_list['%.iwate-ip.dti.ne.jp'] = '岩手県';
$area_list['%.ishikawa-ip.dti.ne.jp'] = '石川県';
$area_list['%.ibaraki-ip.dti.ne.jp'] = '茨城県';
$area_list['%.hyogo-ip.dti.ne.jp'] = '兵庫県';
$area_list['%.kobe.%.dti.ne.jp'] = '兵庫県';
$area_list['%.hiroshima-ip.dti.ne.jp'] = '広島県';
$area_list['%.gunma-ip.dti.ne.jp'] = '群馬県';
$area_list['%.gifu-ip.dti.ne.jp'] = '岐阜県';
$area_list['%.fukushima-ip.dti.ne.jp'] = '福島県';
$area_list['%.fukuoka-ip.dti.ne.jp'] = '福岡県';
$area_list['%.fukui-ip.dti.ne.jp'] = '福井県';
$area_list['%.ehime-ip.dti.ne.jp'] = '愛媛県';
$area_list['%.chiba-ip.dti.ne.jp'] = '千葉県';
$area_list['%.aomori-ip.dti.ne.jp'] = '青森県';
$area_list['%.akita-ip.dti.ne.jp'] = '秋田県';
$area_list['%.aichi-ip.dti.ne.jp'] = '愛知県';

//hi-ho------------------------------------------------------------------------
$area_list['%ymt%.%.hi-ho.ne.jp'] = '山形県';
$area_list['%ymn%.%.hi-ho.ne.jp'] = '山梨県';
$area_list['%ymc%.%.hi-ho.ne.jp'] = '山口県';
$area_list['%wky%.%.hi-ho.ne.jp'] = '和歌山県';
$area_list['%tym%.%.hi-ho.ne.jp'] = '富山県';
$area_list['%ttr%.%.hi-ho.ne.jp'] = '鳥取県';
$area_list['%tky%.%.hi-ho.ne.jp'] = '東京都';
$area_list['%tks%.%.hi-ho.ne.jp'] = '徳島県';
$area_list['%tcg%.%.hi-ho.ne.jp'] = '栃木県';
$area_list['%szk%.%.hi-ho.ne.jp'] = '静岡県';
$area_list['%stm%.%.hi-ho.ne.jp'] = '埼玉県';
$area_list['%smn%.%.hi-ho.ne.jp'] = '島根県';
$area_list['%shg%.%.hi-ho.ne.jp'] = '滋賀県';
$area_list['%sag%.%.hi-ho.ne.jp'] = '佐賀県';
$area_list['%osk%.%.hi-ho.ne.jp'] = '大阪府';
$area_list['%oky%.%.hi-ho.ne.jp'] = '岡山県';
$area_list['%okn%.%.hi-ho.ne.jp'] = '沖縄県';
$area_list['%oit%.%.hi-ho.ne.jp'] = '大分県';
$area_list['%ngt%.%.hi-ho.ne.jp'] = '新潟県';
$area_list['%ngs%.%.hi-ho.ne.jp'] = '長崎県';
$area_list['%ngn%.%.hi-ho.ne.jp'] = '長野県';
$area_list['%nar%.%.hi-ho.ne.jp'] = '奈良県';
$area_list['%myz%.%.hi-ho.ne.jp'] = '宮崎県';
$area_list['%myg%.%.hi-ho.ne.jp'] = '宮城県';
$area_list['%mie%.%.hi-ho.ne.jp'] = '三重県';
$area_list['%kyt%.%.hi-ho.ne.jp'] = '京都府';
$area_list['%kng%.%.hi-ho.ne.jp'] = '神奈川県';
$area_list['%kmm%.%.hi-ho.ne.jp'] = '熊本県';
$area_list['%kgw%.%.hi-ho.ne.jp'] = '香川県';
$area_list['%kgs%.%.hi-ho.ne.jp'] = '鹿児島';
$area_list['%kch%.%.hi-ho.ne.jp'] = '高知県';
$area_list['%iwt%.%.hi-ho.ne.jp'] = '岩手県';
$area_list['%isk%.%.hi-ho.ne.jp'] = '石川県';
$area_list['%ibr%.%.hi-ho.ne.jp'] = '茨城県';
$area_list['%hyg%.%.hi-ho.ne.jp'] = '兵庫県';
$area_list['%hrs%.%.hi-ho.ne.jp'] = '広島県';
$area_list['%hkd%.%.hi-ho.ne.jp'] = '北海道';
$area_list['%gmm%.%.hi-ho.ne.jp'] = '群馬県';
$area_list['%gif%.%.hi-ho.ne.jp'] = '岐阜県';
$area_list['%fks%.%.hi-ho.ne.jp'] = '福島県';
$area_list['%fkk%.%.hi-ho.ne.jp'] = '福岡県';
$area_list['%fki%.%.hi-ho.ne.jp'] = '福井県';
$area_list['%ehm%.%.hi-ho.ne.jp'] = '愛媛県';
$area_list['%chb%.%.hi-ho.ne.jp'] = '千葉県';
$area_list['%amr%.%.hi-ho.ne.jp'] = '青森県';
$area_list['%akt%.%.hi-ho.ne.jp'] = '秋田県';
$area_list['%aic%.%.hi-ho.ne.jp'] = '愛知県';

//plala------------------------------------------------------------------------
$area_list['%a047.ap.plala.or.jp'] = '沖縄県';
$area_list['%a046.ap.plala.or.jp'] = '鹿児島';
$area_list['%a045.ap.plala.or.jp'] = '宮崎県';
$area_list['%a044.ap.plala.or.jp'] = '大分県';
$area_list['%a043.ap.plala.or.jp'] = '熊本県';
$area_list['%a042.ap.plala.or.jp'] = '長崎県';
$area_list['%a041.ap.plala.or.jp'] = '佐賀県';
$area_list['%a040.ap.plala.or.jp'] = '福岡県';
$area_list['%a039.ap.plala.or.jp'] = '高知県';
$area_list['%a038.ap.plala.or.jp'] = '愛媛県';
$area_list['%a037.ap.plala.or.jp'] = '香川県';
$area_list['%a036.ap.plala.or.jp'] = '徳島県';
$area_list['%a035.ap.plala.or.jp'] = '山口県';
$area_list['%a034.ap.plala.or.jp'] = '広島県';
$area_list['%a033.ap.plala.or.jp'] = '岡山県';
$area_list['%a032.ap.plala.or.jp'] = '島根県';
$area_list['%a031.ap.plala.or.jp'] = '鳥取県';
$area_list['%a030.ap.plala.or.jp'] = '和歌山県';
$area_list['%a029.ap.plala.or.jp'] = '奈良県';
$area_list['%a028.ap.plala.or.jp'] = '兵庫県';
$area_list['%a027.ap.plala.or.jp'] = '大阪府';
$area_list['%a026.ap.plala.or.jp'] = '京都府';
$area_list['%a025.ap.plala.or.jp'] = '滋賀県';
$area_list['%a024.ap.plala.or.jp'] = '三重県';
$area_list['%a023.ap.plala.or.jp'] = '愛知県';
$area_list['%a022.ap.plala.or.jp'] = '静岡県';
$area_list['%a021.ap.plala.or.jp'] = '岐阜県';
$area_list['%a020.ap.plala.or.jp'] = '長野県';
$area_list['%a019.ap.plala.or.jp'] = '山梨県';
$area_list['%a018.ap.plala.or.jp'] = '福井県';
$area_list['%a017.ap.plala.or.jp'] = '石川県';
$area_list['%a016.ap.plala.or.jp'] = '富山県';
$area_list['%a015.ap.plala.or.jp'] = '新潟県';
$area_list['%a014.ap.plala.or.jp'] = '神奈川県';
$area_list['%a013.ap.plala.or.jp'] = '東京都';
$area_list['%a012.ap.plala.or.jp'] = '千葉県';
$area_list['%a011.ap.plala.or.jp'] = '埼玉県';
$area_list['%a010.ap.plala.or.jp'] = '群馬県';
$area_list['%a009.ap.plala.or.jp'] = '茨城県';
$area_list['%a008.ap.plala.or.jp'] = '栃木県';
$area_list['%a007.ap.plala.or.jp'] = '福島県';
$area_list['%a006.ap.plala.or.jp'] = '山形県';
$area_list['%a005.ap.plala.or.jp'] = '秋田県';
$area_list['%a004.ap.plala.or.jp'] = '宮城県';
$area_list['%a003.ap.plala.or.jp'] = '岩手県';
$area_list['%a002.ap.plala.or.jp'] = '青森県';
$area_list['%a001.ap.plala.or.jp'] = '北海道';

//nttpc------------------------------------------------------------------------
$area_list['%matsue.nttpc.ne.jp'] = '島根県';
$area_list['%otsu.nttpc.ne.jp'] = '滋賀県';
$area_list['%omiya.nttpc.ne.jp'] = '埼玉県';
$area_list['%soka.nttpc.ne.jp'] = '埼玉県';
$area_list['%urawa.nttpc.ne.jp'] = '埼玉県';
$area_list['%kawaguchi.nttpc.ne.jp'] = '埼玉県';
$area_list['%honjo.nttpc.ne.jp'] = '埼玉県';
$area_list['%takatsuki.nttpc.ne.jp'] = '大阪府'; //
$area_list['%furukawa.nttpc.ne.jp'] = '宮城県';
$area_list['%tsu.nttpc.ne.jp'] = '三重県';
$area_list['%kofu.nttpc.ne.jp'] = '山梨県';
$area_list['%yokosuka.nttpc.ne.jp'] = '神奈川県';
$area_list['%kawasaki.nttpc.ne.jp'] = '神奈川県';
$area_list['%yokohama.nttpc.ne.jp'] = '神奈川県';
$area_list['%takamatsu.nttpc.ne.jp'] = '香川県';
$area_list['%morioka.nttpc.ne.jp'] = '岩手県';
$area_list['%mito.nttpc.ne.jp'] = '茨城県';
$area_list['%kobe.nttpc.ne.jp'] = '兵庫県';
$area_list['%himeji.nttpc.ne.jp'] = '愛媛県';
$area_list['%ichikawa.nttpc.ne.jp'] = '千葉県';
$area_list['%handa.nttpc.ne.jp'] = '愛知県';
$area_list['%nagoya.nttpc.ne.jp'] = '愛知県';

//携帯各社---------------------------------------------------------------------
$area_list['%.docomo.ne.jp'] = '不明:携帯';
$area_list['%.mopera.net'] = '不明:モバイル通信';

$area_list['%.ezweb.ne.jp'] = '不明:携帯';
$area_list['%.au-net.ne.jp'] = '不明:モバイル通信';

$area_list['%.jp-c.ne.jp'] = '不明:携帯';
$area_list['%.jp-d.ne.jp'] = '不明:携帯';
$area_list['%.jp-h.ne.jp'] = '不明:携帯';
$area_list['%.jp-k.ne.jp'] = '不明:携帯';
$area_list['%.jp-n.ne.jp'] = '不明:携帯';
$area_list['%.jp-q.ne.jp'] = '不明:携帯';
$area_list['%.jp-r.ne.jp'] = '不明:携帯';
$area_list['%.jp-s.ne.jp'] = '不明:携帯';
$area_list['%.jp-t.ne.jp'] = '不明:携帯';

$area_list['%.access-internet.ne.jp'] = '不明:モバイル通信';
$area_list['%.openmobile.ne.jp'] = '不明:モバイル通信';
$area_list['%.panda-world.ne.jp'] = '不明:モバイル通信';
$area_list['%.pcsitebrowser.ne.jp'] = '不明:モバイル通信';

$area_list['%.livedoor.net'] = '不明:モバイル通信';
$area_list['%.prin.ne.jp'] = '不明:携帯';
$area_list['%.uqwimax.jp'] = '不明:モバイル通信';
//
$area_list['%.pool.e-mobile.ne.jp'] = '不明:E-MOBILE';

//コミュファ
$area_list['r-115-36-164-%'] = '三重県';
$area_list['r-115-36-165-%'] = '三重県';
$area_list['r-115-36-185-%'] = '愛知県';
$area_list['r-115-36-187-%'] = '岐阜県';
$area_list['r-115-36-188-%'] = '愛知県';
$area_list['r-115-36-191-%'] = '愛知県';
$area_list['r-115-36-192-%'] = '愛知県';
$area_list['r-115-36-193-%'] = '愛知県';
$area_list['r-115-36-194-%'] = '愛知県';
$area_list['r-115-36-195-%'] = '愛知県';
$area_list['r-115-36-196-%'] = '愛知県';
$area_list['r-115-36-200-%'] = '岐阜県';
$area_list['r-115-36-201-%'] = '岐阜県';
$area_list['r-115-36-202-%'] = '岐阜県';
$area_list['r-115-36-203-%'] = '岐阜県';
$area_list['r-115-36-204-%'] = '愛知県';
$area_list['r-115-36-207-%'] = '愛知県';
$area_list['r-115-36-208-%'] = '愛知県';
$area_list['r-115-36-215-%'] = '愛知県';
$area_list['r-115-36-216-%'] = '愛知県';
$area_list['r-115-36-217-%'] = '愛知県';
$area_list['r-115-36-218-%'] = '愛知県';
$area_list['r-115-36-219-%'] = '愛知県';
$area_list['r-115-36-220-%'] = '愛知県';
$area_list['r-115-36-221-%'] = '愛知県';
$area_list['r-115-36-222-%'] = '愛知県';
$area_list['r-115-36-231-%'] = '静岡県';
$area_list['r-115-36-235-%'] = '愛知県';
$area_list['r-115-36-236-%'] = '愛知県';
$area_list['r-115-36-238-%'] = '愛知県';
$area_list['r-115-36-239-%'] = '愛知県';
$area_list['r-115-36-240-%'] = '愛知県';
$area_list['r-115-36-241-%'] = '愛知県';
$area_list['r-115-36-243-%'] = '岐阜県';
$area_list['r-115-36-244-%'] = '愛知県';

$area_list['r-115-37-139-%'] = '愛知県';
$area_list['r-115-37-144-%'] = '静岡県';

$area_list['r-118-104-196-%'] = '三重県';
$area_list['r-118-104-198-%'] = '三重県';
$area_list['r-118-104-200-%'] = '三重県';
$area_list['r-118-104-212-%'] = '愛知県';
$area_list['r-118-104-218-%'] = '三重県';
$area_list['r-118-104-222-%'] = '三重県';
$area_list['r-118-104-228-%'] = '愛知県';
$area_list['r-118-104-223-%'] = '三重県';
$area_list['r-118-104-232-%'] = '愛知県';
$area_list['r-118-104-233-%'] = '静岡県';
$area_list['r-118-104-246-%'] = '静岡県';

$area_list['r-118-105-132-%'] = '愛知県';
$area_list['r-118-105-133-%'] = '愛知県';
$area_list['r-118-105-140-%'] = '愛知県';
$area_list['r-118-105-143-%'] = '愛知県';
$area_list['r-118-105-144-%'] = '愛知県';
$area_list['r-118-105-148-%'] = '愛知県';
$area_list['r-118-105-149-%'] = '岐阜県';
$area_list['r-118-105-155-%'] = '愛知県';
$area_list['r-118-105-157-%'] = '愛知県';
$area_list['r-118-105-204-%'] = '愛知県';
$area_list['r-118-105-224-%'] = '愛知県';
$area_list['r-118-105-230-%'] = '愛知県';
$area_list['r-118-105-231-%'] = '愛知県';
$area_list['r-118-105-233-%'] = '愛知県';

$area_list['r-118-106-166-%'] = '愛知県';
$area_list['r-118-106-168-%'] = '愛知県';
$area_list['r-118-106-172-%'] = '愛知県';
$area_list['r-118-106-180-%'] = '愛知県';
$area_list['r-118-106-182-%'] = '愛知県';
$area_list['r-118-106-185-%'] = '愛知県';
$area_list['r-118-106-190-%'] = '愛知県';
$area_list['r-118-106-217-%'] = '愛知県';
$area_list['r-118-106-219-%'] = '愛知県';

$area_list['r-123-48-13-%'] = '岐阜県';
$area_list['r-123-48-25-%'] = '愛知県';
$area_list['r-123-48-31-%'] = '愛知県';
$area_list['r-123-48-32-%'] = '愛知県';
$area_list['r-123-48-33-%'] = '愛知県';
$area_list['r-123-48-34-%'] = '愛知県';
$area_list['r-123-48-35-%'] = '愛知県';
$area_list['r-123-48-36-%'] = '愛知県';
$area_list['r-123-48-42-%'] = '愛知県';
$area_list['r-123-48-44-%'] = '愛知県';
$area_list['r-123-48-58-%'] = '愛知県';
$area_list['r-123-48-66-%'] = '愛知県';
$area_list['r-123-48-73-%'] = '愛知県';
$area_list['r-123-48-96-%'] = '愛知県';
$area_list['r-123-48-97-%'] = '愛知県';
$area_list['r-123-48-98-%'] = '愛知県';
$area_list['r-123-48-99-%'] = '愛知県';
$area_list['r-123-48-100-%'] = '愛知県';
$area_list['r-123-48-101-%'] = '愛知県';
$area_list['r-123-48-102-%'] = '愛知県';
$area_list['r-123-48-103-%'] = '愛知県';
$area_list['r-123-48-104-%'] = '愛知県';
$area_list['r-123-48-105-%'] = '愛知県';
$area_list['r-123-48-106-%'] = '愛知県';
$area_list['r-123-48-107-%'] = '愛知県';
$area_list['r-123-48-108-%'] = '愛知県';
$area_list['r-123-48-109-%'] = '愛知県';
$area_list['r-123-48-110-%'] = '愛知県';
$area_list['r-123-48-111-%'] = '愛知県';
$area_list['r-123-48-112-%'] = '愛知県';
$area_list['r-123-48-113-%'] = '愛知県';
$area_list['r-123-48-114-%'] = '愛知県';
$area_list['r-123-48-115-%'] = '愛知県';
$area_list['r-123-48-116-%'] = '愛知県';
$area_list['r-123-48-117-%'] = '愛知県';
$area_list['r-123-48-118-%'] = '愛知県';
$area_list['r-123-48-119-%'] = '愛知県';
$area_list['r-123-48-141-%'] = '愛知県';
$area_list['r-123-48-156-%'] = '愛知県';
$area_list['r-123-48-158-%'] = '愛知県';
$area_list['r-123-48-162-%'] = '愛知県';
$area_list['r-123-48-164-%'] = '愛知県';
$area_list['r-123-48-165-%'] = '静岡県';
$area_list['r-123-48-169-%'] = '愛知県';
$area_list['r-123-48-173-%'] = '愛知県';
$area_list['r-123-48-188-%'] = '愛知県';

$area_list['r-124-18-1-%'] = '岐阜県';
$area_list['r-124-18-6-%'] = '愛知県';
$area_list['r-124-18-7-%'] = '愛知県';
$area_list['r-124-18-40-%'] = '愛知県';
$area_list['r-124-18-40-%'] = '静岡県';
$area_list['r-124-18-51-%'] = '愛知県';
$area_list['r-124-18-57-%'] = '愛知県';
$area_list['r-124-18-70-%'] = '愛知県';
$area_list['r-124-18-72-%'] = '静岡県';
$area_list['r-124-18-73-%'] = '静岡県';
$area_list['r-124-18-82-%'] = '愛知県';
$area_list['r-124-18-89-%'] = '愛知県';
$area_list['r-124-18-97-%'] = '岐阜県';
$area_list['r-124-18-102-%'] = '愛知県';
$area_list['r-124-18-103-%'] = '愛知県';
$area_list['r-124-18-104-%'] = '愛知県';
$area_list['r-124-18-105-%'] = '愛知県';
$area_list['r-124-18-134-%'] = '静岡県';
$area_list['r-124-18-166-%'] = '愛知県';
$area_list['r-124-18-167-%'] = '愛知県';
$area_list['r-124-18-192-%'] = '静岡県';
$area_list['r-124-18-199-%'] = '愛知県';
$area_list['r-124-18-224-%'] = '岐阜県';
$area_list['r-124-18-230-%'] = '愛知県';
$area_list['r-124-18-231-%'] = '愛知県';
$area_list['r-124-18-232-%'] = '愛知県';//224-?
$area_list['r-124-18-239-%'] = '愛知県';
$area_list['r-202-142-226-%'] = '愛知県';
$area_list['r-202-142-244-%'] = '愛知県';
$area_list['r-202-142-254-%'] = '愛知県';

//YahooBB-------------------------------------------------------------
$area_list['softbank060090045___.bbtec.net'] = '新潟県';
$area_list['softbank060096150___.bbtec.net'] = '岐阜県';
$area_list['softbank060098152___.bbtec.net'] = '東京都';

$area_list['softbank126097214___.bbtec.net'] = '千葉県';
$area_list['softbank126100050___.bbtec.net'] = '北海道';
$area_list['softbank126104027___.bbtec.net'] = '大阪府';
$area_list['softbank126108078___.bbtec.net'] = '大阪府';
$area_list['softbank126109153___.bbtec.net'] = '広島県';
$area_list['softbank126112051___.bbtec.net'] = '東京都';
$area_list['softbank126114030___.bbtec.net'] = '東京都';
$area_list['softbank126114183___.bbtec.net'] = '東京都';
$area_list['softbank126116044___.bbtec.net'] = '長野県';
$area_list['softbank126116097___.bbtec.net'] = '栃木県';
$area_list['softbank126116133___.bbtec.net'] = '神奈川県';
$area_list['softbank126118066___.bbtec.net'] = '福島県';//sp
$area_list['softbank126118088___.bbtec.net'] = '福島県';

$area_list['softbank218113080___.bbtec.net'] = '長崎県';
$area_list['softbank218113192___.bbtec.net'] = '沖縄県';
$area_list['softbank218113212___.bbtec.net'] = '沖縄県';
$area_list['softbank218115223___.bbtec.net'] = '高知県';
$area_list['softbank218116039___.bbtec.net'] = '福岡県';
$area_list['softbank218120______.bbtec.net'] = '広島県'; //120-ALL
$area_list['softbank218121______.bbtec.net'] = '大阪府';

$area_list['softbank218122______.bbtec.net'] = '愛知県';
$area_list['softbank218123______.bbtec.net'] = '京都府';

$area_list['softbank218126129___.bbtec.net'] = '奈良県';
$area_list['softbank218130183___.bbtec.net'] = '大阪府';
$area_list['softbank218131045___.bbtec.net'] = '岐阜県';
$area_list['softbank218131071___.bbtec.net'] = '岐阜県';
$area_list['softbank218135184___.bbtec.net'] = '千葉県';
$area_list['softbank218137114___.bbtec.net'] = '茨城県';
$area_list['softbank218142109___.bbtec.net'] = '愛知県';
$area_list['softbank218142112___.bbtec.net'] = '愛知県';
$area_list['softbank218142148___.bbtec.net'] = '愛知県';
$area_list['softbank218143068___.bbtec.net'] = '静岡県';
$area_list['softbank218143085___.bbtec.net'] = '静岡県';
$area_list['softbank218176132___.bbtec.net'] = '東京都';
$area_list['softbank218178001___.bbtec.net'] = '千葉県';
$area_list['softbank218179______.bbtec.net'] = '愛知県';
$area_list['softbank218180001___.bbtec.net'] = '千葉県';
$area_list['softbank218181001___.bbtec.net'] = '千葉県';
$area_list['softbank218181001___.bbtec.net'] = '大阪府';
$area_list['softbank218182144___.bbtec.net'] = '大阪府';
$area_list['softbank218182157___.bbtec.net'] = '大阪府';
$area_list['softbank219007130___.bbtec.net'] = '東京都';
$area_list['softbank219016036___.bbtec.net'] = '千葉県';
$area_list['softbank219021090___.bbtec.net'] = '大阪府';
$area_list['softbank219022218___.bbtec.net'] = '兵庫県';
$area_list['softbank219023064___.bbtec.net'] = '兵庫県';
$area_list['softbank219023228___.bbtec.net'] = '大阪府';
$area_list['softbank219027114___.bbtec.net'] = '兵庫県';
$area_list['softbank219029084___.bbtec.net'] = '京都府';
$area_list['softbank219029178___.bbtec.net'] = '石川県';
$area_list['softbank219030193___.bbtec.net'] = '和歌山県';

$area_list['softbank219032______.bbtec.net'] = '愛知県';
$area_list['softbank219033______.bbtec.net'] = '愛知県';
$area_list['softbank219034______.bbtec.net'] = '埼玉県';
$area_list['softbank219035______.bbtec.net'] = '神奈川県';
$area_list['softbank219036______.bbtec.net'] = '東京都';
$area_list['softbank219037______.bbtec.net'] = '東京都';
$area_list['softbank219038______.bbtec.net'] = '神奈川県';
$area_list['softbank219039______.bbtec.net'] = '神奈川県';
$area_list['softbank2190400_____.bbtec.net'] = '栃木県'; //127まで
$area_list['softbank21904010_____.bbtec.net'] = '栃木県';
$area_list['softbank21904011_____.bbtec.net'] = '栃木県';
$area_list['softbank219040120____.bbtec.net'] = '栃木県';
$area_list['softbank219040121____.bbtec.net'] = '栃木県';
$area_list['softbank219040122____.bbtec.net'] = '栃木県';
$area_list['softbank219040123____.bbtec.net'] = '栃木県';
$area_list['softbank219040124____.bbtec.net'] = '栃木県';
$area_list['softbank219040125____.bbtec.net'] = '栃木県';
$area_list['softbank219040126____.bbtec.net'] = '栃木県';
$area_list['softbank219040127____.bbtec.net'] = '栃木県';
$area_list['softbank219040128 ___.bbtec.net'] = '新潟県';
$area_list['softbank219040129 ___.bbtec.net'] = '新潟県';
$area_list['softbank21904013_____.bbtec.net'] = '新潟県';
$area_list['softbank21904014_____.bbtec.net'] = '新潟県';
$area_list['softbank21904015_____.bbtec.net'] = '新潟県';
$area_list['softbank21904016_____.bbtec.net'] = '新潟県';
$area_list['softbank21904017_____.bbtec.net'] = '新潟県';
$area_list['softbank21904018_____.bbtec.net'] = '新潟県';
$area_list['softbank21904019_____.bbtec.net'] = '新潟県';
$area_list['softbank2190402______.bbtec.net'] = '新潟県';
$area_list['softbank219041______.bbtec.net'] = '北海道';
$area_list['softbank21904200____.bbtec.net'] = '東京都'; //15まで
$area_list['softbank219042010___.bbtec.net'] = '東京都'; //15まで
$area_list['softbank219042011___.bbtec.net'] = '東京都'; //15まで
$area_list['softbank219042012___.bbtec.net'] = '東京都'; //15まで
$area_list['softbank219042013___.bbtec.net'] = '東京都'; //15まで
$area_list['softbank219042014___.bbtec.net'] = '東京都'; //15まで
$area_list['softbank219042015___.bbtec.net'] = '東京都'; //15まで
$area_list['softbank219042016___.bbtec.net'] = '神奈川県';
$area_list['softbank219042017___.bbtec.net'] = '神奈川県';
$area_list['softbank219042018___.bbtec.net'] = '神奈川県';
$area_list['softbank219042019___.bbtec.net'] = '神奈川県';
$area_list['softbank21904202____.bbtec.net'] = '神奈川県';
$area_list['softbank21904203____.bbtec.net'] = '神奈川県';
$area_list['softbank21904204____.bbtec.net'] = '神奈川県';
$area_list['softbank21904205____.bbtec.net'] = '神奈川県';
$area_list['softbank21904206____.bbtec.net'] = '神奈川県';
$area_list['softbank21904207____.bbtec.net'] = '神奈川県';
$area_list['softbank21904208____.bbtec.net'] = '神奈川県';
$area_list['softbank21904209____.bbtec.net'] = '神奈川県';
$area_list['softbank2190421_____.bbtec.net'] = '神奈川県';
$area_list['softbank2190422_____.bbtec.net'] = '神奈川県';
$area_list['softbank219043228___.bbtec.net'] = '神奈川県';
$area_list['softbank219044066___.bbtec.net'] = '神奈川県';
$area_list['softbank219045110___.bbtec.net'] = '神奈川県';
$area_list['softbank219046077___.bbtec.net'] = '神奈川県';
$area_list['softbank219046198___.bbtec.net'] = '山梨県';
$area_list['softbank219048______.bbtec.net'] = '静岡県';
$area_list['softbank219049______.bbtec.net'] = '愛知県';

$area_list['softbank2190500_____.bbtec.net'] = '愛知県';
$area_list['softbank21905010____.bbtec.net'] = '愛知県';
$area_list['softbank21905011____.bbtec.net'] = '愛知県';
$area_list['softbank219050123___.bbtec.net'] = '愛知県'; //128から三重
$area_list['softbank219051138___.bbtec.net'] = '福島県';
$area_list['softbank219053250___.bbtec.net'] = '山形県';
$area_list['softbank219054088___.bbtec.net'] = '三重県';
$area_list['softbank219054181___.bbtec.net'] = '岐阜県';
$area_list['softbank219055112___.bbtec.net'] = '栃木県';
$area_list['softbank219055192___.bbtec.net'] = '栃木県';
$area_list['softbank219056142___.bbtec.net'] = '長野県';
$area_list['softbank219057______.bbtec.net'] = '青森県';//057-ALL
$area_list['softbank219058055___.bbtec.net'] = '群馬県';
$area_list['softbank2190630_____.bbtec.net'] = '岡山県';
$area_list['softbank219063145___.bbtec.net'] = '岡山県';//-191まで
$area_list['softbank219063233___.bbtec.net'] = '広島県';
$area_list['softbank219168089___.bbtec.net'] = '北海道';
$area_list['softbank219169098___.bbtec.net'] = '北海道';
$area_list['softbank219170______.bbtec.net'] = '愛知県'; //170-ALL
$area_list['softbank219171122___.bbtec.net'] = '宮城県';
$area_list['softbank219172207___.bbtec.net'] = '福島県';
$area_list['softbank219173009___.bbtec.net'] = '岩手県';
$area_list['softbank219173170___.bbtec.net'] = '秋田県'; 
$area_list['softbank219173202___.bbtec.net'] = '青森県';
$area_list['softbank219176018___.bbtec.net'] = '千葉県';
$area_list['softbank219178138___.bbtec.net'] = '神奈川県';

$area_list['softbank219180034___.bbtec.net'] = '神奈川県';
$area_list['softbank219180102___.bbtec.net'] = '神奈川県';
$area_list['softbank219180132___.bbtec.net'] = '神奈川県';
$area_list['softbank219184223___.bbtec.net'] = '東京都';
$area_list['softbank219187010___.bbtec.net'] = '東京都';
$area_list['softbank219195008___.bbtec.net'] = '東京都';
$area_list['softbank219196092___.bbtec.net'] = '東京都';
$area_list['softbank219199116___.bbtec.net'] = '埼玉県';
$area_list['softbank219199203___.bbtec.net'] = '埼玉県';
$area_list['softbank219201194___.bbtec.net'] = '埼玉県';
$area_list['softbank219201204___.bbtec.net'] = '埼玉県';
$area_list['softbank219202226___.bbtec.net'] = '神奈川県';
$area_list['softbank219203033___.bbtec.net'] = '愛知県';
$area_list['softbank219203084___.bbtec.net'] = '愛知県';
$area_list['softbank219205084___.bbtec.net'] = '神奈川県';
$area_list['softbank219208058___.bbtec.net'] = '山形県';
$area_list['softbank219208090___.bbtec.net'] = '岩手県';
$area_list['softbank219209100___.bbtec.net'] = '愛知県';
$area_list['softbank219209124___.bbtec.net'] = '愛知県';
$area_list['softbank219209215___.bbtec.net'] = '愛知県';
$area_list['softbank219210102___.bbtec.net'] = '愛知県';
$area_list['softbank219211020___.bbtec.net'] = '岐阜県';
$area_list['softbank219211148___.bbtec.net'] = '愛知県';
$area_list['softbank219211215___.bbtec.net'] = '愛知県';
$area_list['softbank219214008___.bbtec.net'] = '長野県';

$area_list['softbank220000135___.bbtec.net'] = '静岡県';
$area_list['softbank220000252___.bbtec.net'] = '愛知県';
$area_list['softbank220000254___.bbtec.net'] = '愛知県';
$area_list['softbank220001123___.bbtec.net'] = '埼玉県';
$area_list['softbank220003020___.bbtec.net'] = '群馬県';
$area_list['softbank220003176___.bbtec.net'] = '群馬県';
$area_list['softbank220004230___.bbtec.net'] = '栃木県';
$area_list['softbank220010088___.bbtec.net'] = '山梨県';
$area_list['softbank220019019___.bbtec.net'] = '鳥取県';
$area_list['softbank220020199___.bbtec.net'] = '佐賀県';
$area_list['softbank220021049___.bbtec.net'] = '富山県';
$area_list['softbank220023030___.bbtec.net'] = '岐阜県';
$area_list['softbank220023099___.bbtec.net'] = '岐阜県';
$area_list['softbank220023248___.bbtec.net'] = '静岡県';
$area_list['softbank220024120___.bbtec.net'] = '福井県';
$area_list['softbank220024150___.bbtec.net'] = '富山県';
$area_list['softbank220024190___.bbtec.net'] = '富山県';
$area_list['softbank220027______.bbtec.net'] = '愛知県';
$area_list['softbank220028______.bbtec.net'] = '愛知県';
$area_list['softbank220029082___.bbtec.net'] = '愛知県';
$area_list['softbank220029232___.bbtec.net'] = '愛知県';
$area_list['softbank220029234___.bbtec.net'] = '愛知県';
$area_list['softbank220029246___.bbtec.net'] = '愛知県';
$area_list['softbank220030______.bbtec.net'] = '愛知県'; //030-ALL
$area_list['softbank220031092___.bbtec.net'] = '愛知県';
$area_list['softbank220031166___.bbtec.net'] = '愛知県';
$area_list['softbank220031183___.bbtec.net'] = '岐阜県';
$area_list['softbank220031252___.bbtec.net'] = '岐阜県';
$area_list['softbank220033204___.bbtec.net'] = '大阪府';
$area_list['softbank220034186___.bbtec.net'] = '大阪府';
$area_list['softbank220036018___.bbtec.net'] = '大阪府';
$area_list['softbank220036248___.bbtec.net'] = '大阪府';
$area_list['softbank220037132___.bbtec.net'] = '大阪府';
$area_list['softbank220038180___.bbtec.net'] = '大阪府';
$area_list['softbank220040043___.bbtec.net'] = '兵庫県';
$area_list['softbank220042234___.bbtec.net'] = '大阪府';
$area_list['softbank220045176___.bbtec.net'] = '香川県';
$area_list['softbank220045198___.bbtec.net'] = '香川県';
$area_list['softbank220046064___.bbtec.net'] = '京都府';
$area_list['softbank220049052___.bbtec.net'] = '山口県';
$area_list['softbank220049254___.bbtec.net'] = '高知県';
$area_list['softbank220050075___.bbtec.net'] = '和歌山県';
$area_list['softbank220050142___.bbtec.net'] = '和歌山県';
$area_list['softbank220051128___.bbtec.net'] = '滋賀県';
$area_list['softbank220054066___.bbtec.net'] = '福岡県';
$area_list['softbank220059002___.bbtec.net'] = '福岡県';
$area_list['softbank220060148___.bbtec.net'] = '福岡県';
$area_list['softbank220061030___.bbtec.net'] = '福岡県';
$area_list['softbank220062051___.bbtec.net'] = '宮崎県';

$area_list['softbank221016198___.bbtec.net'] = '新潟県';
$area_list['softbank221017233___.bbtec.net'] = '新潟県';
$area_list['softbank221019010___.bbtec.net'] = '神奈川県';
$area_list['softbank221019164___.bbtec.net'] = '東京都';
$area_list['softbank221022072___.bbtec.net'] = '千葉県';
$area_list['softbank2210240_____.bbtec.net'] = '三重県';
$area_list['softbank22102410____.bbtec.net'] = '三重県';
$area_list['softbank22102411____.bbtec.net'] = '三重県';
$area_list['softbank221024120___.bbtec.net'] = '三重県';
$area_list['softbank221024121___.bbtec.net'] = '三重県';
$area_list['softbank221024122___.bbtec.net'] = '三重県';
$area_list['softbank221024123___.bbtec.net'] = '三重県';
$area_list['softbank221024124___.bbtec.net'] = '三重県';
$area_list['softbank221024125___.bbtec.net'] = '三重県';
$area_list['softbank221024126___.bbtec.net'] = '三重県';
$area_list['softbank221024127___.bbtec.net'] = '三重県';
$area_list['softbank221024132___.bbtec.net'] = '静岡県';
$area_list['softbank221024252___.bbtec.net'] = '愛知県';
$area_list['softbank221026029___.bbtec.net'] = '山形県';
$area_list['softbank221026100___.bbtec.net'] = '宮城県';
$area_list['softbank221026154___.bbtec.net'] = '岩手県';
$area_list['softbank221027080___.bbtec.net'] = '茨城県';
$area_list['softbank221027156___.bbtec.net'] = '茨城県';
$area_list['softbank221027191___.bbtec.net'] = '茨城県';
$area_list['softbank221030067___.bbtec.net'] = '静岡県';
$area_list['softbank221030104___.bbtec.net'] = '静岡県';
$area_list['softbank221031194___.bbtec.net'] = '栃木県';
$area_list['softbank221032000___.bbtec.net'] = '北海道';
$area_list['softbank221032172___.bbtec.net'] = '北海道';
$area_list['softbank221033009___.bbtec.net'] = '岩手県';
$area_list['softbank221034012___.bbtec.net'] = '長野県';
$area_list['softbank221034183___.bbtec.net'] = '千葉県';
$area_list['softbank221035084___.bbtec.net'] = '岐阜県';
$area_list['softbank221035254___.bbtec.net'] = '愛知県';
$area_list['softbank221038000___.bbtec.net'] = '群馬県';
$area_list['softbank221038235___.bbtec.net'] = '千葉県';
$area_list['softbank221042114___.bbtec.net'] = '愛知県';
$area_list['softbank221042224___.bbtec.net'] = '愛知県';
$area_list['softbank221042226___.bbtec.net'] = '愛知県';
$area_list['softbank221043000___.bbtec.net'] = '茨城県';
$area_list['softbank221043196___.bbtec.net'] = '群馬県';
$area_list['softbank221045175___.bbtec.net'] = '静岡県';
$area_list['softbank221045197___.bbtec.net'] = '三重県';
$area_list['softbank221046131___.bbtec.net'] = '北海道';
$area_list['softbank221047231___.bbtec.net'] = '千葉県';
$area_list['softbank221048018___.bbtec.net'] = '山梨県';
$area_list['softbank221048028___.bbtec.net'] = '静岡県';
$area_list['softbank221056153___.bbtec.net'] = '新潟県';

$area_list['softbank22105700____.bbtec.net'] = '岐阜県';//1-63
$area_list['softbank22105701____.bbtec.net'] = '岐阜県';//1-63
$area_list['softbank22105702____.bbtec.net'] = '岐阜県';//1-63
$area_list['softbank22105703____.bbtec.net'] = '岐阜県';//1-63
$area_list['softbank22105704____.bbtec.net'] = '岐阜県';//1-63
$area_list['softbank22105705____.bbtec.net'] = '岐阜県';//1-63
$area_list['softbank221057060___.bbtec.net'] = '岐阜県';//1-63
$area_list['softbank221057061___.bbtec.net'] = '岐阜県';//1-63
$area_list['softbank221057062___.bbtec.net'] = '岐阜県';//1-63
$area_list['softbank221057063___.bbtec.net'] = '岐阜県';//1-63
$area_list['softbank221057064___.bbtec.net'] = '愛知県';
$area_list['softbank221057065___.bbtec.net'] = '愛知県';
$area_list['softbank221057066___.bbtec.net'] = '愛知県';
$area_list['softbank221057067___.bbtec.net'] = '愛知県';
$area_list['softbank221057068___.bbtec.net'] = '愛知県';
$area_list['softbank221057069___.bbtec.net'] = '愛知県';
$area_list['softbank22105707____.bbtec.net'] = '愛知県';
$area_list['softbank22105708____.bbtec.net'] = '愛知県';
$area_list['softbank22105709____.bbtec.net'] = '愛知県';
$area_list['softbank2210571_____.bbtec.net'] = '愛知県';
$area_list['softbank2210572_____.bbtec.net'] = '愛知県';
$area_list['softbank221062144___.bbtec.net'] = '北海道';
$area_list['softbank221063131___.bbtec.net'] = '愛媛県';
$area_list['softbank221067037___.bbtec.net'] = '長崎県'; //1-63
$area_list['softbank221071083___.bbtec.net'] = '香川県';
$area_list['softbank221077013___.bbtec.net'] = '愛知県';
$area_list['softbank221077180___.bbtec.net'] = '長崎県';
$area_list['softbank221079012___.bbtec.net'] = '広島県';
$area_list['softbank221079048___.bbtec.net'] = '富山県';
$area_list['softbank221080135___.bbtec.net'] = '香川県';
$area_list['softbank221081038___.bbtec.net'] = '石川県';
$area_list['softbank221081050___.bbtec.net'] = '石川県';
$area_list['softbank221081064___.bbtec.net'] = '富山県';
$area_list['softbank221082209___.bbtec.net'] = '長崎県';
$area_list['softbank221083050___.bbtec.net'] = '宮崎県';
$area_list['softbank221084065___.bbtec.net'] = '鳥取県';
$area_list['softbank221084127___.bbtec.net'] = '鳥取県';
$area_list['softbank221084156___.bbtec.net'] = '広島県';
$area_list['softbank221088188___.bbtec.net'] = '宮崎県';
$area_list['softbank221088244___.bbtec.net'] = '佐賀県';
$area_list['softbank221088255___.bbtec.net'] = '福岡県';
$area_list['softbank221089082___.bbtec.net'] = '広島県';
$area_list['softbank221089129___.bbtec.net'] = '高知県';

$area_list['softbank221092175___.bbtec.net'] = '兵庫県';
$area_list['softbank221092210___.bbtec.net'] = '東京都';
$area_list['softbank221094035___.bbtec.net'] = '広島県';
$area_list['softbank221094190___.bbtec.net'] = '岡山県';
$area_list['softbank221095098___.bbtec.net'] = '石川県';
$area_list['softbank221108041___.bbtec.net'] = '神奈川県';

//IP
$area_list['210.236.67.%'] = '愛知県';
$area_list['220.213.187.%'] = '福井県';

$area_list['124.215.249.%'] = '愛知県';
$area_list['133.163.%'] = '群馬県';
$area_list['61.8.88.%'] = '石川県';
$area_list['210.190.117.%'] = '北海道';
$area_list['202.214.149.%'] = '京都府';
$area_list['138.212.%'] = '東京都';
$area_list['202.222.77.%'] = '富山県';
$area_list['202.32.113.%'] = '大阪府';

//engine

//海外
$area_list['207.46.%'] = '不明';
$area_list['207.46.%'] = '不明';
$area_list['118.100.%'] = '不明';
$area_list['118.101.%'] = '不明';
$area_list['119.27.62.%'] = '不明';
$area_list['98.192.%'] = '不明';
$area_list['98.194.%'] = '不明';
$area_list['%.mediaWays.net'] = '不明'; //ドイツ
$area_list['%.altushost.com'] = '不明'; //ドイツ
$area_list['124.126.%'] = '不明'; //中国
$area_list['124.127.%'] = '不明'; //中国

$area_list['%.charter.com'] = '不明'; //アメリカ
$area_list['148.177.%'] = '不明'; //アメリカ
$area_list['%.elaninet.com'] = '不明'; //ウクライナ
$area_list['74.125.%'] = '不明';
$area_list['%.de'] = '不明';
$area_list['%.ca'] = '不明';
$area_list['%.cn'] = '不明'; //中国
$area_list['%.mil'] = '不明'; //米国軍
$area_list['%.usamimi.info'] = '不明';

//.ne.jp
$area_list['%.aba.ne.jp'] = '愛知県';
$area_list['%.aics.ne.jp'] = '東京都';
$area_list['%.akina.ne.jp'] = '福島県';
$area_list['%.akira.ne.jp'] = '東京都';
$area_list['%.asama.ne.jp'] = '長野県';
$area_list['%.biwa.ne.jp'] = '滋賀県';
$area_list['%.cats.ne.jp'] = '埼玉県';
$area_list['%.cds.ne.jp'] = '神奈川県';
$area_list['%.clio.ne.jp'] = '長野県';
$area_list['%.comlink.ne.jp'] = '山梨県';
$area_list['%.cosmos.ne.jp'] = '沖縄県';
$area_list['%.cypress.ne.jp'] = '和歌山県';

$area_list['%.dcn.ne.jp'] = '長野県';
$area_list['%.dreams.ne.jp'] = '茨城県';
$area_list['%ii-okinawa.ne.jp'] = '沖縄県';
$area_list['%.invoice.ne.jp'] = '東京都';

$area_list['%.echigo.ne.jp'] = '岩手県';
$area_list['%.echna.ne.jp'] = '岩手県';
$area_list['%.edit.ne.jp'] = '東京都';
$area_list['%.okym.enjoy.ne.jp'] = '岡山県';
$area_list['aic-%.enjoy.ne.jp'] = '愛知県';

$area_list['%.harenet.ne.jp'] = '岡山県';
$area_list['%.hatena.ne.jp'] = '京都府';
$area_list['%.hotcn.ne.jp'] = '北海道';
$area_list['%.iam.ne.jp'] = '大阪府';
$area_list['%.incl.ne.jp'] = '石川県/富山県/福井県';
$area_list['%.infoaomori.ne.jp'] = '青森県';
$area_list['%.infosnow.ne.jp'] = '北海道';
$area_list['%.jah.ne.jp'] = '東京都';
$area_list['%.jnc.ne.jp'] = '埼玉県';
$area_list['%.jomon.ne.jp'] = '青森県';
$area_list['%.keihanna.ne.jp'] = '京都府';
$area_list['%.knet.ne.jp'] = '埼玉県';
$area_list['%.matsumoto.ne.jp'] = '長野県';
$area_list['%.maxs.ne.jp'] = '長野県';
$area_list['%.bbcat.jp'] = '長野県';
$area_list['%.mco.ne.jp'] = '沖縄県';
$area_list['%.meon.ne.jp'] = '岡山県/鳥取県';

$area_list['%.mis.ne.jp'] = '愛知県';
$area_list['%.nima-cho.ne.jp'] = '島根県';

$area_list['SOD%odn.__.jp'] = '北海道'; //札幌（北海道）
$area_list['OKI%odn.__.jp'] = '青森県'; //沖館（青森県）
$area_list['MRN%odn.__.jp'] = '岩手県'; //盛岡（岩手県）
$area_list['NKD%odn.__.jp'] = '秋田県'; //中通（秋田県）
$area_list['IMZ%odn.__.jp'] = '山形県'; //今塚（山形県）
$area_list['AOB%odn.__.jp'] = '宮城県'; //青葉（宮城県）
$area_list['HNZ%odn.__.jp'] = '福島県'; //花園（福島県）
$area_list['HRD%odn.__.jp'] = '栃木県'; //平出（栃木県）
$area_list['AKA%odn.__.jp'] = '茨城県'; //赤塚（茨城県）
$area_list['KKR%odn.__.jp'] = '群馬県'; //国領（群馬県）
$area_list['SKN%odn.__.jp'] = '埼玉県'; //草加（埼玉県）
$area_list['FNA%odn.__.jp'] = '千葉県'; //船橋（千葉県）
$area_list['OFS%odn.__.jp'] = '東京都'; //大手町ファーストスクエア（東京都）
$area_list['HDO%odn.__.jp'] = '神奈川県'; //保土ヶ谷（神奈川県）
$area_list['NGN%odn.__.jp'] = '新潟県'; //新潟西（新潟県）
$area_list['TYN%odn.__.jp'] = '富山県'; //富山（富山県）
$area_list['KNN%odn.__.jp'] = '石川県'; //金沢（石川県）
$area_list['FKN%odn.__.jp'] = '福井県'; //福井（福井県）
$area_list['KFN%odn.__.jp'] = '山梨県'; //甲府（山梨県）
$area_list['SYD%odn.__.jp'] = '長野県'; //信濃吉田（長野県）
$area_list['SDD%odn.__.jp'] = '静岡県'; //静岡電電（静岡県）
$area_list['CEP%odn.__.jp'] = '愛知県';
$area_list['SSJ%odn.__.jp'] = '愛知県'; //笹島（愛知県）
$area_list['YKM%odn.__.jp'] = '三重県'; //四日市（三重県）
$area_list['OTU%odn.__.jp'] = '滋賀県'; //大津（滋賀県）
$area_list['KYN%odn.__.jp'] = '京都府'; //京都（京都府）
$area_list['NWT%odn.__.jp'] = '大阪府'; //浪花町（大阪府）
$area_list['KBM%odn.__.jp'] = '兵庫県'; //神戸港（兵庫県）
$area_list['DAJ%odn.__.jp'] = '奈良県'; //大安寺（奈良県）
$area_list['WKN%odn.__.jp'] = '和歌山県'; //和歌山（和歌山県）
$area_list['TTN%odn.__.jp'] = '鳥取県'; //鳥取（鳥取県）
$area_list['SMN%odn.__.jp'] = '島根県'; //島根（島根県）
$area_list['IMM%odn.__.jp'] = '岡山県'; //今村（岡山県）
$area_list['NIH%odn.__.jp'] = '広島県'; //仁保（広島県）
$area_list['YGN%odn.__.jp'] = '山口県'; //山口（山口県）
$area_list['TKN%odn.__.jp'] = '徳島県'; //徳島（徳島県）
$area_list['TMN%odn.__.jp'] = '香川県'; //高松（香川県）
$area_list['MYN%odn.__.jp'] = '愛媛県'; //松山（愛媛県）
$area_list['KCM%odn.__.jp'] = '高知県'; //高知（高知県）
$area_list['FKC%odn.__.jp'] = '福岡県'; //福岡中央（福岡県）
$area_list['TGS%odn.__.jp'] = '佐賀県'; //高木瀬（佐賀県）
$area_list['SCO%odn.__.jp'] = '長崎県'; //新長（長崎県）
$area_list['OBY%odn.__.jp'] = '熊本県'; //帯山（熊本県）
$area_list['OMC%odn.__.jp'] = '大分県'; //大道（大分県）
$area_list['MZN%odn.__.jp'] = '宮崎県'; //宮崎（宮崎県）
$area_list['KMI%odn.__.jp'] = '鹿児島'; //鴨池（鹿児島県）
$area_list['YRM%odn.__.jp'] = '沖縄県'; //寄宮（沖縄県）

$area_list['%.owari.ne.jp'] = '愛知県';

$area_list['%.point.ne.jp'] = '東京都';
$area_list['%.psn.ne.jp'] = '大阪府';
$area_list['%.px0256.ne.jp'] = '新潟県';
$area_list['%.rmc.ne.jp'] = '滋賀県';
$area_list['%.safins.ne.jp'] = '福島県';
$area_list['%.sdx.ne.jp'] = '埼玉県';
$area_list['%.sni.ne.jp'] = '佐賀県/長崎県';
$area_list['%.spnet.ne.jp'] = '新潟県';
$area_list['%.synapse.ne.jp'] = '鹿児島県';
$area_list['%.tcpweb.ne.jp'] = '京都府';
$area_list['%.tam.ne.jp'] = '富山県';

$area_list['%.valley.ne.jp'] = '長野県';
$area_list['%.wakwak.ne.jp'] = '不明';
$area_list['%.wbs.ne.jp'] = '静岡県';
$area_list['%.webport.ne.jp'] = '北海道';
$area_list['%.wide.ne.jp'] = '群馬県';
$area_list['%.wind.ne.jp'] = '群馬県';

//.or.jp
$area_list['%.across.or.jp'] = '静岡県';
$area_list['%.amie.or.jp'] = '東京都';
$area_list['%.aya.or.jp'] = '埼玉県';
$area_list['%.big-c.or.jp'] = '福井県';
$area_list['%.enshu-net.or.jp'] = '静岡県';
$area_list['%.hakodate.or.jp'] = '北海道';
$area_list['%.ic-net.or.jp'] = '山形県';
$area_list['%.index.or.jp'] = '東京都';
$area_list['%.inet-shibata.or.jp'] = '新潟県';
$area_list['%.inetshonai.or.jp'] = '山形県';
$area_list['%.inetmie.or.jp'] = '三重県';
$area_list['%.intio.or.jp'] = '茨城県';
$area_list['%.janis.or.jp'] = '長野県';
$area_list['%.kamome.or.jp'] = '神奈川県';
$area_list['%.kinet.or.jp'] = '神奈川県';
$area_list['%.kyoto-inet.or.jp'] = '京都府';
$area_list['%.nasuinfo.or.jp'] = '栃木県';
$area_list['%.nasu-net.or.jp'] = '栃木県';
$area_list['%.niji.or.jp'] = '香川県';
$area_list['%.nnc.or.jp'] = '和歌山県';
$area_list['%.okazaki-med.or.jp'] = '愛知県';
$area_list['%.sainet.or.jp'] = '埼玉県';
$area_list['%.takauji.or.jp'] = '北関東:栃木県/群馬県';
$area_list['%.tama.or.jp'] = '東京都';
$area_list['%.tenrikyo.or.jp'] = '奈良県';
$area_list['%.t-cnet.or.jp'] = '栃木県';
$area_list['%.tgn.or.jp'] = '東京都';
$area_list['%.yin.or.jp'] = '山梨県';
$area_list['%.yutopia.or.jp'] = '秋田県';

//.co.jp
$area_list['%.ace-net.co.jp'] = '千葉県';
$area_list['%.advance.jp'] = '東京都';
$area_list['%.aplus.co.jp'] = '東京都';
$area_list['%.alchemia.co.jp'] = '東京都';
$area_list['%.beacon-it.co.jp'] = '東京都';
$area_list['%.blg.co.jp'] = '兵庫県';
$area_list['%.chabashira.co.jp'] = '静岡県';
$area_list['%.conami.co.jp'] = '東京都';
$area_list['%.cybird.co.jp'] = '東京都';
$area_list['%.daikin.co.jp'] = '大阪府';
$area_list['ogaki-ns.densan-s.co.jp'] = '岐阜県';
$area_list['%.daiken.co.jp'] = '富山県';
$area_list['%.dnp.co.jp'] = '東京都';
$area_list['t2.fujitsu.co.jp'] = '神奈川県';
$area_list['%.globalcoms.co.jp'] = '東京都';
$area_list['%.hankyu-hanshin.co.jp'] = '大阪府';
$area_list['%.hudson.co.jp'] = '東京都';
$area_list['%.hikari.co.jp'] = '東京都';
$area_list['%.inax.co.jp'] = '愛知県';
$area_list['%.ibrains.co.jp'] = '東京都';
$area_list['%.infotechnica.co.jp'] = '福井県';
$area_list['%.itsol.co.jp'] = '東京都';
$area_list['k-denkai.co.jp'] = '茨城県';
$area_list['%.keyence.co.jp'] = '大阪府';
$area_list['%.kuronekoyamato.co.jp'] = '東京都';
$area_list['%.kodansha.co.jp'] = '東京都';
$area_list['%.meidensha.co.jp'] = '東京都';
$area_list['%.mew.co.jp'] = '大阪府';
$area_list['%.nat.co.jp'] = '神奈川県';
$area_list['%.naris.co.jp'] = '大阪府';
$area_list['%.nec.co.jp'] = '東京都';
$area_list['%.neive.co.jp'] = '石川県';
$area_list['%.nippon-access.co.jp'] = '東京都';
$area_list['%.nissanchem.co.jp'] = '東京都';
$area_list['%.nsplanning.co.jp'] = '東京都';
$area_list['%.nsc-kk.co.jp'] = '東京都';
$area_list['%.ntt-it.co.jp'] = '神奈川県';
$area_list['%.nttdata-tokai.co.jp'] = '愛知県';
$area_list['%.nttdocomo.co.jp'] = '東京都';
$area_list['%.ntts.co.jp'] = '東京都';
$area_list['%.oitechno.co.jp'] = '神奈川県';
$area_list['%.jp.panasonic.com'] = '大阪府';
$area_list['%.pca.co.jp'] = '東京都';
$area_list['%.senkyo.co.jp'] = '宮城県';
$area_list['%.siliconstudio.co.jp'] = '東京都';
$area_list['%.sony.co.jp'] = '東京都';
$area_list['%.Sony.CO.JP'] = '東京都';
$area_list['%.sonique.co.jp'] = '群馬県';
$area_list['%.softbanktelecom.co.jp'] = '東京都';
$area_list['%.sunnyhealth.co.jp'] = '長野県';
$area_list['%.takara.co.jp'] = '京都府';
$area_list['%.tbs.co.jp'] = '東京都';
$area_list['%.tose.co.jp'] = '京都府';
$area_list['%.toyodenki.co.jp'] = '東京都';
$area_list['%.toyotasmile.co.jp'] = '愛知県';
$area_list['210.148.193.%'] = '東京都';
$area_list['%.toppan.co.jp'] = '東京都';

$area_list['%.u-4.co.jp'] = '宮城県';
$area_list['%.ymir.co.jp'] = '東京都';
$area_list['%.koanet.co.jp'] = '長野県';
$area_list['%.fork.co.jp'] = '東京都';
$area_list['mutenet.co.jp'] = '北海道';
$area_list['%.systemd.co.jp'] = '京都府';
$area_list['%.webjapan.co.jp'] = '東京都';
$area_list['%.weathermap.co.jp'] = '東京都';

//.ad.jp
$area_list['%.cypress.ad.jp'] = '和歌山県';
$area_list['%.hotnet.ad.jp'] = '北海道';
$area_list['%.infovalley.ad.jp'] = '長野県';

//.ed.jp--------------------------------------------------------------------------------------
$area_list['%-oky.ed.jp%'] = '岡山県';
$area_list['%.toyota.ed.jp'] = '愛知県';
$area_list['%.owariasahi.ed.jp'] = '愛知県';
$area_list['%.asn.ed.jp'] = '青森県';

//.jp
$area_list['%.aqstage.jp'] = '大阪府';
$area_list['%.bbix.jp'] = '山形県';
$area_list['%.shizuoka.jp'] = '静岡県';
$area_list['%.chukyo-hosp.jp'] = '愛知県';
$area_list['%.datacoa.jp'] = '秋田県';
$area_list['%.fenics.jp'] = '東京都';
$area_list['%.hbb.jp'] = '北海道';

$area_list['%.jsd-nsp.jp'] = '北海道';

$area_list['%.3sannet.jp'] = '宮城県';
$area_list['%.kyoto-inetbb.jp'] = '京都府';
$area_list['%.nbgi.jp'] = '東京都';
$area_list['%.is-ja.jp'] = '石川県';
$area_list['%.maine.jp'] = '群馬県';
$area_list['%.mexne.jp'] = '東京都';
$area_list['%.mis.jp'] = '愛知県';

$area_list['%.ouk.jp'] = '大阪府';
$area_list['%.soka.jp'] = '不明';
$area_list['%.ubsecure.jp'] = '東京都';
$area_list['%.ybnet.jp'] = '秋田県';

//.lg.jp
$area_list['%.nasushiobara.lg.jp'] = '栃木県';

//.go.jp
$area_list['%.riken.go.jp'] = '埼玉県';

$area_list['%.ktr.mlit.go.jp'] = '埼玉県';
$area_list['%.tokai-sc.jaea.go.jp'] = '茨城県';
//.info
$area_list['%.sbb-sys.info'] = '東京都';

//.net

$area_list['%.asua.net'] = '愛知県';
$area_list['%.bitcat.net'] = '東京都';
$area_list['%.hokkai.net'] = '北海道';
$area_list['p1.hi-hoinc.net'] = '大阪府';
$area_list['%.netaro.net'] = '山口県';
$area_list['%.net3-tv.net'] = '富山県';
$area_list['%.hinet.net'] = '不明';

//.com
$area_list['%.7-dj.com'] = '青森県';
$area_list['%.dososhin.com'] = '東京都';
$area_list['%.e-realize.com'] = '富山県';
$area_list['%.gaiax.com'] = '東京都';
$area_list['%.iplanet-inc.com'] = '東京都';
$area_list['%.mizuho-sc.com'] = '東京都';
$area_list['%.xrea.com'] = '不明';

//biz

//不明

//.ac.jp
//大学
//--北海道ここから-->
$area_list['%.asahikawa-med.ac.jp'] = '北海道';
$area_list['%.hokudai.ac.jp'] = '北海道';
$area_list['%.chitose.ac.jp'] = '北海道';
$area_list['%.tus.ac.jp'] = '北海道'; //n
//--北海道ここまで-->
//--青森ここから-->
$area_list['%.hachinohe-ct.ac.jp'] = '青森県';

//--青森ここまで-->
//--岩手ここから-->

//--岩手ここまで-->
//--宮城ここから-->
$area_list['130.34.%'] = '宮城県';
$area_list['%.tohoku-gakuin.ac.jp'] = '宮城県';
$area_list['%.jc-21.ac.jp'] = '宮城県';
//--宮城ここまで-->
//--秋田ここから-->

//--秋田ここまで-->
//--山形ここから-->
//--山形ここまで-->
//--福島ここから-->
//--福島ここまで-->
//--茨城ここから-->
//--茨城ここまで-->
//--栃木ここから-->
$area_list['%.jichi.ac.jp'] = '栃木県';
//--栃木ここまで-->
//--群馬ここから-->
$area_list['%.jobu.ac.jp'] = '群馬県';
$area_list['%.yamasaki.ac.jp'] = '群馬県';
//--群馬ここまで-->
//--埼玉ここから-->
$area_list['%.nit.ac.jp'] = '埼玉県';
$area_list['%.arsnet.ac.jp'] = '埼玉県';
//--埼玉ここまで-->
//--千葉ここから-->
//--千葉ここまで-->
//--神奈川ここから--> '神奈川県'
$area_list['%.ynu.ac.jp'] = '神奈川県';
$area_list['%.toin.ac.jp'] = '神奈川県';
$area_list['%.ferris.ac.jp'] = '神奈川県';

//--神奈川ここまで-->
//--東京ここから-->
$area_list['%.uec.ac.jp'] = '東京都';
$area_list['%.kaiyodai.ac.jp'] = '東京都';
$area_list['%.geidai.ac.jp'] = '東京都';
$area_list['%.tuat.ac.jp'] = '東京都';
$area_list['%.tmu.ac.jp'] = '東京都';
$area_list['%.obirin.ac.jp'] = '東京都';
$area_list['%.gakushuin.ac.jp'] = '東京都';
$area_list['%.kyorin-u.ac.jp'] = '東京都';
$area_list['%.keio.ac.jp'] = '東京都';
$area_list['%.icu.ac.jp'] = '東京都';
$area_list['%.takachiho.ac.jp'] = '東京都';
$area_list['%.teu.ac.jp'] = '東京都';
$area_list['%.jikei.ac.jp'] = '東京都';
$area_list['%.nihon-u.ac.jp'] = '東京都';
$area_list['%.ndu.ac.jp'] = '東京都';
$area_list['%.meiji.ac.jp'] = '東京都';
$area_list['%.meisei-u.ac.jp'] = '東京都';
$area_list['%.waseda.ac.jp'] = '東京都';
$area_list['%.jec.ac.jp'] = '東京都';
$area_list['%.neec.ac.jp'] = '東京都';
$area_list['%.tokyo-ct.ac.jp'] = '東京都';
//--東京ここまで-->
//--新潟ここから-->
$area_list['%.nagaokaut.ac.jp'] = '新潟県';
$area_list['%.nuis.ac.jp'] = '新潟県';
//--新潟ここまで-->
//--富山ここから-->
$area_list['%.urayama.ac.jp'] = '富山県';
//--富山ここまで-->
//--石川ここから-->
$area_list['%.jaist.ac.jp'] = '石川県';
//--石川ここまで-->
//--福井ここから-->
//--福井ここまで-->
//--山梨ここから-->
//--山梨ここまで-->
//--長野ここから-->
$area_list['%.isahaya-cc.ac.jp'] = '長野県';
//--長野ここまで-->
//--岐阜ここから-->
$area_list['133.66.%'] = '岐阜県';
//--岐阜ここまで-->
//--静岡ここから-->
$area_list['%.tokoha.ac.jp'] = '静岡県';
$area_list['%.rad.ac.jp'] = '静岡県';
//--静岡ここまで-->
//--愛知ここから-->
$area_list['%.nitech.ac.jp'] = '愛知県';
$area_list['%.tut.ac.jp'] = '愛知県';
$area_list['163.214.%'] = '愛知県';
$area_list['%.aitech.ac.jp'] = '愛知県';
$area_list['%.chubu.ac.jp'] = '愛知県';
$area_list['%.ims.ac.jp'] = '愛知県';
$area_list['%.nakanishi.ac.jp'] = '愛知県';
$area_list['%.nagoya.hal.ac.jp'] = '愛知県';

//--愛知ここまで-->
//--三重ここから-->
//--三重ここまで-->
//--滋賀ここから-->
//--滋賀ここまで-->
//--京都ここから-->
$area_list['%.kit.ac.jp'] = '京都府';
$area_list['%.ritsumei.ac.jp'] = '京都府';
$area_list['%.kyoto-kct.ac.jp'] = '京都都';
//--京都ここまで-->
//--大阪ここから-->
$area_list['%.oit.ac.jp'] = '大阪府';
$area_list['133.64.%'] = '大阪府';
$area_list['%.kansai-u.ac.jp'] = '大阪府';
$area_list['%.osaka-pct.ac.jp'] = '大阪府';
//--大阪ここまで-->
//--兵庫ここから-->
$area_list['%.hyo-med.ac.jp'] = '兵庫県';
//--兵庫ここまで-->
//--奈良ここから-->
//--奈良ここまで-->
//--和歌山ここから-->
//--和歌山ここまで-->
//--鳥取ここから-->
//--鳥取ここまで-->
//--島根ここから-->
//--島根ここまで-->
//--岡山ここから-->
$area_list['%.kusa.ac.jp'] = '岡山県';
//--岡山ここまで-->
//--広島ここから-->
$area_list['133.41.%'] = '広島県';
$area_list['%.hirokoku-u.ac.jp'] = '広島県';
$area_list['%.hkg.ac.jp'] = '広島県';
//--広島ここまで-->
//--山口ここから-->
$area_list['%.yic.ac.jp'] = '山口県';
//--山口ここまで-->
//--徳島ここから-->
//--徳島ここまで-->
//--香川ここから-->
//--香川ここまで-->
//--愛媛ここから-->
$area_list['192.218.200.%'] = '愛媛県';
//--愛媛ここまで-->
//--高知ここから-->
//--高知ここまで-->
//--福岡ここから-->
$area_list['%.kyushu-u.ac.jp'] = '福岡県';
$area_list['%.kyutech.ac.jp'] = '福岡県';
$area_list['%.fit.ac.jp'] = '福岡県';
$area_list['%.ariake-nct.ac.jp'] = '福岡県';
//--福岡ここまで-->
//--佐賀ここから-->
//--佐賀ここまで-->
//--長崎ここから-->
//--長崎ここまで-->
//--熊本ここから-->
$area_list['%.knct.ac.jp'] = '熊本県';
//--熊本ここまで-->
//--大分ここから-->
//--大分ここまで-->
//--宮崎ここから-->
//--宮崎ここまで-->
//--鹿児島ここから-->
//--鹿児島ここまで-->
//--沖縄ここから-->
$area_list['%.ryudai.jp'] = '沖縄県';
//--沖縄ここまで-->
//--.ac.jpここから-->
//--.ac.jpここまで-->

// CATV
//北海道
//JCOM 札幌 不明
//テレビ寿都放送 不明
$area_list['%.potato.ne.jp'] = '北海道';
$area_list['%.lan-do.ne.jp'] = '北海道';
$area_list['%.ncv.ne.jp'] = '北海道';
//苫小牧ケーブルテレビ TikiTikiインターネット
//大滝ケーブルテレビ インターネットなし
$area_list['%.octv.ne.jp'] = '北海道';
$area_list['%.vill.nishiokoppe.hokkaido.jp'] = '北海道';
$area_list['%.kctvnet.ne.jp'] = '北海道';
$area_list['210.229.186.%'] = '北海道';

//青森
$area_list['%.actv.ne.jp'] = '青森県';
$area_list['%.htv-net.ne.jp'] = '青森県';
//風間浦村営共聴システム インターネットなし
//田子町ケーブルテレビジョン
$area_list['%.mctvnet.ne.jp'] = '青森県';
//弘前ケーブルテレビ

//岩手県
$area_list['%.icn-net.ne.jp'] = '岩手県';
$area_list['%.ictnet.ne.jp'] = '岩手県';
$area_list['%.waiwai-net.ne.jp'] = '岩手県';
$area_list['%.ginga-net.ne.jp'] = '岩手県';
$area_list['%.tonotv.com'] = '岩手県';
//花巻ケーブルテレビ TikiTikiインターネット
$area_list['%.catv-mic.ne.jp'] = '岩手県';
$area_list['%.wtv-net.jp'] = '岩手県';

//秋田
$area_list['%.cna.ne.jp'] = '秋田県';
$area_list['%.ont.ne.jp'] = '秋田県';
//大館ケーブルテレビ TikiTikiインターネット

//宮城
$area_list['%.k-macs.ne.jp'] = '宮城県';
$area_list['%.c-marinet.ne.jp'] = '宮城県';
$area_list['%.cat-v.ne.jp'] = '宮城県';
$area_list['%.mni.ne.jp'] = '宮城県';
//利府・青山CATV管理組合';
//大崎ケーブルテレビ TikiTikiインターネット

//山形
$area_list['%.catvy.ne.jp'] = '山形県';
$area_list['%.omn.ne.jp'] = '山形県';

//福島
$area_list['%.nct.ne.jp'] = '福島県';
//たじまケーブルテレビジョン

//茨城
$area_list['%.accsnet.ne.jp'] = '茨城県';
$area_list['%.rcctv.jp'] = '茨城県/埼玉県';
$area_list['%.speedway.ne.jp'] = '茨城県';
$area_list['%.jway.ne.jp'] = '茨城県';

//栃木
$area_list['%.ucatv.ne.jp'] = '栃木県';
$area_list['%.tvoyama.ne.jp'] = '栃木県';
$area_list['%.bc9.ne.jp'] = '栃木県';
$area_list['%.sctv.jp'] = '栃木県';
$area_list['%.watv.ne.jp'] = '栃木県';
$area_list['%.cc9.ne.jp'] = '栃木県'; //群馬県館林
$area_list['%.i-berry.ne.jp'] = '栃木県';
//茂木町ケーブルテレビ
//粟野ケーブルテレビ
//塩原テレビ共同聴視事業協同組合
//大日光ケーブルテレビ

//群馬
$area_list['%.otv.ne.jp'] = '群馬県';
$area_list['%.ktv.ne.jp'] = '群馬県';
$area_list['%.nanmoku.ne.jp'] = '群馬県';
$area_list['%.uenomura.ne.jp'] = '群馬県';
//草津テレビ
//宝町テレビ共同受信施設組合
//嬬恋ケーブルビジョン
//東吾妻町あづまケーブルテレビ
$area_list['%.kannamachi.jp'] = '群馬県';
//六合村健康管理等情報連絡施設

//埼玉
$area_list['%.cablenet.ne.jp'] = '埼玉県';
$area_list['%.tcat.ne.jp'] = '埼玉県';
$area_list['%.kcv-net.ne.jp'] = '埼玉県';
$area_list['%.hctv.ne.jp'] = '埼玉県';
//飯能ケーブルテレビ
$area_list['%.warabi.ne.jp'] = '埼玉県';
$area_list['%.ccn.ne.jp'] = '埼玉県';
//メディアッティ東上 （メディアッティ東上）<J:COM系>
//本庄ケーブルテレビ
$area_list['%.s-cat.ne.jp'] = '埼玉県';
//行田ケーブルテレビ
$area_list['%.tvkumagaya.ne.jp'] = '埼玉県';
$area_list['%.ictv.ne.jp'] = '埼玉県'; //東京都
//リバーシティ・ケーブルテレビ
$area_list['%.chichibu.ne.jp'] = '埼玉県';

//千葉
$area_list['%.icnet.ne.jp'] = '千葉県';
$area_list['%.catv296.ne.jp'] = '千葉県';
$area_list['%.icntv.ne.jp'] = '千葉県';
$area_list['%.fnnr.j-cnet.jp'] = '千葉県';
$area_list['%.fnnr-static.j-cnet.jp'] = '千葉県';
$area_list['%.seaple.ne.jp'] = '千葉県';
$area_list['%.cnc.jp'] = '千葉県';
$area_list['%.nctv.co.jp'] = '千葉県';
$area_list['%.rurbannet.ne.jp'] = '千葉県';
$area_list['%.catv9.ne.jp'] = '千葉県';
$area_list['%.koalanet.ne.jp'] = '千葉県';
$area_list['%.snu.ne.jp'] = '埼玉県';

//東京
$area_list['%.tcv.jp'] = '東京都'; //神奈川・埼玉
$area_list['%.ohta.j-cnet.jp'] = '東京都';
$area_list['%.ohta-static.j-cnet.jp'] = '東京都';
$area_list['%.kakt.j-cnet.jp'] = '東京都';

$area_list['%.baynet.ne.jp'] = '東京都';
$area_list['%.cts.ne.jp'] = '東京都';
$area_list['%.rosenet.ne.jp'] = '東京都';

$area_list['%.tctv.ne.jp'] = '東京都';
$area_list['%.ocv.ne.jp'] = '東京都';
$area_list['%.toshima.ne.jp'] = '東京都';
$area_list['%.ctn.co.jp'] = '東京都';
$area_list['%.nkno.j-cnet.jp'] = '東京都';

$area_list['%.kitanet.ne.jp'] = '東京都';
$area_list['%.adachi.ne.jp'] = '東京都';
$area_list['%.tcn-catv.ne.jp'] = '東京都';
$area_list['%.mmcatv.co.jp'] = '東京都';
$area_list['%.parkcity.ne.jp'] = '東京都';

$area_list['%.mytv.co.jp'] = '東京都';
$area_list['%.ttv.ne.jp'] = '東京都';
$area_list['%.t-net.ne.jp'] = '東京都';
$area_list['%.hinocatv.ne.jp'] = '東京都';
$area_list['%.htoj.j-cnet.jp'] = '東京都';
$area_list['%.htmnet.ne.jp'] = '東京都';
$area_list['%.m-net.ne.jp'] = '東京都';

$area_list['%.tepco.hi-ho.ne.jp'] = '東京都';

//神奈川
$area_list['%.kamakuranet.ne.jp'] = '神奈川県';

$area_list['%.ctktv.ne.jp'] = '神奈川県';
$area_list['%.catv-yokohama.ne.jp'] = '神奈川県';
$area_list['%.odwr.j-cnet.jp'] = '神奈川県';
$area_list['%.kamakuranet.ne.jp'] = '神奈川県';

$area_list['%.c3-net.ne.jp'] = '神奈川県';
$area_list['%.icc.ne.jp'] = '神奈川県';
$area_list['%.ttmy.ne.jp'] = '神奈川県';

$area_list['%.scn-net.ne.jp'] = '神奈川県';
$area_list['%.netyou.jp'] = '神奈川県';
//ケーブルシティ横浜 (CCY)
$area_list['%.ayu.ne.jp'] = '神奈川県';
$area_list['%.tmtv.ne.jp'] = '神奈川県';

//山梨
$area_list['%.nns.ne.jp'] = '山梨県';
//ケーブルネットワーク大月（大月テレビ利用者組合・大月ケーブルビジョン）
$area_list['%.cvk.ne.jp'] = '山梨県';
$area_list['%.kawaguchiko.ne.jp'] = '山梨県';
$area_list['%.lcnet.jp'] = '山梨県';
$area_list['%.fgo.jp'] = '山梨県';
$area_list['%.nus.ne.jp'] = '山梨県';
$area_list['%.fruits.ne.jp'] = '山梨県';
$area_list['%.kcnet.ne.jp'] = '山梨県';
$area_list['61.117.150.%'] = '山梨県';

//笛吹きらめきテレビ（IFT）
$area_list['%.katsunuma.ne.jp'] = '山梨県';
//都留市テレビ利用者組合
//いちのみやふれあいテレビ（笛吹市一宮有線テレビ）(IFT)
//小淵沢町農村多元情報システム (にこにこすていしょん)
//北富士有線テレビ放送
//ケーブルテレビ富士
//富士川シーエーティーヴィ
//峡南CATV
//富沢町テレビ共聴組合
//上野原ブロードバンドコミュニケーションズ
$area_list['%.ubcnet.jp'] = '山梨県';

//新潟県
//エヌ・シィ・ティ (NCT) <CCJ系>
$area_list['%.nct9.ne.jp'] = '新潟県';
$area_list['%.ecatv.ne.jp'] = '新潟県';
$area_list['%.tlp.ne.jp'] = '新潟県';

$area_list['%.e-sadonet.tv'] = '新潟県';
$area_list['%.joetsu.ne.jp'] = '新潟県';
//コミュニケーションネットワーク佐渡 (CNS)
//刈羽村ケーブルテレビ
$area_list['%.nou.ne.jp'] = '新潟県';
$area_list['%.uct.ne.jp'] = '新潟県';
//上越市三和ケーブルテレビ
//上越市吉川ケーブルテレビ
//上越市安塚ケーブルテレビ
//朝日村総合情報ネットワークシステム（村上市）
//山北町情報通信施設（村上市）

//長野
$area_list['%.avis.ne.jp'] = '長野県';
$area_list['%.iiyama-catv.ne.jp'] = '長野県';
//小谷村ケーブルテレビ
//ふう太ネット木島平　(木島平村)
$area_list['%.kijimadaira.jp'] = '長野県';
//テレビ菜の花　(野沢温泉村)
//中野市豊田情報センター (中野市豊田地区　TCV)
//テレビ北信ケーブルビジョン (中野市（豊田地区を除く）・山ノ内町　THV)
//須高ケーブルテレビ (須坂市・小布施町　STV)
//インフォメーション・ネットワーク・コミュニティ（長野市（戸隠地区・鬼無里地区を除く）　INC長野ケーブルテレビ）
//戸隠ケーブルテレビ (長野市戸隠地区)
//鬼無里ケーブルテレビ　(長野市鬼無里地区)
//コミュニティーネットワーク信州新町 (CNS)
//信州ケーブルテレビジョン（千曲市　ケーブルネット千曲）
//上田ケーブルビジョン (上田市（丸子地区・武石地区を除く）・東御市（北御牧地区を除く）・青木村・坂城町　UCV)
$area_list['%.ueda.ne.jp'] = '長野県';
$area_list['%.marukotv.jp'] = '長野県';
//とうみケーブルテレビ (東御市北御牧地区　TCT)
$area_list['%.ctk23.ne.jp'] = '長野県';
$area_list['%.kokuyou.ne.jp'] = '長野県';
//佐久ケーブルテレビ (佐久市（望月地区の一部を除く）SCT)
$area_list['%.sakunet.ne.jp'] = '長野県';
//佐久高原ケーブルビジョン (佐久穂町　SCV)
//協和ビジョン (軽井沢町　KVC)
//西軽井沢ケーブルテレビ　(御代田町)
$area_list['%.lcv.ne.jp'] = '長野県';
$area_list['%.tvm.ne.jp'] = '長野県';
$area_list['%.anc-tv.ne.jp'] = '長野県';
//飯田ケーブルテレビ (ICTV)
$area_list['%.inacatv.ne.jp'] = '長野県';
//アルプスケーブルビジョン (ACV)
$area_list['%.cek.ne.jp'] = '長野県';
$area_list['%.takamori.ne.jp'] = '長野県';
$area_list['%.ch-you.ne.jp'] = '長野県';
//蓼科ケーブルビジョン (立科町（白樺湖畔を除く）・佐久市望月地区の一部　TCV)
//朝日村有線テレビ (AYT)
//とよおか放送ネットワーク (THN)
//ふれあいネットワーク長谷 (CNH)
//ケーブルテレビミアサ (CTM)
//コミュニケーションネットワーク阿南
//木曽広域ケーブルテレビ
//阿智村情報化事業サービス
//生坂村コミュニケーションネットワーク (ICN)
//泰阜村コミュニケーションネットワーク (YCN)
$area_list['%.miasa.ne.jp'] = '長野県';

//岐阜
$area_list['%.ogaki-tv.ne.jp'] = '岐阜県';
//おりべネットワーク
//シーシーエヌ
$area_list['%.ctk.ne.jp'] = '岐阜県';
$area_list['yo%.ccnet.ne.jp'] = '岐阜県';
$area_list['mo%.ccnet.ne.jp'] = '岐阜県';
$area_list['ai%.ccnet.ne.jp'] = '愛知県';
$area_list['%.gujo-tv.ne.jp'] = '岐阜県';
$area_list['61.87.124.%'] = '岐阜県';
$area_list['61.87.125.%'] = '岐阜県';
$area_list['%.hidatakayama.ne.jp'] = '岐阜県';
$area_list['%.seiryu.ne.jp'] = '岐阜県';
$area_list['%.ccy.ne.jp'] = '岐阜県';
$area_list['%.kawaue.jp'] = '岐阜県';
$area_list['%.50913.ne.jp'] = '岐阜県';
$area_list['%.hida-catv.jp'] = '岐阜県';
$area_list['%.gujocity.net'] = '岐阜県';
//アミックスコム
//恵那市山岡ケーブルテレビネットワーク
//恵那市串原ケーブルテレビネットワーク
$area_list['%.amixcom.jp'] = '岐阜県';
//さかうち田園ネット
//シーテック（CCNet養老局）
$area_list['%.mirai.ne.jp'] = '岐阜県';

//静岡
$area_list['%.i-younet.ne.jp'] = '静岡県';
$area_list['%.hctnet.ne.jp'] = '静岡県';
//ビック東海（@TCOM）<ビック東海系>
$area_list['%.tokai.or.jp'] = '静岡県';
$area_list['%.tnc.ne.jp'] = '静岡県';
$area_list['%.thn.ne.jp'] = '静岡県';
$area_list['%.maotv.ne.jp'] = '静岡県';
$area_list['%.s-cnet.ne.jp'] = '静岡県';
$area_list['%.gotemba.ne.jp'] = '静岡県';
//東伊豆有線テレビ放送 (HI-CAT)
//下田有線テレビ放送 (SHK)
//東豆有線
//伊東テレビクラブ
//小林テレビ設備
//シオヤ
//伊豆太陽農業共同組合
//小山町テレビ共聴組合

//愛知県
$area_list['%.orihime.ne.jp'] = '愛知県';
$area_list['%.c-d-k.ne.jp'] = '愛知県';
$area_list['%.toptower.ne.jp'] = '愛知県';
$area_list['%.katch.ne.jp'] = '愛知県';
$area_list['%.gctv.ne.jp'] = '愛知県';
$area_list['%.cac-net.ne.jp'] = '愛知県';
$area_list['%.starcat.ne.jp'] = '愛知県';
$area_list['%.tac-net.ne.jp'] = '愛知県';
$area_list['%.medias.ne.jp'] = '愛知県';
$area_list['%.ccnw.ne.jp'] = '愛知県';
$area_list['%.tctvnet.ne.jp'] = '愛知県';
$area_list['%.kctv.ne.jp'] = '愛知県';
$area_list['%.tees.ne.jp'] = '愛知県';
$area_list['%.clovernet.ne.jp'] = '愛知県';
$area_list['%.catvmics.ne.jp'] = '愛知県';
//ひまわりネットワーク
//三河湾ネットワーク
//名古屋ケーブルビジョン
//シーテック（CCNet豊川局）
$area_list['%.ccnet-ai.ne.jp'] = '愛知県';

//三重
$area_list['%.amigo.ne.jp'] = '三重県';
$area_list['%.amigo2.ne.jp'] = '三重県';
$area_list['%.nava21.ne.jp'] = '三重県';
$area_list['%.asint.jp'] = '三重県';
$area_list['%.assp.jp'] = '三重県';
$area_list['%.ict.ne.jp'] = '三重県';
$area_list['%.mecha.ne.jp'] = '三重県';
$area_list['%.cty-net.ne.jp'] = '三重県';
$area_list['%.intsurf.ne.jp'] = '三重県';
$area_list['%.mctv.ne.jp'] = '三重県';
$area_list['%.ccnetmie.ne.jp'] = '三重県';
$area_list['%.ciaotv.ne.jp'] = '三重県';

//富山
$area_list['%.ctt.ne.jp'] = '富山県';
$area_list['%.cty8.com'] = '富山県';
$area_list['%.canet.ne.jp'] = '富山県';
$area_list['%.tcnet.ne.jp'] = '富山県';
$area_list['%.tst.ne.jp'] = '富山県';
$area_list['%.nice-tv.jp'] = '富山県';
$area_list['%.cnh.ne.jp'] = '富山県';
$area_list['%.milare-tv.ne.jp'] = '富山県';
$area_list['%.knei.jp'] = '富山県';

//石川県
$area_list['%.kagacable.ne.jp'] = '石川県';
$area_list['%.spacelan.ne.jp'] = '石川県';
$area_list['%.scnet.tv'] = '石川県';
$area_list['%.notojima.jp'] = '石川県';
$area_list['%.tvk.ne.jp'] = '石川県';
$area_list['%.asagaotv.ne.jp'] = '石川県';
//柳田ふれあいNet
$area_list['%.kaga-tv.com'] = '石川県';
//北陸アイティエス
//輪島市ケーブルテレビ

//福井
//和泉ケーブルネットワーク
//大野ケーブルテレビ
//ケーブルテレビ若狭小浜（チャンネルO）
//おおいテレビ (OH-CATV)
//ケーブルネットワークかみなか (CNK)
//高浜町ケーブルテレビ
$area_list['%.wakasa-takahama.tv'] = '福井県';
//こしの国広域事務組合
$area_list['%.vipa.ne.jp'] = '福井県';
$area_list['%.ttn.ne.jp'] = '福井県';
$area_list['%.fctv.ne.jp'] = '福井県';
$area_list['%.sctv.ne.jp'] = '福井県';
$area_list['%.mmnet-ai.ne.jp'] = '福井県';
//南越前町ケーブルテレビ
$area_list['%.rcn.ne.jp'] = '福井県';
$area_list['%.mitene.or.jp'] = '福井県';

//大阪
//ジェイコムウエスト（J:COM大阪、J:COMかわち、J:COMりんくう、J:COM堺、J:COM南大阪、J:COM和泉・泉大津、　J:COM 北摂、J:COM 大阪セントラル、J:COM 東大阪、J:COM 北河内、J:COM 吹田、J:COM 高槻、J:COM 豊中・池田）<J:COM系>
//近鉄ケーブルネットワーク (KCN)<KCN系>
//テレビ岸和田<KCN系
$area_list['%.sensyu.ne.jp'] = '大阪府';
//京阪神ケーブルビジョン
//ケイ・キャット (K-CAT)
//ケイ・キャット eo光テレビ※1
//KCN eo光テレビ※1

//滋賀
$area_list['%.cable-net.ne.jp'] = '滋賀県';
//ZTV
//甲賀ケーブルネットワーク (KCN)ZAQかな
$area_list['%.e-omi.ne.jp'] = '滋賀県';
$area_list['%.hottv.ne.jp'] = '滋賀県';
//木之本町ケーブルテレビ
//K-CAT eo光テレビ※1

//京都
//KCN京都<KCN系>
$area_list['%.kinet-tv.ne.jp'] = '京都府';
//京都ケーブルコミュニケーションズ（みやびじょん）<J:COM系>
//ケイ・キャット
//洛西ケーブルビジョン
$area_list['%.city.nantan.kyoto.jp'] = '京都府';
$area_list['%.town.kyotamba.kyoto.jp'] = '京都府';
$area_list['%.kyt-net.ne.jp'] = '京都府';
//全関西ケーブルテレビジョン京丹後局（2009年12月サービス開始予定）
//日本ケーブルビジョン
//京阪神ケーブルビジョン
//K-CAT eo光テレビ※1
$area_list['%.shinet.ne.jp'] = '京都府';

//ここまで
//兵庫
//ジェイコムウエスト（J:COM 宝塚・川西）<J:COM系>
//ケーブルネット神戸芦屋（J:COM 神戸・芦屋、J:COM 神戸・三木（旧ケーブルテレビ神戸））<J:COM系>
$area_list['%.kobe-catv.ne.jp'] = '兵庫県';
//明石ケーブルテレビ (ACTV135) zaq?
$area_list['%.sumoto.gr.jp'] = '兵庫県';
$area_list['%.sansan-net.jp'] = '兵庫県';
$area_list['%.banban.jp'] = '兵庫県';
//京阪神ケーブルビジョン zaq?
$area_list['%.winknet.ne.jp'] = '兵庫県';
$area_list['%.cin.ne.jp'] = '兵庫県';
$area_list['%.yabu-catv.or.jp'] = '兵庫県';
$area_list['%.asago-net.jp'] = '兵庫県';
$area_list['%.yumenet.tv'] = '兵庫県';
//加東ケーブルビジョン（KCV）
//（「加東市滝野ケーブルコミュニケーション（TCC）」と「テレネットやしろ（TNY）」は、加東ケーブルビジョンに統合された。）
$area_list['%.tccnet.tv'] = '兵庫県';

//たかテレビ（多可町）
$area_list['%.kamitv.ne.jp'] = '兵庫県';
$area_list['%.kcni.ne.jp'] = '兵庫県';
$area_list['%.yumetv.jp'] = '兵庫県';

//K-CAT eo光テレビ※1

//奈良
//近鉄ケーブルネットワーク (KCN)<KCN系>
//こまどりケーブル<KCN系>
$area_list['%.komadori.ne.jp'] = '奈良県';

//和歌山
//ジェイコムウエスト（J:COM 和歌山）<J:COM系>
//全関西ケーブルテレビジョン紀の川放送局（ACTV 紀の川局）
//ZTV
//K-CAT eo光テレビ※1

//鳥取
//中海テレビ放送
$area_list['%.chukai.ne.jp'] = '鳥取県';
//鳥取中央有線放送 (TCC)
$area_list['%.torichu.ne.jp'] = '鳥取県';
$area_list['%.hcvnet.jp'] = '鳥取県';
$area_list['%.tcbnet.ne.jp'] = '鳥取県';
//.yurihama.jp
//.e-hokuei.net
$area_list['%.inabapyonpyon.net'] = '鳥取県';
//日本海ケーブルネットワーク (NCN)
$area_list['%.oninosato.jp'] = '鳥取県';

//島根
$area_list['%.icv.ne.jp'] = '島根県';
$area_list['%.ginzan-tv.ne.jp'] = '島根県';
$area_list['%.iwamicatv.jp'] = '島根県';
$area_list['%.i-yume.ne.jp'] = '島根県';
$area_list['%.kkm.ne.jp'] = '島根県';
$area_list['%.okuizumo.ne.jp'] = '島根県';
//鹿島ケーブルビジョン（鹿島すまいるネット）

$area_list['%.mable.ne.jp'] = '島根県';
$area_list['%.sun-net.jp'] = '島根県';
$area_list['%.hit-5.net'] = '島根県';
$area_list['%.herecall.jp'] = '島根県';
$area_list['%.ohtv.ne.jp'] = '島根県';

//岡山
$area_list['%.ibara.ne.jp'] = '岡山県';
$area_list['%.oninet.ne.jp'] = '岡山県';
$area_list['%.kcv.ne.jp'] = '岡山県';
$area_list['%.kibi.ne.jp'] = '岡山県';
$area_list['%.kct.ne.jp'] = '岡山県';
$area_list['%.kct.ad.jp'] = '岡山県';
$area_list['%.cnknet.jp'] = '岡山県';
$area_list['%.tamatele.ne.jp'] = '岡山県';
$area_list['%.tvt.ne.jp'] = '岡山県';
$area_list['%.nariwa.ne.jp'] = '岡山県';
$area_list['%.hinase.ne.jp'] = '岡山県';
$area_list['%.yct.ne.jp'] = '岡山県';
//ACT新見放送
//鏡野町有線テレビ
//真庭市CATV
//みさきネット
//美作市ケーブルテレビ
//あわくら光ネット

//広島
//井原放送(岡山）
//オプティキャスト
$area_list['%.bbbn.jp'] = '広島県';
//きたひろネット（北広島町）
$area_list['%.ccjnet.ne.jp'] = '広島県';
$area_list['%.kamon.ne.jp'] = '広島県';
$area_list['%.hicat.ne.jp'] = '広島県';
$area_list['%.fch.ne.jp'] = '広島県';
$area_list['%.cc22.ne.jp'] = '広島県';
$area_list['%.ccv.ne.jp'] = '広島県';
$area_list['%.mcat.ne.jp'] = '広島県';
//せらケーブルねっと
//三次ケーブルビジョン (Pionet)
$area_list['%.pionet.ne.jp'] = '広島県';
//安芸矢野ニュータウンCATV組合
//仁保南CATV施設管理組合
//A.CITYヒルズ&タワーズ管理組合
//ケーブルとよはま
$area_list['%.hbs.ne.jp'] = '広島県';

//山口
//ケーブルネット下関（J:COM下関）<J:COM系>
//アイ・キャン
$area_list['%.icn-tv.ne.jp'] = '山口県';
//Kビジョン
$area_list['%.kvision.ne.jp'] = '山口県';
//シティーケーブル周南
$area_list['%.ccsnet.ne.jp'] = '山口県';
//萩ケーブルネットワーク
$area_list['%.haginet.ne.jp'] = '山口県';
//ほっちゃテレビ（長門市ケーブルテレビ）
$area_list['%.hot-cha.tv'] = '山口県';
//山口ケーブルビジョン (C-able)
$area_list['%.c-able.ne.jp'] = '山口県';

//美祢市有線テレビ
$area_list['%.m-netbb.com'] = '山口県';
//岐北地区テレビ共同受信施設組合
$area_list['%.suo-cable.net'] = '山口県';

//徳島
$area_list['%.awaikeda.net'] = '山口県';
//石井町有線放送農業協同組合（石井CATV）
//エーアイテレビ
$area_list['%.tcn.ne.jp'] = '徳島県';
$area_list['%.kbctv.ne.jp'] = '徳島県';
$area_list['%.tv-naruto.ne.jp'] = '徳島県';
//徳島県南メディアネットワーク (MTCTV)
$area_list['%.jctv.ne.jp'] = '徳島県';
$area_list['%.whk.ne.jp'] = '徳島県';
$area_list['%.e-awa.net'] = '徳島県';
//ひのき (キューテレビ)
//テレビ阿波
$area_list['%.njctv.ne.jp'] = '徳島県';
//ケーブルテレビあなん
//上板町有線テレビ
//ケーブルネットおえ
//ピカラ光てれび※1
//スカパー!光※1

//香川
$area_list['%.kbn.ne.jp'] = '香川県';
//ケーブルメディア四国（高松ケーブルテレビ）(CMS)
$area_list['%.sanuki.ne.jp'] = '香川県';
//中讃ケーブルビジョン（中讃テレビ）(CVC)
$area_list['%.mcbnet.ne.jp'] = '香川県';
//高松市塩江ケーブルネットワーク
//苗羽坂手テレビ共聴施設組合

//愛媛
$area_list['%.icknet.ne.jp'] = '愛媛県';
//宇和島ケーブルテレビ (UCAT)
//愛媛CATV※3
$area_list['%.e-catv.ne.jp'] = '愛媛県';
$area_list['%.cnw.ne.jp'] = '愛媛県';
$area_list['%.hearts.ne.jp'] = '愛媛県';
//八西地域総合情報センター（八西CATV）
//野村ケーブルテレビ（かぼちゃ）
//西予CATV
$area_list['%.cosmostv.jp'] = '愛媛県';
$area_list['%.namikata.ne.jp'] = '愛媛県';

//高知
$area_list['%.kcb-net.ne.jp'] = '高知県';
$area_list['%.gallery.ne.jp'] = '高知県';
$area_list['%.scatv.ne.jp'] = '高知県';
//香南施設農業協同組合（香南ケーブルテレビ）
$area_list['%.shimanto.tv'] = '高知県';
//甲浦テレビ共同聴視施設組合
//土佐有線テレビ施設組合
//梼原あんしん光ネット

//四国
$area_list['%.ai%.catvnet.ne.jp'] = '徳島県';
$area_list['%.ihk%.catvnet.ne.jp'] = '徳島県';
$area_list['%.hic%.catvnet.ne.jp'] = '愛媛県';
$area_list['%.catvnet.ne.jp'] = '四国:徳島/香川/愛媛/高知';

//福岡
//ジェイコム福岡（J:COM福岡）<J:COM系>（旧・福岡ケーブルネットワーク）
//ケーブルビジョン21（福岡ケーブルネットワークに吸収合併された）<J:COM系>
$area_list['%.fcv.ne.jp'] = '福岡県';
$area_list['%.csf.ne.jp'] = '福岡県';
//ジェイコム北九州（J:COM北九州）<J:COM系>
//北九州ケーブルビジョン
$area_list['%.kumin.ne.jp'] = '福岡県/佐賀県';
$area_list['%.cv-net.jp'] = '福岡県';
//ケーブルネットワーク桂川
$area_list['%.ymdtv.jp'] = '福岡県';

//佐賀
//伊万里ケーブルテレビジョン
$area_list['%.hachigamenet.ne.jp'] = '佐賀県';
//佐賀シティビジョン（ぶんぶんテレビ）
$area_list['%.bunbun.ne.jp'] = '佐賀県';
$area_list['%.aritanet.ne.jp'] = '佐賀県';
$area_list['%.people-i.ne.jp'] = '佐賀県';
$area_list['%.cableone.ne.jp'] = '佐賀県';
$area_list['%.chun2.ne.jp'] = '佐賀県';
$area_list['%.taku.ne.jp'] = '佐賀県';
$area_list['%.ktknet.ne.jp'] = '佐賀県';
$area_list['%.hagakure.ne.jp'] = '佐賀県';
$area_list['%.asunet.ne.jp'] = '佐賀県';
$area_list['%.netfour.ne.jp'] = '佐賀県';
//唐津市有線テレビジョン
$area_list['%.hhc.ne.jp'] = '佐賀県';
//多久市テレビ共同聴視組合
//ふじ有線テレビ

//長崎
$area_list['%.tvs12.jp'] = '長崎県';
$area_list['%.shimabara.jp'] = '長崎県';
$area_list['%.cncm.ne.jp'] = '長崎県';
$area_list['%.icv-net.ne.jp'] = '長崎県';
//諫早市小長井地域有線テレビジョン施設
//オクト・パルス
$area_list['%.octp-net.ne.jp'] = '長崎県';
$area_list['%.himawarinet.ne.jp'] = '長崎県';
//波佐見ケーブルテレビ
$area_list['%.fctv-net.jp'] = '長崎県';
//美津島町有線テレビ (MYT) （対馬市市営）
$area_list['203.215.56.%'] = '長崎県';
//上対馬町比田勝テレビ協同聴視組合（対馬市）
//厳原有線テレビ利用共同組合（対馬市）
//西彼ケーブルネットワーク
//東彼ケーブルテレビ

//熊本
$area_list['%.kcn-tv.ne.jp'] = '熊本県';
$area_list['%.acn-tv.ne.jp'] = '熊本県';
$area_list['%.toyo-tv.jp'] = '熊本県';
$area_list['%.sakamoto-catv.jp'] = '熊本県';
//みなみチャンネル

//大分
//大分ケーブルテレコム (OCT)
$area_list['%.oct-net.jp'] = '大分県';
$area_list['%.ocn-catv.ne.jp'] = '大分県';
//佐賀関テレビ
//北大ケーブル情報センター
//姫島村ケーブルテレビ
//国東市ケーブルテレビセンター
//豊後高田市ケーブルネットワーク施設
$area_list['%.kdt.ne.jp'] = '大分県';
//CTBメディア (CTB)
$area_list['%.ctb.ne.jp'] = '大分県';
//KCVコミュニケーションズ (KCV)
$area_list['%.kcv.jp'] = '大分県';
//日田市大山情報センター
//玖珠テレビ
//豊後大野市おおのケーブルテレビ
//臼杵市ケーブルネットワーク (U-net)
$area_list['%.oct-net.ne.jp'] = '大分県';
//ケーブルテレビ佐伯 (CTS)
$area_list['%.cts-net.ne.jp'] = '大分県';
//上浦ケーブルテレビ※CTS経由(テレビ・インターネット)
//宇目ケーブルテレビ※CTS経由(テレビ・インターネット)
//ケーブルテレビかまえ※CTS経由(テレビ・インターネット)
//直川ケーブルテレビ※CTS経由(テレビ・インターネット)
//本匠ケーブルテレビ（ホタル）※インターネットのみCTS経由
//弥生ケーブルテレビ※インターネットのみCTS経由
//ケーブルテレビつるみ※インターネットのみCTS経由
//米水津ケーブルテレビ※インターネットのみCTS経由

//宮崎
//ケーブルメディアワイワイ（わぃWaiTV）
$area_list['%.wainet.ne.jp'] = '宮崎県';
//宮崎ケーブルテレビ (MCN)
$area_list['%.miyazaki-catv.ne.jp'] = '宮崎県';
//きららびじょん

//鹿児島
$area_list['%.nils.ne.jp'] = '鹿児島県';
//ビィーティーヴィーケーブルテレビ（BTVケーブルテレビ 都城局・鹿児島局）
$area_list['%.mct.ne.jp'] = '鹿児島県';
//西之表テレビ共同視聴施設組合
//瀬戸内ケーブルテレビ
//屋久町テレビ共同受信施設組合
//美浜テレビ共同アンテナ受信施設組合
//天城町ユイの里テレビ <AYT>
//和泊町有線テレビ
//姶良共同受信組合
//南部有線テレビ放送
//鹿児島光テレビ （BBIQ光テレビ）※1

//沖縄
//沖縄ケーブルネットワーク (OCN)
$area_list['%.nirai.ne.jp'] = '沖縄県';
$area_list['%.ictweb.ne.jp'] = '沖縄県';
$area_list['%.miyako-net.ne.jp'] = '沖縄県';
$area_list['%.miyako-ma.jp'] = '沖縄県'; //宮古島

//複数地域
$area_list['%.kcn.ne.jp'] = '大阪府/奈良県/京都府'; //大阪　奈良　京都の一部

$area_list['%.bai.ne.jp'] = '兵庫県/大阪府'; //大阪　兵庫の一部

$area_list['%.ztv.ne.jp'] = '三重県/滋賀県/和歌山県';

$area_list['203-211-35-%.btvm.ne.jp'] = '鹿児島県';
$area_list['203-211-42-%.btvm.ne.jp'] = '宮崎県';
$area_list['%.btvm.ne.jp'] = '鹿児島県/宮崎県';

$area_list['%.zaq.ne.jp'] = '大阪府/兵庫県'; //大阪　兵庫の一部

$area_list['hemutite.aitai.ne.jp'] = '愛知県';
$area_list['%.aitai.ne.jp'] = '愛知県/岐阜県';

$area_list['%.tpch01.itscom.jp'] = '東京都';
$area_list['%.catv01.itscom.jp'] = '神奈川県';
$area_list['%.catv02.itscom.jp'] = '神奈川県';
$area_list['%.itscom.jp'] = '東京都/神奈川県';
$area_list['%.246.ne.jp'] = '東京都/神奈川県';

$area_list['%.netwave.or.jp'] = '四国:徳島/香川/愛媛/高知';

//エネルギアコミニュケーションズ';
$area_list['%.megaegg.jp'] = '中国:鳥取/島根/岡山/広島/山口'; //中国
//ぴから
$area_list['%.pikara.ne.jp'] = '四国:徳島/香川/愛媛/高知';//四国

//コミュファ光

//IP

//終了

//-------------------------------------------
//地域判定（大まか）
//-------------------------------------------
$area_list['%hokkaido%'] = '北海道';
$area_list['%yamanashi%'] = '山梨県';
$area_list['%yamaguchi%'] = '山口県';
$area_list['%yamagata%'] = '山形県';
$area_list['%wakayama%'] = '和歌山県';
$area_list['%toyama%'] = '富山県';
$area_list['%tottori%'] = '鳥取県';
$area_list['%tokyo%'] = '東京都';
$area_list['%tokushima%'] = '徳島県';
$area_list['%tochigi%'] = '栃木県';
$area_list['%shizuoka%'] = '静岡県';
$area_list['%shimane%'] = '島根県';
$area_list['%shiga%'] = '滋賀県';
$area_list['%saitama%'] = '埼玉県';
$area_list['%.saga-%'] = '佐賀県';
$area_list['%osaka%'] = '大阪府';
$area_list['%.oita%'] = '大分県';
$area_list['%okinawa%'] = '沖縄県';
$area_list['%okayama%'] = '岡山県';
$area_list['%kumamoto%'] = '熊本県';
$area_list['%niigata%'] = '新潟県';
$area_list['%nara%'] = '奈良県';
$area_list['%nagasaki%'] = '長崎県';
$area_list['%nagano%'] = '長野県';
$area_list['%miyazaki%'] = '宮崎県';
$area_list['%miyagi%'] = '宮城県';
$area_list['%.mie.%'] = '三重県';
$area_list['%.mie-%'] = '三重県';
$area_list['%-mie.%'] = '三重県';
$area_list['%kyoto%'] = '京都府';
$area_list['%kochi%'] = '高知県';
$area_list['%kanagawa%'] = '神奈川県';
$area_list['%kagoshima%'] = '鹿児島県';
$area_list['%kagawa%'] = '香川県';
$area_list['%iwate%'] = '岩手県';
$area_list['%ishikawa%'] = '石川県';
$area_list['%ibaraki.osaka%'] = '大阪府';
$area_list['%ibaraki%'] = '茨城県';
$area_list['%hyogo%'] = '兵庫県';
$area_list['%hiroshima%'] = '広島県';
$area_list['%gunma%'] = '群馬県';
$area_list['%gifu%'] = '岐阜県';
$area_list['%fukushima%'] = '福島県';
$area_list['%fukuoka%'] = '福岡県';
$area_list['%fukui%'] = '福井県';
$area_list['%ehime%'] = '愛媛県';
$area_list['%chiba%'] = '千葉県';
$area_list['%aomori%'] = '青森県';
$area_list['%akita%'] = '秋田県';
$area_list['%aichi%'] = '愛知県';

$area_list['%yonezawa.%'] = '山形県';
$area_list['%yonago.%'] = '鳥取県';
$area_list['%yokosuka.%'] = '神奈川県';
$area_list['%yokohama.%'] = '神奈川県';
$area_list['%yokkaichi.%'] = '三重県';
$area_list['%ymgc.%'] = '山口県';
$area_list['%ykhm.%'] = '神奈川県';
$area_list['%yao.osaka.%'] = '大阪府';
$area_list['%yamatotakada.%'] = '奈良県';
$area_list['%warabi.%'] = '埼玉県';
$area_list['%utsunomiya.%'] = '栃木県';
$area_list['%urasoe.%'] = '沖縄県';
$area_list['%uozu.%'] = '富山県';
$area_list['%uji.kyoto.%'] = '京都府';
$area_list['%ueda.%'] = '長野県';
$area_list['%tsuyama.%'] = '岡山県';
$area_list['%tsuruoka.%'] = '山形県';
$area_list['%tsuchiura.%'] = '茨城県';
$area_list['%.tsukuba%'] = '栃木県';
$area_list['%tsu.mie.%'] = '三重県';
$area_list['%toyohashi.%'] = '愛知県';
$area_list['%tondabayashi.%'] = '大阪府';
$area_list['%tomakomai.%'] = '北海道';
$area_list['%tokorozawa.%'] = '埼玉県';
$area_list['%tkyo.%'] = '東京都';
$area_list['%tky.%'] = '東京都';
$area_list['%tksm.%'] = '徳島県';
$area_list['%takasaki.%'] = '群馬県';
$area_list['%takaoka.%'] = '富山県';
$area_list['%takamatsu.%'] = '香川県';
$area_list['%takakise.%'] = '佐賀県';
$area_list['%tachikawa.%'] = '東京都';
$area_list['%suwa.%'] = '長野県';
$area_list['%souka.%'] = '埼玉県';
$area_list['%simodate.%'] = '茨城県';
$area_list['%shinshu.%'] = '長野県';
$area_list['%shimonoseki.%'] = '山口県';
$area_list['%shimizu.%'] = '静岡県';
$area_list['%seto.%'] = '愛知県';
$area_list['%senndai.%'] = '宮城県';
$area_list['%sasebo.%'] = '長崎県';
$area_list['%sapporo.%'] = '北海道';
$area_list['%sakai.osaka.%'] = '大阪府';
$area_list['%sagamihara.%'] = '神奈川県';
$area_list['%ryukyu.%'] = '沖縄県';
$area_list['%ryugasaki.%'] = '茨城県';
$area_list['%oyama.%'] = '栃木県';
$area_list['%otsu.%'] = '滋賀県';
$area_list['%ota.%'] = '群馬県';
$area_list['%oska.%'] = '大阪府';
$area_list['%osk.%'] = '大阪府';
$area_list['%oomichi.%'] = '大分県';
$area_list['%onomichi.%'] = '広島県';
$area_list['%omuta.%'] = '福岡県';
$area_list['%omiya.%'] = '埼玉県';
$area_list['%okn.%'] = '沖縄県';
$area_list['%okazaki.%'] = '愛知県';
$area_list['%ogaki.%'] = '岐阜県';
$area_list['%odawara.%'] = '神奈川県';
$area_list['%obihiro.%'] = '北海道';
$area_list['%numazu.%'] = '静岡県';
$area_list['%nishinomiya.%'] = '兵庫県';
$area_list['%niihama.%'] = '愛媛県';
$area_list['%ngy.%'] = '愛知県';
$area_list['%narita.%'] = '千葉県';
$area_list['%naha.%'] = '沖縄県';
$area_list['%nagoya.%'] = '愛知県';
$area_list['%.nagoya%'] = '愛知県';
$area_list['%nagaoka.%'] = '新潟県';
$area_list['%musashino.%'] = '東京都';
$area_list['%muroran.%'] = '北海道';
$area_list['%morioka.%'] = '岩手県';
$area_list['%miyakonojo.%'] = '宮崎県';
$area_list['%mitsukaido.%'] = '茨城県';
$area_list['%mito.%'] = '茨城県';
$area_list['%matsuyama.%'] = '愛媛県';
$area_list['%matsumoto.%'] = '長野県';
$area_list['%matsue.%'] = '島根県';
$area_list['%matsudo.%'] = '千葉県';
$area_list['%marugame.%'] = '香川県';
$area_list['%maebashi.%'] = '群馬県';
$area_list['%kushiro.%'] = '北海道';
$area_list['%kurume.%'] = '福岡県';
$area_list['%kure.%'] = '広島県';
$area_list['%kurashiki.%'] = '岡山県';
$area_list['%kumagaya.%'] = '埼玉県';
$area_list['%koshigaya.%'] = '埼玉県';
$area_list['%koriyama.%'] = '福島県';
$area_list['%komaki.%'] = '愛知県';
$area_list['%kofu.%'] = '山梨県';
$area_list['%.kobe%'] = '兵庫県';
$area_list['%kitakyushu.%'] = '福岡県';
$area_list['%kisarazu.%'] = '千葉県';
$area_list['%kgw.%'] = '香川県';
$area_list['%kgs.%'] = '鹿児島県';
$area_list['%kch.%'] = '高知県';
$area_list['%kawasaki.%'] = '神奈川県';
$area_list['%kawaguchi.%'] = '埼玉県';
$area_list['%kawagoe.%'] = '埼玉県';
$area_list['%kashima.%'] = '茨城県';
$area_list['%.kanazawa%'] = '石川県';
$area_list['%kamakura.%'] = '神奈川県';
$area_list['%kakogawa.%'] = '兵庫県';
$area_list['%kakegawa.%'] = '静岡県';
$area_list['%kajiki.%'] = '鹿児島県';
$area_list['%kaizuka.%'] = '大阪府';
$area_list['%joyo.%'] = '京都府';
$area_list['%izumi.%'] = '大阪府';
$area_list['%iwata.%'] = '静岡県';
$area_list['%iwakuni.%'] = '山口県';
$area_list['%iwaki.%'] = '福島県';
$area_list['%ishinomaki.%'] = '宮城県';
$area_list['%isesaki.%'] = '群馬県';
$area_list['%ina.nagano.%'] = '長野県';
$area_list['%imazuka.%'] = '山形県';
$area_list['%imabari.%'] = '愛媛県';
$area_list['%ikeda.osaka.%'] = '大阪府';
$area_list['%ichinomiya.%'] = '愛知県';
$area_list['%ichihara.%'] = '千葉県';
$area_list['%ibrk.%'] = '茨城県';
$area_list['%hyg.%'] = '兵庫県';
$area_list['%hrsm.%'] = '広島県';
$area_list['%honjo.%'] = '埼玉県';
$area_list['%hodogaya.%'] = '神奈川県';
$area_list['%hirosaki.%'] = '青森県';
$area_list['%hiratsuka.%'] = '神奈川県';
$area_list['%hirakata.%'] = '大阪府';
$area_list['%hiraido.%'] = '栃木県';
$area_list['%handa.%'] = '愛知県';
$area_list['%hamamatsu.%'] = '静岡県';
$area_list['%hakodate.%'] = '北海道';
$area_list['%hachioji.%'] = '東京都';
$area_list['%hachinohe.%'] = '青森県';
$area_list['%funabasi.%'] = '千葉県';
$area_list['%fukuyama.%'] = '広島県';
$area_list['%fujisawa.%'] = '神奈川県';
$area_list['%fuchu.%'] = '東京都';
$area_list['%fksm.%'] = '福島県';
$area_list['%ehm.%'] = '愛媛県';
$area_list['%chofu.%'] = '東京都';
$area_list['%atsugi.%'] = '神奈川県';
$area_list['%asahikawa.%'] = '北海道';
$area_list['%anjo.%'] = '愛知県';
$area_list['%aizu.%'] = '福島県';
$area_list['%abiko.%'] = '千葉県';
$area_list['%.ube.%'] = '山口県';
$area_list['%.tama.%'] = '東京都';
$area_list['%.ome.%'] = '東京都';
?>