<div class="title">ダウンロード</div>
<?php
/*=========================================*/
/* mogura     Plug-in【ダウンロード】      */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

if(defined("SELECT_LOG_FLG")){
	$where = "date LIKE '".mk_sql_date($ym, $d)."%' AND dl='".$send_sel."'";
	$sql_dl = true;
}else{
	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	$sql = array();
	$main = array();

	/*=========================================*/
	/* SQL                                     */
	/*=========================================*/
	$sql["select"] = "date,dl,COUNT(*) as pv";
	$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%' AND dl IS NOT NULL";
	$sql["group"] = "dl";
	$sql["dl"] = true;
	$sql["op"] = "order by pv desc";

	$res = $db->query(mk_sql($sql));
	check_err($res);

	$i = 0;
	$max_int = 0;
	$total = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$main[$i]["dl"] = $row["dl"];
		$main[$i]["pv"] = $row["pv"];
		if($max_int < $row["pv"]) $max_int = $row["pv"];

		$total += $row["pv"];

		$i++;
	}
	$res->free();

	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	echo '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
	echo '<tr>';
	echo '<th nowrap width="150">DLファイル</th>';
	echo '<th nowrap width="80">DL数</th>';
	echo '<th nowrap>グラフ</th>';
	echo '</tr>'."\n";
	foreach($main as $k => $v){
		//ユニーク値
		$pv = $v["pv"];
		//link
		$link = sel_link(query_edit("sel",$v["dl"]),basename($v["dl"]));
		
		echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
		echo '<td width="150">'.$link.'</td>';
		echo '<td width="80" align="right"><font color="#FF0000">'.$pv.'</font></td>';
		echo '<td align="left">'.mk_graph_01($pv,$total,$max_int).'</td>';
		echo '</tr>'."\n";
	}
	echo '<tr class="bg_total">';
	echo '<td width="150" align="right"><b>合計：</b></td>';
	echo '<td width="80" align="right"><b><font color="#FF0000">'.$total.'</font></b></td>';
	echo '<td align="left"></td>';
	echo '</tr>'."\n";
	echo '</table>';
}
?>
