<?php
/*=========================================*/
/* mogura     Plug-in【月別】              */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*  Customized by yap (http://yap.jp/)     */
/*                                         */
/*      on 2008/06/08 as Version 1.0.1     */
/*                                         */
/*=========================================*/

if(defined("SELECT_LOG_FLG")){
	if(isset($_GET["seld_t"]) && isset($_GET["seld_e"])){
		$where = "date LIKE 'r' AND MONTH(date)=".$send_sel;
	}else{
		$where = "date LIKE '".mk_sql_date(substr($ym,0,4).sprintf('%02d', $send_sel))."%'";
	}
}else{
	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	$sql = array();
	$main = array();
	for($i=1; $i <= 12; $i++){
		$main[$i]["pv"] = 0;
		$main[$i]["uniq"] = 0;
	}


	/*=========================================*/
	/* SQL                                     */
	/*=========================================*/
	$sql["select"] = "MONTH(date) as month,COUNT(*) as pv,COUNT(DISTINCT id) as uniq";
	$sql["where"] = "date LIKE '".substr($ym,0,4)."%'";
	$sql["group"] = "month";
	$res = $db->query(mk_sql($sql));
	check_err($res);

	$max_int = 0;
	$total_uniq = 0;
	$total_pv = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$main[$row["month"]]["pv"] = $row["pv"];
		$main[$row["month"]]["uniq"] = $row["uniq"];
		if($max_int < $row["pv"]) $max_int = $row["pv"];

		$total_pv += $row["pv"];
		$total_uniq += $row["uniq"];
	}
	$res->free();


	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	echo '<div class="title">月別&nbsp;&nbsp;<small>［年合計］</small></div>';
	echo '<table width="100%" cellpadding="0" cellspacing="0">';
	echo '<tr>';
	echo '<th nowrap width="50">月</th>';
	echo '<th nowrap width="100">ユニークユーザ</th>';
	echo '<th nowrap width="100">ページビュー</th>';
	echo '<th nowrap>グラフ</th>';
	echo '</tr>'."\n";
	foreach($main as $k => $v){
		//ユニーク値
		$pv = $v["pv"];
		$uniq = $v["uniq"];
		//link
		$link = sel_link(query_edit("sel",$k),$k);

		echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
		echo '<td align="right" width="50">'.$link.'</td>';
		echo '<td align="right" width="100"><font color="#0000FF">'.$uniq.'</font></td>';
		echo '<td align="right" width="100"><font color="#FF0000">'.$pv.'</font></td>';
		echo '<td align="left">'.mk_graph($pv,$uniq,$max_int).'</td>';
		echo '</tr>'."\n";
	}
	echo '<tr class="bg_total">';
	echo '<td align="right" width="50"><b>合計：</b></td>';
	echo '<td align="right" width="100"><b><font color="#0000FF">'.$total_uniq.'</font></b></td>';
	echo '<td align="right" width="100" class="bg_total"><b><font color="#FF0000">'.$total_pv.'</font></b></td>';
	echo '<td>&nbsp;</td>';
	echo '</tr>'."\n";
	echo '</table>';
}
?>
