<div class="title">生ログ</div>
<?php
/*=========================================*/
/* mogura     Plug-in【生ログ】            */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

/*=========================================*/
/* 設定                                    */
/*=========================================*/
// ■ ログ表示数
// all -> 常に全件表示
// 0   -> config.phpのlimit値
$limit_hack = 0;

/*=========================================*/
/* SQL                                     */
/*=========================================*/
//初期化
$main = array();
$sel_date = array();

//select date
$sql = array();
$sql["select"] = "LEFT(date, 10) as ymd";
$sql["group"] = "ymd";
$sql["op"] = "order by date desc";

$res = $db->query(mk_sql($sql));
check_err($res);
while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	$sel_date[] = $row["ymd"];
}
$res->free();

//SQL
$sql = array();
$sql["select"] = "COUNT(*)";
if($send_op) $sql["where"] = "date LIKE '".$send_op."%'";
//max_val
$res = $db->query(mk_sql($sql));
check_err($res);
$row = $res->fetchRow(DB_FETCHMODE_ORDERED);
$max_val = $row[0];
$res->free();

$sql["select"] = "no";
$sql["op"] = "order by no desc";
//move
if($limit_hack !== "all"){
	if(!$limit_hack) $limit_hack = constant("LIMIT");
	$move_p = $send_p." , ".$limit_hack;
	$move = move_bt($send_p, $max_val,$limit_hack);
}
if(isset($move_p)) $sql["limit"] = $move_p;
$res = $db->query(mk_sql($sql));
check_err($res);
if($res->numRows() > 0){
	$sql["where"] = "no IN(";
	while ($row = $res->fetchRow(DB_FETCHMODE_ORDERED)){
		$sql["where"] .= $row[0].",";
	}
	$res->free();
	$sql["where"] = substr($sql["where"], 0, -1);
	$sql["where"] .= ")";
	unset($sql["select"]);
	unset($sql["limit"]);
	$sql["from"] = "(".constant("DB_TABLE_LOG")." left join ".constant("DB_CH_ID")
	." on ".constant("DB_TABLE_LOG").".id = ".constant("DB_CH_ID").".ch_id)";
	$sql["from"] = "(".$sql["from"]." left join ".constant("DB_TABLE_UA")
	." on ".constant("DB_TABLE_LOG").".ua = ".constant("DB_TABLE_UA").".ua_id)";
	$res = $db->query(mk_sql($sql));
	check_err($res);
	//main
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$main[] = $row;
	}
	$res->free();
}else{
	$res->free();
}

/*=========================================*/
/* 日付指定                                */
/*=========================================*/
if($sel_date){
	echo '<form action="'.constant("FILENAME").'" method="get" name="form">'."\n";
	echo '<input type="hidden" name="ym" value="'.$ym.'">';
	echo '<input type="hidden" name="d" value="'.$d.'">';
	echo '<input type="hidden" name="act" value="'.$act.'">'."\n";
	echo '<input type="hidden" name="sel" value="'.$send_sel.'">'."\n";
	echo '日付指定:&nbsp;<select name="op">'."\n";
	if($send_op){
		echo '<option value="">&nbsp;全期間&nbsp;</option>'."\n";
	}else{
		echo '<option value="" selected style="background-color:#666;color:#FFF;">&nbsp;&#187;&nbsp;全期間&nbsp;</option>'."\n";
	}
	echo '<option value="">&nbsp;---&nbsp;</option>'."\n";
	foreach ($sel_date as $v) {
		if($v == $send_op){
			echo '<option value="'.$v.'" selected style="background-color:#666;color:#FFF;">&nbsp;&#187;&nbsp;'.$v.'&nbsp;</option>'."\n";
		}else{
			echo '<option value="'.$v.'">&nbsp;'.$v.'&nbsp;</option>';
		}
	}
	echo '</select>'."\n";
	echo '&nbsp;<input value="view" type="submit">';
	echo '</form><hr size="1">';
}

/*=========================================*/
/* メイン処理                              */
/*=========================================*/
if($main){
	echo 'ログ数:&nbsp;<b>'.number_format($max_val).'</b>件';
	if($send_op) echo '&nbsp;&nbsp;&nbsp;日付指定:&nbsp;<b>'.$send_op.'</b>';
	echo '<br><br>';

	foreach($main as $k => $v){
		//No
		echo 'LogNo.<b>'.$v["no"].'</b>';
		echo '<table width="100%">';
		//date
		echo '<tr><th class="no_th" width="100">Date:</th><td>&nbsp;'.$v["date"].'</td></tr>';
		//ID
		if($v["ch_name"]) $v["id"] .= "&nbsp;[&nbsp;".$v["ch_name"]."&nbsp;]";
		echo '<tr><th class="no_th" width="100">ID:</th><td>&nbsp;'.$v["id"].'</td></tr>';
		//path
		if(trim($v["path"])){
			if($v["title"]) $v["path"] .= "&nbsp;[&nbsp;".$v["title"]."&nbsp;]";
			echo '<tr><th class="no_th" width="100">Path:</th><td>&nbsp;'.$v["path"].'</td></tr>';
		}
		//IP.Host
		if(trim($v["ip"])) echo '<tr><th class="no_th" width="100">IP:</th><td>&nbsp;'.$v["ip"].'</td></tr>';
		if(trim($v["host"])) echo '<tr><th class="no_th" width="100">Host:</th><td>&nbsp;'.$v["host"].'</td></tr>';
		//Referer
		if(trim($v["ref"])){
			if($v["ref_q"]) $v["ref"] .= "?".$v["ref_q"];
			echo '<tr><th class="no_th" width="100">Referer:</th><td>&nbsp;'.@mb_convert_encoding(urldecode($v["ref"]), "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS").'</td></tr>';
		}
		//UserAgent
		if(trim($v["ua_name"])) echo '<tr><th class="no_th" width="100">UserAgent:</th><td>&nbsp;'.$v["ua_name"].'</td></tr>';
		//Lang
		if(trim($v["lang"])) echo '<tr><th class="no_th" width="100">Language:</th><td>&nbsp;'.$v["lang"].'</td></tr>';
		//Monitor
		if(trim($v["monitor"])) echo '<tr><th class="no_th" width="100">Monitor:</th><td>&nbsp;'.$v["monitor"].'</td></tr>';
		//Color
		if(trim($v["color"])) echo '<tr><th class="no_th" width="100">Color:</th><td>&nbsp;'.$v["color"].'</td></tr>';

		echo "</table>";
		echo "<br>\n";
	}
	if(isset($move)) echo $move;
}elseif($send_sel && !$main){
	echo '<div id="error">該当するログは見つかりませんでした。</div>';
}
?>