<div class="title">OS</div>
<?php
/*=========================================*/
/* mogura     Plug-in【OS】                */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

/*=========================================*/
/* 設定                                    */
/*=========================================*/
// ■ Crawlerの表示
// 1 -> しない , 0 -> する
$no_crawler = 0;


if(defined("SELECT_LOG_FLG")){
	$sel1 = "";
	$sel2 = "";
	list($sel1,$sel2) = explode("---",$send_sel);
	if($sel2){
		$where = "date LIKE '".mk_sql_date($ym,$d)."%' AND os='".$sel1."' AND os_v='".$sel2."'";
	}else{
		$where = "date LIKE '".mk_sql_date($ym,$d)."%' AND os='".$sel1."'";
	}
}else{
	//ソート用
	function cmp($a , $b){
		if ($a["total"] == $b["total"]) return 0;
		return ($a["total"] > $b["total"]) ? -1 : 1;
	}

	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	//ページ用配列 初期化
	$main = array();

	/*=========================================*/
	/* SQL - MAIN                              */
	/*=========================================*/
	$sql = array();
	$sql["select"] = "os,os_v,COUNT(*) as pv,COUNT(DISTINCT id) as uniq";
	$sql["from"] = "(".constant("DB_TABLE_LOG")." IGNORE INDEX (ua) left join ".constant("DB_TABLE_UA")
	." on ".constant("DB_TABLE_LOG").".ua = ".constant("DB_TABLE_UA").".ua_id)";
	$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%' AND NOT ISNULL(os)";
	$sql["group"] = "os,os_v";
	$sql["sort"] = true;

	$res = $db->query(mk_sql($sql));
	if(DB::isError($res)){
		$sql["from"] = str_replace(' IGNORE INDEX (ua)', '', $sql["from"]);
		$res = $db->query(mk_sql($sql));
		check_err($res);
	}

	$max_int = 0;
	$i = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		if($no_crawler && $row["os"] == 'Crawler') continue;
		
		$main[$row["os"]][$row["os_v"]]["pv"] = $row["pv"];
		$main[$row["os"]][$row["os_v"]]["uniq"] = $row["uniq"];
		
		//PV Total
		$main[$row["os"]]["total"] += $row["pv"];
		//Uniq Total
		$main[$row["os"]]["total2"] += $row["uniq"];
		
		if($max_int < $row["pv"]) $max_int = $row["pv"];
		
		$i++;
	}
	$res->free();

	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	if($main){
		uasort($main , "cmp");
		//Unknown OSを最後に移動
		if(isset($main["Unknown OS"])){
			$tmp = $main["Unknown OS"];
			unset($main["Unknown OS"]);
			$main["Unknown OS"] = $tmp;
		}

		echo '<table width="100%" cellpadding="0" cellspacing="0">';
		echo '<tr>';
		echo '<th nowrap>OS</th>';
		echo '<th nowrap>バージョン</th>';
		echo '<th nowrap width="70">ユニーク</th>';
		echo '<th nowrap width="70">件数</th>';
		echo '<th nowrap width="200">グラフ</th>';
		echo '</tr>';
		foreach($main as $k => $v){
			if($k == "total") continue;
			if($k == "total2") continue;
			
			$tmp = "";
			$i = 0;
			
			$c = false;
			foreach($main[$k] as $k2 => $v2){
				if($k2 == "total") continue;
				if($k2 == "total2") continue;
				//link
				if($k == $tmp){
					$link = "&nbsp;";
				}else{
					$link = set_icon($k).sel_link(query_edit("sel",$k),$k);
				}
				$tmp = $k;
				
				$link2 = sel_link(query_edit("sel",$k."---".$k2),$k2);
				
				echo '<tr'.tr_color($c).' id="bg_id'.$k.$i.'" onmouseover="chBG(\'bg_id'.$k.$i.'\', 1);" onmouseout="chBG(\'bg_id'.$k.$i.'\', 0);">';
				echo '<td nowrap>'.$link.'</td>';
				echo '<td nowrap>'.$link2.'</td>';
				echo '<td width="70" align="right"><font color="#0000FF">'.$v2["uniq"].'</font></td>';
				echo '<td width="70" align="right"><font color="#FF0000">'.$v2["pv"].'</font></td>';
				echo '<td width="200">'.mk_graph($v2["pv"],$v2["uniq"],$max_int).'</td>';
				echo '</tr>'."\n";
				$i++;
			}
			//合計
			echo '<tr class="bg_total">';
			echo '<td>&nbsp;</td>';
			echo '<td align="right"><b>合計：</b></td>';
			echo '<td width="70" align="right"><b><font color="#0000FF">'.$main[$k]["total2"].'</font></b></td>';
			echo '<td width="70" align="right"><b><font color="#FF0000">'.$main[$k]["total"].'</font></b></td>';
			echo '<td width="200">&nbsp;</td>';
			echo '</tr>'."\n";
		}
		echo '</table>';
	}else{
		echo '<div id="error">解析ログが見つかりません。</div>';
	}
}
?>