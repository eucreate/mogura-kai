<div class="title">ページ</div>
<?php
/*=========================================*/
/* mogura     Plug-in【ページ】            */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

define("INDEX", "[index]");

if(defined("SELECT_LOG_FLG")){
	$send_sel = str_replace (INDEX, "", $send_sel);
	$where = "date LIKE '".mk_sql_date($ym, $d)."%' AND path = '".$send_sel."'";
}else{
	//ソート用
	function cmp($a , $b){
		if ($a["total"] == $b["total"]) return 0;
		return ($a["total"] > $b["total"]) ? -1 : 1;
	}
	/*=========================================*/
	/* 設定                                    */
	/*=========================================*/
	// ■ ページ指定用のアイコンを表示
	// 1 -> する , 0 -> しない
	$p_sel_icon = 1;

	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	$sql = array();
	$main = array();

	/*=========================================*/
	/* SQL                                     */
	/*=========================================*/
	$sql["select"] = "path,title,COUNT(*) as pv,COUNT(DISTINCT id) as uniq";
	$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%' AND path IS NOT NULL";
	$sql["group"] = "path";
	$sql["sort"] = true;

	$res = $db->query(mk_sql($sql));
	check_err($res);

	$max_int = 0;
	$i = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$f = basename($row["path"]);
		$p = str_replace ($f, "", $row["path"]);
		if($f === "") $f = INDEX;
		else{
			$nLen = strlen($p);
			$p = preg_replace("/((.*[^:])|^)\/\/$/", "$1/", $p);
			if(strlen($p) != $nLen){
				$f = $f."/";
			}
		}
		
		$main[$p][$f]["pv"] = $row["pv"];
		$main[$p][$f]["uniq"] = $row["uniq"];
		
		//PV Total
		$main[$p]["total"] += $row["pv"];
		//Uniq Total
		$main[$p]["total2"] += $row["uniq"];
		
		if($max_int < $row["pv"]) $max_int = $row["pv"];
		
		$i++;
	}
	$res->free();

	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	if($main){
		uasort($main , "cmp");

		echo '<table width="100%" cellpadding="0" cellspacing="0">';
		echo '<tr>';
		echo '<th nowrap>Path</th>';
		echo '<th nowrap>file</th>';
		echo '<th nowrap width="70">ユニーク</th>';
		echo '<th nowrap width="70">件数</th>';
		echo '<th nowrap>グラフ</th>';
		echo '</tr>';
		foreach($main as $k => $v){
			if($k == "total") continue;
			if($k == "total2") continue;
			
			$tmp = "";
			$i = 0;
			
			$c = false;
			$path_name = $k;
			foreach($main[$k] as $k2 => $v2){
				
				if($k2 == "total") continue;
				if($k2 == "total2") continue;
				//link
				$link = sel_link(query_edit("sel",$k.$k2),$k2);
				
				echo '<tr'.tr_color($c).' id="bg_id'.$k.$i.'" onmouseover="chBG(\'bg_id'.$k.$i.'\', 1);" onmouseout="chBG(\'bg_id'.$k.$i.'\', 0);">';
				echo '<td>'.$path_name.'</td>';
				echo '<td>'.$link.'</td>';
				echo '<td width="70" align="right"><font color="#0000FF">'.$v2["uniq"].'</font></td>';
				echo '<td width="70" align="right"><font color="#FF0000">'.$v2["pv"].'</font></td>';
				echo '<td width="200">'.mk_graph($v2["pv"],$v2["uniq"],$max_int).'</td>';
				echo '</tr>'."\n";
				$i++;
				$path_name = "&nbsp;";
			}
			//合計
			echo '<tr class="bg_total">';
			echo '<td>&nbsp;</td>';
			echo '<td align="right"><b>合計：</b></td>';
			echo '<td width="70" align="right"><b><font color="#0000FF">'.$main[$k]["total2"].'</font></b></td>';
			echo '<td width="70" align="right"><b><font color="#FF0000">'.$main[$k]["total"].'</font></b></td>';
			echo '<td>&nbsp;</td>';
			echo '</tr>'."\n";
		}
		echo '</table>';
	}else{
		echo '<div id="error">解析ログが見つかりません。</div>';
	}
}
?>