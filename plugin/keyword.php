<div class="title">検索キーワード</div>
<?php
/*=========================================*/
/* mogura     Plug-in【検索キーワード】    */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/
//検索エンジンリスト
include_once(constant("DIR_LIST")."engine.php");

if(defined("SELECT_LOG_FLG")){
	$select = "*,REPLACE(ref_q,'%','') as search";
	
	$where = "date LIKE '".mk_sql_date($ym, $d)."%' AND\n";
	$where .= "(";
	$s = urlencode($send_sel);
	$s = str_replace ("%", "%%%", $s);
	$where .= "ref_q LIKE '%".$s."%'";
	$where .= " OR ";
	$s = urlencode(mb_convert_encoding($send_sel, "EUC-JP", "ASCII,JIS,UTF-8,EUC-JP,SJIS"));
	$s = str_replace ("%", "%%%", $s);
	$where .= "ref_q LIKE '%".$s."%'";
	$where .= " OR ";
	$s = urlencode(mb_convert_encoding($send_sel, "SJIS", "ASCII,JIS,UTF-8,EUC-JP,SJIS"));
	$s = str_replace ("%", "%%%", $s);
	$where .= "ref_q LIKE '%".$s."%'";
	$where .= ")";
}else{
	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	$sql = array();
	$main = array();

	$max_int = 0;
	$total = 0;
	
	$sql["select"] = "ref_q,CASE ";
	foreach($engine_list as $uri => $list){
		$sql["select"] .= "WHEN (ref LIKE '%".$uri."%' AND ref_q LIKE '%".$list[1]."=%') THEN '".$uri."' ";
	}
	$sql["select"] .= "END engine_group";
	$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%'";
	$sql["group"] = "ref_q,engine_group";
	$sql["op"] = " having engine_group is not null";

	$res = $db->query(mk_sql($sql));
	check_err($res);

	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		//キーワード取得
		list(,$eng["q"],$eng["enc"]) = $engine_list[$row["engine_group"]];
		$keyword = "";
		$keyword = get_keyword($row["ref_q"] ,$eng["q"], $eng["enc"]);
		if($keyword == "") continue;
		$keyword_edit = eregi_replace("(［|］)","",$keyword);
		foreach (explode('&nbsp;',$keyword_edit) as $search_key) {
			if(!array_key_exists($search_key,$main)){
				$main[$search_key] = 1;
			}else{
				$main[$search_key]++;
			}
		}
		
		if($max_int < $main[$search_key]) $max_int = $main[$search_key];
		$total++;
	}
	$res->free();

	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	if($main){
		//ソート
		arsort($main ,SORT_NUMERIC);

		echo '<table width="100%" cellpadding="0" cellspacing="0">';
		echo '<tr>';
		echo '<th nowrap>キーワード</th>';
		echo '<th nowrap width="50">件数</th>';
		echo '<th nowrap>グラフ</th>';
		echo '</tr>';
		$i = 0;
		foreach($main as $k => $v){
			//link
			$link = sel_link(query_edit("sel",$k),$k);
			
			echo '<tr'.tr_color($c).' id="bg_id'.$i.'" onmouseover="chBG(\'bg_id'.$i.'\', 1);" onmouseout="chBG(\'bg_id'.$i.'\', 0);">';
			echo '<td>'.$link.'</td>';
			echo '<td width="50" align="right"><font color="#FF0000">'.$v.'</font></td>';
			echo '<td>'.mk_graph_01($v, $max_int, $total).'</td>';
			echo '</tr>';
			$i++;
		}
		//合計
		echo '<tr class="bg_total">';
		echo '<td align="right"><b>合計：</b></td>';
		echo '<td width="50" align="right"><b>'.array_sum($main).'</b></td>';
		echo '<td>&nbsp;</td>';
		echo '</tr>'."\n";
		echo '</table>';
	}else{
		echo '<div id="error">解析ログが見つかりません。</div>';
	}
}
?>