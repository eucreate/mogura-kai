<?php
/*=========================================*/
/* mogura     Plug-in【トップ】            */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

//最新アクセス表示数
$limit = 30;

/*=========================================*/
/* アクセス数                              */
/*=========================================*/
$main = array(
"today_pv" => 0,
"today_uniq" => 0,
);

$sql = array();
$sql["select"] = "COUNT(*) as pv,COUNT(DISTINCT id) as uniq";
$sql["where"] = "date LIKE '".gmdate("Y-m-d", time()+constant("TIME_DIFF")*3600)."%'";

$res = $db->query(mk_sql($sql));
check_err($res);
while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	$main["today_pv"] = number_format($row["pv"]);
	$main["today_uniq"] = number_format($row["uniq"]);
}
$res->free();

if($main){
echo <<<EOM

<script language="JavaScript">
<!--
var bBlink = true;
var eleTotal_blink = null;

function blinkTotal(){
	if(bBlink){
		eleTotal_blink.fadeOut("slow").fadeIn("slow");
		setTimeout(blinkTotal, 1200);
	}
}

$.event.add(window, "load", function(){
	eleTotal_blink = $("span.total_blink");
	blinkTotal();
});

$(function(){
	$.post("./async/top_total_count.php", function(data){
		if(data){
			var lstTotalCount = decodeURIComponent(data).split("\t");
			$("#total_pv").html(lstTotalCount[0]);
			$("#total_uniq").html(lstTotalCount[1]);
		}

		bBlink = false;
	});
});
// -->
</script>
<br>
<table cellpadding="0" cellspacing="0">
<tr>
<th width="150">ページビュー（今日）</th>
<th width="150">ユニークユーザ（今日）</th>
</tr>
<tr align="right">
<td width="150"><b>{$main["today_pv"]}</b></td>
<td width="150"><b>{$main["today_uniq"]}</b></td>
</tr>
<tr align="right">
<th width="150">ページビュー（合計）</th>
<th width="150">ユニークユーザ（合計）</th>
</tr>
<tr align="right">
<td width="150"><b id="total_pv"><span class="total_blink">―</span></b></td>
<td width="150"><b id="total_uniq"><span class="total_blink">―</span></b></td>
</tr>
</table><br>
EOM;
}


/*=========================================*/
/* 最新アクセスユーザー                    */
/*=========================================*/
$main = array();

$sql = array();
$sql["select"] = "max(no) as no";
$sql["where"] = "date > '".gmdate("Y-m-d H:i:s", time()+constant("TIME_DIFF")*3600-86400)."'";
$sql["group"] = "id";
$sql["limit"] = $limit;
$sql["op"] = "order by no desc";
$res = $db->query(mk_sql($sql));
check_err($res);

if($res->numRows() < $limit){
	$sql["where"] = "date > '".gmdate("Y-m-d H:i:s", time()+constant("TIME_DIFF")*3600-86400*((int)round(($limit+1)/($res->numRows()+1)))*2)."'";
	$res->free();
	$res = $db->query(mk_sql($sql));
	check_err($res);
}
if($res->numRows() < $limit){
	unset($sql["where"]);
	$res->free();
	$res = $db->query(mk_sql($sql));
	check_err($res);
}

if($res->numRows() > 0){
	$sql = array();
	$sql["where"] = "no IN(";
	while ($row = $res->fetchRow(DB_FETCHMODE_ORDERED)){
		$sql["where"] .= $row[0].",";
	}
	$res->free();
	$sql["where"] = substr($sql["where"], 0, -1);
	$sql["where"] .= ")";
	
	$sql["from"] = "(".constant("DB_TABLE_LOG")." left join ".constant("DB_CH_ID")
	." on ".constant("DB_TABLE_LOG").".id = ".constant("DB_CH_ID").".ch_id)";
	$sql["select"] = "date,id,title,path,ch_name,ref,ref_q,unix_timestamp(".constant("DB_TABLE_LOG").".date) as ut";
	$sql["op"] = "order by no desc";

	$res = $db->query(mk_sql($sql));
	check_err($res);

	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$main[] = $row;
	}
	$res->free();
}else{
	$res->free();
}

echo '最新アクセスユーザー（'.$limit.'件）';
echo '<table width="100%" cellpadding="0" cellspacing="0">';
echo '<tr>';
echo '<th nowrap width="150">日時</th>';
echo '<th nowrap width="120">ID</th>';
echo '<th nowrap>ページ</th>';
#echo '<th nowrap>リンク元</th>';
echo '</tr>'."\n";
foreach($main as $k => $v){
	$online_flg = (($login_ut-$v["ut"]) <= $online_timeout ? true : false);
	
	$v["date"] = str_replace(" ", "&nbsp;&nbsp;", $v["date"]);
	if($v["ref_q"]) $v["ref"] .= "?".$v["ref_q"];
	#if(!$v["ref"]) $v["ref"] = "(Direct or Bookmark)";
	#if($v["ref"] != "(Direct or Bookmark)"){
	#	$v["ref"] = '<a href="./inc/redirect.php?redirect='.urlencode($v["ref"]).'" target="_blank">'.set_img(constant("ICON_JUMP"),"ジャンプ").'</a>&nbsp;'.mb_convert_encoding($v["ref"], "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
	#}
	$v["path"] = $v["path"] != "" ? "［&nbsp;".$v["path"]."&nbsp;］" : "unknown path";
	$v["path"] .= $v["title"] ? "<br>".$v["title"] : "";
	$v["name"] = $v["ch_name"] ? $v["ch_name"] : $v["id"];
	$track = query_edit("act", "track");
	$track = query_edit("sel", $v["id"], $track);
	$track = '<a href="'.constant("FILENAME").$track.'">'.set_img(constant("ICON_TRACK"),"[ ".$v["name"]." ] 追跡").'</a>&nbsp;';
	
	if($online_flg){
		$v["date"] .= "&nbsp;".set_img("image/online.gif","オンライン");
	}else{
		$v["date"] .= "&nbsp;".set_img("image/offline.gif","オフライン");
	}
	
	echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
	echo '<td nowrap width="150">'.$v["date"].'</td>';
	echo '<td nowrap width="120">'.$track.$v["name"].'</td>';
	echo '<td>'.$v["path"].'</td>';
	#echo '<td>'.$v["ref"].'</td>';
	echo '</tr>'."\n";
}
echo '</table>';
?>
