<div class="title">ユーザー追跡&nbsp;&nbsp;<small>［日別］</small></div>
<?php
/*=========================================*/
/* mogura     Plug-in【日別ユーザー追跡】  */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/* custumized by kiyori                    */
/*=========================================*/

/*=========================================*/
/* 設定                                    */
/*=========================================*/
//追跡ユーザー環境の表示
// 1 -> する , 0 -> しない
$user_env = 1;
//タイムアウト(分)
$time_out = 30;
//月のアクセスが n件以下のユーザーを追跡フォームに表示しない
// 0 -> 無効
$min_disp = 0;

/*=========================================*/
/* 初期化                                  */
/*=========================================*/
$sql = array();
$main = array();
$user = array();
$form = array();
$form_chid = array();
$form_chname = array();

/*=========================================*/
/* LIST                                    */
/*=========================================*/
include_once(constant("DIR_LIST")."lang.php");
include_once(constant("DIR_LIST")."isp.php");

/*=========================================*/
/* SQL - FORM                              */
/*=========================================*/
$sql["dl"] = true;
$sql["from"] = "(".constant("DB_TABLE_LOG")." left join ".constant("DB_CH_ID")
." on ".constant("DB_TABLE_LOG").".id = ".constant("DB_CH_ID").".ch_id)";
$sql["select"] = "id,COUNT(*) as pv,ch_name";
$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%'";
$sql["group"] = "id";
$sql["sort"] = true;
if($min_disp) $sql["having"] = "pv > ".$min_disp;

$res = $db->query(mk_sql($sql));
check_err($res);

while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	$form[$row["id"]] = $row["pv"];
	$form_chid[$row["id"]] = $row["ch_name"] ? $row["ch_name"] : $row["id"];
	if($row["ch_name"]) $form_chname[$row["id"]] = $row["ch_name"];
}
$res->free();

/*=========================================*/
/* SQL - MAIN                              */
/*=========================================*/
if($send_sel){
	//初期化
	$user["ip"] = "";
	$user["host"] = "";
	$user["isp"] = "";
	$user["ua"] = "";
	$user["os"] = "";
	$user["browser"] = "";
	$user["monitor"] = "";
	$user["lang"] = "";
	
	//move ページャー処理
	$sql = array();
	$sql["dl"] = true;
	$sql["select"] = "COUNT(*)";
	$sql["where"] = "id = '".$send_sel."'";
	//日付で絞る
	$sql["where"] .= " AND date LIKE '".mk_sql_date($ym, $d)."%'";
	
	$res = $db->query(mk_sql($sql));
	$row = $res->fetchRow(DB_FETCHMODE_ORDERED);
	$max_val = $row[0];
	$res->free();
	
	if($send_p !== "all"){
		$move_p = $send_p." , ".constant("LIMIT");
		$move = move_bt($send_p, $max_val);
	}

	//user info
	$sql = array();
	$sql["select"] = "*,COUNT(*) as total_pv";
	$sql["from"] = "(".constant("DB_TABLE_LOG")." left join ".constant("DB_CH_ID")
	." on ".constant("DB_TABLE_LOG").".id = ".constant("DB_CH_ID").".ch_id)";
	$sql["where"] = constant("DB_TABLE_LOG").".id = '".$send_sel."'";
	$sql["group"] = "id";
	$sql["limit"] = "1";
	
	$res = $db->query(mk_sql($sql));
	check_err($row);
	$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
	$user["name"] = $row["ch_name"] ? $row["ch_name"]."&nbsp;&nbsp;[&nbsp;ID:".$row["id"]."&nbsp;]" : $row["id"];
	$user["first"] = $row["date"];
	$user["lang"] = $row["lang"];
	$user["monitor"] = $row["monitor"];
	$user["color"] = $row["color"];
	$user["count"] = $row["total_pv"];
	if($isp_list){
		foreach($isp_list as $k => $v){
			if(strstr($row["host"],$k) != false){
				list($user["isp"]) = explode("/",$v);
				break;
			}
		}
	}
	$res->free();
	//user info[ua]
	$sql = array();
	$sql["from"] = "(".constant("DB_TABLE_LOG")." left join ".constant("DB_TABLE_UA")
	." on ".constant("DB_TABLE_LOG").".ua = ".constant("DB_TABLE_UA").".ua_id)";
	$sql["select"] = "ua_name,brow,brow_v,os,os_v";
	$sql["where"] = constant("DB_TABLE_LOG").".id = '".$send_sel."'";
	$sql["group"] = "ua";
	$res = $db->query(mk_sql($sql));
	check_err($res);
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		if($row["ua_name"]) $user["ua"] = $row["ua_name"];
		if($user["ua"]){
			if($row["brow"] && $row["brow_v"] && $row["os"] && $row["os_v"]){
				$user["browser"] = $row["brow"];
				$user["browser_v"] = $row["brow_v"];
				$user["os"] = $row["os"];
				$user["os_v"] = $row["os_v"];
			}else{
				list($user["browser"], $user["browser_v"], $user["os"], $user["os_v"]) = getuseragent($user["ua"]);
			}
		}
	}
	$res->free();
	
	//user info[ip]
	$sql = array();
	$sql["dl"] = true;
	$sql["select"] = "ip";
	$sql["where"] = constant("DB_TABLE_LOG").".id = '".$send_sel."'";
	$sql["group"] = "ip";
	$res = $db->query(mk_sql($sql));
	check_err($res);
	$user["ip"] = "";
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		if(strstr($user["ip"], $row["ip"]) === false) $user["ip"] .= $row["ip"]."\n";
	}
	$res->free();
	
	//user info[host]
	$sql = array();
	$sql["dl"] = true;
	$sql["select"] = "host";
	$sql["where"] = constant("DB_TABLE_LOG").".id = '".$send_sel."'";
	$sql["group"] = "host";
	$res = $db->query(mk_sql($sql));
	check_err($res);
	$user["host"] = "";
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		if(strstr($user["host"], $row["host"]) === false) $user["host"] .= $row["host"]."\n";
	}
	$res->free();
	
	//user info[last]
	$sql = array();
	$sql["dl"] = true;
	$sql["select"] = "date";
	$sql["where"] = constant("DB_TABLE_LOG").".id = '".$send_sel."'";
	$sql["limit"] = "1";
	$sql["op"] = "order by date desc";
	$res = $db->query(mk_sql($sql));
	check_err($res);
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$user["last"] = $row["date"];
	}
	$res->free();
	
	//main track
	$sql = array();
	$sql["dl"] = true;
	$sql["select"] = "*,UNIX_TIMESTAMP(date) as ut";
	$sql["where"] = constant("DB_TABLE_LOG").".id = '".$send_sel."'";
	$sql["op"] = "order by date";
	if(isset($move_p)) $sql["limit"] = $move_p;
	//日付で絞る
	$sql["where"] .= " AND date LIKE '".mk_sql_date($ym, $d)."%'";
	$res = $db->query(mk_sql($sql));
	check_err($res);

	$i = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		//自サイト内リンク削除
		if(strstr($row["ref"], $_SERVER["HTTP_HOST"]) != false){
			$row["ref"] = "";
		}else{
			if($row["ref_q"]) $row["ref"] .= "?".$row["ref_q"];
		}
		
		foreach ($row as $k => $v) {
			$v = trim($v);
			if($v == "") continue;
			$main[$i][$k] = $v;
		}
		$i++;
	}
	$res->free();
}

/*=========================================*/
/* フォーム作成                            */
/*=========================================*/
if($form){
	echo '<form action="'.constant("FILENAME").'" method="get" name="form">'."\n";
	echo '<input type="hidden" name="ym" value="'.$ym.'">';
	echo '<input type="hidden" name="d" value="'.$d.'">';
	echo '<input type="hidden" name="act" value="trackaday">'."\n";
	echo 'ID:&nbsp;<select name="sel">'."\n";
	echo '<option value="">&nbsp;追跡IDを指定して下さい&nbsp;</option>'."\n";
	if($form_chname){
		echo '<option value="" style="background-color:#EEEEEE;">&nbsp;---&nbsp;▽&nbsp;NameUser&nbsp;---</option>'."\n";
		foreach ($form_chname as $k => $v) {
			echo '<option value="'.$k.'">&nbsp;'.$v.'</option>'."\n";
		}
	}
	echo '<option value="" style="background-color:#EEEEEE;">&nbsp;---&nbsp;▽&nbsp;ID/Name&nbsp;---</option>'."\n";
	foreach ($form as $k => $v) {
		if($k == $send_sel){
			echo '<option value="'.$k.'" selected style="background-color:#666;color:#FFF;">&nbsp;&#187;&nbsp;'.$form_chid[$k].'&nbsp;（'.$v.'）</option>'."\n";
		}else{
			echo '<option value="'.$k.'">&nbsp;'.$form_chid[$k].'&nbsp;（'.$v.'）</option>'."\n";
		}
	}
	echo '</select>'."\n";
	echo '&nbsp;<input value="追跡" type="submit">';
	echo '</form><br>';
}

/*=========================================*/
/* ユーザー情報                            */
/*=========================================*/
if($user && $user_env){
	$user["first"] = str_replace("-", "/", $user["first"]);
	$user["first"] = str_replace(" ", "&nbsp;&nbsp;", $user["first"]);
	$user["last"] = str_replace("-", "/", $user["last"]);
	$user["last"] = str_replace(" ", "&nbsp;&nbsp;", $user["last"]);
	
	$user["ip"] = str_replace ("\n", "<br>\n&nbsp;", trim($user["ip"]));
	$user["host"] = str_replace ("\n", "<br>\n&nbsp;", trim($user["host"]));
	
	if($user["color"]) $user["color"] .= "bit";
	
	if($user["os"]) $user["os"] = set_icon($user["os"]).$user["os"];
	#if($user["os_v"] == 'N/A') $user["os_v"] = "";
	
	if($user["browser"]) $user["browser"] = set_icon($user["browser"]).$user["browser"];
	#if($user["browser_v"] == 'N/A') $user["browser_v"] = "";

	
	echo '<table>';
	echo '<tr><th nowrap class="no_th">追跡ユーザーID</th><td>&nbsp;'.$user["name"].'&nbsp;</td></tr>'."\n";
	echo '<tr><th nowrap class="no_th">初回アクセス</th><td>&nbsp;'.$user["first"].'&nbsp;</td></tr>'."\n";
	echo '<tr><th nowrap class="no_th">最新アクセス</th><td>&nbsp;'.$user["last"].'&nbsp;</td></tr>'."\n";
	echo '<tr><th nowrap class="no_th">総アクセス数</th><td>&nbsp;'.$user["count"].'&nbsp;</td></tr>'."\n";
	if($user["ip"])      echo '<tr><th nowrap class="no_th">IP</th><td>&nbsp;'.$user["ip"].'&nbsp;</td></tr>'."\n";
	if($user["host"])    echo '<tr><th nowrap class="no_th">Host</th><td>&nbsp;'.$user["host"].'&nbsp;</td></tr>'."\n";
	if($user["isp"])     echo '<tr><th nowrap class="no_th">ISP</th><td>&nbsp;'.$user["isp"].'&nbsp;</td></tr>'."\n";
	if($user["ua"])      echo '<tr><th nowrap class="no_th">UserAgent</th><td>&nbsp;'.$user["ua"].'&nbsp;</td></tr>'."\n";
	if($user["os"])      echo '<tr><th nowrap class="no_th">OS</th><td>&nbsp;'.$user["os"].'&nbsp;'.$user["os_v"].'&nbsp;</td></tr>'."\n";
	if($user["browser"]) echo '<tr><th nowrap class="no_th">ブラウザ</th><td>&nbsp;'.$user["browser"].'&nbsp;'.$user["browser_v"].'&nbsp;</td></tr>'."\n";
	if($user["monitor"] || $user["color"]) echo '<tr><th nowrap class="no_th">モニタ/色深度</th><td>&nbsp;'.$user["monitor"].'&nbsp;/&nbsp;'.$user["color"].'&nbsp;</td></tr>'."\n";
	if($user["lang"])   echo '<tr><th nowrap class="no_th">言語</th><td>&nbsp;'.($lang_list[$user["lang"]] ? $lang_list[$user["lang"]] : $user["lang"]).'&nbsp;</td></tr>'."\n";
	echo '</table><br>';
}

/*=========================================*/
/* メイン処理                              */
/*=========================================*/
if($main){
	//日付初期化
	$track_day = "";
	//タイムアウト秒化
	$time_out = $time_out*60;
	//基準時間初期化
	$def_time = 0;
	//処理開始
	foreach ($main as $track) {
		list($track["ymd"],$track["his"]) = explode(" ",$track["date"]);
		
		if(!$def_time) $def_time = $track["ut"];
		$time = $track["ut"]-$def_time;
		
		if($track_day != $track["ymd"]){
			// 日付ヘッダ処理
			if($track_day){
				echo '</table><br>';
				$def_time = $track["ut"];
				$time = $track["ut"]-$def_time;
			}
			echo '<b>'.$track["ymd"].'</b>';
			echo '<table width="100%">';
			echo '<tr>';
			echo '<th nowrap width="70">時間</th>';
			echo '<th nowrap>ページ</th>';
			echo '</tr>';
			$track_day = $track["ymd"];
		}else{
			$def_time = $track["ut"];
			//タイムアウト処理
			if($time >= $time_out){
				$time = $track["ut"]-$def_time;
				echo '<tr><td colspan=3 class="lite">'.set_img('image/timeout.gif').'&nbsp;<font color="#FF0000"><b>TimeOut</b></font></td></tr>';
			}
		}
		//時間変換
		$time_h = floor($time/3600);
		if($time_h) $time -= $time_h*3600;
		$time_m = floor($time/60);
		if($time_m) $time -= $time_m*60;
		$time_s = $time;
		$view_time = "";
		if($time_h) $view_time .= $time_h."時間";
		if($time_m) $view_time .= $time_m."分";
		if($time_s) $view_time .= $time_s."秒";
		if($view_time) echo '<tr><td colspan=2 class="lite">'.set_img('image/down.gif').'&nbsp;'.$view_time.'</td></tr>';
		//生成
		if(isset($track["dl"])){
			echo '<tr>';
			echo '<td width="70" align="center">'.$track["his"]."</td>\n";
			echo '<td nowrap>'.set_img('image/dl.gif').'&nbsp;'.$track["dl"]."</td>\n";
			echo '</tr>';
		}else{
			if(!isset($track["ref"])){
				$track["ref"] = "none";
			}else{
				//JUMP
				$jump = './inc/redirect.php?redirect='.$track["ref"];
				
				$track["ref"] = urldecode($track["ref"]);
				if(function_exists('mb_convert_encoding')){
					$track["ref"] = @mb_convert_encoding($track["ref"], "UTF-8", "auto");
				}
				$track["ref"] = str_replace('+', ' ', $track["ref"]);
				if(function_exists('mb_ereg_replace')){
					$track["ref"] = @mb_ereg_replace('　', ' ', $track["ref"]);
					$track["ref"] = @mb_ereg_replace(' {2,}', ' ', $track["ref"]);
				}
				$track["ref"] = trim($track["ref"]);
				$track["ref"] = '<a href="'.$jump.'" target="_blank">'.set_img(constant("ICON_JUMP"),"ジャンプ").'&nbsp;'.$track["ref"].'</a>';
			}
			foreach ($track as $k => $v) {
				$v = trim($v);
				if($v == "" && $k != "title") $v = "&nbsp;";
				$track[$k] = $v;
			}
			//タイトル追加
			if(isset($track["path"])){
				if(isset($track["title"])) $track["path"] = "[&nbsp;".str_cut($track["title"])."&nbsp;]<br>".$track["path"];
			}else{
				$track["path"] = "unknown path";
			}
			echo '<tr>';
			echo '<td width="70" align="center">'.$track["his"]."</td>\n";
			echo '<td nowrap>'.$track["path"]."</td>\n";
			echo '</tr>';
			
			if($track["ref"] != "none"){
				echo '<tr>';
				echo '<th width="70" nowrap class="no_th">リンク元</th>'."\n";
				echo '<td>'.$track["ref"]."</td>\n";
				echo '</tr>';
			}
		}
	}
	echo '</table>';
	if(isset($move)) echo $move;
}
?>