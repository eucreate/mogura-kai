<div class="title">クローラ追跡&nbsp;&nbsp;<small>［全期間］</small></div>
<?php
/*=========================================*/
/* mogura     Plug-in【クローラ追跡】      */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

/*=========================================*/
/* 設定                                    */
/*=========================================*/
//タイムアウト(分)
$time_out = 30;

/*=========================================*/
/* 初期化                                  */
/*=========================================*/
$sql = array();
$main = array();
$sel_date = array();
$form = array();

/*=========================================*/
/* FORM                                    */
/*=========================================*/
$sql = array();
$main = array();
$sql["from"] = "(".constant("DB_TABLE_LOG")." left join ".constant("DB_TABLE_UA")
." on ".constant("DB_TABLE_LOG").".ua = ".constant("DB_TABLE_UA").".ua_id)";
$sql["select"] = "COUNT(*) as pv,os_v";
$sql["group"] = "os_v";
$sql["where"] = "os = 'Crawler'";
$sql["craw"] = true;
$sql["op"] = "order by pv desc";

$res = $db->query(mk_sql($sql));
check_err($res);

$total_pv = 0;
$i = 0;
while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	$form[$i] = $row;
	$total_pv += $row["pv"];
	$i++;
}
$res->free();
/*=========================================*/
/* フォーム作成                            */
/*=========================================*/
if($form){
	echo '<form action="'.constant("FILENAME").'" method="get" name="form">'."\n";
	echo '<input type="hidden" name="ym" value="'.$ym.'">';
	echo '<input type="hidden" name="d" value="'.$d.'">';
	echo '<input type="hidden" name="act" value="bot">'."\n";
	echo 'Crawler:&nbsp;<select name="sel">'."\n";
	echo '<option value="" style="background-color:#EEEEEE;">&nbsp;追跡クローラを指定して下さい&nbsp;</option>'."\n";
	echo '<option value="">&nbsp;---</option>'."\n";
	foreach ($form as $cl) {
		$cl["os_v"];
		if($cl["os_v"] == $send_sel){
			echo '<option value="'.$cl["os_v"].'" selected style="background-color:#666;color:#FFF;">&nbsp;&#187;&nbsp;'.$cl["os_v"].'&nbsp;('.$cl["pv"].')</option>'."\n";
		}else{
			echo '<option value="'.$cl["os_v"].'">&nbsp;'.$cl["os_v"].'&nbsp;('.$cl["pv"].')</option>'."\n";
		}
	}
	echo '</select>'."\n";
	echo '&nbsp;<input value="追跡" type="submit">';
	echo '</form><br>';
}else{
	echo '<div id="error">クローラの訪問履歴はありません。</div>';
}


/*=========================================*/
/* SQL - MAIN                              */
/*=========================================*/
if($send_sel){
	//move
	$sql = array();
	$sql["select"] = "COUNT(*) as max_val,os_v";
	$sql["from"] = "(".constant("DB_TABLE_LOG")." left join ".constant("DB_TABLE_UA")
	." on ".constant("DB_TABLE_LOG").".ua = ".constant("DB_TABLE_UA").".ua_id)";
	$sql["group"] = "os_v";
	$sql["where"] = "os_v = '".$send_sel."'";
	$sql["craw"] = true;
	$res = $db->query(mk_sql($sql));
	check_err($res);
	$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
	$max_val = $row["max_val"];
	$res->free();
	
	if($send_p !== "all"){
		$move_p = $send_p." , ".constant("LIMIT");
		$move = move_bt($send_p, $max_val);
	}
	
	//select date
	$sql = array();
	$sql["from"] = "(".constant("DB_TABLE_LOG")." left join ".constant("DB_TABLE_UA")
	." on ".constant("DB_TABLE_LOG").".ua = ".constant("DB_TABLE_UA").".ua_id)";
	$sql["select"] = "DISTINCT DATE_FORMAT(date, '%Y-%m-%d') as ymd,os_v";
	$sql["where"] = "os_v = '".$send_sel."'";
	$sql["craw"] = true;
	$sql["op"] = "order by date desc";
	
	$res = $db->query(mk_sql($sql));
	check_err($res);
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$sel_date[] = $row["ymd"];
	}
	$res->free();
	
	//main track
	$sql = array();
	$sql["from"] = "(".constant("DB_TABLE_LOG")." left join ".constant("DB_TABLE_UA")
	." on ".constant("DB_TABLE_LOG").".ua = ".constant("DB_TABLE_UA").".ua_id)";
	$sql["select"] = "*,UNIX_TIMESTAMP(date) as ut,os_v";
	$sql["where"] = "os_v = '".$send_sel."'";
	$sql["craw"] = true;
	$sql["op"] = "order by date";
	if(isset($move_p)) $sql["limit"] = $move_p;
	if(isset($send_op)) $sql["where"] .= " AND date LIKE '".$send_op."%'";

	$res = $db->query(mk_sql($sql));
	check_err($res);

	$i = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		//自サイト内リンク削除
		if(strstr($row["ref"], $_SERVER["HTTP_HOST"]) != false){
			$row["ref"] = "";
		}else{
			if($row["ref_q"]) $row["ref"] .= "?".$row["ref_q"];
		}
		
		foreach ($row as $k => $v) {
			$v = trim($v);
			if($v == "") continue;
			$main[$i][$k] = $v;
		}
		$i++;
	}
	$res->free();
}



/*=========================================*/
/* 日付指定                                */
/*=========================================*/
if($sel_date){
	echo '<form action="'.constant("FILENAME").'" method="get" name="form">'."\n";
	echo '<input type="hidden" name="ym" value="'.$ym.'">';
	echo '<input type="hidden" name="d" value="'.$d.'">';
	echo '<input type="hidden" name="act" value="bot">'."\n";
	echo '<input type="hidden" name="sel" value="'.$send_sel.'">'."\n";
	echo '<input type="hidden" name="p" value="all">'."\n";
	echo '指定日解析:&nbsp;<select name="op">'."\n";
	echo '<option value="" style="background-color:#EEEEEE;">&nbsp;---&nbsp;▽&nbsp;解析日&nbsp;---&nbsp;</option>'."\n";
	foreach ($sel_date as $v) {
		echo '<option value="'.$v.'">&nbsp;'.$v.'</option>'."\n";
	}
	echo '</select>'."\n";
	echo '&nbsp;<input value="view" type="submit">';
	echo '</form><br>';
}


/*=========================================*/
/* メイン処理                              */
/*=========================================*/
if($main){
	echo "<hr size=1>\n";
	echo "■ Crawler［&nbsp;<b>".$send_sel."</b>&nbsp;］の追跡<br><br>\n";
	
	//日付初期化
	$track_day = "";
	//タイムアウト秒化
	$time_out = $time_out*60;
	//基準時間初期化
	$def_time = 0;
	//処理開始
	foreach ($main as $track) {
		list($track["ymd"],$track["his"]) = explode(" ",$track["date"]);
		
		if(!$def_time) $def_time = $track["ut"];
		$time = $track["ut"]-$def_time;
		
		if($track_day != $track["ymd"]){
			if($track_day){
				echo '</table><br>';
				$def_time = $track["ut"];
				$time = $track["ut"]-$def_time;
			}
			echo '<b>'.$track["ymd"].'</b>';
			echo '<table width="100%">';
			echo '<tr>';
			echo '<th nowrap width="100">時間</th>';
			echo '<th nowrap>ページ</th>';
			echo '</tr>';
			$track_day = $track["ymd"];
		}else{
			$def_time = $track["ut"];
			//タイムアウト処理
			if($time >= $time_out){
				$time = $track["ut"]-$def_time;
				echo '<tr><td colspan=3 class="lite">'.set_img('image/timeout.gif').'&nbsp;<font color="#FF0000"><b>TimeOut</b></font></td></tr>';
			}
		}

		//時間変換
		$time_h = floor($time/3600);
		if($time_h) $time -= $time_h*3600;
		$time_m = floor($time/60);
		if($time_m) $time -= $time_m*60;
		$time_s = $time;
		$view_time = "";
		if($time_h) $view_time .= $time_h."時間";
		if($time_m) $view_time .= $time_m."分";
		if($time_s) $view_time .= $time_s."秒";
		if($view_time) echo '<tr><td colspan=2 class="lite">'.set_img('image/down.gif').'&nbsp;'.$view_time.'</td></tr>';
		//生成
		if(!isset($track["ref"])){
			$track["ref"] = "none";
		}else{
			//JUMP
			$jump = './inc/redirect.php?redirect='.$track["ref"];
			
			$track["ref"] = urldecode($track["ref"]);
			if(function_exists('mb_convert_encoding')){
				$track["ref"] = @mb_convert_encoding($track["ref"], "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
			}
			$track["ref"] = str_replace('+', ' ', $track["ref"]);
			if(function_exists('mb_ereg_replace')){
				$track["ref"] = @mb_ereg_replace('　', ' ', $track["ref"]);
				$track["ref"] = @mb_ereg_replace(' {2,}', ' ', $track["ref"]);
			}
			$track["ref"] = trim($track["ref"]);
			$track["ref"] = '<a href="'.$jump.'" target="_blank">'.set_img(constant("ICON_JUMP"),"ジャンプ").'&nbsp;'.$track["ref"].'</a>';
		}
		foreach ($track as $k => $v) {
			$v = trim($v);
			if($v == "" && $k != "title") $v = "&nbsp;";
			$track[$k] = $v;
		}
		//タイトル追加
		if(isset($track["path"])){
			if(isset($track["title"])) $track["path"] = "[&nbsp;".str_cut($track["title"])."&nbsp;]<br>".$track["path"];
		}else{
			$track["path"] = "unknown path";
		}
		echo '<tr>';
		echo '<td width="70" align="center">'.$track["his"]."</td>\n";
		echo '<td nowrap>'.$track["path"]."</td>\n";
		echo '</tr>';
		
		if($track["ua_name"]){
			echo '<tr>';
			echo '<th class="no_th" nowrap width="100">UserAgent</th>'."\n";
			echo '<td>'.$track["ua_name"]."</td>\n";
			echo '</tr>';
		}
		if($track["host"]){
			echo '<tr>';
			echo '<th class="no_th" nowrap width="100">Host</th>'."\n";
			echo '<td>'.$track["host"]."</td>\n";
			echo '</tr>';
		}
	}
	echo '</table>';
	if(isset($move)) echo $move;
}
?>