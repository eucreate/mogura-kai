<div class="title">ページ</div>
<?php
/*=========================================*/
/* mogura     Plug-in【ページ】            */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

if(defined("SELECT_LOG_FLG")){
	$where = "date LIKE '".mk_sql_date($ym, $d)."%' AND path = '".$send_sel."'";
}else{
	/*=========================================*/
	/* 設定                                    */
	/*=========================================*/
	// ■ ページ指定用のアイコンを表示
	// 1 -> する , 0 -> しない
	$p_sel_icon = 1;

	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	$sql = array();
	$main = array();

	/*=========================================*/
	/* SQL                                     */
	/*=========================================*/
	$sql["select"] = "path,title,COUNT(*) as pv,COUNT(DISTINCT id) as uniq";
	$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%' AND path IS NOT NULL";
	$sql["group"] = "path";
	$sql["sort"] = true;

	$res = $db->query(mk_sql($sql));
	check_err($res);

	$max_int = 0;
	$i = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$main[$i]["path"] = $row["path"];
		$main[$i]["title"] = $row["title"];
		$main[$i]["pv"] = $row["pv"];
		$main[$i]["uniq"] = $row["uniq"];
		if($max_int < $row["pv"]) $max_int = $row["pv"];
		$i++;
	}
	$res->free();

	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	if($main){
		echo '<table width="100%" cellpadding="0" cellspacing="0">';
		echo '<tr>';
		echo '<th nowrap>ページ</th>';
		mk_pu();
		echo '<th nowrap>グラフ</th>';
		echo '</tr>';
		foreach($main as $k => $v){
			if($p_sel_icon) $sel_path = '<a href="'.query_edit("p_sel",$v["path"]).'">'.set_img(constant("ICON_SELPATH"),"ページ指定").'</a>&nbsp;';

			//Pathタイトル変換
			$path_title = $v["title"] ? "<br>&nbsp;".$v["title"] : "";
			//link
			$link = $sel_path
					."［&nbsp;".sel_link(query_edit("sel",$v["path"]),$v["path"])."&nbsp;］"
					.$path_title;
			
			echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
			echo '<td>&nbsp;'.$link.'&nbsp;</td>';
			echo '<td width="70" align="right">&nbsp;<font color="#0000FF">'.$v["uniq"].'</font>&nbsp;</td>';
			echo '<td width="70" align="right">&nbsp;<font color="#FF0000">'.$v["pv"].'</font>&nbsp;</td>';
			echo '<td>&nbsp;'.mk_graph($v["pv"],$v["uniq"],$max_int).'&nbsp;</td>';
			echo '</tr>'."\n";
		}
		echo '</table>';
	}else{
		echo '<div id="error">解析ログが見つかりません。</div>';
	}
}
?>