<div class="title">日別&nbsp;&nbsp;<small>［月合計］</small></div>
<?php
/*=========================================*/
/* mogura     Plug-in【日別】              */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

if(defined("SELECT_LOG_FLG")){
	if(isset($_GET["seld_t"]) && isset($_GET["seld_e"])){
		$where = "date LIKE 'r' AND DAYOFMONTH(date)=".$send_sel;
	}else{
		$where = "date LIKE '".mk_sql_date($ym, sprintf('%02d', $send_sel))."%'";
	}
}else{
	/*=========================================*/
	/* 設定                                    */
	/*=========================================*/
	// ■ カレンダーの表示
	// 1 -> する , 0 -> しない
	$day_cal = 0;

	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	$sql = array();
	$main = array();
	for($i=1; $i <= date("t", mktime(0, 0, 0, $month, 1, $year)); $i++){
		$main[$i]["pv"] = 0;
		$main[$i]["uniq"] = 0;
	}


	/*=========================================*/
	/* SQL                                     */
	/*=========================================*/
	$sql["select"] = "DAYOFMONTH(date) as day,COUNT(*) as pv,COUNT(DISTINCT id) as uniq";
	$sql["where"] = "date LIKE '".mk_sql_date($ym)."%'";
	$sql["group"] = "day";
	$res = $db->query(mk_sql($sql));
	check_err($res);

	$max_int = 0;
	$total_uniq = 0;
	$total_pv = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$main[$row["day"]]["pv"] = $row["pv"];
		$main[$row["day"]]["uniq"] = $row["uniq"];
		if($max_int < $row["pv"]) $max_int = $row["pv"];
		
		$total_pv += $row["pv"];
		$total_uniq += $row["uniq"];
	}
	$res->free();


	/*=========================================*/
	/* カレンダー生成                          */
	/*=========================================*/
	if($day_cal){
	// 1日目の曜日
	$wday = date("w", mktime(0, 0, 0, $month, 1, $year));
// Header
echo <<<CAL
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<th>日曜日</th>
<th>月曜日</th>
<th>火曜日</th>
<th>水曜日</th>
<th>木曜日</th>
<th>金曜日</th>
<th>土曜日</th>
</tr>
<tr align="center" valign="top">
CAL;

	// Before Blank
	for ($i=0; $i < $wday; $i++) { 
		echo '<td class="cal_blank">&nbsp;</td>'."\n"; 
	}

	// Calendar Create
	$day = 1;
	while(checkdate($month,$day,$year)){
		//ユニーク値
		$pv = $main[$day]["pv"];
		$uniq = $main[$day]["uniq"];
		
		$value = "";
		if($pv){
			$value = "<b>".$day."</b>"
						.'<br>&nbsp;<font color="#0000FF">'.$uniq.'</font>&nbsp;'
						.'<br>&nbsp;<font color="#FF0000">'.$pv.'</font>&nbsp;';
		}else{
			$value = "<b>".$day."</b>";
		}
		
		echo '<td width="50" height="50">'.$value."</td>\n";
		// 改行
		if($wday == 6) echo '</tr>'."\n".'<tr align=center valign="top">';
		$day++;
		$wday++;
		$wday = $wday % 7;
	}

	// After Blank
	if($wday > 0){
		while($wday < 7){
			echo '<td class="cal_blank">&nbsp;</td>'."\n";
			$wday++;
		}
	}else{
		echo "<td colspan=\"7\"></td>\n";
	}
// Footer
echo <<<CAL
</tr>
</table>
<br>
CAL;
	}


	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	echo '<table width="100%" cellpadding="0" cellspacing="0">';
	echo '<tr>';
	echo '<th nowrap width="50">日</th>';
	echo '<th nowrap width="100">ユニークユーザ</th>';
	echo '<th nowrap width="100">ページビュー</th>';
	echo '<th nowrap>グラフ</th>';
	echo '</tr>'."\n";
	foreach($main as $k => $v){
		//ユニーク値
		$pv = $v["pv"];
		$uniq = $v["uniq"];
		//link
		$link = sel_link(query_edit("sel",$k),$k);
		
		echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
		echo '<td align="right" width="50">'.$link.'</td>';
		echo '<td align="right" width="100"><font color="#0000FF">'.$uniq.'</font></td>';
		echo '<td align="right" width="100"><font color="#FF0000">'.$pv.'</font></td>';
		echo '<td align="left">'.mk_graph($pv,$uniq,$max_int).'</td>';
		echo '</tr>'."\n";
	}
	echo '<tr class="bg_total">';
	echo '<td align="right" width="50"><b>合計：</b></td>';
	echo '<td align="right" width="100"><b><font color="#0000FF">'.$total_uniq.'</font></b></td>';
	echo '<td align="right" width="100" class="bg_total"><b><font color="#FF0000">'.$total_pv.'</font></b></td>';
	echo '<td>&nbsp;</td>';
	echo '</tr>'."\n";
	echo '</table>';
}
?>
