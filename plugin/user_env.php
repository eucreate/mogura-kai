<div class="title">ユーザー環境</div>
<?php
/*=========================================*/
/* mogura     Plug-in【ユーザー環境】      */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/


/*=========================================*/
/* Javascript有効率                        */
/*=========================================*/
$sql = array();
$main = array(0 => 0, 1 => 0, 2 => 0);

$sql["select"] = "js,COUNT(DISTINCT id) as on_off";
$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%'";
$sql["group"] = "js";
$sql["op"] = "order by on_off desc";


$res = $db->query(mk_sql($sql));
check_err($res);

$max_int = 0;
$total = 0;
while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	if(!$row["js"]) $row["js"] = 0;
	$main[$row["js"]] = $row["on_off"];
	if($max_int < $row["on_off"]) $max_int = $row["on_off"];
	
	$total += $row["on_off"];
}
$res->free();
echo 'Javascript有効率';
echo '<table width="100%" cellpadding="0" cellspacing="0">';
echo '<tr>';
echo '<th nowrap width="100">JavaScript</th>';
echo '<th nowrap width="100">ユーザー数</th>';
echo '<th nowrap>グラフ</th>';
echo '</tr>'."\n";
foreach($main as $k => $v){
	switch ($k) {
	case 1: $js = "ON";    break;
	case 2: $js = "OFF";   break;
	default: $js = "不明"; break;
	}
	//link
	echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
	echo '<td width="100">'.$js.'</td>';
	echo '<td width="100" align="right"><font color="#FF0000">'.$v.'</font></td>';
	echo '<td align="left">'.mk_graph_01($v,$max_int,$total).'</td>';
	echo '</tr>'."\n";
}
echo '<tr class="bg_total">';
echo '<td width="100" align="right"><b>合計：</b></td>';
echo '<td width="100" align="right"><b><font color="#FF0000">'.$total.'</font></b></td>';
echo '<td align="left">&nbsp;</td>';
echo '</tr>'."\n";
echo '</table><br>';


/*=========================================*/
/* Cookie有効率                            */
/*=========================================*/
$sql = array();
$main = array(0 => 0, 1 => 0, 2 => 0);

$sql["select"] = "cookie,COUNT(DISTINCT id) as on_off";
$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%'";
$sql["group"] = "cookie";
$sql["op"] = "order by on_off desc";

$res = $db->query(mk_sql($sql));
check_err($res);

$max_int = 0;
$total = 0;
while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	if(!$row["cookie"]) $row["cookie"] = 0;
	$main[$row["cookie"]] = $row["on_off"];
	if($max_int < $row["on_off"]) $max_int = $row["on_off"];
	
	$total += $row["on_off"];
}
$res->free();

echo 'Cookie有効率';
echo '<table width="100%" cellpadding="0" cellspacing="0">';
echo '<tr>';
echo '<th nowrap width="100">Cookie</th>';
echo '<th nowrap width="100">ユーザー数</th>';
echo '<th nowrap>グラフ</th>';
echo '</tr>'."\n";
foreach($main as $k => $v){
	switch ($k) {
	case 1: $co = "ON";    break;
	case 2: $co = "OFF";   break;
	default: $co = "不明"; break;
	}
	//link
	echo '<tr'.tr_color($c).' id="bg_id2'.$k.'" onmouseover="chBG(\'bg_id2'.$k.'\', 1);" onmouseout="chBG(\'bg_id2'.$k.'\', 0);">';
	echo '<td width="100">'.$co.'</td>';
	echo '<td width="100" align="right"><font color="#FF0000">'.$v.'</font></td>';
	echo '<td align="left">'.mk_graph_01($v,$max_int,$total).'</td>';
	echo '</tr>'."\n";
}
echo '<tr class="bg_total">';
echo '<td width="100" align="right"><b>合計：</b></td>';
echo '<td width="100" align="right"><b><font color="#FF0000">'.$total.'</font></b></td>';
echo '<td align="left">&nbsp;</td>';
echo '</tr>'."\n";
echo '</table><br>';

/*=========================================*/
/* モニター解像度                          */
/*=========================================*/
$sql = array();
$main = array();

$sql["select"] = "monitor,COUNT(DISTINCT id) as uniq";
$sql["select"] .= ",CASE WHEN ISNULL(monitor) THEN '不明' ELSE monitor END monitor";
$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%'";
$sql["group"] = "monitor";
$sql["op"] = "order by uniq desc";


$res = $db->query(mk_sql($sql));
check_err($res);

$max_int = 0;
$total = 0;
while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	if(!$row["monitor"]) continue;
	$main[$row["monitor"]] = $row["uniq"];
	if($max_int < $row["uniq"]) $max_int = $row["uniq"];
	
	$total += $row["uniq"];
}
$res->free();

if($main){
	echo 'モニター解像度';
	echo '<table width="100%" cellpadding="0" cellspacing="0">';
	echo '<tr>';
	echo '<th nowrap width="100">Monitor</th>';
	echo '<th nowrap width="100">ユーザー数</th>';
	echo '<th nowrap>グラフ</th>';
	echo '</tr>'."\n";
	foreach($main as $k => $v){
		//link
		echo '<tr'.tr_color($c).' id="bg_id3'.$k.'" onmouseover="chBG(\'bg_id3'.$k.'\', 1);" onmouseout="chBG(\'bg_id3'.$k.'\', 0);">';
		echo '<td width="100">'.$k.'</td>';
		echo '<td width="100" align="right"><font color="#FF0000">'.$v.'</font></td>';
		echo '<td align="left">'.mk_graph_01($v,$max_int,$total).'</td>';
		echo '</tr>'."\n";
	}
	echo '<tr class="bg_total">';
	echo '<td width="100" align="right"><b>合計：</b></td>';
	echo '<td width="100" align="right"><b><font color="#FF0000">'.$total.'</font></b></td>';
	echo '<td align="left">&nbsp;</td>';
	echo '</tr>'."\n";
	echo '</table><br>';
}

/*=========================================*/
/* カラー深度                              */
/*=========================================*/
$sql = array();
$main = array();

$sql["select"] = "color,COUNT(DISTINCT id) as uniq";
$sql["select"] .= ",CASE WHEN ISNULL(color) THEN '不明' ELSE CONCAT(color,' bit') END color";
$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%'";
$sql["group"] = "color";
$sql["op"] = "order by uniq desc";


$res = $db->query(mk_sql($sql));
check_err($res);

$max_int = 0;
$total = 0;
while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	if(!$row["color"]) continue;
	$main[$row["color"]] = $row["uniq"];
	if($max_int < $row["uniq"]) $max_int = $row["uniq"];
	
	$total += $row["uniq"];
}
$res->free();

if($main){
	echo 'カラー深度';
	echo '<table width="100%" cellpadding="0" cellspacing="0">';
	echo '<tr>';
	echo '<th nowrap width="100">Color</th>';
	echo '<th nowrap width="100">ユーザー数</th>';
	echo '<th nowrap>グラフ</th>';
	echo '</tr>'."\n";
	foreach($main as $k => $v){
		//link
		echo '<tr'.tr_color($c).' id="bg_id4'.$k.'" onmouseover="chBG(\'bg_id4'.$k.'\', 1);" onmouseout="chBG(\'bg_id4'.$k.'\', 0);">';
		echo '<td width="100">'.$k.'</td>';
		echo '<td width="100" align="right"><font color="#FF0000">'.$v.'</font></td>';
		echo '<td align="left">'.mk_graph_01($v,$max_int,$total).'</td>';
		echo '</tr>'."\n";
	}
	echo '<tr class="bg_total">';
	echo '<td width="100" align="right"><b>合計：</b></td>';
	echo '<td width="100" align="right"><b><font color="#FF0000">'.$total.'</font></b></td>';
	echo '<td align="left">&nbsp;</td>';
	echo '</tr>'."\n";
	echo '</table>';
}
?>