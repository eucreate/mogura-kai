<div class="title">地域</div>
<?php
/*=========================================*/
/* mogura     Plug-in【地域】              */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

/*=========================================*/
/* 設定                                    */
/*=========================================*/
// ■ 不明の表示
// 1 -> する , 0 -> しない
$no_area = 0;


/*=========================================*/
/* リスト                                  */
/*=========================================*/
include_once(constant("DIR_LIST")."area.php");

//ソート用
function cmp($a , $b){
	if (strlen($a) == strlen($b)) return 0;
	return (strlen($a) > strlen($b)) ? -1 : 1;
}
uksort($area_list , "cmp");


if(defined("SELECT_LOG_FLG")){
	$where = "date LIKE '".mk_sql_date($ym, $d)."%' AND\n";
	$where .= "(";
	foreach($area_list as $k => $v){
		if($v != $send_sel) continue;
		$where .= "host LIKE '".$k."' OR ";
	}
	$where = "(".substr($where, 0, -4).")";
	$where .= ")";
}else{
	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	$sql = array();
	$main = array();
	/*=========================================*/
	/* エリア判別リスト                        */
	/*=========================================*/
	$area_sql = ",CASE ";
	foreach($area_list as $k => $v){
		if(function_exists('mysql_escape_string')) $k = mysql_escape_string($k);
		$area_sql .= "WHEN (host LIKE '".$k."') THEN '".$v."' ";
	}
	if($no_area) $area_sql .= " ELSE '99'";
	$area_sql .= " END area";
	/*=========================================*/
	/* SQL                                     */
	/*=========================================*/
	$sql["select"] = "COUNT(*) as pv,COUNT(DISTINCT id) as uniq".$area_sql;
	$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%'";
	$sql["group"] = "area";
	$sql["having"] = "area IS NOT NULL";
	$sql["sort"] = true;
	
	$res = $db->query(mk_sql($sql));
	check_err($res);

	$total_pv = 0;
	$total_uniq = 0;
	$max_int = 0;
	$i = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$main[$i]["area"] = $row["area"];
		$main[$i]["pv"] = $row["pv"];
		$main[$i]["uniq"] = $row["uniq"];
		if($max_int < $row["pv"]) $max_int = $row["pv"];

		$total_pv += $row["pv"];
		$total_uniq += $row["uniq"];

		$i++;
	}
	$res->free();
	
	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	if($main){
		echo '<table width="100%" cellpadding="0" cellspacing="0">';
		echo '<tr>';
		echo '<th nowrap>地域</th>';
		mk_pu();
		echo '<th nowrap>グラフ</th>';
		echo '</tr>';
		foreach($main as $k => $v){
			$pv = $v["pv"];
			$uniq = $v["uniq"];
			//link
			$link = sel_link(query_edit("sel",$v["area"]),$v["area"]);
			
			echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
			echo '<td>'.$link.'</td>';
			echo '<td width="70" align="right"><font color="#0000FF">'.$uniq.'</font></td>';
			echo '<td width="70" align="right"><font color="#FF0000">'.$pv.'</font></td>';
			echo '<td align="left">'.mk_graph($pv,$uniq,$max_int).'</td>';
			echo '</tr>';
		}
		//合計
		echo '<tr class="bg_total">';
		echo '<td align="right"><b>合計：</b></td>';
		echo '<td align="right" width="70"><b><font color="#0000FF">'.$total_uniq.'</font></b></td>';
		echo '<td align="right" width="70"><b><font color="#FF0000">'.$total_pv.'</font></b></td>';
		echo '<td>&nbsp;</td>';
		echo '</tr>'."\n";
		echo '</table>';
	}else{
		echo '<div id="error">解析ログが見つかりません。</div>';
	}
}
?>