<div class="title">時間別</div>
<?php
/*=========================================*/
/* mogura     Plug-in【時間別】            */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

if(defined("SELECT_LOG_FLG")){
	if(isset($_GET["seld_t"]) && isset($_GET["seld_e"])){
		$where = "date LIKE 'r' AND HOUR(date)=".$send_sel;
	}elseif($d == 99){
		$where = "date LIKE '".mk_sql_date($ym, $d)."%' AND HOUR(date)=".$send_sel;
	}else{
		$where = "date LIKE '".mk_sql_date($ym, $d)." ".sprintf('%02d', $send_sel)."%'";
	}
}else{
	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	$sql = array();
	$main = array();

	/*=========================================*/
	/* SQL                                     */
	/*=========================================*/
	$sql["select"] = "HOUR(date) as hour,COUNT(*) as pv,COUNT(DISTINCT id) as uniq";
	$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%'";
	$sql["group"] = "hour";

	$res = $db->query(mk_sql($sql));
	check_err($res);

	$max_int = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$main[$row["hour"]]["pv"] = $row["pv"];
		$main[$row["hour"]]["uniq"] = $row["uniq"];
		if($max_int < $row["pv"]) $max_int = $row["pv"];
	}
	$res->free();

	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	if($main){
		$tmp = array("time" => "","main" => "");
		for($i=0; $i < 24; $i++){
			//時間
			$tmp["time"] .= '<th width="20" nowrap>'.$i.'</th>'."\n";
			
			$tmp["main"] .= '<td>';
			//アクセス数
			if(isset($main[$i])){
				$pv = $main[$i]["pv"];
				$uniq = $main[$i]["uniq"];
			}else{
				$pv = 0;
				$uniq = 0;
			}
			
			//グラフ
			$tmp["main"] .= mk_graph_h($pv,$uniq,$max_int);
			$tmp["main"] .= '</td>'."\n";
		}
		//表示
		echo '<div id="time_w">';
		echo '<table border="0" cellpadding="0" cellspacing="0">';
		echo '<tr align="center" valign="bottom">'.$tmp["main"].'</tr>';
		echo '<tr>'.$tmp["time"].'</tr>';
		echo '</table>';
		echo '</div>';

		echo '<br>';
		
		$dd = array();
		$dd["am"]["t"] = '<th nowrap>HOUR</th>';
		$dd["am"]["pv"] = '<th nowrap>ページビュー</th>';
		$dd["am"]["uniq"] = '<th nowrap>ユニークユーザ</th>';
		$dd["pm"]["t"] = '<th nowrap>HOUR</th>';
		$dd["pm"]["pv"] = '<th nowrap>ページビュー </th>';
		$dd["pm"]["uniq"] = '<th nowrap>ユニークユーザ</th>';
		
		for($i=0; $i < 24; $i++){
			//link
			$link = sel_link(query_edit("sel",$i),$i);
			
			//時間
			$ap = ($i < 12 ? "am" : "pm");
			
			$dd[$ap]["t"] .= '<th width="30" nowrap>'.$link.'</th>'."\n";
			//アクセス数
			if(isset($main[$i])){
				$pv = $main[$i]["pv"];
				$uniq = $main[$i]["uniq"];
			}else{
				$pv = 0;
				$uniq = 0;
			}
			$dd[$ap]["pv"] .= '<td><font color="#FF0000">'.$pv.'</font></td>';
			$dd[$ap]["uniq"] .= '<td><font color="#0000FF">'.$uniq.'</font></td>';
		}
		//表示
		echo 'AM<table>';
		echo '<tr>'.$dd["am"]["t"].'</tr>';
		echo '<tr>'.$dd["am"]["pv"].'</tr>';
		echo '<tr>'.$dd["am"]["uniq"].'</tr>';
		echo '</table>';
		echo 'PM<table>';
		echo '<tr>'.$dd["pm"]["t"].'</tr>';
		echo '<tr>'.$dd["pm"]["pv"].'</tr>';
		echo '<tr>'.$dd["pm"]["uniq"].'</tr>';
		echo '</table>';
	}else{
		echo '<div id="error">解析ログが見つかりません。</div>';
	}
}
?>
