<div class="title">曜日別&nbsp;&nbsp;<small>［月合計］</small></div>
<?php
/*=========================================*/
/* mogura     Plug-in【曜日別】            */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

if(defined("SELECT_LOG_FLG")){
	$where = "date LIKE '".mk_sql_date($ym)."%' AND DAYOFWEEK(date)=".$send_sel;
}else{
	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	$sql = array();
	//曜日別用配列 初期化
	for($i=1; $i <= 7; $i++){
		$main[$i]["pv"] = 0;
		$main[$i]["uniq"] = 0;
	}

	//曜日変換配列
	$youbi = array("1" => "日","2" => "月","3" => "火","4" => "水","5" => "木","6" => "金","7" => "土");


	/*=========================================*/
	/* SQL                                     */
	/*=========================================*/
	$sql["select"] = "DAYOFWEEK(date) as week,COUNT(*) as pv,COUNT(DISTINCT id) as uniq";
	$sql["where"] = "date LIKE '".mk_sql_date($ym)."%'";
	$sql["group"] = "week";

	$res = $db->query(mk_sql($sql));
	check_err($res);

	$max_int = 0;
	$total_uniq = 0;
	$total_pv = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$main[$row["week"]]["pv"] = $row["pv"];
		$main[$row["week"]]["uniq"] = $row["uniq"];
		if($max_int < $row["pv"]) $max_int = $row["pv"];
		
		$total_pv += $row["pv"];
		$total_uniq += $row["uniq"];
		
	}
	$res->free();

	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	echo '<table width="100%" cellpadding="0" cellspacing="0">';
	echo '<tr>';
	echo '<th nowrap width="50">日</th>';
	echo '<th nowrap width="100">ユニークユーザ</th>';
	echo '<th nowrap width="100">ページビュー</th>';
	echo '<th nowrap>グラフ</th>';
	echo '</tr>'."\n";
	foreach($main as $k => $v){
		//ユニーク値
		$pv = $v["pv"];
		$uniq = $v["uniq"];
		//link
		$link = sel_link(query_edit("sel",$k),$youbi[$k]);
		
		echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
		echo '<td width="50">&nbsp;'.$link.'&nbsp;</td>';
		echo '<td width="100" align="right">&nbsp;<font color="#0000FF">'.$uniq.'</font>&nbsp;</td>';
		echo '<td width="100" align="right">&nbsp;<font color="#FF0000">'.$pv.'</font>&nbsp;</td>';
		echo '<td align="left">&nbsp;'.mk_graph($pv,$uniq,$max_int).'&nbsp;</td>';
		echo '</tr>'."\n";
	}
	echo '<tr class="bg_total">';
	echo '<td align="right" width="50">&nbsp;<b>合計：</b>&nbsp;</td>';
	echo '<td align="right" width="100">&nbsp;<b><font color="#0000FF">'.$total_uniq.'</font></b>&nbsp;</td>';
	echo '<td align="right" width="100">&nbsp;<b><font color="#FF0000">'.$total_pv.'</font></b>&nbsp;</td>';
	echo '<td>&nbsp;</td>';
	echo '</tr>'."\n";
	echo '</table>';
}
?>
