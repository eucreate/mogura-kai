<div class="title">ドメイン</div>
<?php
/*=========================================*/
/* mogura     Plug-in【ドメイン】          */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

include_once(constant("DIR_LIST")."domain.php");

if(defined("SELECT_LOG_FLG")){
	$where = "date LIKE '".mk_sql_date($ym, $d)."%' AND host LIKE '%".$send_sel."'".(isset($domain_overlap_list[$send_sel]) ? " AND host NOT LIKE '%".preg_replace(" *, *", "' AND host NOT LIKE '%", $domain_overlap_list[$send_sel])."'" : "");
}else{
	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	$sql = array();
	$main = array();

	/*=========================================*/
	/* domリスト                               */
	/*=========================================*/
	$domain_sql = ",CASE ";
	$detail_sql = ",CASE ";
	foreach($domain_list as $k => $v){
		if(function_exists('mysql_escape_string')){
			$k = mysql_escape_string($k);
			$v = mysql_escape_string($v);
		}
		$domain_sql .= "WHEN (host LIKE '%".$k."') THEN '".$k."' ";
		$detail_sql .= "WHEN (host LIKE '%".$k."') THEN '".$v."' ";
	}
	$domain_sql .= "END domain";
	$detail_sql .= "END dom";

	$max_int = 0;
	$total_uniq = 0;
	$total_pv = 0;
	if($domain_list){
		$sql["select"] = "COUNT(*) as pv,COUNT(DISTINCT id) as uniq".$detail_sql.$domain_sql;
		$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%'";
		$sql["group"] = "dom";
		$sql["having"] = "dom IS NOT NULL";
		$sql["sort"] = true;
		
		$res = $db->query(mk_sql($sql));
		check_err($res);
		
		$max_int = 0;
		$i = 0;
		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
			$main[$i]["dom"] = $row["dom"];
			$main[$i]["domain"] = $row["domain"];
			$main[$i]["pv"] = $row["pv"];
			$main[$i]["uniq"] = $row["uniq"];
			if($max_int < $row["pv"]) $max_int = $row["pv"];

			$total_pv += $row["pv"];
			$total_uniq += $row["uniq"];

			$i++;
		}
		$res->free();
	}

	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	if($main){
		echo '<table width="100%" cellpadding="0" cellspacing="0">';
		echo '<tr>';
		echo '<th nowrap width="70">Domain</th>';
		echo '<th nowrap width="120">種別</th>';
		mk_pu();
		echo '<th nowrap>グラフ</th>';
		echo '</tr>';
		foreach($main as $k => $v){
			$pv = $v["pv"];
			$uniq = $v["uniq"];
			//link
			$link = sel_link(query_edit("sel",$v["domain"]),$v["domain"]);
			
			echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
			echo '<td nowrap width="70">'.$link.'</td>';
			echo '<td width="120">'.$v["dom"].'</td>';
			echo '<td width="70" align="right"><font color="#0000FF">'.$uniq.'</font></td>';
			echo '<td width="70" align="right"><font color="#FF0000">'.$pv.'</font></td>';
			echo '<td align="left">'.mk_graph($pv,$uniq,$max_int).'</td>';
			echo '</tr>';
		}
		//合計
		echo '<tr class="bg_total">';
		echo '<td width="70">&nbsp;</td>';
		echo '<td width="120" nowrap align="right"><b>合計：</b></td>';
		echo '<td width="70" nowrap align="right"><b><font color="#0000FF">'.$total_uniq.'</font></b></td>';
		echo '<td width="70" align="right"><b><font color="#FF0000">'.$total_pv.'</font></b></td>';
		echo '<td>&nbsp;</td>';
		echo '</tr>'."\n";
		echo '</table>';
	}else{
		echo '<div id="error">解析ログが見つかりません。</div>';
	}
}
?>