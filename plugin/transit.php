<?php
/*=========================================*/
/* mogura     Plug-in【月推移】            */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*  Customized by yap (http://yap.jp/)     */
/*                                         */
/*      on 2008/06/08 as Version 1.0.1     */
/*                                         */
/*=========================================*/

if(defined("SELECT_LOG_FLG")){
	if(isset($_GET["seld_t"]) && isset($_GET["seld_e"])){
		$where = "DATE_FORMAT(date,'%Y%m') = ".$send_sel;
	}else{
		$where = "date LIKE '".mk_sql_date($send_sel)."%'";
	}
}else{
	/*=========================================*/
	/* 設定                                    */
	/*=========================================*/
	// ■ 抽出件数
	// 0 -> 無制限 , n -> 過去nか月分
	$extract_count = 0;

	// ■ 抽出開始年月(必ず6桁で指定 例:200806)
	// 0 -> すべて , yyyymm -> yyyy年mm月以降抽出
	$extract_start = 0;

	// ■ 表示順
	// 0 -> 昇順 , 1 -> 降順
	$order_desc = 1;

	// ■ 年月の分離記号
	$separator = "/";

	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	$sql = array();
	$main = array();

	/*=========================================*/
	/* SQL                                     */
	/*=========================================*/
	$start_ym = 0;
	if($extract_count) $start_ym = date("Ym",mktime(0,0,0,substr($ym,4,2) - $extract_count + 1,1,substr($ym,0,4)));
	if($extract_start > $start_ym) $start_ym = $extract_start;

	$sql["select"] = "DATE_FORMAT(date,'%Y%m') as yymm,COUNT(*) as pv,COUNT(DISTINCT id) as uniq";
	if($start_ym){
		$sql["where"] = "date >= '".mk_sql_date($start_ym)."'";
	}
	$sql["group"] = "yymm";
	if($order_desc)  $sql["op"] = "order by yymm desc";
	$res = $db->query(mk_sql($sql));
	check_err($res);

	$max_int = 0;
	$total_uniq = 0;
	$total_pv = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$main[$row["yymm"]]["pv"] = $row["pv"];
		$main[$row["yymm"]]["uniq"] = $row["uniq"];
		if($max_int < $row["pv"]) $max_int = $row["pv"];

		$total_pv += $row["pv"];
		$total_uniq += $row["uniq"];
	}
	$res->free();


	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	if($extract_count) $period = "［".$extract_count."か月合計］"; else $period = "［累計］";
	echo '<div class="title">月推移&nbsp;&nbsp;<small>'.$period.'</small></div>';
	echo '<table width="100%" cellpadding="0" cellspacing="0">';
	echo '<tr>';
	echo '<th nowrap width="80">年'.$separator.'月</th>';
	echo '<th nowrap width="100">ユニークユーザ</th>';
	echo '<th nowrap width="100">ページビュー</th>';
	echo '<th nowrap>グラフ</th>';
	echo '</tr>'."\n";
	foreach($main as $k => $v){
		//ユニーク値
		$pv = $v["pv"];
		$uniq = $v["uniq"];
		//link
		$edt_yymm = substr($k,0,4).$separator.substr($k,4,2);
		$link = sel_link(query_edit("sel",$k),$edt_yymm);

		echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
		echo '<td align="right" width="50">'.$link.'</td>';
		echo '<td align="right" width="100"><font color="#0000FF">'.$uniq.'</font></td>';
		echo '<td align="right" width="100"><font color="#FF0000">'.$pv.'</font></td>';
		echo '<td align="left">'.mk_graph($pv,$uniq,$max_int).'</td>';
		echo '</tr>'."\n";
	}
	echo '<tr class="bg_total">';
	echo '<td align="right" width="50"><b>合計：</b></td>';
	echo '<td align="right" width="100"><b><font color="#0000FF">'.$total_uniq.'</font></b></td>';
	echo '<td align="right" width="100" class="bg_total"><b><font color="#FF0000">'.$total_pv.'</font></b></td>';
	echo '<td>&nbsp;</td>';
	echo '</tr>'."\n";
	echo '</table>';
}
?>
