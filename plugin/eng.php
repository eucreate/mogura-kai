<div class="title">検索エンジン</div>
<?php
/*=========================================*/
/* mogura     Plug-in【検索エンジン】      */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/
//検索エンジンリスト
include_once(constant("DIR_LIST")."engine.php");


if(defined("SELECT_LOG_FLG")){
	foreach($engine_list as $uri => $list){
		list($eng["name"],$eng["q"],$eng["enc"]) = $list;
		if($eng["name"] != $send_sel) continue;
		$where .= "(ref LIKE '%".$uri."%' AND ref_q LIKE '%".$eng["q"]."=%') OR ";
	}
	$where = "(".substr($where, 0, -4).")";
	$where = "date LIKE '".mk_sql_date($ym, $d)."%' AND ".$where;
}else{
	//ソート用
	function cmp($a , $b){
	    if ($a["total"] == $b["total"]) return 0;
	    return ($a["total"] > $b["total"]) ? -1 : 1;
	}
	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	$sql = array();
	$main = array();

	$max_int = 0;
	$total = 0;
	
	$sql["select"] = "ref_q,CASE ";
	foreach($engine_list as $uri => $list){
		$sql["select"] .= "WHEN (ref LIKE '%".$uri."%' AND ref_q LIKE '%".$list[1]."=%') THEN '".$uri."' ";
	}
	$sql["select"] .= "END engine_group";
	$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%'";
	$sql["group"] = "ref_q,engine_group";
	$sql["op"] = " having engine_group is not null";
	
	$res = $db->query(mk_sql($sql));
	check_err($res);

	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		//キーワード取得
		list($eng["name"],$eng["q"],$eng["enc"]) = $engine_list[$row["engine_group"]];
		$keyword = "";
		$keyword = get_keyword($row["ref_q"] ,$eng["q"], $eng["enc"]);
		if($keyword == "") continue;
		if(isset($main[$eng["name"]])){
			if(!array_key_exists($keyword,$main[$eng["name"]])){
				$main[$eng["name"]][$keyword] = 1;
			}else{
				$main[$eng["name"]][$keyword]++;
			}
		}else{
			$main[$eng["name"]][$keyword] = 1;
		}
		//total
		if(isset($main[$eng["name"]]["total"])){
			$main[$eng["name"]]["total"]++;
		}else{
			$main[$eng["name"]]["total"] = 1;
		}
		
		if($max_int < $main[$eng["name"]][$keyword]) $max_int = $main[$eng["name"]][$keyword];
		$total++;
	}
	$res->free();

	//ソート
	uasort($main , "cmp");

	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	if($main){
		echo '<table width="100%" cellpadding="0" cellspacing="0">';
		echo '<tr>';
		echo '<th nowrap>検索エンジン</th>';
		echo '<th nowrap width="250">キーワード</th>';
		echo '<th nowrap width="50">件数</th>';
		echo '<th nowrap>グラフ</th>';
		echo '</tr>';
		foreach($main as $k => $v){
			if($k == "total") continue;
			
			//ソート
			arsort($main[$k],SORT_NUMERIC);
			$tmp = "";
			$i = 0;
			
			$c = false;
			foreach($main[$k] as $k2 => $v2){
				if($k2 == "total") continue;
				//link
				if($k == $tmp){
					$link = "&nbsp;";
				}else{
					$link = set_icon($k).sel_link(query_edit("sel",$k),$k);
				}
				$tmp = $k;
				
				echo '<tr'.tr_color($c).' id="bg_id'.$k.$i.'" onmouseover="chBG(\'bg_id'.$k.$i.'\', 1);" onmouseout="chBG(\'bg_id'.$k.$i.'\', 0);">';
				echo '<td nowrap>'.$link.'</td>';
				$k2 = str_replace ("］&nbsp;［", "］&nbsp;［", $k2);
				echo '<td width="250">'.$k2.'</td>';
				echo '<td width="50" align="right">'.$v2.'</td>';
				echo '<td>'.mk_graph_01($v2, $max_int, $total).'</td>';
				echo '</tr>'."\n";
				$i++;
			}
			//合計
			echo '<tr class="bg_total">';
			echo '<td>&nbsp;</td>';
			echo '<td width="250" align="right"><b>合計：</b></td>';
			echo '<td width="50" align="right"><b>'.$main[$k]["total"].'</b></td>';
			echo '<td>&nbsp;</td>';
			echo '</tr>'."\n";
		}
		echo '</table>';
	}else{
		echo '<div id="error">解析ログが見つかりません。</div>';
	}
}
?>