<div class="title">ホスト</div>
<?php
/*=========================================*/
/* mogura     Plug-in【ホスト】　          */
/*                                         */
/* c-atwork | http://c-atwork.com          */
/*                                         */
/*=========================================*/

if(defined("SELECT_LOG_FLG")){
	$where = "date LIKE '".mk_sql_date($ym, $d)."%' AND host = '".$send_sel."'";
}else{
	/*=========================================*/
	/* main                                    */
	/*=========================================*/
	$sql = array();
	$main = array();
	$sql["select"] = "host,COUNT(*) as pv,COUNT(DISTINCT id) as uniq";
	$sql["group"] = "host";
	$sql["sort"] = true;
	$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%'";

	$res = $db->query(mk_sql($sql));
	check_err($res);

	$max_int = 0;
	$total_pv = 0;
	$i = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$main[$i] = $row;
		if($max_int < $row["pv"]) $max_int = $row["pv"];
		$i++;
		
		$total_pv += $row["pv"];
	}
	$res->free();

	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	if($main){
		echo '<table width="100%" cellpadding="0" cellspacing="0">';
		echo '<tr>';
		echo '<th nowrap>ホスト</th>';

		$pv_title = "件数";

		switch (constant("SORT_MODE")) {
			case 'p_u':
				$pv_title = set_img(constant("ICON_SORTUP"),"UP").'<a href="'.query_edit("s",'p_d').'">'.$pv_title.'</a>';
				break;
			case 'u_u':
				$pv_title = set_img(constant("ICON_SORTOFF"),"OFF").'<a href="'.query_edit("s",'p_d').'">'.$pv_title.'</a>';
				break;
			case 'u_d':
				$pv_title = set_img(constant("ICON_SORTOFF"),"OFF").'<a href="'.query_edit("s",'p_d').'">'.$pv_title.'</a>';
				break;
			case 'p_d':
			default:
				$pv_title = set_img(constant("ICON_SORTDOWN"),"DOWN").'<a href="'.query_edit("s",'p_u').'">'.$pv_title.'</a>';
				break;
		}
		echo '<th nowrap width="70">'.$pv_title.'</th>';
		
		echo '<th nowrap>グラフ</th>';
		echo '</tr>';
		foreach($main as $k => $v){
			//link
			$link = sel_link(query_edit("sel",$v["host"]),str_cut($v["host"]));
			
			echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
			echo '<td>'.$link.'</td>';
			echo '<td width="70" align="right"><font color="#FF0000">'.$v["pv"].'</font></td>';
			echo '<td>'.mk_graph($v["pv"],$v["uniq"],$max_int).'</td>';
			echo '</tr>';
			
			flush();
		}
		echo '<tr class="bg_total">';
		echo '<td align="right" class="bg_total"><b>合計：</b></td>';
		echo '<td width="70" align="right"><b><font color="#FF0000">'.$total_pv.'</font></b></td>';
		echo '<td align="left">&nbsp;</td>';
		echo '</tr>'."\n";
		echo '</table>';
	}else{
		echo '<div id="error">解析ログが見つかりません。</div>';
	}
}
?>