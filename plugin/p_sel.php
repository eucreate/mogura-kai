<div class="title">ページ指定</div>
<?php
/*=========================================*/
/* mogura     Plug-in【ページ指定】        */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

/*=========================================*/
/* 初期化                                  */
/*=========================================*/
$sql = array();
$main = array();

/*=========================================*/
/* SQL                                     */
/*=========================================*/
$sql["select"] = "path,title";
#$sql["where"] = "date LIKE '".mk_sql_date($ym)."%' AND path IS NOT NULL";
$sql["group"] = "path";

$res = $db->query(mk_sql($sql));
check_err($res);

$i = 0;
while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	if($row["path"] == "") continue;
	$main[$i]["path"] = $row["path"];
	$main[$i]["title"] = $row["title"];
	$i++;
}
$res->free();

/*=========================================*/
/* メイン処理                              */
/*=========================================*/
if($main){
	echo '<table width="100%" cellpadding="0" cellspacing="0">';
	echo '<tr>';
	echo '<th nowrap width="250">ページパス</th>';
	echo '<th nowrap>タイトル</th>';
	echo '</tr>';
	foreach($main as $k => $v){
		$query = query_edit("act","DELETE");
		$link = '<a href="'.query_edit("p_sel", $v["path"], $query).'">'.set_img(constant("ICON_SELPATH"),"ページ指定").'</a>'
				.'&nbsp;'
				.'<a href="'.query_edit("p_sel", $v["path"], $query).'">'.$v["path"].'</a>';
		echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
		echo '<td width="250">'.$link.'</td>';
		echo '<td>'.$v["title"].'</td>';
		echo '</tr>';
	}
	echo '</table>';
}else{
	echo '<div id="error">解析ログが見つかりません。</div>';
}

?>