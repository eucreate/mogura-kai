<div class="title">リンク元</div>
<?php
/*=========================================*/
/* mogura     Plug-in【リンク元】          */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

/*=========================================*/
/* グループ                                */
/*=========================================*/
$sql = "";
$ch_url = array();

$sql = "SELECT DISTINCT * FROM ".constant("DB_CH_URL");
$sql .= " order by ch_url desc";
$sql .= ";";
$res = $db->query($sql);
check_err($res);
while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	$ch_url[trim($row["ch_url"])] = trim($row["ch_urlname"]);
}
$res->free();

if(defined("SELECT_LOG_FLG")){
	if($send_sel == 'G--(Direct or Bookmark)'){
		$where = "date LIKE '".mk_sql_date($ym, $d)."%' AND ISNULL(ref)";
	}elseif(strpos($send_sel,'G--') === 0){
		$where = "date LIKE '".mk_sql_date($ym, $d)."%' AND (";
		foreach($ch_url as $k => $v){
			if(substr($send_sel,3) == $v) $where .= "ref LIKE '%".$k."%' OR ";
		}
		$where = substr($where, 0, -4).")";
	}else{
		$where = "date LIKE '".mk_sql_date($ym, $d)."%' AND ref LIKE '%".$send_sel."%'";
	}
}else{
	/*=========================================*/
	/* main                                    */
	/*=========================================*/
	$sql = array();
	$main = array();
	$sql["select"] = "ref,COUNT(*) as pv,COUNT(DISTINCT id) as uniq";
	$sql["select"] .= ",CASE ";
	$sql["select"] .= "WHEN ISNULL(ref) THEN '(Direct or Bookmark)' ";
	foreach($ch_url as $k => $v){
		$sql["select"] .= "WHEN (ref LIKE '%".$k."%') THEN '".$v."' ";
	}
	$sql["select"] .= "ELSE ref ";
	$sql["select"] .= "END ref_group";
	$sql["group"] = "ref_group";
	$sql["sort"] = true;
	$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%'";

	$res = $db->query(mk_sql($sql));
	check_err($res);

	$max_int = 0;
	$total_pv = 0;
	$total_uniq = 0;
	$i = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		//自サイト内リンク削除
		if(strstr($row["ref"], $_SERVER["HTTP_HOST"]) !== false){
			continue;
		}
		
		$main[$i] = $row;
		if($max_int < $row["pv"]) $max_int = $row["pv"];
		$i++;
		
		
		$total_pv += $row["pv"];
		$total_uniq += $row["uniq"];
	}
	$res->free();

	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	if($main){
		echo '<table width="100%" cellpadding="0" cellspacing="0">';
		echo '<tr>';
		echo '<th nowrap>リンク元</th>';
		mk_pu();
		echo '<th nowrap>グラフ</th>';
		echo '</tr>';
		foreach($main as $k => $v){
			//JUMP
			$flg_jump = true;
			if($v["ref"] != $v["ref_group"]) $flg_jump = false;
			if(trim($v["ref"]) == '(Direct or Bookmark)') $flg_jump = false;
			
			if($flg_jump){
				$jump = '<a href="./inc/redirect.php?redirect='.urlencode($v["ref_group"]).'" target="_blank">'.set_img(constant("ICON_JUMP"),"ジャンプ").'</a>&nbsp;';
			}else{
				$jump = "";
			}
			
			//link
			if($flg_jump){
				$link = sel_link(query_edit("sel",$v["ref_group"]),str_cut($v["ref_group"]));
			}else{
				if($v["ref_group"]){
					$link = sel_link(query_edit("sel","G--".$v["ref_group"]),str_cut($v["ref_group"]));
				}else{
					$link = str_cut($v["ref_group"]);
				}
			}
			
			echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
			echo '<td>'.$jump.$link.'</td>';
			echo '<td width="70" align="right"><font color="#0000FF">'.$v["uniq"].'</font></td>';
			echo '<td width="70" align="right"><font color="#FF0000">'.$v["pv"].'</font></td>';
			echo '<td>'.mk_graph($v["pv"],$v["uniq"],$max_int).'</td>';
			echo '</tr>';
			
			flush();
		}
		echo '<tr class="bg_total">';
		echo '<td align="right" class="bg_total"><b>合計：</b></td>';
		echo '<td width="70" align="right"><b><font color="#0000FF">'.$total_uniq.'</font></b></td>';
		echo '<td width="70" align="right"><b><font color="#FF0000">'.$total_pv.'</font></b></td>';
		echo '<td align="left">&nbsp;</td>';
		echo '</tr>'."\n";
		echo '</table>';
	}else{
		echo '<div id="error">解析ログが見つかりません。</div>';
	}
}
?>