<div class="title">ISP</div>
<?php
/*=========================================*/
/* mogura     Plug-in【ISP】               */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

if(defined("SELECT_LOG_FLG")){
	$where = "date LIKE '".mk_sql_date($ym, $d)."%' AND host LIKE '".$send_sel."'";
}else{
	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	$sql = array();
	$main = array();

	/*=========================================*/
	/* ISPリスト                               */
	/*=========================================*/
	include_once(constant("DIR_LIST")."isp.php");
	$domain_sql = ",CASE ";
	$isp_sql = ",CASE ";
	foreach($isp_list as $k => $v){
		if(function_exists('mysql_escape_string')){
			$k = mysql_escape_string($k);
			$v = mysql_escape_string($v);
		}
		$domain_sql .= "WHEN (host LIKE '".$k."') THEN '".$k."' ";
		$isp_sql .= "WHEN (host LIKE '".$k."') THEN '".str_replace("%", "*", $k)."/".$v."' ";
	}
	$domain_sql .= "END domain";
	$isp_sql .= "END isp";

	$max_int = 0;
	$total_uniq = 0;
	$total_pv = 0;
	if($isp_list){
		$sql["select"] = "COUNT(*) as pv,COUNT(DISTINCT id) as uniq".$isp_sql.$domain_sql;
		$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%'";
		$sql["group"] = "isp";
		$sql["having"] = "isp IS NOT NULL";
		$sql["sort"] = true;
		
		$res = $db->query(mk_sql($sql));
		check_err($res);
		
		$max_int = 0;
		$i = 0;
		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
			list($main[$i]["isp"],$main[$i]["com"]) = explode("/",$row["isp"]);
			$main[$i]["domain"] = $row["domain"];
			$main[$i]["pv"] = $row["pv"];
			$main[$i]["uniq"] = $row["uniq"];
			if($max_int < $row["pv"]) $max_int = $row["pv"];

			$total_pv += $row["pv"];
			$total_uniq += $row["uniq"];

			$i++;
		}
		$res->free();
	}

	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	if($main){
		echo '<table width="100%" cellpadding="0" cellspacing="0">';
		echo '<tr>';
		echo '<th nowrap width="180">ISP</th>';
		mk_pu();
		echo '<th nowrap>グラフ</th>';
		echo '</tr>';
		foreach($main as $k => $v){
			$pv = $v["pv"];
			$uniq = $v["uniq"];
			//link
			$link = sel_link(query_edit("sel",$v["domain"]),$v["isp"]);
			if($v["com"]) $link .= "<br>".$v["com"];
			
			echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
			echo '<td>'.$link.'</td>';
			echo '<td width="70" align="right"><font color="#0000FF">'.$uniq.'</font></td>';
			echo '<td width="70" align="right"><font color="#FF0000">'.$pv.'</font></td>';
			echo '<td align="left">'.mk_graph($pv,$uniq,$max_int).'</td>';
			echo '</tr>';
		}
		//合計
		echo '<tr class="bg_total">';
		echo '<td align="right"><b>合計：</b></td>';
		echo '<td align="right" width="70"><b><font color="#0000FF">'.$total_uniq.'</font></b></td>';
		echo '<td align="right" width="70"><b><font color="#FF0000">'.$total_pv.'</font></b></td>';
		echo '<td>&nbsp;</td>';
		echo '</tr>'."\n";
		echo '</table>';
	}else{
		echo '<div id="error">解析ログが見つかりません。</div>';
	}
}
?>