<div class="title">ログ検索</div>
<?php
/*=========================================*/
/* mogura     Plug-in【ログ検索】          */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

function sel_color($str){
	global $send_sel_arr;
	foreach($send_sel_arr as $v){
		$v = trim($v);
		if($v == "") continue;
		$str = str_replace($v,"<b style=\"background-color: #FFFF00;\">$v</b>",$str);
	}

	return $str;
}

/*=========================================*/
/* 検索                                    */
/*=========================================*/
if($send_sel != ""){
	$send_sel = @mb_convert_kana($send_sel, "KVas");
	$send_sel = preg_replace("/ {2,}/" , " " , $send_sel );
	$send_sel = trim($send_sel);
}

//初期化
$sql = array();
$main = array();
if($send_sel != ""){
	
	$sql["select"] = "CONCAT("
	."ifnull(id,''),' ',"
	."ifnull(path,''),' ',"
	."ifnull(ip,''),' ',"
	."ifnull(host,''),' ',"
	."ifnull(ref,''),' ',"
	."ifnull(ref_q,''),' ',"
	."ifnull(title,''),' ',"
	."ifnull(lang,''),' ',"
	."ifnull(ch_name,''),' ',"
	."ifnull(ua_name,''),' '"
	.") AS search";
	$sql["from"] = "(".constant("DB_TABLE_LOG")." left join ".constant("DB_CH_ID")
	." on ".constant("DB_TABLE_LOG").".id = ".constant("DB_CH_ID").".ch_id)";
	$sql["from"] = "(".$sql["from"]." left join ".constant("DB_TABLE_UA")
	." on ".constant("DB_TABLE_LOG").".ua = ".constant("DB_TABLE_UA").".ua_id)";
	
	$send_op = ($send_op == "OR" || $send_op == "AND") ? $send_op : "OR";
	$send_sel_arr = explode(" ", $send_sel);
	$send_sel_arr = array_unique ($send_sel_arr);
	$sql["having"] = "";
	foreach($send_sel_arr as $v){
		$v = trim($v);
		if($v == "") continue;
	   $sql["having"] .= "search LIKE '%".$v."%' ".$send_op." ";
	}
	$sql["having"] = "(".substr($sql["having"], 0, -(strlen($send_op)+2)).")";
	

	//max_val
	$res = $db->query(mk_sql($sql));
	check_err($res);
	$max_val = $res->numRows();
	$res->free();
	
	//move
	if($send_p !== "all"){
		$move_p = $send_p." , ".constant("LIMIT");
		$move = move_bt($send_p, $max_val);
	}
	
	//search
	$sql["select"] = "*,".$sql["select"];
	$sql["op"] = "order by no desc";
	if(isset($move_p)) $sql["limit"] = $move_p;
	$res = $db->query(mk_sql($sql));
	check_err($res);
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		unset($row["search"]);
		$main[] = $row;
	}
	$res->free();
}

?>
<form action="<?php echo constant("FILENAME");?>" method="GET" class="search_box">
<input type="hidden" name="ym" value="<?php echo $ym;?>">
<input type="hidden" name="d" value="<?php echo $d;?>">
<input type="hidden" name="act" value="search">
<?php if(isset($_GET["p_sel"])) echo '<input type="hidden" name="p_sel" value="'.htmlspecialchars($_GET["p_sel"]).'">'."\n"; ?>
<input type=text name="sel" value="<?php echo stripslashes($send_sel);?>" class="search_txt" style="width: 300px;">&nbsp;
<select name="op">
<option value="OR"<?php echo ($send_op != "AND" ? " selected" : "");?>>OR</option>
<option value="AND"<?php echo ($send_op == "AND" ? " selected" : "");?>>AND</option>
</select>
<input type=submit value="検索">
</form>
<br>
<?php
/*=========================================*/
/* メイン処理                              */
/*=========================================*/
if($send_sel != ""){
	echo '■&nbsp;ログ検索キーワード&nbsp;';
	foreach($send_sel_arr as $v){
		$v = trim($v);
		if($v == "") continue;
	   echo "［&nbsp;<b>".stripslashes($v)."</b>&nbsp;］";
	}
	echo '&nbsp;'.$send_op.'検索<hr size="1">';
}else{
	echo '■&nbsp;ログ検索キーワードを入力してください';
}
if($main){
	echo '<b>'.number_format($max_val).'</b>件のログが見つかりました。<br><br>';
	foreach($main as $k => $v){
		//表示データ生成
		echo '<table width="100%">';
		//date
		echo '<tr><th class="no_th" width="100">Date:</th><td>&nbsp;'.$v["date"].'</td></tr>';
		//ID
		$track = query_edit("act", "track");
		$track = query_edit("sel", $v["id"], $track);
		$v["id"] = '<a href="'.constant("FILENAME").$track.'">'.set_img(constant("ICON_TRACK"),"追跡")."&nbsp;".sel_color($v["ch_name"] ? $v["ch_name"]."&nbsp;&nbsp;[&nbsp;ID:".$v["id"]."&nbsp;]" : $v["id"]).'</a>&nbsp;';
		echo '<tr><th class="no_th" width="100">ID:</th><td nowrap>&nbsp;'.$v["id"].'</td></tr>';
		
		//path
		if(trim($v["path"])){
			echo '<tr><th class="no_th" width="100">Path:</th><td>';
			echo '&nbsp;<a href="'.query_edit("p_sel",$v["path"]).'">'.set_img(constant("ICON_SELPATH"),"ページ指定").'</a>&nbsp;';
			if($v["title"] != "") echo "[&nbsp;".sel_color($v["title"])."&nbsp;]<br>";
			echo '&nbsp;'.sel_color($v["path"]).'</td></tr>';
		}
		//host
		if(trim($v["ip"])) echo '<tr><th class="no_th" width="100">IP:</th><td>&nbsp;'.sel_color($v["ip"]).'</td></tr>';
		if(trim($v["host"])) echo '<tr><th class="no_th" width="100">Host:</th><td>&nbsp;'.sel_color($v["host"]).'</td></tr>';
		//Referer
		if(trim($v["ref"])){
				
			if($v["ref_q"]) $v["ref"] .= "?".$v["ref_q"];
			$jump = urldecode($v["ref"]);
			$jump = @mb_convert_encoding($jump, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
			$v["ref"] = '<a href="./inc/redirect.php?redirect='.urlencode($v["ref"]).'" target="_blank">'.set_img(constant("ICON_JUMP"),"ジャンプ").'&nbsp;'.$jump.'</a>';
			echo '<tr><th class="no_th" width="100">Referer:</th><td>&nbsp;'.$v["ref"].'</td></tr>';
		}
		//UserAgent
		if(trim($v["ua_name"])) echo '<tr><th class="no_th" width="100">UserAgent:</th><td>&nbsp;'.sel_color($v["ua_name"]).'</td></tr>';
		echo "</table>";
		echo "<br>\n";
	}
	if(isset($move)) echo $move;
}elseif($send_sel && !$main){
	echo '<div id="error">該当するログは見つかりませんでした。</div>';
}
?>