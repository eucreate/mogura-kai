<div class="title">言語</div>
<?php
/*=========================================*/
/* mogura     Plug-in【言語】              */
/*                                         */
/* オーサカPHP:hi | http://fmono.sub.jp    */
/*                                         */
/*=========================================*/

if(defined("SELECT_LOG_FLG")){
	$where = "date LIKE '".mk_sql_date($ym, $d)."%' AND ";
	if($send_sel == 'Unknown'){
		$where .= "ISNULL(lang)";
	}else{
		$where .= "lang = '".$send_sel."'";
	}
}else{
	/*=========================================*/
	/* 初期化                                  */
	/*=========================================*/
	$sql = array();
	$main = array();

	include_once(constant("DIR_LIST")."lang.php");
	$sql["select"] = "lang,COUNT(DISTINCT id) as uniq";
	$sql["select"] .= ",CASE WHEN ISNULL(lang) THEN 'Unknown' ELSE lang END lang";
	$sql["where"] = "date LIKE '".mk_sql_date($ym, $d)."%'";
	$sql["group"] = "lang";
	$sql["op"] = "order by uniq desc";

	$res = $db->query(mk_sql($sql));
	check_err($res);
	
	$total = 0;
	$max_int = 0;
	while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
		$main[$row["lang"]] = $row["uniq"];
		if($max_int < $row["uniq"]) $max_int = $row["uniq"];
		$total += $row["uniq"];
	}
	$res->free();

	/*=========================================*/
	/* メイン処理                              */
	/*=========================================*/
	if($main){
		echo '<table width="100%" cellpadding="0" cellspacing="0">';
		echo '<tr>';
		echo '<th nowrap width="100">言語</th>';
		echo '<th nowrap width="80">ユーザー数</th>';
		echo '<th nowrap>グラフ</th>';
		echo '</tr>';
		foreach($main as $k => $v){
			//link
			$link = sel_link(query_edit("sel",$k),(isset($lang_list[$k]) ? $lang_list[$k] : $k));
			
			echo '<tr'.tr_color($c).' id="bg_id'.$k.'" onmouseover="chBG(\'bg_id'.$k.'\', 1);" onmouseout="chBG(\'bg_id'.$k.'\', 0);">';
			echo '<td width="100">'.$link.'</td>';
			echo '<td width="80" align="right"><font color="#FF0000">'.$v.'</font></td>';
			echo '<td align="left">'.mk_graph_01($v,$max_int,$total).'</td>';
			echo '</tr>';
		}
		//合計
		echo '<tr class="bg_total">';
		echo '<td width="100" align="right"><b>合計：</b></td>';
		echo '<td width="80" align="right"><b><font color="#FF0000">'.$total.'</font></b></td>';
		echo '<td>&nbsp;</td>';
		echo '</tr>'."\n";
		echo '</table>';
	}else{
		echo '<div id="error">解析ログが見つかりません。</div>';
	}
}
?>