<?php
/*=========================================*/
/* Ver.                                    */
/*=========================================*/
$version = '1.0.0';

/*=========================================*/
/* 設定読み込み                            */
/*=========================================*/
include_once("./inc/config.php");
include_once("./inc/function.php");
//ユーザー認証
if(constant("ADMIN_MODE") != 2) include_once("./inc/userauth.php");
//マルチバイト関数設定
include_once("./inc/mb_check.php");

/*=========================================*/
/* タイムアウト対策等                      */
/*=========================================*/
//charset
header ("Content-Type: text/html; charset=UTF-8");
//キャッシュクリア
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

//内部文字エンコード
@mb_internal_encoding("UTF-8");
//マルチバイト文字列正規表現関数で使用される文字エンコーディング
@mb_regex_encoding("UTF-8");
//HTTP出力文字エンコーディング
@mb_http_output("UTF-8");

//実行時間制限なし
@set_time_limit(0);
//256バイト空文字出力
echo str_pad('',256);
flush();

/*=========================================*/
/* 初期化 & 初期設定                       */
/*=========================================*/
//データ用 初期化
$w3a = array();
//実行フラグ
define ("W3A", "run");
//処理時間計算用
define ("STIME", getmicrotime());
//実行ファイル名
define ("FILENAME", 'w3a.php');

/*=========================================*/
/* 年月日の取得                            */
/*=========================================*/
if(isset($_GET["y"]) && isset($_GET["m"])){
	$_GET["ym"] = $_GET["y"].$_GET["m"];
	unset($_GET["y"],$_GET["m"]);
}
$_GET["ym"] = (isset($_GET["ym"]) ? $_GET["ym"] : "");
$_GET["d"] = (isset($_GET["d"]) ? $_GET["d"] : "");
$ym = (ereg("^[0-9]{6}$",$_GET["ym"]) ? $_GET["ym"] : gmdate("Ym", time()+constant("TIME_DIFF")*3600));
$d = (ereg("^[0-9]{2}$",$_GET["d"]) ? $_GET["d"] : gmdate("d", time()+constant("TIME_DIFF")*3600));
if(!$_GET["ym"]) unset($_GET["ym"]);
if(!$_GET["d"]) unset($_GET["d"]);

/*=========================================*/
/* SORT_MODE                               */
/*=========================================*/
$_GET["s"] = (isset($_GET["s"]) ? $_GET["s"] : "");
if(ereg("^(p_u|u_u|u_d|p_d)$",$_GET["s"])){
	define ("SORT_MODE", $_GET["s"]);
}else{
	define ("SORT_MODE", "p_d");
}
unset($_GET["s"]);

/*=========================================*/
/* ICON                                    */
/*=========================================*/
// ■ リンク元ジャンプ用マーク
define ("ICON_JUMP",     "image/jump.gif");
// ■ 追跡用マーク
define ("ICON_TRACK",    "image/track.gif");
// ■ パス指定用マーク
define ("ICON_SELPATH",  "image/sel_path.gif");
// ■ ソート用マーク
define ("ICON_SORTUP",   "image/sort_up.gif");
define ("ICON_SORTDOWN", "image/sort_down.gif");
define ("ICON_SORTOFF", "image/sort_off.gif");
// ■ 閉じる用マーク
define ("ICON_CLOSE",    "image/close.gif");


#-------------------------------------------------
#  DB接続
#-------------------------------------------------
$db = DB::connect(constant("DSN"));
check_err($db);
//エンコード
$db->query("SET NAMES utf8;");

#-------------------------------------------------
#  拒否リストの読み込み
#-------------------------------------------------
$res = $db->query("SELECT * FROM ".constant("DB_EXCLUDE").";");
check_err($res);

while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	$GLOBALS['exclude'][$row["exclude_type"]][] = $row["exclude_val"];
}
$res->free();

#-------------------------------------------------
#  初期アクション／メニューリスト／スキン
#-------------------------------------------------
$res = $db->query("SELECT * FROM ".constant("DB_TABLE_SETTING").";");
check_err($res);
while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	if($row["setting_id"] == 'def_act') $act = trim($row["setting_val"]);
	if($row["setting_id"] == 'act') $menu_act = explode("\n", $row["setting_val"]);
	if($row["setting_id"] == 'skin'){
		$skin_dir = constant("DIR_SKIN").trim($row["setting_val"]);
		$skin_file = $skin_dir."/skin.html";
	}
}
$res->free();
if(isset($_GET["act"])) $act = $_GET["act"];


#-------------------------------------------------
#  COPYRIGHT(削除禁止)
#-------------------------------------------------
$w3a["COPYRIGHT"] = '<a href="http://fmono.sub.jp/" target="_blank">&copy;&nbsp;mogura&nbsp;'.$version.'</a>&nbsp;/&nbsp;Edit&nbsp;:&nbsp;<a href="http://tekito.jp/" target="_blank">tekito&nbsp;2.13.5</a>';


#-------------------------------------------------
#  send data
#-------------------------------------------------
$_GET["sel"] = (isset($_GET["sel"]) ? $_GET["sel"] : "");
$_GET["p"] = (isset($_GET["p"]) ? $_GET["p"] : "");
$_GET["op"] = (isset($_GET["op"]) ? $_GET["op"] : "");
//select data
$send_sel = sql_escape($_GET["sel"]);
unset($_GET["sel"]);
//page data
$send_p = $_GET["p"];
unset($_GET["p"]);
if(!$send_p) $send_p = 0;
//option data
$send_op = sql_escape($_GET["op"]);
unset($_GET["op"]);

//seld
if(isset($_GET["seld_t"]) && isset($_GET["seld_t"])){
	
}elseif(isset($_GET["seld_top_y"]) && isset($_GET["seld_top_y"]) && isset($_GET["seld_top_y"])
	&& isset($_GET["seld_end_y"]) && isset($_GET["seld_end_y"]) && isset($_GET["seld_end_y"])){
	$_GET["seld_t"] = $_GET["seld_top_y"].$_GET["seld_top_m"].$_GET["seld_top_d"];
	$_GET["seld_e"] = $_GET["seld_end_y"].$_GET["seld_end_m"].$_GET["seld_end_d"];
}
unset($_GET["seld_top_y"],$_GET["seld_top_m"],$_GET["seld_top_d"]);
unset($_GET["seld_end_y"],$_GET["seld_end_m"],$_GET["seld_end_d"]);

?>
