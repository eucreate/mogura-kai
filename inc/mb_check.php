<?php
/*=========================================*/
/* マルチバイト関数チェック                */
/*=========================================*/
$flg_mb_check = false;

if(!$flg_mb_check && function_exists('mb_language'))          $flg_mb_check = true;
if(!$flg_mb_check && function_exists('mb_internal_encoding')) $flg_mb_check = true;
if(!$flg_mb_check && function_exists('mb_eregi'))             $flg_mb_check = true;
if(!$flg_mb_check && function_exists('mb_substr'))            $flg_mb_check = true;
if(!$flg_mb_check && function_exists('mb_convert_encoding'))  $flg_mb_check = true;
if(!$flg_mb_check && function_exists('mb_ereg_replace'))      $flg_mb_check = true;

if(!$flg_mb_check){
	#include_once("./jcode/jcode.php");
	#include_once("./jcode/jcode_wrapper.php");
}else{
	@mb_language('Japanese');
	@mb_internal_encoding('UTF-8');
}
?>