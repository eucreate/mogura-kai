<?php
if($_SERVER["REQUEST_METHOD"] !== "POST"){
	header('HTTP/1.0 404 Not Found');
	exit;
}

/*=========================================*/
/* 設定読み込み                            */
/*=========================================*/
include_once("../inc/config.php");
include_once("../inc/function.php");

if(constant("ADMIN_MODE") != 2){
	if(constant("USERAUTH_FLG") == 1){
		if(!isset($_SERVER[PHP_AUTH_USER]) ||
				$_SERVER[PHP_AUTH_USER] != constant("USERAUTH_ID") ||
				$_SERVER[PHP_AUTH_PW] != constant("USERAUTH_PWD")){
			header('HTTP/1.0 404 Not Found');
			exit;
		}
	}elseif(constant("USERAUTH_FLG") == 2){
		//セッション開始
		session_start();
		if(!$_SESSION['login']){
			header('HTTP/1.0 404 Not Found');
			exit;
		}
	}
}

//マルチバイト関数設定
include_once("../inc/mb_check.php");

//charset
header ("Content-Type: text/plain; charset=UTF-8");
//キャッシュクリア
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

//内部文字エンコード
@mb_internal_encoding("UTF-8");
//マルチバイト文字列正規表現関数で使用される文字エンコーディング
@mb_regex_encoding("UTF-8");
//HTTP出力文字エンコーディング
@mb_http_output("UTF-8");

//実行時間制限なし
@set_time_limit(0);


#-------------------------------------------------
#  DB接続
#-------------------------------------------------
$db = DB::connect(constant("DSN"));
check_err($db);
//エンコード
$db->query("SET NAMES utf8;");

#-------------------------------------------------
#  拒否リストの読み込み
#-------------------------------------------------
$res = $db->query("SELECT * FROM ".constant("DB_EXCLUDE").";");
check_err($res);

while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	$GLOBALS['exclude'][$row["exclude_type"]][] = $row["exclude_val"];
}
$res->free();
?>