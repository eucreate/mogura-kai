<?php
//デバッグ
function debug($data){
	print "<XMP>";
	print_r($data);
	print "</XMP>";
	exit;
}

/*=========================================*/
/* 行色付                                  */
/*=========================================*/
function tr_color(&$color){
	$color = $color ? false : true;
	
	return ($color ? ' class="color1"' : ' class="color2"');
}

/*=========================================*/
/* 設定読み込み                            */
/*=========================================*/
function setting_read($uri){
	if(file_exists($uri)){
		if($arr_exclude = @file($uri)){
			$arr_exclude = @array_unique($arr_exclude);
			foreach($arr_exclude as $k => $v){
				$arr_exclude[$k] = trim($v);
			}
			return $arr_exclude;
		}
	}
}

/*=========================================*/
/* 割合計算                                */
/*=========================================*/
function make_rate($num, $nums){
	$rate = 0;
	$rate = @round(($num/$nums)*100, 1);
	return sprintf ("%01.1f", $rate).'%';
}

/*=========================================*/
/* 画像表示                                */
/*=========================================*/
function set_img($img ,$alt = "" ,$border = 0 ,$resize_w = 0){
	$size = @GetImageSize($img);
	if($size[2] == 1 || $size[2] == 2 || $size[2] == 3){
		// リサイズ値取得
		if($resize_w){
			$W = $size[0];
			$H = $size[1];
		    if($resize_w < $W){ //指定幅以外の時
		         $re_W = $resize_w;
		         $re_H = round(($resize_w/$W)*$H);
		         $size[3] = 'width="'.$re_W.'" height="'.$re_H.'"';
		    }
	    }
		$alt = ($alt ? 'alt="'.$alt.'"  ' : "");
		$border = ($border ? ' style="border: '.$border.'px solid #333333;"' : ' border="0"');
		$align = ' align="absmiddle"';
		return '<img src="'.$img.'" '.$alt.$size[3].$border.$align.'>';
	}else{
		return '<img src="'.$img.'">';
		#return $img;
	}
}

/*=========================================*/
/* アイコン設定                            */
/*=========================================*/
function set_icon($str){
	include_once(constant("DIR_LIST")."icon.php");
	global $icon_list;
	
	$str = trim($str);
	if(isset($icon_list[$str])){
		return set_img(constant("DIR_ICON").$icon_list[$str],$str)."&nbsp;";
	}else{
		return set_img(constant("DIR_ICON")."unknown.gif",$str)."&nbsp;";
	}
}

/*=========================================*/
/* クエリ編集                              */
/*=========================================*/
function query_edit($edit_key ,$edit_val = "" ,$query = null){
	//($edit_val = "DELETE" で値削除)
	
	$qs = "";
	unset($arr_query);
	$flg_edit = false;
	
	if($query !== null){
		$query = preg_replace('/\?/', '', $query);
		foreach(explode("&", $query) as $tmp){
			if(strstr($tmp, "=") === false){
				$tmp = preg_replace('/amp;/', '', $tmp);
				$arr_query[$tmp] = "";
			}else{
				list($k,$v) = explode("=", $tmp);
				$k = preg_replace('/amp;/', '', $k);
				$arr_query[$k] = urldecode($v);
			}
		}
	}else{
		$arr_query = $_GET;
	}
	
	if(is_array($arr_query)){
		foreach($arr_query as $k => $v){
			if($k == $edit_key){
				$flg_edit = true;
				//削除
				if($edit_val === 'DELETE') continue;
				//置換
				$v = $edit_val;
			}
			//結合
			$qs .= ($v === "" ? $k : $k."=".@urlencode($v)).'&';
		}
		//無かったら追加
		if(!$flg_edit && $edit_val !== 'DELETE'){
			$qs .= ($edit_val === "" ? $edit_key : $edit_key."=".@urlencode($edit_val)).'&';
		}
		
		//最後の[&]削除
		$qs = substr($qs, 0, -1);
		//先頭の[&]削除
		$qs = preg_replace("/^&/", "", $qs);
		//先頭に「?」追加
		if($qs) $qs = "?".$qs;
	}
	
	return $qs;
}

/*=========================================*/
/* ファイルリスト取得                      */
/*=========================================*/
function getfilelist($directory){
	$directory = preg_replace("/\/$/", "", $directory);
	if(!is_dir($directory)) return ;
	
	$files = array();
	$index = 0;
	if($dirh = @opendir($directory)){
		while (false !== ($filename = readdir($dirh))){
			if(is_dir($directory.$filename)) continue;
			if($filename == '.' || $filename == '..') continue;
			$files[$index++] = $filename;
		}
		sort($files);
		return $files;
	}
}

/*=========================================*/
/* グラフ                                  */
/*=========================================*/
//グラフ表示(横)
function mk_graph($val, $uniq_val, $max_val){
	if(!$val || !$uniq_val) return;
	
	//UNIQ
	$uniq_width = round(($uniq_val/$max_val)*200);
	$uniq_bar_img = '<img src="./image/bar2.gif" height=12 width='.$uniq_width.' alt="'.$uniq_val.'">';
	//PV
	$width = round(($val/$max_val)*200);
	$width -= $uniq_width;
	$bar_img = '<img src="./image/bar1.gif" height=12 width='.$width.' alt="'.$val.'">';
	
	return $uniq_bar_img.$bar_img;
}

//グラフ表示(縦)
function mk_graph_h($val ,$uniq_val ,$max_val){
	if(!$val || !$uniq_val) return;
	
	//UNIQ
	$height_uniq = round(($uniq_val/$max_val)*200);
	$bar_img_uniq = '<img src="./image/h_bar2.gif" height='.$height_uniq.' width=12 alt="'.$uniq_val.'">';
	//PV
	$height = round(($val/$max_val)*200);
	$height -= $height_uniq;
	$bar_img = '<img src="./image/h_bar1.gif" height='.$height.' width=12 alt="'.$val.'">';

	return $bar_img."<br>".$bar_img_uniq;
}
//グラフ表示(通常)
function mk_graph_01($val, $max_val, $total){
	if(!$val || !$max_val || !$total) return;
	
	$width = round(($val/$max_val)*200);
	return '<img src="./image/bar1.gif" height=12 width='.$width.' align="absmiddle">&nbsp;'.make_rate($val, $total);
}


/*=========================================*/
/* 配列に指定値があるかチェック            */
/*=========================================*/
function inc_array($str,$arr){
	$retval = FALSE;
	if(is_array($arr)){
		foreach($arr as $v){
			if(eregi(trim($v), trim($str))){
				$retval = TRUE;
				break;
			}
		}
	}
	return $retval;
}

function inc_array_key($str,$arr){
	$retval = FALSE;
	if(is_array($arr)){
		foreach($arr as $k => $v){
			if(eregi(trim($k), trim($str))){
				$retval = TRUE;
				break;
			}
		}
	}
	return $retval;
}

/*=========================================*/
/* 時間取得(秒)                            */
/*=========================================*/
function getmicrotime(){
	list($usec, $sec) = explode(" ",microtime()); 
	return ((float)$sec + (float)$usec); 
}

/*=========================================*/
/* 長すぎる文字列を丸める                  */
/*=========================================*/
function str_cut($str){
	if(constant("STR_CUT") && 
		function_exists("mb_strlen") && 
		function_exists("mb_substr")){
		if(mb_strlen($str) > constant("STR_CUT")) $str = '<span title="'.$str.'">'.mb_substr($str, 0, constant("STR_CUT"))."...</span>";
	}
	return $str;
}

/*=========================================*/
/* 指定ログ用リンク生成                    */
/*=========================================*/
function sel_link($url,$str){
	$link = "";
	
	if(constant("SELECT_LOG_MODE")){
		$link = "\n<script type='text/javascript'>\n<!--\n"
				.'document.write(\'<a href="select.php'.$url.'" target="select" onClick="showLayWin(lwObj)">'.$str.'</a>\');'
				."\n-->\n</script>\n";
		$link .= '<noscript><a href="select.php'.$url.'" target="_blank">'.$str.'</a></noscript>';
	}else{
		$link .= '<a href="select.php'.$url.'" target="_blank">'.$str.'</a>';
	}
	
	return $link;
}

/*=========================================*/
/* スキン読込                              */
/*=========================================*/
function read_skin($skin_file,$data,$none_main=false){
	$html = "";
	if(function_exists('file_get_contents')){
		$html = @file_get_contents($skin_file);
	}else{
		$html = implode('', @file($skin_file));
	}
	if(!$html) die("Skin File Read Error : <b>".$skin_file."</b>");
	
	if($data && is_array($data)){
		if($none_main){
			list($html_sp["up"],$tmp["main"],$html_sp["down"]) = explode('<!--SP-->',$html);
			
			list($html_sp["main_up"],$html_sp["main_down"]) = explode('{/DATA_VIEW/}',$tmp["main"]);
			
			
			foreach($data as $k => $v){
				$html_sp["up"] = str_replace('{/'.$k.'/}',$v, $html_sp["up"]);
				$html_sp["down"] = str_replace('{/'.$k.'/}',$v, $html_sp["down"]);
				$html_sp["main_up"] = str_replace('{/'.$k.'/}',$v, $html_sp["main_up"]);
				$html_sp["main_down"] = str_replace('{/'.$k.'/}',$v, $html_sp["main_down"]);
			}
			//指定外のものを削除
			$html_sp["up"] = preg_replace("/\{\/[_a-zA-Z0-9]+\/}/","",$html_sp["up"]);
			$html_sp["down"] = preg_replace("/\{\/[_a-zA-Z0-9]+\/}/","",$html_sp["down"]);
			$html_sp["main_up"] = preg_replace("/\{\/[_a-zA-Z0-9]+\/}/","",$html_sp["main_up"]);
			$html_sp["main_down"] = preg_replace("/\{\/[_a-zA-Z0-9]+\/}/","",$html_sp["main_down"]);
			#$html_sp["up"] = ereg_replace("{/[_a-zA-Z0-9]+/}","",$html_sp["up"]);
			#$html_sp["down"] = ereg_replace("{/[_a-zA-Z0-9]+/}","",$html_sp["down"]);
			#$html_sp["main_up"] = ereg_replace("{/[_a-zA-Z0-9]+/}","",$html_sp["main_up"]);
			#$html_sp["main_down"] = ereg_replace("{/[_a-zA-Z0-9]+/}","",$html_sp["main_down"]);
			
			$html_sp["up"] = "\n".$html_sp["up"];
			
			return $html_sp;
		}else{
			foreach($data as $k => $v){
				$html = str_replace('{/'.$k.'/}',$v, $html);
			}
			//指定外のものを削除
			$html = preg_replace("/\{\/[_a-zA-Z0-9]+\/}/","",$html);
			#$html = ereg_replace("{/[_a-zA-Z0-9]+/}","",$html);
			return $html;
		}
	}else{
		die("unknown data.");
	}
}
/*=========================================*/
/* DB Error                                */
/*=========================================*/
function check_err($db){
	if(DB::isError($db)){
		echo "<font color=\"#CC0000\"><b>DB Error:</b>&nbsp;";
		if((defined("DB_TYPE_MOGURA") ? constant("DB_TYPE_MOGURA") : constant("DB_TYPE")) == "mysql"){
			echo mysql_error();
		}else{
			echo DB::errorMessage($db);
		}
		die("</font>");
	}
}
/*=========================================*/
/* DB                                      */
/*=========================================*/
function mk_sql_date($ym, $d = ""){
	$ym = substr($ym, 0, 4)."-".substr($ym, 4, 2);
	if($d && $d <> 99){
		$d = "-".$d;
	}else{
		$d = "";
	}
	return $ym.$d;
}
function mk_sql($set_sql ,$seld_off = 0){
	$where_flg = false;
	$exclude_flg = false;
	$sql = "";
	$sql .= "SELECT ".(isset($set_sql["select"]) ? trim($set_sql["select"]) : "*");
	$sql .= " FROM ".(isset($set_sql["from"]) ? trim($set_sql["from"]) : constant("DB_TABLE_LOG"));
	
	if(!isset($set_sql["from"]) || strstr($set_sql["from"], constant("DB_TABLE_LOG")) != FALSE){
		$exclude_flg = true;
		if((isset($GLOBALS['exclude']['ua']) || (!isset($set_sql["craw"]) && isset($_COOKIE['anlCraw']) && $_COOKIE['anlCraw'] == '2')) && !strstr($set_sql["from"], constant("DB_TABLE_UA"))){
			$sql .= " LEFT JOIN ".constant("DB_TABLE_UA")." ON ".constant("DB_TABLE_LOG").".ua = ".constant("DB_TABLE_UA").".ua_id";
		}
	}
	
	if(isset($set_sql["where"])){
		$sql .= " WHERE (".trim($set_sql["where"]).")";
		$where_flg = true;
	}
	//解析期間の指定
	if(!$seld_off && preg_match("/date LIKE '(.+?)'/i", $sql) && isset($_GET["seld_t"]) && isset($_GET["seld_e"])){
		$sql = preg_replace("/date LIKE '(.+?)'/i", "DATE_FORMAT(date,'%Y%m%d%H%i%s') between '".$_GET["seld_t"]."000000' and '".$_GET["seld_e"]."235959'", $sql);
	}
	
	//拒否リストの追加
	if($exclude_flg){
		if(isset($GLOBALS['exclude'])){
			$sql .= ($where_flg ? " AND " : " WHERE ");
			foreach($GLOBALS['exclude'] as $type => $exclude){
				if(!preg_match("/^(id|path|ua|ip|host|url)$/", $type)) continue;
				
				if($type == "url"){
					$type = "ref";
				}else if($type == "ua"){
					$type = "ua_name";
				}
				$sql .= "(NOT(";
				foreach($exclude as $v){
					$sql .= $type." LIKE '%".$v."%' OR ";
				}
				$sql = substr($sql, 0, -4);
				$sql .= ") OR ".$type." IS NULL) AND ";
			}
			$sql = substr($sql, 0, -4);
			$where_flg = true;
		}
	}
	
	if(!isset($set_sql["from"]) || strstr($set_sql["from"], constant("DB_TABLE_LOG")) != FALSE){
		//ページ指定
		if(isset($_GET["p_sel"])){
			if($_GET["p_sel"] != ""){
				$sql .= ($where_flg ? " AND " : " WHERE ");
				$sql .= "path = '".$_GET["p_sel"]."' ";
				$where_flg = true;
			}
		}
		//DL
		if(!isset($set_sql["dl"])){
			$sql .= ($where_flg ? " AND " : " WHERE ");
			$sql .= "dl IS NULL ";
			$where_flg = true;
		}
		//クローラ
		if(!isset($set_sql["craw"]) && isset($_COOKIE['anlCraw']) && $_COOKIE['anlCraw'] == '2'){
			$sql .= ($where_flg ? " AND " : " WHERE ");
			$sql .= "(os <> 'Crawler' OR os IS NULL) ";
			$where_flg = true;
		}
	}
	
	if(isset($set_sql["group"])) $sql .= " GROUP BY ".trim($set_sql["group"]);
	if(isset($set_sql["having"])) $sql .= " HAVING ".trim($set_sql["having"]);
	if(isset($set_sql["sort"])){
		switch (constant("SORT_MODE")) {
		case 'p_u':
		   $sql .= " order by pv";
		   break;
		case 'u_u':
		   $sql .= " order by uniq";
		   break;
		case 'u_d':
		   $sql .= " order by uniq desc";
		   break;
		case 'p_d':
		   $sql .= " order by pv desc";
		   break;
		default:
			$sql .= " order by pv desc";
		   break;
		}
	}
	if(isset($set_sql["op"])) $sql .= " ".trim($set_sql["op"]);
	if(isset($set_sql["limit"])) $sql .= " LIMIT ".trim($set_sql["limit"]);
	$sql = trim($sql).";";
	//SQLエンコード
	$sql = @mb_convert_encoding($sql, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");

	return $sql;

}
function mk_pu(){
	$uniq_title = "ユニーク";
	$pv_title = "件数";

	switch (constant("SORT_MODE")) {
	case 'p_u':
		$uniq_title = set_img(constant("ICON_SORTOFF"),"OFF").'<a href="'.query_edit("s",'u_d').'">'.$uniq_title.'</a>';
		$pv_title = set_img(constant("ICON_SORTUP"),"UP").'<a href="'.query_edit("s",'p_d').'">'.$pv_title.'</a>';
	   break;
	case 'u_u':
		$uniq_title = set_img(constant("ICON_SORTUP"),"UP").'<a href="'.query_edit("s",'u_d').'">'.$uniq_title.'</a>';
		$pv_title = set_img(constant("ICON_SORTOFF"),"OFF").'<a href="'.query_edit("s",'p_d').'">'.$pv_title.'</a>';
	   break;
	case 'u_d':
		$uniq_title = set_img(constant("ICON_SORTDOWN"),"DOWN").'<a href="'.query_edit("s",'u_u').'">'.$uniq_title.'</a>';
		$pv_title = set_img(constant("ICON_SORTOFF"),"OFF").'<a href="'.query_edit("s",'p_d').'">'.$pv_title.'</a>';
	   break;
	case 'p_d':
	default:
		$uniq_title = set_img(constant("ICON_SORTOFF"),"OFF").'<a href="'.query_edit("s",'u_d').'">'.$uniq_title.'</a>';
		$pv_title = set_img(constant("ICON_SORTDOWN"),"DOWN").'<a href="'.query_edit("s",'p_u').'">'.$pv_title.'</a>';
	   break;
	}
	echo '<th nowrap width="70">'.$uniq_title.'</th>';
	echo '<th nowrap width="70">'.$pv_title.'</th>';
}
function getuseragent($hua, $path = ""){
	//各種リスト読み込み
	include_once($path.constant("DIR_LIST")."browser.php");
	include_once($path.constant("DIR_LIST")."os.php");
	include_once($path.constant("DIR_LIST")."crawler.php");
	if(constant("UA_PLUS_MOBILE")) include_once($path.constant("DIR_LIST")."mobile.php");
	
	global $browser_list,$os_list,$crawler_list;

	$browser = "";
	$browser_v = "";
	$platform = "";
	$platform_v = "";
	//---ブラウザ
	foreach($browser_list as $k => $v){
		if(stristr($hua, $k) != False){
			list($browser, $browser_v) = each($v);
			if(!$browser) $browser = $k;
			break;
		}
	}
	//---OS
	//Windows
	if(strstr($hua, "Win") != False){
		$platform = 'Windows';
		foreach($os_list["win"] as $k => $v){
			settype($k, "string");
			if(stristr($hua, $k) != False){
				if(!$v) $v = $k;
				$platform_v = $v;
				break;
			}
		}
	}
	//Macintosh
	elseif(strstr($hua, "Mac OS") != False && strstr($hua, "Macintosh") != False){
		$platform = 'Mac OS X';
		foreach($os_list["mac"] as $k => $v){
			if(stristr($hua, $k) != False){
				if(!$v) $v = $k;
				$platform_v = $v;
				break;
			}
		}
	}
	else{
		//その他のOS
		foreach($os_list["other"] as $k => $v){
			if(stristr($hua, $k) != False){
				list($platform, $platform_v) = each($v);
				break;
			}
		}
		//モバイル
		if(isset($os_list["mobile"]) && constant("UA_PLUS_MOBILE")){
			foreach($os_list["mobile"] as $k => $v){
				if(stristr($hua, $k) != False){
					list($platform, $platform_v) = each($v);
					break;
				}
			}
		}
	}
	//クローラ
	if(!$browser || !$platform){
		foreach($crawler_list as $k => $v){
			if(stristr($hua, $k) != False){
				if(!$platform){
					$platform = "Crawler";
					$platform_v = $v;
				}
				if(!$browser){
					$browser = "Crawler";
				}
				break;
			}
		}
	}
	
	
	/*---Unknown---*/
	if(!$browser) $browser = 'Unknown Browser';
	if(!$browser_v) $browser_v = 'N/A';
	if(!$platform) $platform = 'Unknown OS';
	if(!$platform_v) $platform_v = 'N/A';
	
	return array($browser,$browser_v,$platform,$platform_v);
}

/*=========================================*/
/* 検索キーワード取得                      */
/*=========================================*/
function get_keyword($query, $query_key, $enc = "ASCII,JIS,UTF-8,EUC-JP,SJIS"){
	$keyword = "";
	
	$query = str_replace ("&amp;", "&", $query);
	
	foreach(explode("&", $query) as $tmp){
		list($k,$v) = explode("=", $tmp);
		if($k == $query_key){
			if(trim($v) == "") continue;
			
			$v = urldecode($v);
			$v = @mb_convert_encoding($v, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
			
			$v = str_replace('+', ' ', $v);
			$v = mb_ereg_replace("　", " ", $v);
			$v = ereg_replace(" {2,}", " ", $v);
			$v = trim($v);
			
			if($v == "") continue;

			$v = "［".ereg_replace(' ', '］&nbsp;［', $v)."］";
			
			$keyword = $v;
			break;
		}
	}
	
	return $keyword;
}


/*=========================================*/
/* 移動ボタン                              */
/*=========================================*/
function move_bt($page,$max_val,$limit = 0){
	global $send_sel,$send_op;
	
	$send = defined("FILENAME_SEL") ? constant("FILENAME_SEL") : constant("FILENAME");
	
	if(!$limit) $limit = constant("LIMIT");
	if($max_val < $limit) return;
	
	$end_p = number_format($page+$limit);
	if($end_p > $max_val) $end_p = $max_val;
	
	$move['now_p'] = "<b>".number_format($max_val)."</b> 件中 <b>".number_format($page+1)." - ".$end_p."</b> 件目";
	$move['top_bt'] = '';
	$move['end_bt'] = '';
	$move['prev_bt'] = '';
	$move['next_bt'] = '';
	$p = 0;
	
	$query = query_edit("sel",$send_sel);
	$query = query_edit("op",$send_op,$query);

	//prev_bt
	if($page > 0){
		$p = ($page-$limit) > 0 ? $page-$limit : 0;
		$move['prev_bt'] = '<a href="'.$send.query_edit("p", $p, $query).'">'.set_img("image/bt_prev.gif", "前へ").'</a>';
		$move['top_bt'] = '<a href="'.$send.query_edit("p", 0, $query).'">'.set_img("image/bt_top.gif", "先頭").'</a>';
	}
	//next_bt
	if($page < ($max_val-$limit)){
		#$p = ($page+$limit) > ($max_val-$limit) ? $max_val-$limit : $page+$limit;
		$p = $page+$limit;
		$move['next_bt'] = '<a href="'.$send.query_edit("p", $p, $query).'">'.set_img("image/bt_next.gif", "次へ").'</a>';
		$move['end_bt'] = '<a href="'.$send.query_edit("p", $max_val-$limit, $query).'">'.set_img("image/bt_end.gif", "最終").'</a>';
	}
	$move_html = '<br>'
					.'<div align="center">'.$move["now_p"].'</div><br>'
					.'<div align="center" class="Move">'.$move['top_bt'].$move["prev_bt"].'&nbsp;'.$move["next_bt"].$move["end_bt"].'</div>';
	return $move_html;
}

/*=========================================*/
/* sql_escape                              */
/*=========================================*/
//SQL修正
function sql_escape($value){
	$str = trim($value);
	if(function_exists('mb_convert_encoding')) $value = @mb_convert_encoding($value, "UTF-8", constant("TO_ENCODING"));
	if(get_magic_quotes_gpc()) $value = stripslashes($value);
    $link = mysqli_connect(DB_HOST_MOGURA, DB_USER_MOGURA, DB_PWD_MOGURA, DB_NAME_MOGURA);
	return mysqli_real_escape_string($link, $value);
}

/*=========================================*/
/* 携帯個体識別番号取得                    */
/*=========================================*/
class MobileCheck{
	var $http_ua = null;
	var $zone;
	
	function MobileCheck($http_ua){
		if(!empty($http_ua)){
			$this->http_ua = $http_ua;
		}
	}
 
	function GetSub($env){
		$id = null;
		if($env === 'docomo' && isset($_SERVER['HTTP_X_DCMGUID'])){
			$id = $_SERVER['HTTP_X_DCMGUID'];
		}elseif($env === 'softbank'){
			if(isset($this->http_ua) && preg_match("/\/SN([a-zA-Z0-9]+)\//",$this->http_ua,$vprg)){
				$id = $vprg[1];
			}
			if(!empty($_SERVER['HTTP_X_JPHONE_UID'])){
				$id = $_SERVER['HTTP_X_JPHONE_UID'];
			}
		}elseif($env === 'au' && isset($_SERVER['HTTP_X_UP_SUBNO'])){
			$id = $_SERVER['HTTP_X_UP_SUBNO'];
		}
 
		return $id;
	
	}//func-GetSub
 
	function GetZone($env){
		include_once(dirname(__FILE__).'/../'.constant('DIR_LIST').'mobile_ip.php');
		global $mobile_ip_list;
		
		/* IP帯域の設定 */
		if($env === 'docomo'){
			//i-mode(NTT DoCoMo)のIPアドレス帯域を設定
			$this->zone = $mobile_ip_list['docomo'];
		}elseif($env === 'au'){
			//EZWeb(au)のIPアドレス帯域を設定
			$this->zone = $mobile_ip_list['au'];
		}elseif($env === 'softbank'){
			//Yahoo!ケータイ(SoftBank)のIPアドレス帯域を設定
			$this->zone = $mobile_ip_list['softbank'];
		}elseif($env === 'willcom'){
			//AIR-EDGE PHONE(WILLCOM)のIPアドレス帯域を設定
			$this->zone = $mobile_ip_list['willcom'];
		}
	}//func-GetZone
 
	function CheckUA(){
		if(!isset($this->http_ua)){
			return('other');
		}
		
		/* UserAgentからキャリアを返す */
		if(strpos($this->http_ua,"DoCoMo") !== FALSE){
			return('docomo');
		}elseif(strpos($this->http_ua,"SoftBank") !== FALSE || strpos($this->http_ua,"Vodafone") !== FALSE || strpos($this->http_ua,"J-PHONE") !== FALSE || strpos($this->http_ua,"MOT-") !== FALSE){
			return('softbank');
		}elseif(strpos($this->http_ua,"KDDI-") !== FALSE || strpos($this->http_ua,"UP.Browser/") !== FALSE){
			return('au');
		}elseif(strpos($this->http_ua,"WILLCOM") !== FALSE || strpos($this->http_ua,"DDIPOCKET") !== FALSE){
			return('willcom');
		}else{
			return('other');
		}
	}//func-CheckUA
 
	function CheckIP($area){
		/* IPアドレス帯域($zone)に含まれているか検査 */
		$addr = $_SERVER['REMOTE_ADDR'];
 
		$i = 0;
		$count = count($area);
		$flag = FALSE;
 
		while($i <$count){
			/* ネットワークアドレスの算出 */
			//範囲の特定
			list($ip,$sub) = explode('/',$area[$i]);
			list($mask,$plus) = $this->switchtomask($sub);
			if($mask === FALSE && $plus === FALSE) die('範囲がおかしいです(0-32まで)');
 
			//IP,サブネットマスクの論理積を求める
			$ip = explode('.',$ip);
			$mask = explode('.',$mask);
 
			//それぞれの論理積を求める
			$network[0] = bindec(decbin($ip[0]) & decbin($mask[0]));
			$network[1] = bindec(decbin($ip[1]) & decbin($mask[1]));
			$network[2] = bindec(decbin($ip[2]) & decbin($mask[2]));
			$network[3] = bindec(decbin($ip[3]) & decbin($mask[3]));
 
			//ロングIPアドレスへ
			$naddr = sprintf("%u", ip2long(implode('.',$network)));
			$baddr = $naddr + $plus -1;
 
			/* $addrが範囲内にあるか */
			//$addrをロングIPアドレス化する
			$addr = sprintf("%u", ip2long($addr));
 
			if($naddr <= $addr && $addr <= $baddr){
				$flag = TRUE;
				break;
			}
 
			$i++;
		}
 
		return $flag;
 
	}//func-CheckIP
 
	/* xxx.xxx.xxx.xxx/YYのYY→yyy.yyy.yyy.yyyへ */
	function switchtomask($sub){
		switch($sub){
			case 32 :
				$mask = '255.255.255.255';
				$plus = 1;
				break;
			case 31 :
				$mask = '255.255.255.254';
				$plus = 2;
				break;
			case 30 :
				$mask = '255.255.255.252';
				$plus = 4;
				break;
			case 29 :
				$mask = '255.255.255.248';
				$plus = 8;
				break;
			case 28 :
				$mask = '255.255.255.240';
				$plus = 16;
				break;
			case 27 :
				$mask = '255.255.255.224';
				$plus = 32;
				break;
			case 26 :
				$mask = '255.255.255.192';
				$plus = 64;
				break;
			case 25 :
				$mask = '255.255.255.128';
				$plus = 128;
				break;
			case 24 :
				$mask = '255.255.255.0';
				$plus = 256;
				break;
			case 23 :
				$mask = '255.255.254.0';
				$plus = 512;
				break;
			case 22 :
				$mask = '255.255.252.0';
				$plus = 1024;
				break;
			case 21 :
				$mask = '255.255.248.0';
				$plus = 2048;
				break;
			case 20 :
				$mask = '255.255.240.0';
				$plus = 4096;
				break;
			case 19 :
				$mask = '255.255.224.0';
				$plus = 8192;
				break;
			case 18 :
				$mask = '255.255.192.0';
				$plus = 16384;
				break;
			case 17 :
				$mask = '255.255.128.0';
				$plus = 32768;
				break;
			case 16 :
				$mask = '255.255.0.0';
				$plus = 65536;
				break;
			case 15 :
				$mask = '255.254.0.0';
				$plus = 131072;
				break;
			case 14 :
				$mask = '255.252.0.0';
				$plus = 262144;
				break;
			case 13 :
				$mask = '255.248.0.0';
				$plus = 524288;
				break;
			case 12 :
				$mask = '255.240.0.0';
				$plus = 1048576;
				break;
			case 11 :
				$mask = '255.224.0.0';
				$plus = 2097152;
				break;
			case 10 :
				$mask = '255.192.0.0';
				$plus = 4194304;
				break;
			case 9 :
				$mask = '255.128.0.0';
				$plus = 8388608;
				break;
			case 8 :
				$mask = '255.0.0.0';
				$plus = 16777216;
				break;
			case 7 :
				$mask = '254.0.0.0';
				$plus = 33554432;
				break;
			case 6 :
				$mask = '252.0.0.0';
				$plus = 67108864;
				break;
			case 5 :
				$mask = '248.0.0.0';
				$plus = 134217728;
				break;
			case 4 :
				$mask = '240.0.0.0';
				$plus = 268435456;
				break;
			case 3 :
				$mask = '224.0.0.0';
				$plus = 536870912;
				break;
			case 2 :
				$mask = '192.0.0.0';
				$plus = 1073741824;
				break;
			case 1 :
				$mask = '128.0.0.0';
				$plus = 2147483648;
				break;
			case 0 :
				$mask = '0.0.0.0';
				$plus = 4294967296;
				break;
			default :
				$mask = FALSE;
				$plus = FALSE;
				break;
		}
		return array($mask,$plus);
	}//func-switchtomask
}//class-MobileCheck

?>