<?php
if(basename(__FILE__) == basename($_SERVER["SCRIPT_NAME"])) exit;
/*=================================================*/
/* 【環境設定】                                    */
/*                                                 */
/*  ★ -> 必ず設定変更して下さい                   */
/*  ■ -> 環境に合わせて設定変更して下さい         */
/*                                                 */
/*          (C)オーサカPHP - http://fmono.sub.jp/  */
/*=================================================*/

/*=========================================*/
/* 初期設定 (ログ解析)                     */
/*=========================================*/
// ★ ユーザ認証
// 2 -> フォーム認証
// 1 -> BASIC認証
// 0 -> しない(ID,パスワード無効)
define ("USERAUTH_FLG", 0);
// ★ ユーザ認証 [ID]
define ("USERAUTH_ID", 'admin');
// ★ ユーザ認証 [パスワード]
define ("USERAUTH_PWD", '1234');

// ★ 管理者モード(ログ・設定の操作)
// 1 -> ON , 0 -> OFF
define ("ADMIN_MODE", 1);

/*=========================================*/
/* ディレクトリ設定                        */
/*=========================================*/
// ■ プラグイン
define ("DIR_PLUGIN", './plugin/');
// ■ リスト
define ("DIR_LIST",   './list/');
// ■ アイコン画像
define ("DIR_ICON",   './icon/');
// ■ スキン
define ("DIR_SKIN",   './skin/');

/*=========================================*/
/* 初期設定 (ログ取得)                     */
/*=========================================*/
// ■ アクセス解析の使用(一時的に使用を中止したい場合)
// 1 -> ON , 0 -> OFF
define ("ACC_RUN", 1);

// ★ ログ取得スクリプトのパス
define ("W_PATH", 'http://localhost/mogura/writelog.php');

// ■ PHPログ取得モード
// 1 -> ソースにJavaScriptが挿入されます。
//      ※所得可能な全ての情報を取得します。
// 0 -> JavaScriptを利用しないのでソースに影響が出ません。
//      ※Monitor解像度･色,Cookieの有効率,JavaScriptの有効率は解析出来ません。
define ("PHP_WRITE_MODE", 0);

// ■ docomo端末ID取得モード
//    ※この項目は、WriteTypeが「PHP（拡張子がPHPのページ）」または「PHP outside（拡張子がPHPのページ＆外部サーバー）」の
//      ログ取得用タグを使用しており、かつPHPログ取得モードが[0]の場合のみ設定が必要です。
//      上記以外の場合、ここでの設定内容に関わらずdocomo端末のiモードIDを取得できます。
//      なお、iモードIDはユーザー追跡等で使用するMogura用IDの元になります。
//
// 1 -> docomo端末からのアクセス時のみ、imgタグによりログを取得します。
//      iモードIDを取得できますが、docomo端末からのアクセス時、ログ取得用タグ挿入箇所にimgタグが出力されます。
// 0 -> docomo端末からのアクセス時も、imgタグを使用せずに通常の方法でログを取得します。
//      ソースには何も出力されませんが、iモードIDを取得できないため、Cookieに対応していないdocomo端末のユーザー追跡ができません。
//      ※ただし、解析するページのURIに「guid=ON」が含まれている場合はiモードIDを取得できます。
define ("GET_DOCOMO_ID_MODE", 0);

// ■ エラー表示(PHPログ取得モード[0]のみ有効)
// 1 -> ON , 0 -> OFF
define ("ERROR_VIEW", 0);

// ■ 変換前のエンコーディング
// auto,ASCII,JIS,UTF-8,EUC-JP,SJIS等
define ("TO_ENCODING", 'ASCII,JIS,UTF-8,EUC-JP,SJIS');

// ■ ID用クッキー有効期限(日)
define ("COOKIE_EXPIRE", 3653);

// ■ 同一HOSTからの同ページに対してのアクセスを保存しない(秒)
// 0 -> 無効
define ("UNWRITE", 0);

// ■ タイトルの自動取得モード
// (PHPログ取得モード[0]又はJavaScriptオフ時のみ)
// 2 -> 動的にタイトル生成あり:BLOG等(URIからタイトル取得:低速)
// 1 -> 動的にタイトル生成なし(ローカルファイルからタイトル取得:高速)
// 0 -> 無効(タイトル取得なし:最高速)
define ("GET_TITLE_MODE", 2);

// ■ グリニッジ標準時との時差(時間)
// 日本 = 9
define ("TIME_DIFF", 9);

// ■ ログの保存最大件数
// 0 -> 無効
define ("MAX_LOG", 0);


/*=========================================*/
/* カウンターの設定                        */
/*=========================================*/
// ■ カウンターの表示
// 1 -> ON , 0 -> OFF
define ("COUNTER", 0);
// ■ カウンタータイプ
// img -> 画像 , txt -> テキスト
define ("COUNTER_TYPE", 'txt');
// ★ カウンター画像ディレクトリ(絶対パス)
define ("COUNTER_DIR", "http://localhost/mogura/co_img/");
// ■ カウンター桁数
define ("COUNTER_DIGIT", 10);


/*=========================================*/
/* データベースの設定                      */
/*=========================================*/
// ■ 種類
define ("DB_TYPE_MOGURA", 'mysqli');
// ★ HOST
define ("DB_HOST_MOGURA", 'localhost');
// ★ データベース名
define ("DB_NAME_MOGURA", 'test');
// ★ ユーザー名
define ("DB_USER_MOGURA", '');
// ★ パスワード
define ("DB_PWD_MOGURA",  '');


/*=========================================*/
/* DBテーブルの設定  (基本的に設定不要)    */
/*=========================================*/
// ■ ログ
define ("DB_TABLE_LOG",     'osaka_w3a');
// ■ カウンターログ
define ("DB_COUNTER",       'osaka_count');
// ■ UserAgent
define ("DB_TABLE_UA",      'osaka_ua');
// ■ 設定
define ("DB_TABLE_SETTING", 'osaka_setting');
// ■ ID変換リスト
define ("DB_CH_ID",         'osaka_chid');
// ■ URL変換リスト
define ("DB_CH_URL",        'osaka_churl');
// ■ 拒否リスト
define ("DB_EXCLUDE",       'osaka_exclude');


/*=========================================*/
/* 初期設定 (その他)                       */
/*=========================================*/
// ■ 長すぎる文字を丸める最大文字数(解析画面)
// 0 -> 無効(すべて表示)
define ("STR_CUT", 0);

// ■ 1ページに表示する件数
// 0 -> 無効(常に全表示)
define ("LIMIT", 30);

// ■ 指定ログ表示モード
// 0 -> 新しいウィンドウ
// 1 -> 同ページ内に表示
define ("SELECT_LOG_MODE", 1);

// ■ リスト拡張(携帯機種)
// 1 -> ON , 0 -> OFF
// ※ 設定変更時は、コントロールパネルの「その他の設定」から「UA更新」を実行してください。
define ("UA_PLUS_MOBILE", 1);

/*================== 設定ここまで ==================*/

/*=========================================*/
/* 以下設定不要                            */
/*=========================================*/

error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_STRICT);

//PEAR DB include
@ini_set('include_path', ((file_exists(dirname(__FILE__).'/../DB.php') && !($fp = @fopen('DB.php', 'r', 1))) ? (realpath(dirname(__FILE__).'/../').(strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN' ? ':' : ';')) : '').ini_get('include_path'));if(!empty($fp)){fclose($fp);unset($fp);}
require_once ('DB.php');

//DSN
define ("DSN",
constant("DB_TYPE_MOGURA")."://".
constant("DB_USER_MOGURA").":".constant("DB_PWD_MOGURA")."@".
constant("DB_HOST_MOGURA")."/".constant("DB_NAME_MOGURA")
);

//CONF READ FLG
define ("CONF_READ", true);
?>