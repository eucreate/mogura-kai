<?php
/*=========================================*/
/* パスワード認証                          */
/*=========================================*/
function CheckLogin($username ,$password){
	$login = false;
	if($username == constant("USERAUTH_ID")
	&& $password == constant("USERAUTH_PWD")) $login = true;
	if($login){
		if(!$_POST["cookie"]){
			$username = "";
			$password = "";
		}
		@setcookie("w3a_admin[id]",$username,time()+60*60*24*30,'/');
		@setcookie("w3a_admin[pwd]",$password,time()+60*60*24*30,'/');
	}
	return $login;
}

function loginform($message = ""){
	global $path_hack;
echo <<<EOD
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="robots" content="noindex,nofollow,noarchive">
<style type="text/css">
<!--
BODY {
	font-family: Verdana, Arial, "MS UI Gothic", Osaka, Helvetica, sans-serif;
	background-color: #EEE;
	margin: 0px;
	padding: 0px;
	background-image: none;
}
A         { color: #36C; }
A:link    { text-decoration: none; }
A:visited { text-decoration: none; }
A:hover   { text-decoration: underline; color: #F30; }
H3 {
	margin: 0px;
	padding: 0px;
}
td {
	font-size: 12px;
	line-height: 1em;
}
.error {
	font-size: 12px;
	font-weight: normal;
	color: #F30;
	margin: 10px;
	padding: 0px;
}
#login {
	background-image: url({$path_hack}image/login_bg.gif);
	background-repeat: no-repeat;
	width: 410px;
	height: 290px;
	margin: 0px;
	padding: 75px 10px;
}
#loginform {
	width: 390px;
	margin: 0px;
	padding: 10px;
}
#loginform input {
	font-family: Verdana, Arial, "MS UI Gothic", Osaka, Helvetica, sans-serif;
}
#loginform .id_pwd {
	font-family: Verdana, Arial, "MS UI Gothic", Osaka, Helvetica, sans-serif;
	letter-spacing: 2px;
	font-size: 10px;
	line-height: 10px;
	height: 18px;
	width: 200px;
	padding: 2px;
	ime-mode: inactive;
	border: 1px #CCC solid
	color: #333;
	background-color: #FFF;
}
#loginform .id_pwd_ov {
	font-family: Verdana, Arial, "MS UI Gothic", Osaka, Helvetica, sans-serif;
	letter-spacing: 2px;
	font-size: 10px;
	line-height: 10px;
	height: 18px;
	width: 200px;
	padding: 2px;
	ime-mode: inactive;
	border: 1px #CCC solid
	color: #333;
	background-color: #E9E9E9;
}
#loginform .submit {
	letter-spacing: 1px;
	font-size: 10px;
	line-height: 10px;
	width: 100px;
	padding: 3px;
	color: #333;
}

#copy{
	font-size: 10px;
	color: #333;
	text-align: center;
	margin: 20px;
	padding: 0px;
}
//-->
</style>
<title>mogura</title>
</head>
<body>
<br>
<center>
<div id="login">
EOD;
if($message) $message = '<h3 class="error">'.$message.'</h3>';

$set_id = isset($_POST["username"]) ? $_POST["username"] : "";
$set_pwd = isset($_POST["password"]) ? $_POST["password"] : "";
if(isset($_COOKIE["w3a_admin"]["id"])) $set_id = $_COOKIE["w3a_admin"]["id"];
if(isset($_COOKIE["w3a_admin"]["pwd"])) $set_pwd = $_COOKIE["w3a_admin"]["pwd"];
$post_action = basename($_SERVER["SCRIPT_NAME"]);

echo <<<EOD
<form method="POST" action="{$post_action}" target="_top" id="loginform">

$message

<table border="0" align="center" cellpadding="5" cellspacing="0">
<tr>
	<td align="right">ID:</td>
	<td colspan="2"><input type="text" name="username" value="{$set_id}" class="id_pwd" onFocus="this.className='id_pwd_ov';" onBlur="this.className='id_pwd'"></td>
</tr>
<tr>
	<td align="right">パスワード:</td>
	<td colspan="2"><input type="password" name="password" value="{$set_pwd}" class="id_pwd" onFocus="this.className='id_pwd_ov';" onBlur="this.className='id_pwd'"></td>
</tr>
<tr>
	<td align="right">パスワード保存:</td>
	<td><input type="checkbox" name="cookie" value="on" id="c" checked></td>
	<td align="right">
	<input type="submit" value="Login&gt;&gt;" class="submit">
	</td>
</tr>
</table>
</form>
<div id="copy"><a href="http://fmono.sub.jp/" target="_blank">&copy;&nbsp;OSAKA&nbsp;PHP</a>&nbsp;/&nbsp;Edit&nbsp;:&nbsp;<a href="http://tekito.jp/" target="_blank">tekito</a></div>
</div>
</center>
</body>
</html>
EOD;
exit;
}

/*=========================================*/
/* パスワード認証(BASIC)                   */
/*=========================================*/
if(constant("USERAUTH_FLG") == 1){
	if(!isset($_SERVER[PHP_AUTH_USER]) ||
			$_SERVER[PHP_AUTH_USER] != constant("USERAUTH_ID") ||
			$_SERVER[PHP_AUTH_PW] != constant("USERAUTH_PWD")){
		header('WWW-Authenticate: Basic realm="mogura"');
		header('HTTP/1.0 401 Unauthorized');
		echo "Login Failed.";
		exit;
	}
}
/*=========================================*/
/* パスワード認証(フォーム)                */
/*=========================================*/
elseif(constant("USERAUTH_FLG") == 2){
	//セッション開始
	session_start();
	//ログアウト
	if(isset($_GET["logout"])){
		$_SESSION['login'] = false;
		
		loginform('ログアウトしました。');
	}
	//ログイン
	if(!$_SESSION['login'] && !CheckLogin($_POST["username"] ,$_POST["password"])){
		//ユーザー名のチェック
		if($_POST && $_POST["username"] != "" && $_POST["password"] != ""){
			loginform('LoginError: [ID] または [パスワード] が違います。');
		}elseif($_POST && $_POST["username"] == "" && $_POST["password"] == ""){
			loginform('LoginError: [ID] [パスワード] が入力されていません。');
		}elseif($_POST && $_POST["username"] == ""){
			loginform('LoginError: [ID] が入力されていません。');
		}elseif($_POST && $_POST["password"] == ""){
			loginform('LoginError: [パスワード] が入力されていません。');
		}else{
			loginform('[ID] [パスワード] を入力して下さい。');
		}
	}
	
	$_SESSION['login'] = true;
}
?>