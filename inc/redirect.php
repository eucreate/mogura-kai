<?php
/*=========================================*/
/* リダイレクト                            */
/*=========================================*/
//待ち時間(秒)
$wait = 1;

//内部文字エンコード
@mb_internal_encoding("UTF-8");
//マルチバイト文字列正規表現関数で使用される文字エンコーディング
@mb_regex_encoding("UTF-8");
//HTTP出力文字エンコーディング
@mb_http_output("UTF-8");


//メイン処理
if(isset($_GET["redirect"])){
	$rd = ereg_replace("^redirect=","",$_SERVER["QUERY_STRING"]);
	$rd = urldecode($rd);
}else{
	$rd = 'http://www.google.co.jp/';
}
?>
<html><head><title>mogura</title>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="Refresh" CONTENT="<?php echo $wait;?>; URL=<?php echo $rd;?>">
</head><body>
<small>
RedirectURL: <b><?php echo $rd;?></b><br><br>
<?php echo $wait;?>秒後に上記URLへ自動転送します。<br><br>
転送しない場合には<a href="<?php echo $rd;?>">こちら</a>をクリックして下さい。
</small>
</body></html>
